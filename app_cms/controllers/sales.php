<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('sales_model');
    $this->load->helper('auth');
    is_logged_in();
    check_auth('administrator'); 
  }

	
  function lib()
  {         
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');
    
    $config['base_url'] = site_url('sales/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;
    
    $sales_list = $this->sales_model->sales_list(
						    $data['search'], 
						    $data['filter'], 
						    $data['orderby'], 
						    $config['per_page'], 
						    $this->uri->segment(3)
						    );
    
    $config['total_rows'] = $data['total'] = $sales_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $sales_list->query;
    
    $data['title'] = 'Sales Database';
    $data['table_title'] = 'Sales List';
    $data['main_content'] = 'sales_lib_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu3'] = 0;
    $this->load->view('includes/template', $data);	

  }	


  function index()
  {
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('sales/lib');
  }	
  

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('sales/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('sales/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('sales/lib');
  }
  

  function add()
  {  
    $sales = new stdClass;
    $sales->id 
      = $sales->firstname 
      = $sales->lastname 
      = $sales->email 
      = $sales->phone 
      = $sales->mobile 
      = $sales->fax 
      = $sales->address
      = $sales->suburb  
      = $sales->city 
      = $sales->postcode 
      = $sales->password 
      = $sales->authority 
      = $sales->resetcode 
      = '';

    $data['sales'] = $sales;
    
    $data['title'] = 'Add Sales';
    $data['table_title'] = 'Sales Detail';
    $data['main_content'] = 'sales_edit_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu3'] = 0;
    $this->load->view('includes/template', $data);	
    
  }

	
  function edit()
  {  

    if (!($id = $this->uri->segment(3)))
      {
	redirect('sales/add');
      }
      
    if ($data['sales'] = $this->sales_model->load_by_id($id)) {
	
      $data['title'] = 'Edit Sales';
      $data['table_title'] = 'Sales Detail';
      $data['main_content'] = 'sales_edit_view';  
	
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu3'] = 0;
      $this->load->view('includes/template', $data);	
    }
    
    
    
  }	
  
  function sales_submit()
  { 
    if($id = $this->input->post('id'))
      {	
	if($this->sales_model->check_update())
	  {
	    $this->sales_model->update($id);
	    $msg = 'Sales updated successfully.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('sales/edit/'.$id); 
	  }
	else 
	  {
	    $msg = 'The sales name (firstname) exists.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('sales/edit/'.$id);
	  }
      }
    else
      {	
	if($this->sales_model->check_add($this->input->post('firstname')))
	  {
	    $this->sales_model->add();
	    $msg = 'Sales added successfully.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('sales');
	  }
	else 
	  {
	    $msg = 'The sales name (firstname) exists.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('sales');
	  }
      }
  }

  function del()
  { 

    if ($id = $this->uri->segment(3))
      {
	$this->sales_model->del($id);
	$msg = 'Sales deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

    redirect('sales');
    
  }




  function del_msg($id){
    
    $this->load->model('sales_model');
    if ($sales = $this->sales_model->load_by_id($id)) {
      
      $msg = 'Are you really want to delete [ '.$sales->firstname.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('sales/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('sales/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('sales/lib');	      
    } 

  }


}