<?php

class Order extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth'); 
    $this->load->model('order_model');
    $this->load->model('invoice_model');
    $this->load->model('back_order_model');
    $this->load->model('member_model');
    $this->load->model('item_model');

    is_logged_in();
    session_start();
        
  }
 
  function lib()
  {         
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');
    
    $config['base_url'] = site_url('order/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;

   
    
    $order_list = $this->order_model->order_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $this->uri->segment(3)
					      );
    
    $config['total_rows'] = $data['total'] = $order_list->total;
    
    $this->pagination->initialize($config);

    $data['orders'] = $order_list->query->result();
    foreach($data['orders'] as $row) {
   	 $row->cust_name = $this->member_model->load_by_id($row->cust_id) 
   	 	?  $this->member_model->load_by_id($row->cust_id)->company : '';
    }
    $data['title'] = 'Order Database';
    $data['table_title'] = 'Order List';
    $data['main_content'] = 'order_lib_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu4'] = 0;
    $this->load->view('includes/template', $data);	

  }	


  function index()
  {
    $_SESSION['KCFINDER']['disabled'] = false;
    $_SESSION['KCFINDER']['uploadURL'] = "../../../uploads"; 	
    /* kcfinder config - from kcfinder's root folder */
    
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('order/lib');
  }	
  

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('order/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('order/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('order/lib');
  }
  

  function add()
  {  
    $order = new stdClass;
    $order->id 
      = $order->cust_id 
      = $order->date 
      = $order->total 
      = $order->content
      = '';
    $order->date = date("Y-m-d H:i:s");

    $data['order'] = $order;
    
    $data['title'] = 'Add Order';
    $data['table_title'] = 'Order Detail';
    $data['main_content'] = 'order_edit_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu3'] = 0;
    $this->load->view('includes/template', $data);	
    
  }

	
  function edit()
  {  

    if (!($id = $this->uri->segment(3)))
      {
	redirect('order/add');
      }

    $this->load->model('order_model');
      
    if ($data['order'] = $this->order_model->load_order($id)) {
	
      $data['title'] = 'Edit Order';
      $data['table_title'] = 'Order Detail';
      $data['main_content'] = 'order_edit_view';  
	
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
      $this->load->view('includes/template', $data);	
    }    
  }	
  

  function view($id){  
    
   
    $this->db->where('id', $id);
    $result = $this->db->get('site_order')->result();
    
    $data['order'] = $result[0];
    
    $data['order']->content = json_decode($data['order']->content); 
    
  
    $data['customer'] =  $this->member_model->load_by_id($data['order']->cust_id);
    
    $this->load->view('order_print_view', $data);
    // mark as read
  //  $data['order']->read = 1; 
   // $this->db->where('id', $data['order']->id);
  //  $this->db->update('site_order', $data['order']);
    
  }

  function order_submit()
  { 
    $this->load->model('order_model');	
    if($id = $this->input->post('id'))
      {
	$this->order_model->order_update($id);
	$msg = 'Order updated successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('order/edit/'.$id);
      }
    else
      {
	$this->order_model->order_add();
	$msg = 'Order added successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('site_order');
      }
  }
  
  function trans($id)
  {

    if ($order = $this->order_model->load_order($id)) {

      $invoice = new stdClass;
      $invoice->id = 'new';
      $invoice->date = date("Y-m-d H:i:s");
      $invoice->discount = 0;
      $invoice->paid = 0; 
      $invoice->deleted = 0;
      $invoice->cust_name = $this->member_model->load_member($order->cust_id)->company;
      $invoice->cust_id = $order->cust_id;
      $invoice->order_id = $order->id;     
      $contents= json_decode($order->content); 
      $inv_contents=array();
      $bk_order = clone $invoice;  
      $bk_order->trans = 0;  
      $bk_contents=array();

      //sync stock     
      foreach ($contents as &$item) {   
	$bk_item = new stdClass;
	$bk_item->id = $item->id;
	$bk_item->name = $item->name;
	$bk_item->qty = 0;
	$bk_item->price = $item->price;
	$bk_item->discount = $item->discount = 0;

	if ($stock = $this->item_model->load_item($item->id))
	  {  
	    if ($stock->qty > $item->qty)
	      {
		$stock->qty = $stock->qty - $item->qty;		  
		$this->db->where('id', $stock->id);
		$this->db->update('items', $stock);
	      }
	    else 
	      {
		$bk_item->qty = $item->qty - $stock->qty;	     
		$item->qty = $stock->qty;
		$stock->qty = 0; 
		$this->db->where('id', $stock->id);
		$this->db->update('items', $stock);
	      }	   
	    if ($bk_item->qty) array_push($bk_contents, $bk_item);
	    if ($item->qty) array_push($inv_contents, $item);
	  }
      } 
      // end sync stock
   
      // calc totals 
      $invoice->subtotal = 0;
      foreach ($inv_contents as $item){
	$invoice->subtotal += $item->price * $item->qty; 
      }
 
      $invoice->tax = $invoice->subtotal * 0.15;
      $invoice->total = $invoice->subtotal * 1.15;
     
      $bk_order->subtotal = 0;
      foreach ($bk_contents as $item){
	$bk_order->subtotal += $item->price * $item->qty;
      }
      $bk_order->tax = $bk_order->subtotal * 0.15;
      $bk_order->total = $bk_order->subtotal * 1.15;
    
      // end calc

      $msg = '';   
      if ($bk_order->total)
	{
	  $insert_id = $this->back_order_model->back_order_add($bk_order, $bk_contents);
	  $msg .= 'Order transfered to back order#'.$insert_id.' successfully.';
	  $invoice->bk_order_id = $insert_id;
	}   
      if ($invoice->total)
	{
	  $insert_id = $this->invoice_model->invoice_add($invoice, $inv_contents);
    	  $this->order_model->mark_as_transferred($invoice->order_id);
	  $msg .= 'Order transfered to invoice#'.$insert_id.' successfully.';
	}
      //$msg = 'Order transfered successfully.';
      $this->session->set_flashdata('msg', $msg);
    }      
    
    redirect('order/lib');
  }


  function del()
  { 

    if ($id = $this->uri->segment(3))
      {
	$this->load->model('order_model');	
	$this->order_model->order_del($id);
	$msg = 'Order deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

    redirect('site_order');
    
  }

 
  /* 
  function tran()
  {
    $result = $this->db->get('orders')->result();

    foreach ($result as $row){
      echo $row->title;
      echo '<br />';
      $row->title = str_replace('title','order',$row->title);
      $row->url_title = url_title($row->title, 'dash', TRUE); 

      $this->db->where('id', $row->id);
      
      $this->db->update('orders', $row);
    } 
  }
  */


}