<?php

class Order extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  }

	
  function lib()
  {        
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');
    
    $config['base_url'] = site_url('order/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;

    $this->load->model('order_model');
   
    $order_list = $this->order_model->order_list($data['search'], $data['filter'], $data['orderby'], $config['per_page'], $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $order_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $order_list->query;
    
    $data['title'] = 'Order Database';
    $data['table_title'] = 'Order List';
    $data['main_content'] = 'order_lib_view';  

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_3acms'] = $this->config->item('cf_3acms');
    $this->load->view('includes/template', $data);	  

  }	


  function library()
  {
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('order/lib');
  }	
  

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('order/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('order/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('order/lib');
  }
  

  function add()
  {  
    $order = new stdClass;
    $order->id 
      = $order->category 
      = $order->title 
      = $order->image_small 
      = $order->image_big 
      = $order->detail 
      = $order->detail_member 
      = $order->boolean1 
      = $order->boolean2 
      = '';

    $data['order'] = $order;
    
    $data['title'] = 'Add order';
    $data['table_title'] = 'order Detail';
    $data['main_content'] = 'order_edit_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_3acms'] = $this->config->item('cf_3acms');
    $this->load->view('includes/template', $data);	
    
  }

	
  function edit()
  {  

    if (!($id = $this->uri->segment(3)))
      {
	redirect('order/add');
      }

      $this->load->model('order_model');
      
      if ($data['order'] = $this->order_model->load_order($id)) {
	
	$data['title'] = 'Edit order';
	$data['table_title'] = 'order Detail';
	$data['main_content'] = 'order_edit_view';  
	
	$data['session_msg'] = $this->session->flashdata('msg');
	$data['cf_3acms'] = $this->config->item('cf_3acms');
	$this->load->view('includes/template', $data);	
      }
    
    
    
  }	
  
  function order_submit()
  { 
    $this->load->model('order_model');	
    if($id = $this->input->post('id'))
      {
	$this->order_model->order_update($id);
	$msg = 'order updated successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('order/edit/'.$id);
      }
    else
      {
	$this->order_model->order_add();
	$msg = 'order added successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('order/library');
      }
  }

  function del()
  { 

    if ($id = $this->uri->segment(3))
      {
	$this->load->model('order_model');	
	$this->order_model->order_del($id);
	$msg = 'order deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

	redirect('order/library');
    
  }


  function view($id){
 
      $this->db->where('id', $id);
      $result = $this->db->get('orders')->result();
		
      $data['order'] = $result[0];

      $data['order']->contents = json_decode($data['order']->contents); 
   
      $this->load->view( 'order_print_view', $data);
  }

}
