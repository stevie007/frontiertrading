<?php

class Stat_inven_sale extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->model('item_model');
    $this->load->model('stat_inven_sale_model');
    
    is_logged_in();
    check_auth('administrator'); 
  }
 
	
  function lib()
  {         

    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');
    $data['category'] = $this->session->userdata('category');


    $this->load->library('pagination');
    
    $config['base_url'] = site_url('stat_inven_sale/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;
    $data['offset'] = $this->uri->segment(3, 0);

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');

    $this->load->model('item_model');
    
    $item_list = $this->item_model->item_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $data['offset'],
					      $data['category']
					      );
    
    
    $config['total_rows'] = $data['total'] = $item_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $item_list->query;
    
    $data['title'] = '';
    $data['table_title'] = '';
    $data['main_content'] = 'stat_inven_sale_lib_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu5'] = 0;
    $this->load->view('includes/template', $data);	

  }	

  function index() {

    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $this->session->set_userdata('title', 'Title..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("1 January 2000"));
    $this->session->set_userdata('date_to', time());   

    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');

    $this->stat_inven_sale_model->sync_sold($data['date']);


    $orderby['order'] = 'sold';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('stat_inven_sale/lib');
  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('stat_inven_sale/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('stat_inven_sale/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($title = $this->input->post('title')) {      
      $this->session->set_userdata('title', $title);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_inven_sale/lib');
  }
  

  /*
  function sump(){

    $this->db->select('id, cust_id, total');
    $this->db->select_sum('total');
    $query = $this->db->get_where('payment', array('cust_id' =>  '1694'));
    print_r($query->result());
  }
  function sumi(){

    $this->db->select('id, cust_id, total');
    $this->db->select_sum('total');
    $query = $this->db->get_where('invoice', array('cust_id' =>  '1694'));
    print_r($query->result());
  }
  
  function load_by_uid(){
    $this->db->select('id, cust_id, total, date, type');
    $query = $this->db->get_where('payment', array('cust_id' =>  '1694'));
    $join1 = $this->db->last_query();
    
    $this->db->select('id, cust_id, total, date, type');
    $query = $this->db->get_where('invoice', array('cust_id' =>  '1694'));
    $join2 = $this->db->last_query();
    
    $query = $this->db->query($join1.' UNION '.$join2.' ORDER BY `date` DESC');
    
    print_r($query->result());
  }
  */
  
}