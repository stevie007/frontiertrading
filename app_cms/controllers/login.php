<?php
 
class Login extends CI_Controller
{
  function __construct()
  {    
    parent::__construct();	
    $this->load->helper('auth');
    session_start();
  }
 
  function index()
  {
    $data['title'] = 'Administration Panel';

    $data['main_content'] = 'form_login';  

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);		
  }
  
 
  function validate_login()
  {
    $this->load->model('user_model');
    
    if ($user = $this->user_model->login())
      {
	$data = array(
		      'login_id' => $this->input->post('login_id'),
		      'authority' => $user->authority,
		      'is_logged_in' => true
		      );	
	$this->session->set_userdata($data);
	$_SESSION['KCFINDER'] = array();
	$_SESSION['KCFINDER']['disabled'] = false;
	$_SESSION['KCFINDER']['uploadURL'] = "../../../uploads"; 	/* kcfinder config - from kcfinder's root folder */

        $msg = 'Logged in successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('cms');
      }
    else
      {
	$msg = 'Sorry, we could not log you in with that login ID and password.';
	$this->session->set_flashdata('msg', $msg);
	redirect('login');
      }
  }
  
 
  function logout()
  {
    $this->session->sess_destroy();
    session_destroy();

    redirect('login');	
  }  


}



