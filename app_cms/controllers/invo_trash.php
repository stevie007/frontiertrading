<?php

class Invo_Trash extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    // $this->load->helper('date');
    $this->load->helper('auth');
    is_logged_in();
    $this->load->model('order_model'); 
    $this->load->model('back_order_model'); 
    $this->load->model('member_model');
    $this->load->model('item_model');;
    $this->load->model('invoice_model');
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');         
    $filter[1]['deleted'] = 1;
    $this->session->set_userdata('filter', $filter); 
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('invo_trash/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('invo_trash/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('invoice_model');
    
    $invoice_list = $this->invoice_model->invoice_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $invoice_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $invoice_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'invo_trash_list_view';  
    $data['table_title'] = 'Invo_Trash list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu4'] = 0;
    $this->load->view('includes/template', $data);	

  }



  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('invoice_model');
    
    $invoice_list = $this->invoice_model->invoice_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $invoice_list->total;
    $data['query'] = $invoice_list->query;
    $data['title'] = 'Invoice Report';
    $data['table_title'] = 'Transaction list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('invoice_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('invo_trash/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('invo_trash/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('invo_trash/lib');
  }
  

  function view($id)
  {


    $this->db->where('id', $id);
    $result = $this->db->get('invoice')->result();
    
    $data['invoice'] = $result[0];
    $data['invoice']->duedate = '';
    $data['invoice']->date = date("d/m/y", strtotime($data['invoice']->date));
    $data['invoice']->content = json_decode($data['invoice']->contents); 
    
    // client  
    $data['customer'] =  $this->member_model->load_by_id($data['invoice']->cust_id);
    
    // back order
    if ($data['invoice']->bk_order_id)
      {
	$data['bk_order'] = $this->back_order_model->load_by_id($data['invoice']->bk_order_id);
	$data['bk_order']->content = json_decode($data['bk_order']->contents); 
      } 
//    $this->load->view('invoice_print_view', $data);
      $data['main_content'] = 'print_html/invoice_print_view';
    $this->load->view('includes/template_frame_print', $data);
  }

  
  function rec_msg($id){
      $msg = 'Are you really want to recover [ INVOICE #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('invo_trash/rec/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('invo_trash/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('invo_trash/lib');	
  }

  function rec($id){

    $this->invoice_model->recover_by_id($id);
    $msg = 'INVOICE #'.$id.' Recovered';
    $this->session->set_flashdata('msg', $msg);
    redirect('invo_trash/lib');
  }

}