<?php

class Statement extends CI_Controller
{
  var $cf_site;
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');$this->load->helper('email');
    $this->load->model('member_model');
    $this->load->model('statement_model');
    $this->cf_site = $this->config->item('site');
    is_logged_in();
  }


  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');
    $this->session->set_userdata('page_index', $this->uri->segment(3));

    $data['customer'] = $this->session->userdata('customer');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');

//    $this->statement_model->sync_bal($data['date']);

    $this->load->library('pagination');

    $config['base_url'] = site_url('statement/lib/');
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;

    $this->load->model('member_model');

    // Staff account should cannot see all
    if (pass_auth('staff') && $data['customer'] == 'Customer..' && $data['search'] == 'Search..'){
      $data['filter']['id'] = 0;
    }

    $member_list = $this->member_model->member_list($data['search'], $data['filter'], $data['orderby'], $config['per_page'], $this->uri->segment(3), $data['customer']);

    $config['total_rows'] = $data['total'] = $member_list->total;

    $this->pagination->initialize($config);

    $data['query'] = $member_list->query;

    $data['title'] = '';
    $data['table_title'] = 'Member List';
    $data['main_content'] = 'statement_lib_view';

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu4'] = 0;
    $this->load->view('includes/template', $data);

  }

  function index() {

    //  $this->statement_model->sync_bal();
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $this->session->set_userdata('date_fr', strtotime("1 January 2012"));
    $this->session->set_userdata('date_to', time());
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');


    $orderby['order'] = 'balance';
    $orderby['sort'] = 'desc';
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('statement/lib');
  }

  function orderby()
  {
    if ($order = $this->uri->segment(3))
      {
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort;
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('statement/lib');
  }


  function filter()
  {
    if (($key = $this->uri->segment(3))
	&&
	($value = $this->uri->segment(4)))
      {
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);
      }
    redirect('statement/lib');
  }


  function search()
  {
    if ($search = $this->input->post('search')) {
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {
      $this->session->set_userdata('customer', $customer);
    }
    if ($from = $this->input->post('from')) {
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {
      $this->session->set_userdata('date_to',
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('statement/lib');
  }


  function view($id)
  {
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');

    //client
    $data['customer'] =  $this->member_model->load_by_id($id);

    $data['transactions'] = $this->statement_model->load_by_uid($id, $data['date']);
    
    $data['deduction'] = $this->statement_model->getHisBalance($id, $data['date']['fr']);
    
    $this->load->view('statement_print_view', $data);

  }


  function pdf($id)
  {
       $this->load->helper(array('dompdf', 'file'));
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');

    //client
    $data['customer'] =  $this->member_model->load_by_id($id);

    $data['transactions'] = $this->statement_model->load_by_uid($id, $data['date']);
        
    $data['deduction'] = $this->statement_model->getHisBalance($id, $data['date']['fr']);

 //   $this->load->view('statement_print_view', $data);

    $html = $this->load->view('print_html/statement_print_view', $data, TRUE);

    pdf_create($html, 'STATEMENT_'.date('Y-m-d'));
  }

  /*
  function sump(){

    $this->db->select('id, cust_id, total');
    $this->db->select_sum('total');
    $query = $this->db->get_where('payment', array('cust_id' =>  '1694'));
    print_r($query->result());
  }
  function sumi(){

    $this->db->select('id, cust_id, total');
    $this->db->select_sum('total');
    $query = $this->db->get_where('invoice', array('cust_id' =>  '1694'));
    print_r($query->result());
  }

  function load_by_uid(){
    $this->db->select('id, cust_id, total, date, type');
    $query = $this->db->get_where('payment', array('cust_id' =>  '1694'));
    $join1 = $this->db->last_query();

    $this->db->select('id, cust_id, total, date, type');
    $query = $this->db->get_where('invoice', array('cust_id' =>  '1694'));
    $join2 = $this->db->last_query();

    $query = $this->db->query($join1.' UNION '.$join2.' ORDER BY `date` DESC');

    print_r($query->result());
  }
  */





  function add_statement_to_print() {
    $statement_id = $this->input->post('statement_id');
    $company = $this->input->post('company');

    $statement_print_list = $this->session->userdata('statement_print_list');

	$statement_print_list['statements'][$statement_id] = $company;

	$this->session->set_userdata('statement_print_list', $statement_print_list);

	$data['statements'] = $statement_print_list['statements'];
    $data['count'] = sizeof($statement_print_list['statements']);
    echo json_encode($data);
  }


  function empty_statement_print_queue() {
	  $this->session->unset_userdata('statement_print_list');
	  redirect('statement');
  }


  function print_selected_statements() {
		$print_htmls = "";

		$statement_print_list = $this->session->userdata('statement_print_list');
		if (sizeof($statement_print_list['statements']))
		{
			$is_first_page = 1;

			foreach ($statement_print_list['statements'] as $key => $value) {

				if ($is_first_page){
					$print_htmls .= '<div>';
					$is_first_page = 0;
				}
				else {
					// add print page break
					$print_htmls .= '<div style="page-break-before:always">';
				}

				$print_htmls .= $this->get_statement_html($key);
				$print_htmls .= '</div>';
			}

			$data['main_content'] = $print_htmls;
			$this->load->view('includes/template_frame_bat_print', $data);
		}
		else{

		}


  }


  function get_statement_html($id)
  {
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');

    //client
    $data['customer'] =  $this->member_model->load_by_id($id);

    $data['transactions'] = $this->statement_model->load_by_uid($id, $data['date']);

    $data['deduction'] = $this->statement_model->getHisBalance($id, $data['date']['fr']);

    return $this->load->view('print_html/statement_bat_print_view', $data, true);
  }



  function send_msg($id){
    $msg = 'Are you really want to send statement to [ CUSTOMER #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
    $msg .= '<a href="'.site_url("member/email_statement/$id").'" class="black">Yes</a>';
    $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $msg .= '<a href="'.site_url('statement/lib/'.$this->session->userdata('page_index')).'" class="black">No</a>';
    $msg .= '<form name="input" action="'.site_url("member/email_statement/$id").'" method="post">
<br />Additional Message: <br /><textarea name="email_msg" style="width: 349px; "></textarea>
<input type="submit" value="Send" style="color: #030303;" class="ui-state-highlight ui-corner-all">
</form>';
    $this->session->set_flashdata('msg', $msg);
    redirect('statement/lib/'.$this->session->userdata('page_index'));
  }


    public function sendBatchMsg() {
        $this->db->select('id, email, balance');
        $this->db->where('balance >', '0');
        $query = $this->db->get('member');
        $result = $query->result();

        foreach($result as $member) {

          @require_once(BASEPATH . '/libraries/cemail.php');
          $data['email_msg'] = $this->input->post('email_msg'); 
          $id = $member->id;
            if ($id){
                $this->load->model('member_model');
                $this->load->model('statement_model');	

                $this->load->helper(array('dompdf', 'file'));
                $data['date']['fr'] = $this->session->userdata('date_fr');
                $data['date']['to'] = $this->session->userdata('date_to');


                $data['customer'] =  $this->member_model->load_by_id($id);
                $data['transactions'] = $this->statement_model->load_by_uid($id,  $data['date']);

                $data['deduction'] = $this->statement_model->getHisBalance($id, $data['date']['fr']);

                $html = $this->load->view('print_html/statement_print_view', $data, TRUE);

                $pdf = pdf_create($html, 'STATEMENT_'.date('Y-m-d'), FALSE);

                $file = 'files/STATEMENT_'.date('Y-m-d').'.pdf';

                file_put_contents($file, $pdf);

                $attach = chunk_split(base64_encode(file_get_contents($file)));

                $data['member'] = $this->member_model->load_member($id); 
                $data['sender'] = $this->cf_site['sender'];
                $data['website'] = $this->cf_site['name'];
                $letter = $this->load->view('email_templates/email_statement', $data, true);
                @$subject = 'STATEMENT'.date('Y-m-d');
                @$sendto = $data['member']->email.",".$this->cf_site['email'];
            //    @$sendto = "shawn@3a.co.nz";
                @$replyto = $this->cf_site['email'];
                @$filename = $file; 
                @$mimetype = "application/pdf";

                @$mailfile = new CMailFile($subject,$sendto,$replyto,$letter,$filename,$excelname,$mimetype); 

                @$mailfile->sendfile(); 
		sleep(10);
            }
        }
        
        redirect('statement/lib');
    }


}
