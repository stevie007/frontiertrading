<?php

class Back_Order extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    // $this->load->helper('date');
    $this->load->helper('auth');
    is_logged_in();
    $this->load->model('order_model'); 
    $this->load->model('back_order_model'); 
    $this->load->model('member_model');
    $this->load->model('item_model');
    $this->load->model('invoice_model');
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');     
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('back_order/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('back_order/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('back_order_model');
    
    $back_order_list = $this->back_order_model->back_order_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $back_order_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $back_order_list->query;
    
    $data['title'] = '';
    $data['main_content'] = 'back_order_list_view';  
    $data['table_title'] = 'Back Order list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu4'] = 0;
    $this->load->view('includes/template', $data);	

  }



  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('back_order_model');
    
    $back_order_list = $this->back_order_model->back_order_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $back_order_list->total;
    $data['query'] = $back_order_list->query;
    $data['title'] = 'Back_Order Statement';
    $data['table_title'] = 'Transaction list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu3'] = 0;
    $this->load->view('back_order_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('back_order/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('back_order/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('back_order/lib');
  }
  

  function ajax_back_order($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $back_order = new stdClass;
      $back_order->id = 'new';
      $back_order->date = date("Y-m-d H:i:s");
      $back_order->subtotal 
	= $back_order->tax 
	= $back_order->total 
	= $back_order->discount 
	= $back_order->paid 
	= $back_order->deleted = 0;
      	$back_order->cust_id = $back_order->cust_name = '';

      if ($cart = $this->session->userdata('cart')) {
	$contents = $cart;
      } else {
	$contents=array();
	$contents[0] 
	  = array('product' => '', 'quantity' => 1, 'unit_price' => 0, 'discount' => 0);
      }      

      $back_order->contents = json_encode($contents);

      $json_obj->back_order = $back_order;
      
      echo json_encode($json_obj);
      
    } elseif ($id == 'trans') {
      $id = $this->uri->segment(4);

      
      if ($order = $this->order_model->load_order($id)) {
	
	$back_order = new stdClass;
	$back_order->id = 'new';
	$back_order->date = date("Y-m-d H:i:s");
	$back_order->subtotal = $order->subtotal;
	$back_order->tax = $order->gst;
	$back_order->total = $order->total;
	$back_order->discount = 0;
	$back_order->paid = 0; 
	$back_order->deleted = 0;
	$back_order->cust_name = $this->member_model->load_member($order->cust_id)->company;
	$back_order->cust_id = $order->cust_id;
	$back_order->order_id = $order->id;


	$contents=array();
	foreach (json_decode($order->content) as $item) {
	  $curr_item 
	    = array('id' => $item->id, 'product' => $item->name, 'quantity' => $item->qty, 
		    'unit_price' => $item->price, 'discount' => 0);
	  array_push($contents, $curr_item);
	}
	$back_order->contents = json_encode($contents);

	$json_obj->back_order = $back_order;      

	
	echo json_encode($json_obj);
	
      }      
      
    } else {
     
      $this->load->model('back_order_model');
      if ($back_order = $this->back_order_model->load_back_order($id)) {
	
	$json_obj->back_order = $back_order;
 
	echo json_encode($json_obj);

      }      
    }

  }


  function view($id)
  {


    $this->db->where('id', $id);
    $result = $this->db->get('back_order')->result();
    
    $data['back_order'] = $result[0];
    $data['back_order']->date = date("d/m/y", strtotime($data['back_order']->date));
    $data['back_order']->content = json_decode($data['back_order']->contents); 
    
    
    $data['customer'] =  $this->member_model->load_by_id($data['back_order']->cust_id);
    
    $this->load->view('back_order_print_view', $data);
  
  }

  
  function edit()
  { 
    if (!($id = $this->uri->segment(3)))
      {
	redirect('back_order/add');
      }

    $data['json_url'] = site_url('back_order/ajax_back_order/'.$id); 
        $data['cart_url'] = site_url('back_order/ajax_cart');  
    $data['post_url'] = site_url('back_order/save'); 
    
    $data['main_content'] = 'back_order_edit_view';  
    $data['frame_name'] = 'Edit Back Order';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }


  function ajax_cart()
  { 
    $json_obj = new stdClass;

    if ($cart = $this->cart->contents()) {
      $contents = $cart; 
      $this->cart->destroy();
      $json_obj->cart = $contents;
      echo json_encode($json_obj);
    }    
        
  }


 function add()
  {die;
    $data['json_url'] = site_url('back_order/ajax_back_order/new'); 
    $data['post_url'] = site_url('back_order/save'); 

    $data['main_content'] = 'back_order_edit_view';  
    $data['frame_name'] = 'Add Back_Order';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }


  function trans($id)
  {
  
    if ($back_order = $this->back_order_model->load_back_order($id)) {

      $invoice = new stdClass;
      $invoice->id = 'new';
      $invoice->date = date("Y-m-d H:i:s");
      $invoice->discount = 0;
      $invoice->paid = 0; 
      $invoice->deleted = 0;
      $invoice->cust_name = $this->member_model->load_member($back_order->cust_id)->company;
      $invoice->cust_id = $back_order->cust_id;
      $invoice->order_id = $back_order->id;     
      $contents= json_decode($back_order->contents); 
      $inv_contents=array();
      $bk_order = clone $invoice;    
      $bk_contents=array();
            $bk_order->trans = 0;  
      //sync stock

     
      foreach ($contents as &$item) {   
	$bk_item = new stdClass;
	$bk_item->id = $item->id;
	$bk_item->name = $item->name;
	$bk_item->qty = 0;
	$bk_item->price = $item->price;
	$bk_item->discount = $item->discount = 0;

	if ($stock = $this->item_model->load_item($item->id))
	  {  
	    if ($stock->qty > $item->qty)
	      {
		$stock->qty = $stock->qty - $item->qty;		  
		$this->db->where('id', $stock->id);
		$this->db->update('items', $stock);
	      }
	    else 
	      {
		$bk_item->qty = $item->qty - $stock->qty;	     
		$item->qty = $stock->qty;
		$stock->qty = 0;  
		$this->db->where('id', $stock->id);
		$this->db->update('items', $stock);
	      }	   
	    if ($bk_item->qty) array_push($bk_contents, $bk_item);
	    if ($item->qty) array_push($inv_contents, $item);
	  }
      } 
      // end sync stock
   
      // calc totals 
      $invoice->subtotal = 0;
      foreach ($inv_contents as $item){
	$invoice->subtotal += $item->price * $item->qty;
      }
      $invoice->tax = $invoice->subtotal * 0.15;
      $invoice->total = $invoice->subtotal * 1.15;
     
      $bk_order->subtotal = 0;
      foreach ($bk_contents as $item){
	$bk_order->subtotal += $item->price * $item->qty;
      }
      $bk_order->tax = $bk_order->subtotal * 0.15;
      $bk_order->total = $bk_order->subtotal * 1.15;
    
      // end calc

      $msg = '';   
      if ($bk_order->total)
	{	      	  $this->back_order_model->mark_as_transferred($id);
	  $insert_id = $this->back_order_model->back_order_add($bk_order, $bk_contents);
	  $msg .= 'Order transfered to back order#'.$insert_id.' successfully.';
	  $invoice->bk_order_id = $insert_id;
	}   
      if ($invoice->total)
	{
	  $insert_id = $this->invoice_model->invoice_add($invoice, $inv_contents);
	      	  $this->back_order_model->mark_as_transferred($id);
	  $msg .= 'Order transfered to invoice#'.$insert_id.' successfully.';
	}
      //$msg = 'Order transfered successfully.';
      $this->session->set_flashdata('msg', $msg);
    }      
    
    redirect('back_order/lib');
   
  }
 
 function save()
 {
   $data['session_msg'] = 'Back_Order Saved';
   $this->load->view('includes/session_msg', $data);

   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('back_order_model');

   if ($json_obj->transaction->id == 'new') {
     $this->back_order_model->back_order_add($json_obj->transaction
				       , $json_obj->contents);
   } else {
     $this->back_order_model->back_order_update($json_obj->transaction
					  , $json_obj->contents);
   }

 }



 
  function del(){
    echo 'Authorisation rejected.';
  }

}