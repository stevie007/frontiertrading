<?php

class Invoice extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('email');
    is_logged_in();
    $this->load->model('order_model');
    $this->load->model('back_order_model');
    $this->load->model('member_model');
    $this->load->model('item_model');
    $this->load->model('stat_inven_model');
    $this->load->model('statement_model');
  }

  function test()
  {
  echo pass_auth('administrator') ? 'yes' : 'no';

    echo '123';die;
  }

  function index()
  {

    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $filter[1]['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('sales', 'Sales..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc';
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('invoice/lib');

  }
 

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['customer'] = $this->session->userdata('customer');    
    $data['sales'] = $this->session->userdata('sales');
    $data['search'] = $this->session->userdata('search');
    $this->session->set_userdata('page_index', $this->uri->segment(3));
    
    // staff account do not listing all customers
    if (pass_auth('staff') && $data['customer'] == 'Customer..')
    {
    	 $data['customer'] = 'Customer..';    
    }
    
    if  (pass_auth('staff') && strlen($data['search']) == 7)
    {    
    	 $data['customer'] = 'Customer..';     
    }
    
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('invoice/lib/');
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('invoice_model');
    // Staff account should cannot see all
    if (pass_auth('staff') && $data['customer'] == 'Customer..' && $data['search'] == 'Search..' && $data['sales'] == 'Sales..'){
        $disable['id'] = 0; 
        $data['filter'][] = $disable;
    }

    $invoice_list = $this->invoice_model->invoice_list($data['date'],
						       $data['customer'],
						       $data['filter'],
						       $data['search'],
						       $data['orderby'],
						       $config['per_page'],
						       $this->uri->segment(3),
						       $data['sales']
						       );



    $config['total_rows'] = $data['total'] = $invoice_list->total;

    $this->pagination->initialize($config);

    $data['query'] = $invoice_list->query;

    $data['title'] = '';

    $data['main_content'] = 'invoice_list_view';
    $data['table_title'] = 'Invoice list';

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu4'] = 0;
    $this->load->view('includes/template', $data);

  }

  function export()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['customer'] = $this->session->userdata('customer');
    $data['sales'] = $this->session->userdata('sales');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';



    $this->load->model('invoice_model');

    $invoice_list = $this->invoice_model->invoice_list($data['date'],
						       $data['customer'],
						       $data['filter'],
						       $data['search'],
						       $data['orderby'],
						       '',
						       '',
						       $data['sales']);



$query =  $invoice_list->query;

$this->load->helper('csv');
query_to_csv($query, TRUE, 'invoices.csv');
  }

  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['customer'] = $this->session->userdata('customer');
    $data['sales'] = $this->session->userdata('sales');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';



    $this->load->model('invoice_model');

    $invoice_list = $this->invoice_model->invoice_list($data['date'],
						       $data['customer'],
						       $data['filter'],
						       $data['search'],
						       $data['orderby'],
						       '',
						       '',
						       $data['sales']);

    $data['total'] = $invoice_list->total;
    $data['query'] = $invoice_list->query;
    $data['title'] = 'Invoice Report';
    $data['table_title'] = 'Transaction list';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('invoice_report_view', $data);

  }

  function orderby()
  {
    if ($order = $this->uri->segment(3))
      {
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort;
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('invoice/lib');
  }


  function filter()
  {

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else
	$this->session->set_userdata('per_page', 25);
    }
    else if ($flr_num = $this->uri->segment(3))
      {

	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);
      }
    redirect('invoice/lib');
  }


  function search()
  {
    if ($search = $this->input->post('search')) {
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {
      $this->session->set_userdata('customer', $customer);
    }
    if ($sales = $this->input->post('sales')) {
      $this->session->set_userdata('sales', $sales);
    }
    if ($from = $this->input->post('from')) {
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {
      $this->session->set_userdata('date_to',
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('invoice/lib');
  }


  function ajax_invoice($id)
  {
    $json_obj = new stdClass;

    if ($id == 'new') {
      $invoice = new stdClass;
      $invoice->id = 'new';
      $invoice->date = date("Y-m-d H:i:s");
      $invoice->subtotal
	= $invoice->tax
	= $invoice->total
	= $invoice->discount
	= $invoice->paid
	= $invoice->deleted = 0;
      $invoice->freight = 0;
      $invoice->cust_id = $invoice->cust_name = $invoice->sales = '';
$invoice->delivered = 1;
      if ($cart = $this->cart->contents()) {
	$contents = $cart;

      } else {
	$contents=array();
	$contents[0]
	  = array('name' => '', 'qty' => 1, 'price' => 0, 'discount' => 0);
      }

      $invoice->contents = json_encode($contents);

      $json_obj->invoice = $invoice;

      echo json_encode($json_obj);

    } elseif ($id == 'trans') {
      $id = $this->uri->segment(4);


      if ($order = $this->order_model->load_order($id)) {

	$invoice = new stdClass;
	$invoice->id = 'new';
	$invoice->date = date("Y-m-d H:i:s");
	$invoice->subtotal = $order->subtotal;
	$invoice->tax = $order->gst;
	$invoice->total = $order->total;
	$invoice->discount = 0;
	$invoice->paid = 0;
	$invoice->deleted = 0;
	$invoice->cust_name = $this->member_model->load_member($order->cust_id)->company;
	$invoice->cust_id = $order->cust_id;
	$invoice->order_id = $order->id;
	$invoice->sales = '';
$invoice->delivered = 1;
	$contents=array();
	foreach (json_decode($order->content) as $item) {
	  $curr_item
	    = array('id' => $item->id, 'product' => $item->name, 'quantity' => $item->qty,
		    'unit_price' => $item->price, 'discount' => 0);
	  array_push($contents, $curr_item);
	}
	$invoice->contents = json_encode($contents);

	$json_obj->invoice = $invoice;


	echo json_encode($json_obj);

      }

    } else {

      $this->load->model('invoice_model');
      if ($invoice = $this->invoice_model->load_invoice($id)) {

	$json_obj->invoice = $invoice;

	echo json_encode($json_obj);

      }
    }

  }


  function view($id)
  {


    $this->db->where('id', $id);
    $result = $this->db->get('invoice')->result();

    $data['invoice'] = $result[0];

    $data['invoice']->duedate = date("d/m/Y",
				     mktime(0, 0, 0,
					    date("m",
						 strtotime($data['invoice']->date))+1, '20',
					    date("Y",
						 strtotime($data['invoice']->date))
					    )
				     );

    $data['invoice']->date = date("d/m/Y", strtotime($data['invoice']->date));

    $data['invoice']->content = json_decode($data['invoice']->contents);

    // client
    $data['customer'] =  $this->member_model->load_by_id($data['invoice']->cust_id);

    // back order
    if ($data['invoice']->bk_order_id)
      {
	$data['bk_order'] = $this->back_order_model->load_by_id($data['invoice']->bk_order_id);
	$data['bk_order']->content = json_decode($data['bk_order']->contents);
      }
    //    $this->load->view('print_html/invoice_print_view', $data);

    $data['main_content'] = 'print_html/invoice_print_view';
    foreach ($data['invoice']->content as $item) {
        if(!property_exists($item, 'id')) {
            
            if (!property_exists($item, 'name')) {
                continue;
            }
            
            $name = $item->name;
            $this->db->where('title', $name);
            $result = $this->db->get('items')->result();
        } else {
            $id = $item->id;
            $this->db->where('id', $id);
            $result = $this->db->get('items')->result();
        }
        
        if(empty($result)) continue;
        
        $img = $result[0]->image1;
        $item->img = $img;
    }

    $this->load->view('includes/template_frame_print', $data);
  }
  
  function pdf($id)
  {
    $this->load->helper(array('dompdf', 'file'));
    $this->db->where('id', $id);
    $result = $this->db->get('invoice')->result();

    $data['invoice'] = $result[0];

    $data['invoice']->duedate = date("d/m/Y",
				     mktime(0, 0, 0,
					    date("m",
						 strtotime($data['invoice']->date))+1, '20',
					    date("Y",
						 strtotime($data['invoice']->date))
					    )
				     );

    $data['invoice']->date = date("d/m/Y", strtotime($data['invoice']->date));

    $data['invoice']->content = json_decode($data['invoice']->contents);

    // client
    $data['customer'] =  $this->member_model->load_by_id($data['invoice']->cust_id);

    // back order
    if ($data['invoice']->bk_order_id)
      {
	$data['bk_order'] = $this->back_order_model->load_by_id($data['invoice']->bk_order_id);
	$data['bk_order']->content = json_decode($data['bk_order']->contents);
      }
    $html = $this->load->view('print_html/invoice_print_view', $data, TRUE);

    pdf_create($html, 'INVOICE#'.$data['invoice']->id);


  }


  function edit()
  {
    if (!($id = $this->uri->segment(3)))
      {
	redirect('invoice/add');
      }

    $data['json_url'] = site_url('invoice/ajax_invoice/'.$id);
    $data['cart_url'] = site_url('invoice/ajax_cart');
    $data['post_url'] = site_url('invoice/save');

    $data['main_content'] = 'invoice_edit_view';
    $data['frame_name'] = 'Edit Invoice';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);

  }


  function ajax_cart()
  {
    $json_obj = new stdClass;

    if ($cart = $this->cart->contents()) {
      $contents = $cart;
      $this->cart->destroy();
      $json_obj->cart = $contents;
      echo json_encode($json_obj);
    }

  }



  function add()
  {
    $data['json_url'] = site_url('invoice/ajax_invoice/new');
    $data['cart_url'] = site_url('invoice/ajax_cart');
    $data['post_url'] = site_url('invoice/save');

    $data['main_content'] = 'invoice_edit_view';
    $data['frame_name'] = 'Add Invoice';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);

  }


  function trans($id)
  {
    $data['json_url'] = site_url('invoice/ajax_invoice/trans').'/'.$id;
    $data['cart_url'] = site_url('invoice/ajax_cart');
    $data['post_url'] = site_url('invoice/save');

    $data['main_content'] = 'invoice_edit_view';
    $data['frame_name'] = 'Add Invoice';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);

  }

  function save()
  {
  	  $this->cart->destroy();
    $data['session_msg'] = 'Invoice Saved';
    $this->load->view('includes/session_msg', $data);

    $json_obj = json_decode($this->input->post('json'));
    $this->load->model('invoice_model');

    $json_obj->transaction->date = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $json_obj->transaction->date)));

    if ($json_obj->transaction->id == 'new')
      {

	$invoice = $json_obj->transaction;


	$inv_contents=array();
		$bk_order->date = $invoice->date;
	$bk_order->subtotal = $invoice->subtotal;
	$bk_order->tax = $invoice->tax;
	$bk_order->total = $invoice->total;
	$bk_order->cust_id = $invoice->cust_id;
	$bk_order->cust_name = $invoice->cust_name;
	      $bk_order->trans = 0;
	$bk_contents=array();
	$contents= $json_obj->contents;

	//sync stock
	foreach ($contents as $item) {
	  if ($stock = $this->item_model->load_by_title($item->name))
	    {
	      $bk_item = new stdClass;
	      $bk_item->id = $item->id = $stock->id;
	      $bk_item->name = $stock->title;
	      $bk_item->qty = 0;
	      $bk_item->price = $item->price;
	      $bk_item->discount = $item->discount = 0;

	      if ($stock->qty > $item->qty)
		{
		  $stock->qty = $stock->qty - $item->qty;
		  // static
		  $this->stat_inven_model->adjust($stock->id, $stock->qty);
		  // stock
		  $this->db->where('id', $stock->id);
		  $this->db->update('items', $stock);
		}
	      else
		{
		  $bk_item->qty = $item->qty - $stock->qty;
		  $item->qty = $stock->qty;
		  $stock->qty = 0;
		  // static
		  $this->stat_inven_model->adjust($stock->id, $stock->qty);
		  // stock
		  $this->db->where('id', $stock->id);
		  $this->db->update('items', $stock);
		}
	      if ($bk_item->qty) array_push($bk_contents, $bk_item);
	      if ($item->qty) array_push($inv_contents, $item);

	    }
	  else
	    {
	      array_push($inv_contents, $item);
	    }
	}
	// end sync stock

	// calc totals
	$invoice->subtotal = 0;
	foreach ($inv_contents as $item){
	  $invoice->subtotal += $item->price * $item->qty;
	}
	$invoice->tax = ($invoice->subtotal + $invoice->freight) * 0.15;
	$invoice->total = ($invoice->subtotal + $invoice->freight) * 1.15;

	$bk_order->subtotal = 0;
	foreach ($bk_contents as $item){
	  $bk_order->subtotal += $item->price * $item->qty;
	}
	$bk_order->tax = $bk_order->subtotal * 0.15;
	$bk_order->total = $bk_order->subtotal * 1.15;

	$msg = '';
	if ($bk_order->total)
	  {
	    $insert_id = $this->back_order_model->back_order_add($bk_order, $bk_contents);
	    $msg .= 'invoice transfered to back order#'.$insert_id.' successfully.';
	    $invoice->bk_order_id = $insert_id;
	  }
	if ($invoice->total)
	  {
	    $insert_id = $this->invoice_model->invoice_add($invoice, $inv_contents);
	    $msg .= 'Added invoice#'.$insert_id.' successfully.';
	  }
	$this->session->set_flashdata('msg', $msg);

      }
    else // update invoice
      {

	$invoice = $json_obj->transaction;
	$contents = $json_obj->contents;
	//sync stock
	$prev = $this->invoice_model->load_invoice($invoice->id);
	$prev_cnt = json_decode($prev->contents);
	foreach ($contents as $item) {
	  if (isset($item->id)) {
	    $dec = $item->qty;
	    foreach ($prev_cnt as $prev_i){
	      if (isset($prev_i->id)) {
		if($item->id == $prev_i->id){
		  $dec = $dec - $prev_i->qty;
		}
	      }
	    }
	    $this->db->where('id', $item->id);
	    $query = $this->db->get('items');
	    if ($query->num_rows() > 0)
	      {
		$result = $query->result();
		$stock  = $result[0];
		$stock->qty = $stock->qty - $dec;
		// static
		$this->stat_inven_model->adjust($stock->id, $stock->qty);
		// stock
		$this->db->where('id', $stock->id);
		$this->db->update('items', $stock);
	      }
	  } //end if isset
	}
	//end sync stock

     	$this->invoice_model->invoice_update($invoice, $contents);
      }
      
    //Should update balance after save
    $this->statement_model->sync_bal_by_uid($json_obj->transaction->cust_id);
  }

  function send_msg($id){
  
      $msg = 'Are you really want to send [ INVOICE #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
    $msg .= '<a href="'.site_url("member/email_invoice/$id").'" class="black">Yes</a>';
    $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $msg .= '<a href="'.site_url('invoice/lib/'.$this->session->userdata('page_index')).'" class="black">No</a>';
    $msg .= '<form name="input" action="'.site_url("member/email_invoice/$id").'" method="post">
<br />Additional Message: <br /><textarea name="email_msg" style="width: 349px; "></textarea>
<input type="submit" value="Send" style="color: #030303;" class="ui-state-highlight ui-corner-all">
</form>';
    $this->session->set_flashdata('msg', $msg);
    redirect('invoice/lib/'.$this->session->userdata('page_index'));
    /*
    $msg = 'Are you really want to send [ INVOICE #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
    $msg .= '<a href="'.site_url("member/email_invoice/$id").'" class="black">Yes</a>';
    $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $msg .= '<a href="'.site_url('invoice/lib/'.$this->session->userdata('page_index')).'" class="black">No</a>';
    $this->session->set_flashdata('msg', $msg);
    */
  }

  function del_msg($id){
  
    $msg = 'Are you really want to delete [ INVOICE #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
    $msg .= '<a href="'.site_url('invoice/del/'.$id).'" class="black">Yes</a>';
    $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $msg .= '<a href="'.site_url('invoice/lib').'" class="black">No</a>';
    $this->session->set_flashdata('msg', $msg);
    redirect('invoice/lib');
  }

  function del($id){

    $this->load->model('invoice_model');
    $this->invoice_model->delete_by_id($id);
    $msg = 'Invoice Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('invoice/lib');
  }




  function add_invoice_to_print() {
    $invoice_id = $this->input->post('invoice_id');
    $cust_name = $this->input->post('cust_name');

    $invoice_print_list = $this->session->userdata('invoice_print_list');

	$invoice_print_list['invoices'][$invoice_id] = $cust_name;

	$this->session->set_userdata('invoice_print_list', $invoice_print_list);

	$data['invoices'] = $invoice_print_list['invoices'];
    $data['count'] = sizeof($invoice_print_list['invoices']);
    echo json_encode($data);
  }


  function empty_invoice_print_queue() {
	  $this->session->unset_userdata('invoice_print_list');
	  redirect('invoice');
  }


  function print_selected_invoices() {
		$print_htmls = "";

		$invoice_print_list = $this->session->userdata('invoice_print_list');
		if (sizeof($invoice_print_list['invoices']))
		{
			$is_first_page = 1;

			foreach ($invoice_print_list['invoices'] as $key => $value) {

				if ($is_first_page){
					$print_htmls .= '<div>';
					$is_first_page = 0;
				}
				else {
					// add print page break
					$print_htmls .= '<div style="page-break-before:always">';
				}

				$print_htmls .= $this->get_invoice_html($key);
				$print_htmls .= '</div>';
			}

			$data['main_content'] = $print_htmls;
			$this->load->view('includes/template_frame_bat_print', $data);
		}
		else{

		}


  }


  function get_invoice_html($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('invoice')->result();

    $data['invoice'] = $result[0];

    $data['invoice']->duedate = date("d/m/Y",
				     mktime(0, 0, 0,
					    date("m",
						 strtotime($data['invoice']->date))+1, '20',
					    date("Y",
						 strtotime($data['invoice']->date))
					    )
				     );

    $data['invoice']->date = date("d/m/Y", strtotime($data['invoice']->date));

    $data['invoice']->content = json_decode($data['invoice']->contents);

    // client
    $data['customer'] =  $this->member_model->load_by_id($data['invoice']->cust_id);

    // back order
    if ($data['invoice']->bk_order_id)
      {
	$data['bk_order'] = $this->back_order_model->load_by_id($data['invoice']->bk_order_id);
	$data['bk_order']->content = json_decode($data['bk_order']->contents);
      }

    return $this->load->view('print_html/invoice_bat_print_view', $data, true);

  }






}
