<?php

class Member extends CI_Controller
{
  var $cf_site;

  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');      $this->load->helper('email');
    $this->cf_site = $this->config->item('site');
    is_logged_in();
  }


  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');

    $config['base_url'] = site_url('member/lib/');
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;

    $this->load->model('member_model');

    $member_list = $this->member_model->member_list($data['search'], $data['filter'], $data['orderby'], $config['per_page'], $this->uri->segment(3));

    $config['total_rows'] = $data['total'] = $member_list->total;

    $this->pagination->initialize($config);

    $data['query'] = $member_list->query;

    $data['title'] = 'Database';
    $data['table_title'] = 'Member List';
    $data['main_content'] = 'member_lib_view';

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu3'] = 0;
    $this->load->view('includes/template', $data);

  }


  function index()
  {
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc';
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('member/lib');
  }


  function orderby()
  {
    if ($order = $this->uri->segment(3))
      {
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort;
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('member/lib');
  }


  function filter()
  {
    if (($key = $this->uri->segment(3))
	&&
	($value = $this->uri->segment(4)))
      {
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);
      }
    redirect('member/lib');
  }


  function search()
  {
    if ($search = $this->input->post('search')) {
      $this->session->set_userdata('search', $search);
    }
    redirect('member/lib');
  }


  function add()
  {
    $member = new stdClass;
    $member->id
      = $member->company
      = $member->firstname
      = $member->lastname
      = $member->email
      = $member->telephone
      = $member->mobile
      = $member->fax
      = $member->address
      = $member->suburb
      = $member->city
      = $member->postcode
      = $member->address2
      = $member->suburb2
      = $member->city2
      = $member->postcode2
      = $member->password
      = $member->authority
      = $member->resetcode
      = $member->notes
      = '';

    $member->date_sent = '2012-01-01';
    $member->auto_send = 0;
    $member->active = 0;

    $data['member'] = $member;

    $data['title'] = 'Add Member';
    $data['table_title'] = 'Member Detail';
    $data['main_content'] = 'member_edit_view';

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu3'] = 0;
    $this->load->view('includes/template', $data);

  }


  function edit()
  {

    if (!($id = $this->uri->segment(3)))
      {
	redirect('member/add');
      }

    $this->load->model('member_model');

    if ($data['member'] = $this->member_model->load_member($id)) {

      $data['title'] = 'Edit Member';
      $data['table_title'] = 'Member Detail';
      $data['main_content'] = 'member_edit_view';

      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu3'] = 0;
      $this->load->view('includes/template', $data);
    }



  }

  function member_submit()
  {
    $this->load->model('member_model');
    if($id = $this->input->post('id'))
      {

       	$data['member'] = $this->member_model->load_member($id);

	$this->member_model->member_update($id);
	$msg = 'Member updated successfully.';


	// email for activate noty
	if (!$data['member']->active && ($this->input->post('active') == 'accept'))
	{
	  $data['sender'] = $this->cf_site['sender'];
	  $data['website'] = $this->cf_site['name'];

	  $letter = $this->load->view('email_templates/activate_noty', $data, true);

	  $this->load->library('email');
	  $this->email->from($this->cf_site['email'], $this->cf_site['sender']);
	  $this->email->to($this->input->post('email'));
	  $this->email->subject('Your account have been activated');
	  $this->email->message($letter);
	  $this->email->send();
	  $msg .= ' Automatic email notification has been sent to this client.';
	}
 	$this->session->set_flashdata('msg', $msg);
	redirect('member/edit/'.$id);

      }
    else
      {
	$this->member_model->member_add();
	$msg = 'Member added successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('member');
      }
  }

  function del()
  {

    if ($id = $this->uri->segment(3))
      {
	$this->load->model('member_model');
	$this->member_model->member_del($id);
	$msg = 'Member deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

    redirect('member');

  }


  function email_statement($id = '')
  {
     $data['email_msg'] = $this->input->post('email_msg');
    $id = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    if ($id)
      {
      	$this->load->model('member_model');
      	$this->load->model('statement_model');

        $this->load->helper(array('dompdf', 'file'));
        /*
        $data['date']['fr'] = strtotime("1 January 2000");
        $data['date']['to'] = time();
        */

        // control the statement date
        $data['date']['fr'] = $this->session->userdata('date_fr');
    	$data['date']['to'] = $this->session->userdata('date_to');


	$data['customer'] =  $this->member_model->load_by_id($id);
    	$data['transactions'] = $this->statement_model->load_by_uid($id,  $data['date']);

        $data['deduction'] = $this->statement_model->getHisBalance($id, $data['date']['fr']);

        $html = $this->load->view('print_html/statement_print_view', $data, TRUE);

        $pdf = pdf_create($html, 'STATEMENT_'.date('Y-m-d'), FALSE);

 	$file = 'files/STATEMENT_'.date('Y-m-d').'.pdf';

        file_put_contents($file, $pdf);

  $attach = chunk_split(base64_encode(file_get_contents($file)));

	$data['member'] = $this->member_model->load_member($id);
	$data['sender'] = $this->cf_site['sender'];
	$data['website'] = $this->cf_site['name'];
	$letter = $this->load->view('email_templates/email_statement', $data, true);



// 	$this->load->library('email');
// 	$this->email->from($this->cf_site['email'], $this->cf_site['sender']);
// 	$this->email->to($data['member']->email);
// //	$this->email->bcc($this->cf_site['email']);
// 	$this->email->to('ke@3a.co.nz');
// 	$this->email->subject('STATEMENT'.date('Y-m-d'));

// 	$this->email->message($letter);
// 	$this->email->attach($file);
// 	$this->email->send();



//  @require_once('/home/frontier/public_html/system/libraries/cemail.php');
  @require_once(BASEPATH . '/libraries/cemail.php');

  @$subject = 'STATEMENT'.date('Y-m-d');
  @$sendto = $data['member']->email.",".$this->cf_site['email'];
  //@$sendto = "nizhishuai0909@gmail.com,vick@3a.co.nz";
  @$replyto = $this->cf_site['email'];
  @$filename = $file;
  @$mimetype = "application/pdf";

  @$mailfile = new CMailFile($subject,$sendto,$replyto,$letter,$filename,$excelname,$mimetype);

  @$mailfile->sendfile();








	$msg = 'Statement sent to customer id #'.$id.'<style>#cust'.$id.' {outline: yellow solid 1px;}</style>';
	$this->session->set_flashdata('msg', $msg);
	unlink($file);

      }

    redirect('statement/lib/'.$this->session->userdata('page_index'));

  }


  function email_invoice($id = ''){
    $data['email_msg'] = $this->input->post('email_msg');
    $id = $this->uri->segment(3) ? $this->uri->segment(3) : '';
    if ($id){
      $this->load->model('member_model');
      $this->load->model('back_order_model');
      $this->load->helper(array('dompdf', 'file'));
      $this->db->where('id', $id);
      $result = $this->db->get('invoice')->result();

      $data['invoice'] = $result[0];
      $data['invoice']->duedate = date("d/m/Y",
				     mktime(0, 0, 0,
					    date("m",
						 strtotime($data['invoice']->date))+1, '20',
					    date("Y",
						 strtotime($data['invoice']->date))
					    )
				     );
      $data['invoice']->date = date("d/m/Y", strtotime($data['invoice']->date));
      $data['invoice']->content = json_decode($data['invoice']->contents);

      // client
      $data['customer'] =  $this->member_model->load_by_id($data['invoice']->cust_id);

      // back order
      if ($data['invoice']->bk_order_id) {
      	$data['bk_order'] = $this->back_order_model->load_by_id($data['invoice']->bk_order_id);
      	$data['bk_order']->content = json_decode($data['bk_order']->contents);
      }

      $html = $this->load->view('print_html/invoice_print_view', $data, TRUE);
      $pdf = pdf_create($html, 'INVOICE#'.$data['invoice']->id, FALSE);
      $file = 'files/INVOICE_'.$data['invoice']->id.'.pdf';

      file_put_contents($file, $pdf);
      $attach = chunk_split(base64_encode(file_get_contents($file)));

    	$data['member'] = $data['customer'];
    	$data['sender'] = $this->cf_site['sender'];
    	$data['website'] = $this->cf_site['name'];
    	$letter = $this->load->view('email_templates/email_invoice', $data, true);


      if (!valid_email($data['member']->email)) {
        $msg = 'Email address invalid, INVOICE could not be sent to customer id #'.$id;
        $this->session->set_flashdata('msg', $msg);
        unlink($file);
        redirect('invoice/lib');
      }

      //@require_once('/home/frontier/public_html/system/libraries/cemail.php');
      @require_once(BASEPATH . '/libraries/cemail.php');
      @$subject = 'INVOICE '.$data['invoice']->id;
      @$sendto = $data['member']->email.",".$this->cf_site['email'];
      //@$sendto = "nizhishuai0909@gmail.com,vick@3a.co.nz";
      @$replyto = $this->cf_site['email'];
      @$filename = $file;
      @$mimetype = "application/pdf";
      @$mailfile = new CMailFile($subject,$sendto,$replyto,$letter,$filename,$excelname,$mimetype);
      @$mailfile->sendfile();


    	$msg = 'INVOICE sent to customer id #'.$id.'<style>#cust'.$id.' {outline: yellow solid 1px;}</style>';
    	$this->session->set_flashdata('msg', $msg);
    	unlink($file);
    }

    redirect('invoice/lib/'.$this->session->userdata('page_index'));
  }



  function reset_passwd()
  {

    if ($id = $this->uri->segment(3))
      {
	$this->load->model('member_model');

	if ($code = $this->member_model->set_resetcode($id))
	{
		$data['member'] = $this->member_model->load_member($id);
		$data['sender'] = $this->cf_site['sender'];
		$data['website'] = $this->cf_site['name'];
		$letter = $this->load->view('email_templates/reset_passwd', $data, true);

		$this->load->library('email');
		$this->email->from($this->cf_site['email'], $this->cf_site['sender']);
		$this->email->to($data['member']->email);

		$this->email->subject('Reset your password for our new website');

		$this->email->message($letter);

		$this->email->send();

		$msg = 'reset code sent to customer id #'.$id;
		$this->session->set_flashdata('msg', $msg);
	}

      }

    redirect('member');

  }


  function del_msg($id){

    $this->load->model('member_model');
    if ($member = $this->member_model->load_member($id)) {

      $msg = 'Are you really want to delete [ '.$member->firstname.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('member/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('member/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('member/lib');
    }

  }


}