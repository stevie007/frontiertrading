<?php

class Payment extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    // $this->load->helper('date');
    $this->load->helper('auth');
    is_logged_in();
    $this->load->model('order_model'); 
    $this->load->model('member_model');
    $this->load->model('invoice_model');
    $this->load->model('statement_model');
    is_logged_in();
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');         
    $filter[1]['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);     
    $this->session->set_userdata('customer', 'Customer..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('payment/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('payment/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('payment_model');

    // Staff account should cannot see all
    if (pass_auth('staff') && $data['customer'] == 'Customer..' && $data['search'] == 'Search..'){
      $disable['id'] = 0;
      $data['filter'][] = $disable;
    }

    $payment_list = $this->payment_model->payment_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3));
    
    $config['total_rows'] = $data['total'] = $payment_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $payment_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'payment_list_view';  
    $data['table_title'] = 'Payment list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu4'] = 0;
    $this->load->view('includes/template', $data);	

  }



  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['customer'] = $this->session->userdata('customer');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('payment_model');
    
    $payment_list = $this->payment_model->payment_list($data['date'],
						       $data['customer'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '');
    
    $data['total'] = $payment_list->total;
    $data['query'] = $payment_list->query;
    $data['title'] = 'Payment Report';
    $data['table_title'] = 'Transaction list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('payment_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('payment/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if ($filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('payment/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($customer = $this->input->post('customer')) {      
      $this->session->set_userdata('customer', $customer);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('payment/lib');
  }
  

  function ajax_payment($id)
  { 
    $json_obj = new stdClass;

    if ($id == 'new') {
      $payment = new stdClass;
      $payment->id = 'new';
      $payment->date = date("Y-m-d H:i:s");
      $payment->subtotal 
	= $payment->tax 
	= $payment->total  = 0;
      $payment->cust_id
          = $payment->inv_id
	  = $payment->payment_meth 
	  = $payment->note
	  = $payment->cust_name = '';

      $json_obj->payment = $payment;
      
      echo json_encode($json_obj);
      
    } 
    else if ($id == 'trans')
      {	
	$id = $this->uri->segment(4);
	if ($invoice = $this->invoice_model->load_by_id($id))
	  {
	    $payment = new stdClass;
	    $payment->id = 'new';
	    $payment->date = date("Y-m-d H:i:s");
	    $payment->subtotal = $invoice->subtotal;
	    $payment->tax = $invoice->tax;
	    $payment->total  = $invoice->total;
	    $payment->cust_id = $invoice->cust_id;
	    $payment->cust_name = $invoice->cust_name;
	    $payment->payment_meth = '';
	    
	    $payment->note = '';
	    $payment->inv_id = $invoice->id;
	    
	    $json_obj->payment = $payment;
	    
	    echo json_encode($json_obj);

	  }
      } 
    else {
      
      $this->load->model('payment_model');
      if ($payment = $this->payment_model->load_by_id($id)) {
	
	$json_obj->payment = $payment;
	
	echo json_encode($json_obj);

      }      
    }

  }


  function view($id)
  {

    $this->db->where('id', $id);
    $result = $this->db->get('payment')->result();
    
    $data['payment'] = $result[0];
    $data['payment']->date = date("d/m/y", strtotime($data['payment']->date));
    $data['payment']->cust_id;
    // client
    $this->db->where('id', $data['payment']->cust_id);
    
    $result = $this->db->get('member')->result();

    $data['customer'] =  $result[0];

    $this->load->view('payment_print_view', $data);
  
  }

  
  function edit()
  {
    if (!($id = $this->uri->segment(3)))
      {
	redirect('payment/add');
      }

    $data['json_url'] = site_url('payment/ajax_payment/'.$id); 
    $data['post_url'] = site_url('payment/save'); 
    
    $data['main_content'] = 'payment_edit_view';  
    $data['frame_name'] = 'Edit Payment';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
    
  }



 function add()
  {
    $data['json_url'] = site_url('payment/ajax_payment/new'); 
    $data['post_url'] = site_url('payment/save'); 

    $data['main_content'] = 'payment_edit_view';  
    $data['frame_name'] = 'Add Payment';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	

  }


  function trans($id)
  {
    $data['json_url'] = site_url('payment/ajax_payment/trans').'/'.$id; 
    $data['cart_url'] = site_url('payment/ajax_cart'); 
    $data['post_url'] = site_url('payment/save'); 
   
    $data['main_content'] = 'payment_edit_view';  
    $data['frame_name'] = 'Add Payment';
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template_frame', $data);	
   
  }
 
 function save()
 {
   $data['session_msg'] = 'Payment Saved';
   $this->load->view('includes/session_msg', $data);
  
   $json_obj = json_decode($this->input->post('json')); 
   $this->load->model('payment_model');
   
   if ($json_obj->transaction->id == 'new') {
   
   
    //  $invoice = $this->invoice_model->load_by_id($json_obj->transaction->inv_id);
      $this->invoice_model->pay_by_id($json_obj->transaction->inv_id);

     $this->payment_model->add($json_obj->transaction); 
   } else {
     $this->payment_model->update($json_obj->transaction);
   }

   //Should update balance after save
   $this->statement_model->sync_bal_by_uid($json_obj->transaction->cust_id);
 }





  function del_msg($id){


      $msg = 'Are you really want to delete [ PAYMENT #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('payment/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('payment/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('payment/lib');	
  }

  function del($id){

    $this->load->model('payment_model');
    $this->payment_model->delete_by_id($id);
    $msg = 'Payment Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('payment/lib');
  }


}