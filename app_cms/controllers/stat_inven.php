<?php

class Stat_inven extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
    $this->load->model('order_model'); 
    $this->load->model('back_order_model'); 
    $this->load->model('member_model');
    $this->load->model('item_model');
    $this->load->model('stat_inven_model');
    check_auth('administrator'); 
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {
 
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');         
 
    $this->session->set_userdata('title', 'Title..');
    $this->session->set_userdata('sales', 'Sales..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('date_fr', strtotime("-3 month"));
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('stat_inven/lib');

  }
     

  function lib()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['title'] = $this->session->userdata('title');
    $data['sales'] = $this->session->userdata('sales');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');

    $this->load->library('pagination');
    $config['cur_tag_open'] = '&nbsp;<button disabled="disabled">';
    $config['cur_tag_close'] = '</button>';
    $config['last_link'] = 'Last';
    $config['first_link'] = 'First';
    $config['base_url'] = site_url('stat_inven/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;

    $this->load->model('stat_inven_model');
    
    $stat_inven_list = $this->stat_inven_model->get_list($data['date'],
						       $data['title'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $this->uri->segment(3),
						       $data['sales']
						       );
    
    $config['total_rows'] = $data['total'] = $stat_inven_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $stat_inven_list->query;
    
    $data['title'] = '';

    $data['main_content'] = 'stat_inven_list_view';  
    $data['table_title'] = 'Stat_Inven list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu5'] = 0;
    $this->load->view('includes/template', $data);	

  }



  function report()
  {
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['title'] = $this->session->userdata('title');
    $data['sales'] = $this->session->userdata('sales');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = '';

   

    $this->load->model('stat_inven_model');
    
    $stat_inven_list = $this->stat_inven_model->get_list($data['date'],
						       $data['title'],
						       $data['filter'], 
						       $data['search'], 
						       $data['orderby'], 
						       '', 
						       '',
						       $data['sales']);
    
    $data['total'] = $stat_inven_list->total;
    $data['query'] = $stat_inven_list->query;
    $data['title'] = 'Stat_Inven Report';
    $data['table_title'] = 'Transaction list'; 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('stat_inven_report_view', $data);	

  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('stat_inven/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('stat_inven/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($title = $this->input->post('title')) {      
      $this->session->set_userdata('title', $title);
    }
    if ($sales = $this->input->post('sales')) {      
      $this->session->set_userdata('sales', $sales);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_inven/lib');
  }
  


  function del_msg($id){
      $msg = 'Are you really want to delete [ STAT_INVEN #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('stat_inven/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('stat_inven/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('stat_inven/lib');	
  }

  function del($id){

    $this->load->model('stat_inven_model');
    $this->stat_inven_model->delete_by_id($id);
    $msg = 'Stat_Inven Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('stat_inven/lib');
  }



}