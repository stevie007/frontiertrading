<?php

class Stat_order extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');    
    $this->load->helper('list');
   
    is_logged_in();
    $this->load->model('order_model'); 
    $this->load->model('back_order_model'); 
    $this->load->model('member_model');
    $this->load->model('item_model');
    $this->load->model('stat_model');
    check_auth('administrator'); 
  }

  function test()
  {

    echo '123';die;
  }
  
  function index()
  {

 
   
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');         
     $this->session->unset_userdata('search');

    $this->session->set_userdata('sales', 'Sales..');
    $this->session->set_userdata('search', 'Search..');
    $this->session->set_userdata('category', $this->cart->contents());
    $this->session->set_userdata('date_fr', strtotime("2012-11-01")); //strtotime("-3 month")
    $this->session->set_userdata('date_to', time());    
    $this->session->set_userdata('per_page', 25);
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    

    redirect('stat_order/lib');

  }
     

  function lib()
  {            $this->load->library('table');
    $data['category'] = $this->session->userdata('category');

    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['title'] = $this->session->userdata('title');
    $data['sales'] = $this->session->userdata('sales');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');
 
  
    $data['table1'] = $this->stat_model->ordered_by_month($data['date'], $data['category']);
 
     
 
    $data['ordered'] = $this->stat_model->ordered($data['date'], $data['category'] );
 
     
  
    $data['title'] = '';

    $data['main_content'] = 'stat/stat_ordered_view';  
    $data['table_title'] = 'stat list'; 

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu5'] = 0;
    $this->load->view('includes/template', $data);	

  }


  function print_view()
  {            $this->load->library('table');
    $data['category'] = $this->session->userdata('category');

    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter'); 
    $data['title'] = $this->session->userdata('title');
    $data['sales'] = $this->session->userdata('sales');
    $data['search'] = $this->session->userdata('search');
    $data['date']['fr'] = $this->session->userdata('date_fr');
    $data['date']['to'] = $this->session->userdata('date_to');
    $data['per_page'] = $this->session->userdata('per_page');
 
  
    $data['table1'] = $this->stat_model->ordered_by_month($data['date'], $data['category']);
 
     
 
    $data['ordered'] = $this->stat_model->ordered($data['date'], $data['category'] );
 
     
 
    $this->load->view('stat/stat_ordered_print_view', $data);	

  }

 

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('stat_order/lib');
  }
  

  function filter() 
  { 

    if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if ($flr_num = $this->uri->segment(3)) 
      {	
	
	$key = $this->uri->segment(4);
	$value = $this->uri->segment(5);
	$filter = $this->session->userdata('filter');
	if (isset($filter[$flr_num][$key]) && $filter[$flr_num][$key] == $value):
	  unset($filter[$flr_num][$key]);
	else:
	  $filter[$flr_num][$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('stat_order/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    if ($title = $this->input->post('title')) {      
      $this->session->set_userdata('title', $title);
    }
    if ($sales = $this->input->post('sales')) {      
      $this->session->set_userdata('sales', $sales);
    }
    if ($from = $this->input->post('from')) {      
      $this->session->set_userdata('date_fr',
				   strtotime(str_replace('/', '-', $from)));
    }
    if ($to = $this->input->post('to')) {      
      $this->session->set_userdata('date_to', 
				   strtotime(str_replace('/', '-', $to)));
    }
    redirect('stat_order/lib');
  }
  


  function cate()
  {
    if ($this->uri->segment(4)) 
      {
	$category = urldecode($this->uri->segment(3).'/'.$this->uri->segment(4));
      }
    else 
      {
	$category = urldecode($this->uri->segment(3));
      }

    $this->session->set_userdata('category', $category);
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    redirect('stat_order/lib');
  }
  function del_msg($id){
      $msg = 'Are you really want to delete [ stat #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('stat_order/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('stat_order/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('stat_order/lib');	
  }

  function del($id){

    $this->load->model('stat_model');
    $this->stat_model->delete_by_id($id);
    $msg = 'stat Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('stat_order/lib');
  }



}