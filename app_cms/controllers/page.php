<?php

class Page extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
    check_auth('administrator'); 
  }

	
  function edit()
  {         
    $id = $this->uri->segment(3);

    $this->load->model('page_model');
    
    if ($data['page'] = $this->page_model->load_page($id)) {

    

    $data['title'] = 'Edit: '.$this->config->item('page'.$id,'cf_feslen');
 
    $data['main_content'] = 'page_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
      $data['menu1'] = 0;
    $this->load->view('includes/template', $data);	
   }

  }	


  function page_update()
  {
 	$id = $this->input->post('id');
        $this->load->model('page_model');		

	$this->page_model->page_update($id);
	  
	$msg = 'Page updated successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('page/edit/'.$id);

  }	
  
 

}	

