<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('member_model');
    $this->load->helper('auth');
    is_logged_in();
  }

	
  function lib()
  {         
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');
    
    $config['base_url'] = site_url('member/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;
    
    $member_list = $this->member_model->member_list(
						    $data['search'], 
						    $data['filter'], 
						    $data['orderby'], 
						    $config['per_page'], 
						    $this->uri->segment(3)
						    );
    
    $config['total_rows'] = $data['total'] = $member_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $member_list->query;
    
    $data['title'] = 'Member Database';
    $data['table_title'] = 'Member List';
    $data['main_content'] = 'member_lib_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }	


  function index()
  {
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('member/lib');
  }	
  

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('member/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('member/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('member/lib');
  }
  

  function add()
  {  
    $member = new stdClass;
    $member->id 
      = $member->firstname 
      = $member->lastname 
      = $member->email 
      = $member->phone 
      = $member->mobile 
      = $member->fax 
      = $member->address
      = $member->suburb  
      = $member->city 
      = $member->postcode 
      = $member->password 
      = $member->authority 
      = $member->resetcode 
      = '';

    $data['member'] = $member;
    
    $data['title'] = 'Add Member';
    $data['table_title'] = 'Member Detail';
    $data['main_content'] = 'member_edit_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
    
  }

	
  function edit()
  {  

    if (!($id = $this->uri->segment(3)))
      {
	redirect('member/add');
      }
      
    if ($data['member'] = $this->member_model->load_by_id($id)) {
	
      $data['title'] = 'Edit Member';
      $data['table_title'] = 'Member Detail';
      $data['main_content'] = 'member_edit_view';  
	
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
      $this->load->view('includes/template', $data);	
    }
    
    
    
  }	
  
  function member_submit()
  { 
    if($id = $this->input->post('id'))
      {
	$this->member_model->update($id);
	$msg = 'Member updated successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('member/edit/'.$id);
      }
    else
      {
	$this->member_model->add();
	$msg = 'Member added successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('member');
      }
  }

  function del()
  { 

    if ($id = $this->uri->segment(3))
      {
	$this->member_model->del($id);
	$msg = 'Member deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

    redirect('member');
    
  }




  function del_msg($id){
    
    $this->load->model('member_model');
    if ($member = $this->member_model->load_by_id($id)) {
      
      $msg = 'Are you really want to delete [ '.$member->firstname.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('member/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('member/lib').'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
      redirect('member/lib');	      
    } 

  }


}
