<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  }

  function index()
  {
    //session_start();session_destroy();
    $data['title'] = 'Administration Panel';

    $data['main_content'] = 'service_info';  
    
    error_reporting(0);
    $xmlremote = simplexml_load_file('http://3aweb.co.nz/support/xml/support.xml');
    $xmllocal = <<<XML
      <support>
      <company>3A Web Solution</company>
      <url>http://3aweb.co.nz/</url>
      <email>it@3a.co.nz</email>
      <telephone>09 975 1800</telephone>
      <address>Level 1, 485 Khyber Pass Rd, Newmarket, Auckland</address>
      <workinghours>9:00am-6pm (Monday - Friday)</workinghours>
      <message>3aweb.co.nz</message>
      </support>
XML;
    $data['xml'] = $xmlremote ? $xmlremote : simplexml_load_string($xmllocal);


    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }
	
 
}

/* End of file cms.php */
/* Location: ./application/controllers/cms.php */