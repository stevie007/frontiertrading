<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->model('page_model');
    is_logged_in();
  }
  
	
  function edit()
  {         
    $id = $this->uri->segment(3);
    
    if ($data['page'] = $this->page_model->load_by_id($id)) {

      $data['title'] = 'Edit: '.$this->config->item('page'.$id,'cf_feslen');
 
      $data['main_content'] = 'page_view';  
    
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
      $this->load->view('includes/template', $data);	
    }

  }	


  function page_update()
  {
    $id = $this->input->post('id');

    $this->page_model->update($id);
	  
    $msg = 'Page updated successfully.';
    $this->session->set_flashdata('msg', $msg);
    redirect('page/edit/'.$id);

  }	
  
 

}	


