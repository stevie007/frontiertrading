<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->model('product_model');
    is_logged_in();
    session_start();
  }
 
  function lib()
  {         
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');
    
    $config['base_url'] = site_url('product/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;

    
    $product_list = $this->product_model->product_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $this->uri->segment(3)
					      );
    
    $config['total_rows'] = $data['total'] = $product_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $product_list->query;
    
    $data['title'] = 'Product Database';
    $data['table_title'] = 'Product List';
    $data['main_content'] = 'product_lib_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }	


  function index()
  {
    $_SESSION['KCFINDER']['disabled'] = false;
    $_SESSION['KCFINDER']['uploadURL'] = "../../../uploads"; 	
    /* kcfinder config - from kcfinder's root folder */
    
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('product/lib');
  }	
  

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('product/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('product/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('product/lib');
  }
  

  function add()
  {  
    $product = new stdClass;
    $product->id 
      = $product->category 
      = $product->title 
      = $product->image_small 
      = $product->image_big 
      = $product->detail 
      = $product->detail_member 
      = $product->boolean1 
      = $product->boolean2 
      = '';

    $data['product'] = $product;
    
    $data['title'] = 'Add Product';
    $data['table_title'] = 'Product Detail';
    $data['main_content'] = 'product_edit_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
    
  }

	
  function edit()
  {  

    if (!($id = $this->uri->segment(3)))
      {
	redirect('product/add');
      }
      
    if ($data['product'] = $this->product_model->load_by_id($id)) {
	
      $data['title'] = 'Edit Product';
      $data['table_title'] = 'Product Detail';
      $data['main_content'] = 'product_edit_view';  
	
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
      $this->load->view('includes/template', $data);	
    }
        
  }	
  
  function product_submit()
  { 
    if($id = $this->input->post('id'))
      {
	$this->product_model->update($id);
	$msg = 'Product updated successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('product/edit/'.$id);
      }
    else
      {
	$this->product_model->add();
	$msg = 'Product added successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('product');
      }
  }

  function del()
  { 

    if ($id = $this->uri->segment(3))
      {
	$this->product_model->del($id);
	$msg = 'Product deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

    redirect('product');
    
  }

 
  /* 
  function tran()
  {
    $result = $this->db->get('products')->result();

    foreach ($result as $row){
      echo $row->title;
      echo '<br />';
      $row->title = str_replace('title','product',$row->title);
      $row->url_title = url_title($row->title, 'dash', TRUE); 

      $this->db->where('id', $row->id);
      
      $this->db->update('products', $row);
    } 
  }
  */


}
