<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->model('category_model');
    is_logged_in();
  }
 
  function cate_names(){
    $term = $this->input->post('term'); 
    $data = $this->category_model->load_category_names($term);    
    echo json_encode($data);
  }

  function parent_cate_names(){
    $term = $this->input->post('term'); 
    $data = $this->category_model->load_parent_names($term);      
    array_push($data, '(none)');
    echo json_encode($data);
  }
  
}
/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */