<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->model('category_model');
    is_logged_in();
    session_start();
  }
 
  function lib()
  {         
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');
    
    $config['base_url'] = site_url('category/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;

    
    $category_list = $this->category_model->category_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $this->uri->segment(3)
					      );
    
    $config['total_rows'] = $data['total'] = $category_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $category_list->query;
    
    $data['title'] = 'Category Database';
    $data['table_title'] = 'Category List';
    $data['main_content'] = 'category_lib_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }	


  function index()
  {
    $_SESSION['KCFINDER']['disabled'] = false;
    $_SESSION['KCFINDER']['uploadURL'] = "../../../uploads"; 	
    /* kcfinder config - from kcfinder's root folder */
    
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('category/lib');
  }	
  

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('category/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('category/lib');
  }

  function filter_main_cates() 
  {
    $filter = $this->session->userdata('filter');
    
    if (isset($filter['parent'])):
      unset($filter['parent']);
    else:
      $filter['parent'] = '(none)';
    endif;
    
    $this->session->set_userdata('filter', $filter);	

    redirect('category/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('category/lib');
  }
  

  function add()
  {  
    $category = new stdClass;
    $category->id 
      = $category->name
      = '';
    $category->order = 0;
    $category->parent = '(none)';

    $data['category'] = $category;
    
    $data['title'] = 'Add Category';
    $data['table_title'] = 'Category Detail';
    $data['main_content'] = 'category_edit_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
    
  }

	
  function edit()
  {  

    if (!($id = $this->uri->segment(3)))
      {
	redirect('category/add');
      }

    $this->load->model('category_model');
      
    if ($data['category'] = $this->category_model->load_by_id($id)) {
	
      $data['title'] = 'Edit Category';
      $data['table_title'] = 'Category Detail';
      $data['main_content'] = 'category_edit_view';  
	
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
      $this->load->view('includes/template', $data);	
    }
    
    
    
  }	
  
  function category_submit()
  { 
    $this->load->model('category_model');	
    if($id = $this->input->post('id'))
      {
	$this->category_model->update($id);
	$msg = 'Category updated successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('category/edit/'.$id);
      }
    else
      {
	$this->category_model->add();
	$msg = 'Category added successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('category');
      }
  }

  function del()
  { 

    if ($id = $this->uri->segment(3))
      {
	$this->category_model->category_del($id);
	$msg = 'Category deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

    redirect('category');
    
  }

 
  /* 
  function tran()
  {
    $result = $this->db->get('categorys')->result();

    foreach ($result as $row){
      echo $row->title;
      echo '<br />';
      $row->title = str_replace('title','category',$row->title);
      $row->url_title = url_title($row->title, 'dash', TRUE); 

      $this->db->where('id', $row->id);
      
      $this->db->update('categorys', $row);
    } 
  }
  */


}

/* End of file category.php */
/* Location: ./application/controllers/category.php */