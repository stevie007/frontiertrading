<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class User extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  }
 
  function index()
  {
    $data['title'] = 'Administration Panel';

    $data['main_content'] = 'form_User';  

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);		
  }
  

  function change_password()
  {
    $data['title'] = 'Administration Panel';
    $data['main_content'] = 'form_change_password';  
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);		
  }

 
  
  function validate_change_password()
  {
    $this->load->library('form_validation');
		
    $this->form_validation->set_rules('password_old', 'Current password', 'trim|required|min_length[4]|max_length[32]');
    $this->form_validation->set_rules('password_new', 'New password', 'trim|required|min_length[4]|max_length[32]');
    $this->form_validation->set_rules('password_rep', 'Password Confirmation', 'trim|required|matches[password_new]');
    
    
    if(!$this->form_validation->run())
      {
	$msg = validation_errors();
	$this->session->set_flashdata('msg', $msg);
	redirect('user/change_password');
      }
    
    else
      {			
	
	$this->load->model('user_model');		

	if($this->user_model->change_password())
	  {
	    $msg = 'Password changed successfully.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('user/change_password');
	  }
	else
	  {
	    $msg = 'Password could not be changed.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('user/change_password');
	  }

      }
  }



}





