<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_order extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->model('site_order_model');
    $this->load->model('member_model');
    is_logged_in();
    session_start();
  }
 
  function lib()
  {       
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');
    
    $config['base_url'] = site_url('site_order/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;
    
    $site_order_list = $this->site_order_model->site_order_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $this->uri->segment(3)
					      );
     
    $config['total_rows'] = $data['total'] = $site_order_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $site_order_list->query;
    
    $data['title'] = 'Database';
    $data['table_title'] = 'Site_Order List';
    $data['main_content'] = 'site_order_lib_view';  
 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }	


  function index()
  {
    $_SESSION['KCFINDER']['disabled'] = false;
    $_SESSION['KCFINDER']['uploadURL'] = "../../../uploads"; 	
    /* kcfinder config - from kcfinder's root folder */
    
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('site_order/lib');
  }	
  

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('site_order/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('site_order/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('site_order/lib');
  }
  

  function add()
  {  
    $site_order = new stdClass;
    $site_order->id 
      = $site_order->cust_id 
      = $site_order->date 
      = $site_order->total 
      = $site_order->content
      = '';
    $site_order->date = date("Y-m-d H:i:s");

    $data['site_order'] = $site_order;
    
    $data['title'] = 'Add Order';
    $data['table_title'] = 'Order Detail';
    $data['main_content'] = 'site_order_edit_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	
    
  }

	
  function edit()
  {  

    if (!($id = $this->uri->segment(3)))
      {
	redirect('site_order/add');
      }
      
    if ($data['site_order'] = $this->site_order_model->load_by_id($id)) {
	
      $data['title'] = 'Edit Order';
      $data['table_title'] = 'Order Detail';
      $data['main_content'] = 'site_order_edit_view';  
	
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
      $this->load->view('includes/template', $data);	
    }
    
    
    
  }	
  

  function view($id){  
    
    if ($data['site_order'] = $this->site_order_model->load_by_id($id))
      {	
	$data['site_order']->content 
	  = json_decode($data['site_order']->content); 	
	
	$data['customer'] = $this->member_model->load_by_id($id);
	 
	$data['main_content'] = 'print_html/site_order_print_view';  
	$this->load->view('includes/template_frame', $data);
      }
  }


  function pdf($id)
  {
    $this->load->helper(array('dompdf', 'file'));

    if ($data['site_order'] = $this->site_order_model->load_by_id($id))
      {	
	$data['site_order']->content 
	  = json_decode($data['site_order']->content); 	
	
	$data['customer'] = $this->member_model->load_by_id($id);
	
	$data['main_content'] = 'print_html/site_order_print_view';  

	$html = $this->load->view('includes/template_pdf', $data, true);
	//$this->load->view('print_html/site_order_print_view', $data, true); 

	pdf_create($html, 'order#'.$data['site_order']->id);
      }
  
  }  


  function site_order_submit()
  { 	
    if($id = $this->input->post('id'))
      {
	$this->site_order_model->update($id);
	$msg = 'Order updated successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('site_order/edit/'.$id);
      }
    else
      {
	$this->site_order_model->add();
	$msg = 'Order added successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect('site_order');
      }
  }

  function del()
  { 

    if ($id = $this->uri->segment(3))
      {
	$this->site_order_model->del($id);
	$msg = 'Order deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

    redirect('site_order');
    
  }


}
