<?php

class Func extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  }

  function index()
  {
 
    $data['title'] = 'Any functions can be made by your request.';

    $data['main_content'] = 'function_info';  
    
    error_reporting(0);
    $xmlremote = simplexml_load_file('http://3aweb.co.nz/support/xml/support.xml');
    $xmllocal = <<<XML
<?xml version="1.0" encoding="ISO-8859-1"?>
<support>
  <company>3A web solution</company>
  <url>http://3aweb.co.nz/</url>
  <email>it@3a.co.nz</email>
  <telephone>09 975 1800</telephone>
  <address>Level 1, 485 Khyber Pass Rd, Newmarket, Auckland</address>
  <workinghours>9:00am-6pm (Monday - Friday)</workinghours>
  <message>http://3aweb.co.nz/</message>
</support>
XML;
    $data['xml'] = $xmlremote ? $xmlremote : simplexml_load_string($xmllocal);


    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $this->load->view('includes/template', $data);	

  }
	
 
}
