<?php

class Quoting extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('list');
    is_logged_in();
    session_start();
  }
 
  function lib()
  {         
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');
    $data['category'] = $this->session->userdata('category');
    $data['per_page'] = $this->session->userdata('per_page');
    $data['cart'] = $this->cart;
    $this->load->library('pagination');
    
    $config['base_url'] = site_url('quoting/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['per_page'];
    $config['num_links'] = 5;
    $data['offset'] = $this->uri->segment(3, 0);

    $this->load->model('quoting_model');
    
    $quoting_list = $this->quoting_model->quoting_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $data['offset'],
					      $data['category']
					      );
    
    $config['total_rows'] = $data['total'] = $quoting_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $quoting_list->query;
        $data['json_cart'] = json_encode($this->cart->contents());
    $data['title'] = '';
    $data['table_title'] = 'Quoting List';
    $data['main_content'] = 'quoting_lib_view';  
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu4'] = 0;
    $this->load->view('includes/template', $data);	

  }	


  function index()
  {
    $_SESSION['KCFINDER']['disabled'] = false;
    $_SESSION['KCFINDER']['uploadURL'] = "../../../uploads"; 	
    /* kcfinder config - from kcfinder's root folder */
    
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category'); 

    $filter['deleted'] = 0;
   

    $this->session->set_userdata('filter', $filter);	
    
    $orderby['order'] = 'title';
    $orderby['sort'] = 'asc';     
    $this->session->set_userdata('per_page', 50);

    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('quoting/lib');
  }	
  

  function cate()
  {
    if ($this->uri->segment(4)) 
      {
	$category = urldecode($this->uri->segment(3).'/'.$this->uri->segment(4));
      }
    else 
      {
	$category = urldecode($this->uri->segment(3));
      }

    $this->session->set_userdata('category', $category);
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    redirect('quoting/lib');
  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    redirect('quoting/lib');
  }
  

  function filter() 
  {
     if ($this->uri->segment(3) == 'per_page'
	&& $per_page = $this->uri->segment(4)) {    
      if ($this->session->userdata('per_page') == 25)
	$this->session->set_userdata('per_page', $per_page);
      else 
	$this->session->set_userdata('per_page', 25);
    }    
    else if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('quoting/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('quoting/lib');
  }
  
  function empty_cart() {
  	  $this->cart->destroy();
    redirect('quoting');
  }
  
  function submit() {
		
    
    $insert = array(
		    'id' => $this->input->post('id'),
		    'qty' => $this->input->post('qty'),
		    'price' => $this->input->post('price'),
		    'name' => $this->input->post('name'),
		    'discount' => 0
		    );
	 
		
    $this->cart->insert($insert);

    $data['item'] = $this->cart->total_items();
        $data['contents'] = $this->cart->contents();
    $data['total'] = $this->cart->total();
    echo json_encode($data);
   // redirect($this->input->post('uri').'#cart');
		
  }
  
  function cart() {
	
	  
    echo json_encode($this->cart->contents());
 
		
  }
}
