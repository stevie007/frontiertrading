<?php

class Item extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('list');
    $this->load->model('stat_inven_model');
    is_logged_in();
    session_start();
  }
 
  function lib()
  {         
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');
    $data['category'] = $this->session->userdata('category');

    $this->load->library('pagination');
    
    $config['base_url'] = site_url('item/lib/');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 25;
    $config['num_links'] = 5;
    $data['offset'] = $this->uri->segment(3, 0);

    $this->load->model('item_model');
    
    $item_list = $this->item_model->item_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $data['offset'],
					      $data['category']
					      );
    
    $config['total_rows'] = $data['total'] = $item_list->total;
    
    $this->pagination->initialize($config);

    $data['query'] = $item_list->query;
    
    $data['title'] = '';
    $data['table_title'] = 'Item List';
    $data['main_content'] = 'item_lib_view';  
 
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['cf_feslen'] = $this->config->item('cf_feslen');
    $data['menu2'] = 0;
    $this->load->view('includes/template', $data);	

  }	


  function index()
  {
    $_SESSION['KCFINDER']['disabled'] = false;
    $_SESSION['KCFINDER']['uploadURL'] = "../../../uploads"; 	
    /* kcfinder config - from kcfinder's root folder */
    
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    
    $filter['deleted'] = 0;

    $this->session->set_userdata('filter', $filter);	
	
	
    $this->session->unset_userdata('search');
        $this->session->unset_userdata('category');
 
    $orderby['order'] = 'title';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('item/lib');
  }	
  
  function index_trash()
  {
    $_SESSION['KCFINDER']['disabled'] = false;
    $_SESSION['KCFINDER']['uploadURL'] = "../../../uploads"; 	
    /* kcfinder config - from kcfinder's root folder */
    
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    
    $filter['deleted'] = 1;

    $this->session->set_userdata('filter', $filter);	
	
	
    $this->session->unset_userdata('search');
        $this->session->unset_userdata('category');
 
    $orderby['order'] = 'title';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->keep_flashdata('msg');
    redirect('item/lib');
  }	
  
  function cate()
  {
    if ($this->uri->segment(4)) 
      {
	$category = urldecode($this->uri->segment(3).'/'.$this->uri->segment(4));
      }
    else 
      {
	$category = urldecode($this->uri->segment(3));
      }

    $this->session->set_userdata('category', $category);
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
    $filter['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);	

    redirect('item/lib');
  }

  function orderby() 
  {
    if ($order = $this->uri->segment(3))
      { 
	$orderby = $this->session->userdata('orderby');
	$sort = ($orderby['sort'] == 'asc') ? 'desc' : 'asc';
	$orderby['order'] = $order;
	$orderby['sort'] = $sort; 
	$this->session->set_userdata('orderby', $orderby);
      }
    $filter['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);	
    redirect('item/lib');
  }
  

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    $filter['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);	
    redirect('item/lib');
  }
  

  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    $filter['deleted'] = 0;
    $this->session->set_userdata('filter', $filter);	
    redirect('item/lib');
  }
  

  function add()
  {  
    $item = new stdClass;
    $item->id 
      = $item->category 
      = $item->title 
      = $item->image1
      = $item->detail       
      = '';
    $item->special 
      = $item->price 
      = $item->price_s 
      = $item->new = 0;
    $item->online = 1;
    $item->qty = 1;
    
    $this->load->model('item_model');	
    $this->item_model->item_add($item);
    redirect('item/lib');
  }

	
  function edit()
  {  

    if (!($id = $this->uri->segment(3)))
      {
	redirect('item/add');
      }

    $this->load->model('item_model');
      
    if ($data['item'] = $this->item_model->load_item($id)) {
	
      $data['title'] = 'Edit Item';
      $data['table_title'] = 'Item Detail';
      $data['main_content'] = 'item_edit_view';  
	
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['cf_feslen'] = $this->config->item('cf_feslen');
      $data['menu2'] = 0;
      $this->load->view('includes/template', $data);	
    }
    
    
    
  }	
  
  function item_submit()
  {
    $this->load->model('item_model');	
    if($id = $this->input->post('id'))
      {
	$qty = $this->input->post('qty');
	// static
	$this->stat_inven_model->adjust($id, $qty);
	// stock
	$this->item_model->item_update($id);
	//	echo	$msg = 'Item updated successfully.';
	$item = $this->item_model->load_item($id);
	echo json_encode($item);
      }
    else
      {
	$this->item_model->item_add();
	echo	$msg = 'Item added successfully.';
//	$this->session->set_flashdata('msg', $msg);
	//	redirect('item');
      }
    
  }
/*
  function del()
  { 

    if ($id = $this->uri->segment(3))
      {
	$this->load->model('item_model');	
	$this->item_model->item_del($id);
	$msg = 'Item deleted successfully.';
	$this->session->set_flashdata('msg', $msg);
      }

    redirect('item/lib/'.$this->uri->segment(4));
    
  }
*/

  function del_msg($id){
      $msg = 'Are you really want to delete [ item #'.$id.' ] ?&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('item/del/'.$id).'" class="black">Yes</a>';
      $msg .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
      $msg .= '<a href="'.site_url('item/lib/'.$this->uri->segment(4)).'" class="black">No</a>';
      $this->session->set_flashdata('msg', $msg);
    redirect('item/lib/'.$this->uri->segment(4));
  }

  function del($id){
 

   $this->load->model('item_model');	
	$this->item_model->del($id);
    $msg = 'Item Deleted';
    $this->session->set_flashdata('msg', $msg);
    redirect('item/lib/'.$this->uri->segment(4));
  }

 
  /* 
  function tran()
  {
    $result = $this->db->get('items')->result();

    foreach ($result as $row){
      echo $row->title;
      echo '<br />';
      $row->title = str_replace('title','item',$row->title);
      $row->url_title = url_title($row->title, 'dash', TRUE); 

      $this->db->where('id', $row->id);
      
      $this->db->update('items', $row);
    } 
  }
  */


}