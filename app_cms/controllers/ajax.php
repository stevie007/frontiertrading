<?php

class Ajax extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    is_logged_in();
  }
 
  function cate_names(){
    $term = $this->input->post('term'); 
    $this->load->model('category_model');
    $data = $this->category_model->load_category_names($term);    
    echo json_encode($data);
  }

  function parent_cate_names(){
    $term = $this->input->post('term'); 
    $this->load->model('category_model');
    $data = $this->category_model->load_parent_names($term);    
    echo json_encode($data);
  }
  
  function cust_names(){
    $term = $this->input->post('term'); 
    $this->load->model('member_model');
    $data = $this->member_model->load_names($term);
    echo json_encode($data);
  }
  
  function sales_names(){
    $term = $this->input->post('term'); 
    $this->load->model('sales_model');
    $data = $this->sales_model->load_names($term);
    echo json_encode($data);
  }
  
  function cust_id_names(){
    $term = $this->input->post('term'); 
    $this->load->model('member_model');
    $data = $this->member_model->load_id_names($term);
    echo json_encode($data);
  }

  function item_names(){
    $term = $this->input->post('term'); 
    $this->load->model('item_model');
    $data = $this->item_model->load_names($term);    
    echo json_encode($data);
  }

}
