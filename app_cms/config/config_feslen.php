<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['cf_feslen']['co_name'] = 'Frontier Trading LTD.';
$config['cf_feslen']['main_css'] = '';
$config['cf_feslen']['page1'] = 'Home Page';
$config['cf_feslen']['page2'] = 'Customer Registration';
$config['cf_feslen']['page3'] = '';
$config['cf_feslen']['page4'] = 'Contact';
$config['cf_feslen']['page5'] = 'Terms';
$config['cf_feslen']['page6'] = '';
$config['cf_feslen']['page7'] = '';
$config['cf_feslen']['page8'] = '';
$config['cf_feslen']['page9'] = '';
$config['cf_feslen']['cms_name'] = 'Feslen BMS <i>2012</i>';
$config['cf_feslen']['cms_version'] = 'build 20111005';


$config['site']['email'] = 'sales@frontiertrading.co.nz';
//$config['site']['email'] = 'vick@3a.co.nz';
$config['site']['name'] = 'Frontier International Trading Ltd.';
$config['site']['sender'] = 'sales@frontiertrading.co.nz';