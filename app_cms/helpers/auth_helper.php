<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Form Declaration
 *
 * Creates the opening portion of the form.
 *
 * @access	public
 * @param	string	the URI segments of the form destination
 * @param	array	a key/value pair of attributes
 * @param	array	a key/value pair hidden data
 * @return	string
 */	
if ( ! function_exists('is_logged_in'))
{
	function is_logged_in()
	{
		$CI =& get_instance();

		$is_logged_in = $CI->session->userdata('is_logged_in');

		if(!isset($is_logged_in) || !$is_logged_in )
		{
			redirect('login');	
		}		
	}
}



if ( ! function_exists('check_auth'))
{

  function check_auth($auth, $redirect = TRUE){
    $CI =& get_instance();
    
    $pos = strpos($CI->session->userdata('authority'), $auth);
    
    if($pos === false) {

      if ($redirect) {
      $msg = 'Authorisation rejected.';
      $CI->session->set_flashdata('msg', $msg);
      redirect('');
      }
      else
      {
      echo $msg = 'Authorisation rejected.'; die;
      }
    }
    
  }
}

if ( ! function_exists('pass_auth'))
{

  function pass_auth($auth){
    $CI =& get_instance();
    
    $pos = strpos($CI->session->userdata('authority'), $auth);
    
    if($pos === false) {

      return FALSE;
    }
    else {
	return TRUE;
    }
    
  }
}