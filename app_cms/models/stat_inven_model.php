<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class stat_inven_model extends CI_Model 
{

  var $db_name;
  
  function __construct()
  {
    parent::__construct();
    $this->db_name = 'stat_inventory';
  }
  
  

  function get_list($date = "", $title = "", 
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset, $sales = "")
  {
    
    $list = new stdClass;

    $this->_list_sql($date, $title, $search, $filter, $orderby, $sales);
    
    $list->total = $this->db->get($this->db_name)->num_rows();

    $this->_list_sql($date, $title, $search, $filter, $orderby, $sales);
   
    $list->query = $this->db->get($this->db_name, $per_page, $offset);
    //    echo  $this->db->last_query();
    
    return $list;


  }
	

  function _list_sql($date = "", $title = "", 
			     $search = "", $filter = "", $orderby = "", $sales = ""){
   
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    
    if ($title && $title != 'Title..')
      {
	$this->db->where('title', $title);  
      }

    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `title` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 

  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }

  function adjust($id, $qty){
    $this->db->where('id', $id);
    $query = $this->db->get('items');
    if ($query->num_rows() > 0 )
      { 
	$result = $query->result();
	$item = $result[0];  
	if ($qty != $item->qty)
	  {	   
	    $data = new stdClass;       
	    $data->id = NULL;
	    $data->title = $item->title;
	    $data->date = date("Y-m-d H:i:s");
	    $data->adjust = $qty - $item->qty;
	    $data->add = ($data->adjust > 0) ? 1 : 0;
	    $this->db->insert($this->db_name, $data); 
	  }
      }
  }

  
  function update($id)
  {    

    if($images = $this->_pic_upload())
      {
	$data['image_small'] = $images->small;
        $data['image_big'] = $images->big;
	$this->_unlink_files($id);
      }

    $data['boolean1'] = ($this->input->post('boolean1') == 'accept') ? 1 : 0;
    $data['boolean2'] = ($this->input->post('boolean2') == 'accept') ? 1 : 0;

    if($title = $this->input->post('title'))
      {
	$data['url_title'] = url_title($title, 'dash', TRUE);
      }

    $cols = array('title', 'category', 'detail', 'detail_member');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
    $this->db->where('id', $id);
    $this->db->update($this->db_name, $data);
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;

  }

  
  function add() {

    if($images = $this->_pic_upload())
      {
	$data['image_small'] = $images->small;
        $data['image_big'] = $images->big;
      }

    $data['boolean1'] = ($this->input->post('boolean1') == 'accept') ? 1 : 0;
    $data['boolean2'] = ($this->input->post('boolean2') == 'accept') ? 1 : 0;

    if($title = $this->input->post('title'))
      {
	$data['url_title'] = url_title($title, 'dash', TRUE);
      }

    $cols = array('title','category','detail','detail_member');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);

    $this->db->insert($this->db_name, $data); 
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
  }



  function del($id){
    $this->_unlink_files($id);
    $this->db->where('id', $id);
    $this->db->delete($this->db_name);
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
  }
  
  
 
}
 