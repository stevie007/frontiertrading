<?php

class Customer_model extends CI_Model {

  var $customer;
 
  function __construct() {
    parent::__construct();
    $this->customer = new stdClass;
    $this->customer->id = '';
    $this->customer->name 
      = $this->customer->address 
      = $this->customer->city 
      = $this->customer->ship_name 
      = $this->customer->ship_address 
      = $this->customer->ship_city
      = $this->customer->pricing
      = $this->customer->discount 
      = $this->customer->phone 
      = $this->customer->cellphone 
      = $this->customer->fax 
      = $this->customer->email 
      = $this->customer->url
      = $this->customer->contact = '';
  }


  function load_names($term)
  {
    $this->db->select('name');
    
    $this->db->like('name', $term);
    $result = $this->db->get('member')->result();
		
    $array = array();
    foreach($result as $row) array_push($array, $row->name);
 
    return $array;
    
  }

  function load_customer($id)
  {
    $this->db->where('id', $id);
    if ($result = $this->db->get('member')->result()) {
      $customer =  $result[0];
    } else {
      $customer = $this->customer;
    }	
    return $customer;
  }
  
  function set_balance($id, $bal){
    $this->db->where('id', $id);
    $this->db->set('balance', $bal);
    $this->db->update('member'); 
  }

  function load_by_name($name)
  {
    $this->db->where('name', $name);
    if ($result = $this->db->get('member')->result()) {
      $customer = $result[0];
    } else { 
      $customer = $this->customer;
    }
    return $customer;
    
  }

  function customer_list($cust_name = "", 
			 $filter = "", $search = "", 
			 $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_customer_list_sql( $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('member')->num_rows();
    
    $this->_customer_list_sql($cust_name, $search, $filter, $orderby);
    
    $list->query = $this->db->get('member', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }

  function _customer_list_sql($cust_name = "", 
			      $search = "", $filter = "", $orderby = ""){
    
    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('name', $cust_name);  
      }
   
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']);

      }
        
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `name` LIKE '%$search%' OR `address` LIKE '%$search%' OR `city` LIKE '%$search%' OR `ship_name` LIKE '%$search%' OR `ship_address` LIKE '%$search%' OR `ship_city` LIKE '%$search%' OR `pricing` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `phone` LIKE '%$search%' OR `cellphone` LIKE '%$search%' OR `fax` LIKE '%$search%' OR `email` LIKE '%$search%' OR `url` LIKE '%$search%' OR `contact` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 
  function customer_update($customer)
  {
    // $this->db->select('name');
    $this->db->where('id', $customer->id);
    if ($result = $this->db->get('member')->result()) {
      $prev_data = $result[0];
     
      if($prev_data->name != $customer->name){

	$this->db->select('branch');
	$query = $this->db->get('co_branches');
	foreach ($query->result() as $row)
	  {
	    $branch = $row->branch;		    
	    $this->db->where('cust_name', $prev_data->name);
	    $invo_query = $this->db->get($branch.'_invoices');
	    foreach ($invo_query->result() as $invoice) {
	      //change cust name in invoices
	      $invoice->cust_name = $customer->name;
 
	      $this->db->where('id', $invoice->id);
	      $this->db->update($branch.'_invoices', $invoice);  
	      
	    }

	  }	
      }
      
      $this->db->where('id', $customer->id);
      $this->db->update('member', $customer);
      
    }
   
  }


  function customer_add($customer)
  {
    $customer->id = NULL;
    
    $this->db->insert('member', $customer);
    
  }
  

  function customer_del($id, $receiver)
  {
    $this->db->where('id', $id);

    if ($result = $this->db->get('member')->result()) {
      $target = $result[0];

      if ($receiver != 'Customer..') {
	$this->db->select('branch');
	$query = $this->db->get('co_branches');

	foreach ($query->result() as $row)
	  {
	    $branch = $row->branch;		    
	    $this->db->where('cust_name', $target->name);
	    $invo_query = $this->db->get($branch.'_invoices');

	    foreach ($invo_query->result() as $invoice) {
	      $invoice->cust_name = $receiver;
	      $this->db->where('id', $invoice->id);
	      $this->db->update($branch.'_invoices', $invoice);  	      
	    }
	    
	  }	
      }
      
      $this->db->where('id', $id);
      $this->db->delete('member'); 
      
    }
   
  }


  }
