<?php

class Back_Order_model extends CI_Model {

  var $branch;

  function __construct() {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
  }


  function back_order_list($date = "", $cust_name = "", 
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_back_order_list_sql($date, $cust_name, $search, $filter, $orderby);
    
    $list->total = $this->db->get('back_order')->num_rows();

    $this->_back_order_list_sql($date, $cust_name, $search, $filter, $orderby);
   
    $list->query = $this->db->get('back_order', $per_page, $offset);
    //   echo  $this->db->last_query();
    
    return $list;


  }
	

  function _back_order_list_sql($date = "", $cust_name = "", 
			     $search = "", $filter = "", $orderby = ""){
    $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('cust_name', $cust_name);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }
    
    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `subtotal` LIKE '%$search%' OR `tax` LIKE '%$search%' OR `total` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `paid` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `contents` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_back_order($id) { return $this->load_by_id($id); }


  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get('back_order');
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }
  
  

  function back_order_update($back_order, $contents)
  {

    $this->db->where('id', $back_order->id);
    $back_order->contents = json_encode($contents);

    $this->db->update('back_order', $back_order);
    
  }


  function back_order_add($back_order, $contents)
  {
   
    $back_order->id = NULL;
    $back_order->contents = json_encode($contents);    
    $this->db->insert('back_order', $back_order);
    return $this->db->insert_id();
  }

  


  function back_order_del($id){

    $this->db->where('id', $id);
    $result = $this->db->get('back_orders')->result();

    if ($result[0]->image_small) unlink($this->image_path.'/'.$result[0]->image_small);  
    if ($result[0]->image_big) unlink($this->image_path.'/'.$result[0]->image_big);
  
    $this->db->where('id', $id);
    $this->db->delete('back_order');
  }

  
  function mark_as_transferred ($id)
  {
    $data = $this->load_by_id($id);

    $data->trans = 1; 
    $this->db->where('id', $id);
    $this->db->update('back_order', $data);
  
  }
  
  

  }
