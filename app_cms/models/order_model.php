<?php

class Order_model extends CI_Model {

  //var $image_path;

  function __construct() {
    parent::__construct();
    //  $this->image_path = realpath(APPPATH.'../../uploads/order_images/');
  }


  function order_list($search = "", $filter = "", $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->__order_list_sql($search, $filter, $orderby);
    
    $list->total = $this->db->get('site_order')->num_rows();

    $this->__order_list_sql($search, $filter, $orderby);
   
    $list->query = $this->db->get('site_order', $per_page, $offset);
 
    return $list;

  }
	

  function __order_list_sql($search = "", $filter = "", $orderby = ""){
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']); 
      }
    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	$this->db->where($key, $value); 
	endforeach;
      }
    if ($search)
      {   
	$this->db->where("(`id` LIKE '%$search%' OR `cust_id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `content` LIKE '%$search%' OR `note` LIKE '%$search%' OR `total` LIKE '%$search%')", NULL, FALSE); 
      }
  }
 
  function load_cate_names($term)
  {
    $this->db->distinct();
    $this->db->select('category'); 
    $this->db->order_by('category', 'asc');
    $this->db->like('category', $term);
    $result = $this->db->get('site_order')->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->category);
    return $array;
  }
  
  function load_order($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('site_order')->result();
		
    return $result[0];
		
  }
  
  function mark_as_transferred ($id)
  {
    $data = $this->load_order($id);
      // mark as read
    $data->trans = 1; 
    $this->db->where('id', $id);
    $this->db->update('site_order', $data);
  
  }
 /*
  function order_update($id)
  {
    $this->db->where('id', $id);

    if($images = $this->pic_upload())
      {
	$data['image_small'] = $images->small;
        $data['image_big'] = $images->big;
      }

    $data['boolean1'] = ($this->input->post('boolean1') == 'accept') ? 1 : 0;
    $data['boolean2'] = ($this->input->post('boolean2') == 'accept') ? 1 : 0;

    if($title = $this->input->post('url_title'))
      {
	$data['url_title'] = url_title($title, 'dash', TRUE);
      }

    $cols = array('title', 'category', 'detail', 'detail_member');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
 
    $this->db->update('site_order', $data);

  }

  function order_add() {

    if($images = $this->pic_upload())
      {
	$data['image_small'] = $images->small;
        $data['image_big'] = $images->big;
      }

    $data['boolean1'] = ($this->input->post('boolean1') == 'accept') ? 1 : 0;
    $data['boolean2'] = ($this->input->post('boolean2') == 'accept') ? 1 : 0;

    if($title = $this->input->post('url_title'))
      {
	$data['url_title'] = url_title($title, 'dash', TRUE);
      }

    $cols = array('title','category','detail','detail_member');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);

    $this->db->insert('site_order', $data); 
  }

 

  function order_del($id){

    $this->db->where('id', $id);
    $result = $this->db->get('site_order')->result();

    
    if ($result[0]->image_small 
	&& 
	is_file($this->image_path.'/'.$result[0]->image_small)) 
      unlink($this->image_path.'/'.$result[0]->image_small);  
    
    if ($result[0]->image_big 
	&& 
	is_file($this->image_path.'/'.$result[0]->image_big)) 
      unlink($this->image_path.'/'.$result[0]->image_big);
    

    $this->db->where('id', $id);
    $this->db->delete('site_order');
  }

  function pic_upload() {
		
    $config = array(
		    'allowed_types' => 'jpg|jpeg|gif|png',
		    'upload_path' => $this->image_path,
		    'max_size' => 2000
		    );
		
    $this->load->library('upload', $config);

    if ($this->upload->do_upload())
      {
 
	$image_data = $this->upload->data();

	$this->load->library('image_lib');

	$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => $this->image_path.'/big_'.$image_data['file_name'],
			'maintain_ration' => true,
			'width' => 800,
			'height' => 600
			);
	$this->image_lib->initialize($config); 
	$this->image_lib->resize();
	$this->image_lib->clear();

	$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => $this->image_path.'/small_'.$image_data['file_name'],
			'maintain_ration' => true,
			'width' => 160,
			'height' => 120
			);
	$this->image_lib->initialize($config); 
	$this->image_lib->resize();

	unlink($image_data['full_path']);


	$images = new stdClass;
	$images->big = 'big_'.$image_data['file_name'];
	$images->small = 'small_'.$image_data['file_name'];	
	return $images;
      }
    else
      {
	return false;
      }
 
  }
  */

  }