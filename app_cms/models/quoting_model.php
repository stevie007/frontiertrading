<?php

class Quoting_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }


  function quoting_list($search = "", $filter = "", $orderby = "", $per_page, $offset, $cate = "")
  {
    
    $cates = array();
    if ($cate)
      {
	$this->db->where('name', $cate);
	$query = $this->db->get('category');
	foreach ($query->result() as $cate) {
	  if ($cate->parent != 'none') 
	    {
	      array_push($cates, $cate->name);
	    }
	  else
	    {	
	      array_push($cates, $cate->name);
	      $this->db->where('parent', $cate->name); 
	      $query =  $this->db->get('category');
	      if ($query->num_rows) {	 
		foreach ($query->result() as $row)
		  {
		    array_push($cates, $row->name);
		  }
	      }
	    }
	}
      }
      
    $list = new stdClass;

    $this->__quoting_list_sql($search, $filter, $orderby, $cates);
    
    $list->total = $this->db->get('items')->num_rows();

    $this->__quoting_list_sql($search, $filter, $orderby, $cates);
   
    $list->query = $this->db->get('items', $per_page, $offset);
 //echo $this->db->last_query();
    return $list;

  }
	

  function __quoting_list_sql($search = "", $filter = "", $orderby = "", $cates = ""){
    
    if ($cates)
      {
	foreach ($cates as $cate){
	  $this->db->or_where('category', $cate); 
	}
      }
      
    if ($orderby)
      {      
     // 	$this->db->order_by('length('.$orderby['order'].')', $orderby['sort']); 
	$this->db->order_by($orderby['order'], $orderby['sort']); 

	//$this->db->order_by($this->db->escape('substring_index('.$orderby['order'].', "-", 1) ASC')); 
	
		//$this->db->order_by($orderby['order'], $orderby['sort']); 	     		     
      }
    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	$this->db->where($key, $value); 
	endforeach;
      }
    if ($search)
      {   
	$this->db->where("(`id` LIKE '%$search%' OR `category` LIKE '%$search%' OR `title` LIKE '%$search%' OR `image1` LIKE '%$search%' OR `price` LIKE '%$search%' OR `detail` LIKE '%$search%')", NULL, FALSE);  
      }
      
      
  }
 
  function load_cate_names($term)
  {
    $this->db->distinct();
    $this->db->select('category'); 
    $this->db->order_by('category', 'asc');
    $this->db->like('category', $term);
    $result = $this->db->get('items')->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->category);
    return $array;
  }
  
  function load_quoting($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get('items');
      if ($query->num_rows() > 0 )
	{ 
	  $result = $query->result();
	  return $result[0];
	}
      else 
	{
	  return false;
	}		
  }

  function load_by_title($title)
  {
    $this->db->where('title', $title);
    $query = $this->db->get('items');
    if ($query->num_rows() > 0 )
      { 
	$result = $query->result();
	return $result[0];
      }
    else 
      {
	return false;
      }		
  }
  
}
