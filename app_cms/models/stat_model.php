<?php

class Stat_model extends CI_Model {
  
  
  function __construct() {
    parent::__construct();
  }
  
  
  function ordered($date = '', $cate = ''){

    if (is_array($cate))
      {
	$items = $cate;	
      }
    else 
      {

	// get cates
	$cates = array();
	if ($cate)
	  {
	    $this->db->where('name', $cate);
	    $query = $this->db->get('category');
	    foreach ($query->result() as $cate) {
	      if ($cate->parent != 'none') 
		{
		  array_push($cates, $cate->name);
		}
	      else
		{	
		  array_push($cates, $cate->name);
		  $this->db->where('parent', $cate->name); 
		  $query =  $this->db->get('category');
		  if ($query->num_rows) {	 
		    foreach ($query->result() as $row)
		      {
			array_push($cates, $row->name);
		      }
		  }
		}
	    }
	  }
      
	foreach ($cates as $cate){
	  $this->db->or_where('category', $cate); 
	}     
	// end get cates


	$this->db->select('title');    
	$this->db->select('id');
	$items = $this->db->get('items')->result();
    
      }


    $items_ordered = array();
    
    foreach($items as $item) {
   
      $i = array();
      $i['title'] =  is_array($item) ? $item['name'] : $item->title;
      $i['id'] =  is_array($item) ? $item['id'] : $item->id;
      $i['ordered'] = 0; 

      $this->db->select('content');  
      if ($date)
	{
	  $this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	  $this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
	}
      $this->db->like('content', $i['title']); 
      
      $orders = $this->db->get('site_order')->result();

      foreach ($orders as $order)
	{
	  if ($contents = json_decode($order->content))
	    foreach ($contents as $content)
	      {
		if ($content)
		  if ($content->name == $i['title']) 
		    { 
		      $i['ordered'] += $content->qty;
		    }	      
	      }	  
	}
	
      array_push($items_ordered, $i);
    }
    $total = array();
    foreach ($items_ordered as $key => $row) {
      $total[$key] = $row['ordered'];
    }
  
    array_multisort( $total, SORT_DESC, $items_ordered);
 
    // array_multisort( $items_ordered, SORT_DESC, $table2);
    return $items_ordered;
    
  } // end ordered

  function ordered_by_month($date = '', $cate = ''){
  
    $items_ordered = array();
      
    // get month array
    $date_loop =  $date['fr']; 
    $array_month = array();
    while ( $date_loop  < $date['to']){
      $month = date("M - Y",  $date_loop);
      $array_month[$month] = 0;
      $date_loop =  strtotime ("+1 month", $date_loop);
      $month = date("M - Y",  $date_loop);
      $array_month[$month] = 0;
    }
    $month_totals = array();
    $month_totals['title'] = 'Month Totals';
    $month_totals = array_merge($month_totals, $array_month);
    $month_totals['total'] = 0;
    // end get month array
      
    if (is_array($cate))
      {
	$items = $cate;	
      }
    else 
      {

	// get cates
	$cates = array();
	if ($cate)
	  {
	    $this->db->where('name', $cate);
	    $query = $this->db->get('category');
	    foreach ($query->result() as $cate) {
	      if ($cate->parent != 'none') 
		{
		  array_push($cates, $cate->name);
		}
	      else
		{	
		  array_push($cates, $cate->name);
		  $this->db->where('parent', $cate->name); 
		  $query =  $this->db->get('category');
		  if ($query->num_rows) {	 
		    foreach ($query->result() as $row)
		      {
			array_push($cates, $row->name);
		      }
		  }
		}
	    }
	  }
      
	foreach ($cates as $cate){
	  $this->db->or_where('category', $cate); 
	}     
	// end get cates

	$this->db->select('title');    
	$this->db->select('id');
	$items = $this->db->get('items')->result();
    
      }
    
    foreach($items as $item) {
  
      $i = array();
      $i['title'] =  is_array($item) ? $item['name'] : $item->title;
      //$i['id'] =  $item['id'];
      $i = array_merge($i, $array_month);
      $i['total'] = 0;
	
      $this->db->select('content');        
      $this->db->select('date');  
      if ($date)
	{
	  $this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	  $this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
	}
      $this->db->like('content', $i['title']); 
      
      $orders = $this->db->get('site_order')->result();

      foreach ($orders as $order)
	{

	  $month = date("M - Y", strtotime($order->date));
	  $i[$month] = isset($i[$month]) ? $i[$month] : 0;
	  
	  if ($contents = json_decode($order->content))
	    foreach ($contents as $content)
	      {
		if ($content)
		  if ($content->name == $i['title']) 
		    { 
		      $i[$month] += $content->qty; 
		      $i['total'] += $content->qty;
		      $month_totals[$month] += $content->qty; 
		      $month_totals['total'] += $content->qty;
		    }	      
	      }	  
	}

      array_push($items_ordered, $i);
      
    }
    array_push($items_ordered, $month_totals);
    
    $total = array();
    foreach ($items_ordered as $key => $row) { $total[$key] = $row['total']; }
    array_multisort( $total, SORT_DESC, $items_ordered);
    
    return $items_ordered;
  } // end func
  
  /*** invoice part ***/
    function sold($date = '', $cate = ''){

    if (is_array($cate))
      {
	$items = $cate;	
      }
    else 
      {

	// get cates
	$cates = array();
	if ($cate)
	  {
	    $this->db->where('name', $cate);
	    $query = $this->db->get('category');
	    foreach ($query->result() as $cate) {
	      if ($cate->parent != 'none') 
		{
		  array_push($cates, $cate->name);
		}
	      else
		{	
		  array_push($cates, $cate->name);
		  $this->db->where('parent', $cate->name); 
		  $query =  $this->db->get('category');
		  if ($query->num_rows) {	 
		    foreach ($query->result() as $row)
		      {
			array_push($cates, $row->name);
		      }
		  }
		}
	    }
	  }
      
	foreach ($cates as $cate){
	  $this->db->or_where('category', $cate); 
	}     
	// end get cates


	$this->db->select('title');    
	$this->db->select('id');
	$items = $this->db->get('items')->result();
    
      }


    $items_ordered = array();
    
    foreach($items as $item) {
   
      $i = array();
      $i['title'] =  is_array($item) ? $item['name'] : $item->title;
      $i['id'] =  is_array($item) ? $item['id'] : $item->id;
      $i['ordered'] = 0; 

      $this->db->select('contents');  
      if ($date)
	{
	  $this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	  $this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
	}
      $this->db->like('contents', $i['title']); 
      
      $orders = $this->db->get('invoice')->result();

      foreach ($orders as $order)
	{
	  if ($contents = json_decode($order->contents))
	    foreach ($contents as $content)
	      {
		if ($content)
		  if ($content->name == $i['title']) 
		    { 
		      $i['ordered'] += $content->qty;
		    }	      
	      }	  
	}
	
      array_push($items_ordered, $i);
    }
    $total = array();
    foreach ($items_ordered as $key => $row) {
      $total[$key] = $row['ordered'];
    }
  
    array_multisort( $total, SORT_DESC, $items_ordered);
 
    // array_multisort( $items_ordered, SORT_DESC, $table2);
    return $items_ordered;
    
  } // end ordered

  function sold_by_month($date = '', $cate = ''){
  
    $items_ordered = array();
      
    // get month array
    $date_loop =  $date['fr']; 
    $array_month = array();
    while ( $date_loop  < $date['to']){
      $month = date("M - Y",  $date_loop);
      $array_month[$month] = 0;
      $date_loop =  strtotime ("+1 month", $date_loop);
      $month = date("M - Y",  $date_loop);
      $array_month[$month] = 0;
    }
    $month_totals = array();
    $month_totals['title'] = 'Month Totals';
    $month_totals = array_merge($month_totals, $array_month);
    $month_totals['total'] = 0;
    // end get month array
      
    if (is_array($cate))
      {
	$items = $cate;	
      }
    else 
      {

	// get cates
	$cates = array();
	if ($cate)
	  {
	    $this->db->where('name', $cate);
	    $query = $this->db->get('category');
	    foreach ($query->result() as $cate) {
	      if ($cate->parent != 'none') 
		{
		  array_push($cates, $cate->name);
		}
	      else
		{	
		  array_push($cates, $cate->name);
		  $this->db->where('parent', $cate->name); 
		  $query =  $this->db->get('category');
		  if ($query->num_rows) {	 
		    foreach ($query->result() as $row)
		      {
			array_push($cates, $row->name);
		      }
		  }
		}
	    }
	  }
      
	foreach ($cates as $cate){
	  $this->db->or_where('category', $cate); 
	}     
	// end get cates

	$this->db->select('title');    
	$this->db->select('id');
	$items = $this->db->get('items')->result();
    
      }
    
    foreach($items as $item) {
  
      $i = array();
      $i['title'] =  is_array($item) ? $item['name'] : $item->title;
      //$i['id'] =  $item['id'];
      $i = array_merge($i, $array_month);
      $i['total'] = 0;
	
      $this->db->select('contents');        
      $this->db->select('date');  
      if ($date)
	{
	  $this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	  $this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
	}
      $this->db->like('contents', $i['title']); 
      
      $orders = $this->db->get('invoice')->result();

      foreach ($orders as $order)
	{

	  $month = date("M - Y", strtotime($order->date));
	  $i[$month] = isset($i[$month]) ? $i[$month] : 0;
	  
	  if ($contents = json_decode($order->contents))
	    foreach ($contents as $content)
	      {
		if ($content)
		  if ($content->name == $i['title']) 
		    { 
		      $i[$month] += $content->qty; 
		      $i['total'] += $content->qty;
		      $month_totals[$month] += $content->qty; 
		      $month_totals['total'] += $content->qty;
		    }	      
	      }	  
	}

      array_push($items_ordered, $i);
    }
    array_push($items_ordered, $month_totals);
    

    
    $total = array();
    foreach ($items_ordered as $key => $row) { $total[$key] = $row['total']; }
    array_multisort( $total, SORT_DESC, $items_ordered);
    
    
    return $items_ordered;
  } // end func
  
  /*** end invoice part ***/
  
}// end class