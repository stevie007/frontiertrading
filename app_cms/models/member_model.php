<?php

class Member_model extends CI_Model 
{

  var $image_path;

  function __construct() {
    parent::__construct();
    $this->image_path = realpath(APPPATH.'../../uploads/member_images/');
  }


  function member_list($search = "", $filter = "", $orderby = "", $per_page, $offset, $cust_name = "")
  {
    
    $list = new stdClass;

    $this->__member_list_sql($search, $filter, $orderby, $cust_name);
    
    $list->total = $this->db->get('member')->num_rows();

    $this->__member_list_sql($search, $filter, $orderby, $cust_name);
   
    $list->query = $this->db->get('member', $per_page, $offset);
 
    return $list;


  }
	

  function __member_list_sql($search = "", $filter = "", $orderby = "", $cust_name = "" ){
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']); 
      }
    

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('company', $cust_name);  
      }

    if ($filter)
      {   
	foreach ($filter as $key => $value):
	$this->db->where($key, $value); 
	endforeach;
      }
    if ($search && $search != 'Search..')
      {   
	$this->db->where("(`id` LIKE '%$search%' OR `firstname` LIKE '%$search%' OR `lastname` LIKE '%$search%' OR `email` LIKE '%$search%' OR `telephone` LIKE '%$search%' OR `mobile` LIKE '%$search%' OR `fax` LIKE '%$search%' OR `address` LIKE '%$search%' OR `suburb` LIKE '%$search%' OR `city` LIKE '%$search%' OR `postcode` LIKE '%$search%' OR `password` LIKE '%$search%' OR `resetcode` LIKE '%$search%' OR `company` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_names($term)
  {
    $this->db->select('company');
    
    $this->db->like('company', $term);
    $result = $this->db->get('member')->result();
		
    $array = array();
    foreach($result as $row) array_push($array, $row->company);
 
    return $array;
    
  }
  
  function load_id_names($term)
  {
    $this->db->select('company');
    $this->db->select('id');
    $this->db->like('company', $term);
    $result = $this->db->get('member')->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->id.' | '.$row->company);
    return $array;
  }

  function load_member($id) { return $this->load_by_id($id); }
  
  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get('member');
     if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }


  function set_balance($id, $bal){
    $this->db->where('id', $id);
    $this->db->set('balance', $bal);
    $this->db->update('member'); 
  }

  function member_update($id)
  {
    $this->db->where('id', $id);
    
    if ($this->input->post('password'))
      {
	$data['password'] = sha1($this->input->post('password'));
      }
       $data['active'] = ($this->input->post('active') == 'accept') ? 1 : 0;
       $data['auto_send'] = ($this->input->post('auto_send') == 'accept') ? 1 : 0;
    $cols = array('company', 'firstname','lastname','email','telephone', 'mobile', 'fax', 'address', 'suburb', 'city', 'postcode', 'address2', 'suburb2', 'city2', 'postcode2', 'notes', 'authority', 'resetcode');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
 
    $this->db->update('member', $data);

  }

  function member_add() {
    
    $data['id'] = NULL;
    
    $data['password'] = sha1($this->input->post('password'));
    
    $data['active'] = ($this->input->post('active') == 'accept') ? 1 : 0;
    
    $cols = array('company', 'firstname','lastname','email','telephone', 'mobile', 'fax', 'address', 'suburb', 'city', 'postcode', 'address2', 'suburb2', 'city2', 'postcode2', 'notes', 'authority', 'resetcode');
    
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
    
    $this->db->insert('member', $data); 
  }



  function member_del($id){
    
    $this->db->where('id', $id);
    $result = $this->db->get('member')->result();
    
    $this->db->where('id', $id);
    $this->db->delete('member');
  }


 
  function set_resetcode($id)
  {

    $this->load->helper('string');
    $code = strtolower(random_string('alnum', 8));

    $this->db->where('id', $id);

    $data = array('resetcode' => $code);
    
    $this->db->update('member', $data);
 
    if($this->db->affected_rows() == 1)
      {
	return $code;	
      }
    
  }
  
  
}
