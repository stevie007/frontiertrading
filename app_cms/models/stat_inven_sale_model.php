<?php

class Stat_inven_sale_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }
  
  function sync_sold($date = ''){
    
    $this->db->select('title');
    $items = $this->db->get('items')->result();
    
    foreach($items as $item) {
      
      $sold = 0;

      $this->db->select('contents');  
      if ($date)
	{
	  $this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	  $this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
	}
	  $this->db->like('contents', $item->title); 
      $invoices = $this->db->get('invoice')->result();

      foreach ($invoices as $invoice)
	{
	  if ($contents = json_decode($invoice->contents))
	  foreach ($contents as $content)
	    {
	      if ($content)
	      if ($content->name == $item->title) 
		{ 
		  $sold += $content->qty;
		}	      
	    }	  
	}
      $this->set_sold($item->title, $sold);
    }
    
  }

  function set_sold($title, $sold){
    $this->db->where('title', $title);
    $this->db->set('sold', $sold);
    $this->db->update('items'); 
  }
 
}