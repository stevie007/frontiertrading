<?php

class Invoice_model extends CI_Model {

  var $branch;

  function __construct() {
    parent::__construct();
    $this->branch = $this->session->userdata('branch');
  }


  function invoice_list($date = "", $cust_name = "", 
			$filter = "", $search = "", 
			$orderby = "", $per_page, $offset, $sales = "")
  {
    
    $list = new stdClass;

    $this->_invoice_list_sql($date, $cust_name, $search, $filter, $orderby, $sales);
    
    $list->total = $this->db->get('invoice')->num_rows();

    $this->_invoice_list_sql($date, $cust_name, $search, $filter, $orderby, $sales);
   
    $list->query = $this->db->get('invoice', $per_page, $offset);
    //    echo  $this->db->last_query();
    
    return $list;


  }
	

  function _invoice_list_sql($date = "", $cust_name = "", 
			     $search = "", $filter = "", $orderby = "", $sales = ""){
    //  $this->db->where('deleted', 0);
   
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }

    if ($cust_name && $cust_name != 'Customer..')
      {
	$this->db->where('cust_name', $cust_name);  
      }

    if ($sales && $sales != 'Sales..')
      {
	$this->db->where('sales', $sales);  
      }
    
    if ($orderby)
      {      
	if (strstr($orderby['order'], '-sp-')) 
	  {
	    $this->db->order_by(str_replace('-sp-', ' ', $orderby['order']), 
				$orderby['sort']); 
	  }
	else
	  {
	    $this->db->order_by($orderby['order'], $orderby['sort']);
	  }
      }

    if ($filter)
      {   

	foreach ($filter as $flr_num => $keys):
	  $this->db->where('( 1=', '1', false);
	  
	  $first = TRUE;
	  foreach ($keys as $key => $value):
	  if ($key == 'sql') 
	    {	 	      
	      $data = str_replace('-eq-', ' = ',
				  str_replace('-ne-', ' <> ', $value));
	    }
	  else 
	    {  
	      $data = $key.' = "'.$value.'"';
	    }
	  if ($first)
	    {
	      $this->db->where($data); 
	      $first = FALSE;
	    }
	  else 
	    {
	      $this->db->or_where($data);
	    }
	  endforeach;

	  $this->db->where('1', '1 )', false);
	  endforeach;
      }
    
    if ($search && $search != 'Search..')
      {   
	$search = $this->db->escape_like_str($search);
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `subtotal` LIKE '%$search%' OR `tax` LIKE '%$search%' OR `total` LIKE '%$search%' OR `discount` LIKE '%$search%' OR `paid` LIKE '%$search%'  OR `sales` LIKE '%$search%' OR `cust_name` LIKE '%$search%' OR `contents` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_invoice($id) { return $this->load_by_id($id); }

  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('invoice')->result();    
    return $result[0];	
  }
  

  function invoice_update($invoice, $contents)
  {
      
    $this->db->where('id', $invoice->id);
    $invoice->contents = json_encode($contents);

    $this->db->update('invoice', $invoice);
    
  }


  function invoice_add($invoice, $contents)
  {	
   
    $invoice->id = NULL;
    $invoice->contents = json_encode($contents);    
    $this->db->insert('invoice', $invoice);
    return $this->db->insert_id();
  }

   function pay_by_id($id){

    $this->db->where('id', $id);
    $query = $this->db->get('invoice');
    
     
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	$invoice =  $result[0];
    $invoice->paid = 1;

    $this->db->where('id', $invoice->id);

    $this->db->update('invoice', $invoice);
	return true;
		
      }
    else
      {
	return false;
      }
      
    
  
  }


  function delete_by_id($id){

    $this->db->where('id', $id);
    $result = $this->db->get('invoice')->result();
    $invoice =  $result[0];
    $invoice->deleted = 1;

    $this->db->where('id', $invoice->id);

    $this->db->update('invoice', $invoice);
  
  }

 
  function recover_by_id($id){

    $this->db->where('id', $id);
    $result = $this->db->get('invoice')->result();
    $invoice =  $result[0];
    $invoice->deleted = 0;
    $this->db->where('id', $invoice->id);

    $this->db->update('invoice', $invoice);
  
  }


  }
