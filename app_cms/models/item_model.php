<?php

class Item_model extends CI_Model {

  var $image_path;

  var $db_name;
  function __construct() {
    parent::__construct();
    $this->db_name = 'items';
    $this->image_path = realpath(APPPATH.'../uploads/item_images/');
  }


  function item_list($search = "", $filter = "", $orderby = "", $per_page, $offset, $cate = "")
  {
    
    $cates = array();
    if ($cate)
      {
	$this->db->where('name', $cate);
	$query = $this->db->get('category');
	foreach ($query->result() as $cate) {
	  if ($cate->parent != 'none') 
	    {
	      array_push($cates, $cate->name);
	    }
	  else
	    {	
	      array_push($cates, $cate->name);
	      $this->db->where('parent', $cate->name); 
	      $query =  $this->db->get('category');
	      if ($query->num_rows) {	 
		foreach ($query->result() as $row)
		  {
		    array_push($cates, $row->name);
		  }
	      }
	    }
	}
      }
    $list = new stdClass;

    $this->__item_list_sql($search, $filter, $orderby, $cates);
    
    $list->total = $this->db->get('items')->num_rows();

    $this->__item_list_sql($search, $filter, $orderby, $cates);
   
    $list->query = $this->db->get('items', $per_page, $offset);
 
    return $list;

  }
	

  function __item_list_sql($search = "", $filter = "", $orderby = "", $cates = ""){
     if ($cates)
      {
	foreach ($cates as $cate){
	  $this->db->or_where('category', $cate); 
	}
      }
     if ($orderby)
      {  
      	$this->db->order_by($orderby['order'], $orderby['sort']);     
	//$this->db->order_by('length('.$orderby['order'].'), '.$orderby['order'], $orderby['sort']); 
      }
    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	$this->db->where($key, $value); 
	endforeach;
      }
    if ($search && $search != 'Search..')
      {   
	$this->db->where("(`id` LIKE '%$search%' OR `category` LIKE '%$search%' OR `title` LIKE '%$search%' OR `image1` LIKE '%$search%' OR `price` LIKE '%$search%' OR `detail` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 
  function load_cate_names($term)
  {
    $this->db->distinct();
    $this->db->select('category'); 
    $this->db->order_by('category', 'asc');
    $this->db->like('category', $term);
    $result = $this->db->get('items')->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->category);
    return $array;
  }
  
  function load_names($term)
  {
    $this->db->select('title');
    
    $this->db->like('title', $term);
    $result = $this->db->get($this->db_name)->result();
    
    $array = array();
    foreach($result as $row) array_push($array, $row->title);
    
    return $array;
    
  }

  function load_item($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get('items');
      if ($query->num_rows() > 0 )
	{ 
	  $result = $query->result();
	  return $result[0];
	}
      else 
	{
	  return false;
	}		
  }
  
  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }


  function load_by_title($title)
  {
    $this->db->where('title', $title);
    $query = $this->db->get('items');
    if ($query->num_rows() > 0 )
      { 
	$result = $query->result();
	return $result[0];
      }
    else 
      {
	return false;
      }		
  }
  

  function item_update($id)
  {

    if($images = $this->pic_upload())
      {
	$data['image1'] = $images->img1;
	
	$this->db->where('id', $id);
	$result = $this->db->get('items')->result();
        if ($result[0]->image1 && is_file($this->image_path.'/'.$result[0]->image1)) 
          unlink($this->image_path.'/'.$result[0]->image1);   
      }

    $data['new'] = ($this->input->post('new') == 'accept') ? 1 : 0;
    $data['special'] = ($this->input->post('special') == 'accept') ? 1 : 0;
    $data['online'] = ($this->input->post('online') == 'accept') ? 1 : 0;
    if (($this->input->post('price_s') > 0) && ($this->input->post('price_s') < $this->input->post('price')))
    {
       $data['special'] = 1;
    }
    else
    {
       $data['special'] = 0;
    }
    
    
    if($title = $this->input->post('url_title'))
      {
	$data['url_title'] = url_title($title, 'dash', TRUE);
      }

    $cols = array('title', 'category', 'detail', 'price', 'qty', 'price_s');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
    
    $this->db->where('id', $id);
    $this->db->update('items', $data);

  }

  function item_add($data) {

    $this->db->insert('items', $data); 
  }

  function item_add_old() {

    if($images = $this->pic_upload())
      {
	$data['image1'] = $images->img1;
      }

    $data['new'] = ($this->input->post('new') == 'accept') ? 1 : 0;
    $data['special'] = ($this->input->post('special') == 'accept') ? 1 : 0;
    $data['online'] = ($this->input->post('online') == 'accept') ? 1 : 0;

    if($title = $this->input->post('url_title'))
      {
	$data['url_title'] = url_title($title, 'dash', TRUE);
      }

    $cols = array('title','category','detail','price', 'qty');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);

    $this->db->insert('items', $data); 
  }


/*
  function item_del($id){

    $this->db->where('id', $id);
    $result = $this->db->get('items')->result();

    
    if ($result[0]->image1 && is_file($this->image_path.'/'.$result[0]->image1)) 
      unlink($this->image_path.'/'.$result[0]->image1);  
    
    
    $this->db->where('id', $id);
    $this->db->delete('items');
  }
*/



  function del($id){

    
    $data = $this->load_by_id($id);  

    $data->deleted = 1;

    $this->db->where('id', $data->id);

    $this->db->update($this->db_name, $data);
  
  }


 
  function recover_by_id($id){

    $this->db->where('id', $id);
    $result = $this->db->get($this->db_name)->result();
    $data =  $result[0];
    $data->deleted = 0;
    $this->db->where('id', $data->id);

    $this->db->update($this->db_name, $data);
  
  }

  function pic_upload() {
    
    $config = array(
		    'allowed_types' => 'jpg|jpeg|gif|png',
		    'upload_path' => $this->image_path,
		    'max_size' => 20000
		    );
    
    $this->load->library('upload', $config);
    $this->load->library('image_lib');

    $images = new stdClass;    
    $prefix = time().'_';
    
    if ($this->upload->do_upload('userfile'))
      { 
	$image_data = $this->upload->data();	
	$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => $this->image_path.'/'.$prefix.$image_data['file_name'],
			'maintain_ration' => true,
			'width' => 500,
			'height' => 900
			);
	$this->image_lib->initialize($config); 
	$this->image_lib->resize();
	$this->image_lib->clear();
	
	unlink($image_data['full_path']);
	$images->img1 = $prefix.$image_data['file_name'];
      }
    else
      {
	return false;
      }
 
    return $images;
    
  }

}