<?php

class Page_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

	function load_page($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('pages')->result();
		
		return $result[0];
		
	}

	function page_update($id)
	{
		$this->db->where('id', $id);

		$data = array(
			'text1' => $this->input->post('text1'),
                        'text2' => $this->input->post('text2'),
                        'text3' => $this->input->post('text3')
		);
 
		$this->db->update('pages', $data);

	}
	
 
}
