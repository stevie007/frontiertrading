<?php

class Statement_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  function sync_bal($date = ''){

    $this->db->select('id');
    $members = $this->db->get('member')->result();

    foreach($members as $member) {
      
      $this->db->where('deleted', 0);
      $this->db->select_sum('total');  
      if ($date)
	{
	  $this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	  $this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
	}
      $result = $this->db->get_where('payment', array('cust_id' =>  $member->id))->result();
      $total_p = $result[0]->total;

      $this->db->where('deleted', 0);
      $this->db->select_sum('total');
      if ($date)
	{
	  $this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	  $this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
	}
      $result = $this->db->get_where('invoice', array('cust_id' =>  $member->id))->result();
      $total_i = $result[0]->total;
    
      $balance = $total_i - $total_p;
      $this->set_balance($member->id, $balance);
    }
    
  }

  function set_balance($id, $bal){
    $this->db->where('id', $id);
    $this->db->set('balance', $bal);
    $this->db->update('member'); 
  }
 
  function load_by_uid($id, $date = ''){
  
    $this->db->select('id, cust_id, total, date, type');
    $this->db->where('deleted', 0);
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }
    $query = $this->db->get_where('payment', array('cust_id' => $id));
    $join1 = $this->db->last_query();
    
    $this->db->select('id, cust_id, total, date, type');
    $this->db->where('deleted', 0);
    if ($date)
      {
	$this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
	$this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');  
      }
    $query = $this->db->get_where('invoice', array('cust_id' => $id));
    $join2 = $this->db->last_query();
 
    $join2 = str_replace(array('(', ')'), '', $join2);    
    $join1 = str_replace(array('(', ')'), '', $join1);

    $query = $this->db->query($join1.' UNION '.$join2.' ORDER BY `date` ASC');
 
    return $query->result();
  }
  
    public function getHisBalance($id, $date = ''){
        $this->db->select('total, type, date');
        $this->db->where('deleted', 0);

        if ($date)
        {
          $this->db->where('date <=', date("Y-m-d", $date).' 00:00:00');  
        }
        $query = $this->db->get_where('payment', array('cust_id' => $id));
        $join1 = $this->db->last_query();

        $this->db->select('total, type, date');
        $this->db->where('deleted', 0);
        if ($date)
          {
            $this->db->where('date <=', date("Y-m-d", $date).' 00:00:00');
          }
        $query = $this->db->get_where('invoice', array('cust_id' => $id));
        $join2 = $this->db->last_query();

        $join2 = str_replace(array('(', ')'), '', $join2);    
        $join1 = str_replace(array('(', ')'), '', $join1);

        $query = $this->db->query($join1.' UNION '.$join2);
        
        $total = 0;
        
        foreach ($query->result() as $hist) {
            if($hist->type === 'payment') {
                $total -= $hist->total;
            }elseif ($hist->type === 'invoice') {
                $total += $hist->total;
            }
        }

        return round($total, 2);

    }

    // Add to set balance individually
    function sync_bal_by_uid($id, $date = ''){

        $this->db->where('deleted', 0);
        $this->db->select_sum('total');
        if ($date)
        {
            $this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
            $this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');
        }
        $result = $this->db->get_where('payment', array('cust_id' =>  $id))->result();
        $total_p = $result[0]->total;

        $this->db->where('deleted', 0);
        $this->db->select_sum('total');
        if ($date)
        {
            $this->db->where('date >=', date("Y-m-d", $date['fr']).' 00:00:00');
            $this->db->where('date <=', date("Y-m-d", $date['to']).' 23:59:50');
        }
        $result = $this->db->get_where('invoice', array('cust_id' =>  $id))->result();
        $total_i = $result[0]->total;

        $balance = $total_i - $total_p;
        $this->db->where('id', $id);
        $this->db->set('balance', $balance);
        $this->db->update('member'); 
    }
}
