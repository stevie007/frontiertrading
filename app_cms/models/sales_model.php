<?php

class Sales_model extends CI_Model 
{
  var $db_name;

  function __construct()
  {
    parent::__construct();
    $this->db_name = 'sales';
  }


  function sales_list($search = "", $filter = "", $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_sales_list_sql($search, $filter, $orderby);
    
    $list->total = $this->db->get($this->db_name)->num_rows();

    $this->_sales_list_sql($search, $filter, $orderby);
   
    $list->query = $this->db->get($this->db_name, $per_page, $offset);
 
    return $list;


  }
	

  function _sales_list_sql($search = "", $filter = "", $orderby = ""){
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']); 
      }
    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	  $this->db->where($key, $value); 
	  endforeach;
      }
    if ($search && $search != 'Search..')
      {   

	$cols = array('id','firstname','lastname','email', 'phone', 'mobile', 'fax', 'address', 'suburb', 'city', 'postcode', 'password', 'resetcode');
     	
	$this->db->where('( 1=', '1', false);    
	$first = TRUE;
	foreach ($cols as $col)
	  {
	    if ($first)
	      {
		$this->db->like($col, $search); 
		$first = FALSE;
	      }
	    else 
	      {
		$this->db->or_like($col, $search); 
	      }	
	  }
	$this->db->where('1', '1 )', false);	
      }
  }
 
  function load_names($term)
  {
    $this->db->select('firstname');
    
    $this->db->like('firstname', $term);
    $result = $this->db->get($this->db_name)->result();
		
    $array = array();
    foreach($result as $row) array_push($array, $row->firstname);
 
    return $array;
    
  }

  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }

  function check_update()
  {
    $prev = $this->load_by_id($this->input->post('id')); 
    $firstname = $this->input->post('firstname');
   
    return (
	    !$this->check_add($firstname) && $firstname == $prev->firstname
	    ||
	    $this->check_add($firstname)
	    ) ? TRUE : FALSE; 

  }

  function update($id)
  {
    if ($this->check_update())
      {

	if ($prev = $this->load_by_id($id))
	  { }
	else 
	  {
	    return FALSE;
	  }    

	$this->db->where('id', $id);
    
	if ($this->input->post('password'))
	  {
	    $data['password'] = sha1($this->input->post('password'));
	  }
    
	$cols = array('firstname','lastname','email','phone', 'mobile', 'fax', 'address', 'suburb', 'city', 'postcode');
      
	foreach ($cols as $col) $data[$col] = $this->input->post($col);
 
	$this->db->update($this->db_name, $data);

	if ($this->db->affected_rows() == 1) 
	  {	
	    $this->db->where('sales', $prev->firstname);   
	    $query = $this->db->get('invoice');
	    foreach($query->result() as $invoice) {      
	      $invoice->sales = $data['firstname'];
	      $this->db->where('id', $invoice->id);
	      $this->db->update('invoice', $invoice);
	    }
	    return TRUE;
	  }
	else 
	  {
	    return FALSE;
	  }
      }
  }

  function check_add($firstname){    
    $this->db->where('firstname', $firstname);
    $query = $this->db->get($this->db_name);    
    return ($query->num_rows == 1) ? false : TRUE;
  }
	
  function add() {
    if ($this->check_add($this->input->post('firstname')))
      {
	
	$data['id'] = NULL;
	
	$data['password'] = sha1($this->input->post('password'));
	
	$cols = array('firstname','lastname','email','phone', 'mobile', 'fax', 'address', 'suburb', 'city', 'postcode', 'authority', 'resetcode');
	
	foreach ($cols as $col) $data[$col] = $this->input->post($col);
	
	$this->db->insert($this->db_name, $data); 
	return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
	
      }
    else 
      {
	return FALSE;
      }
  }



  function del($id){
    
    $this->db->where('id', $id);
    $this->db->delete($this->db_name);
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
  }

}