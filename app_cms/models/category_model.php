<?php

class Category_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }


  function category_list($search = "", $filter = "", $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->__category_list_sql($search, $filter, $orderby);
    
    $list->total = $this->db->get('category')->num_rows();

    $this->__category_list_sql($search, $filter, $orderby);
   
    $list->query = $this->db->get('category', $per_page, $offset);
 
    return $list;

  }
	

  function __category_list_sql($search = "", $filter = "", $orderby = ""){
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']); 
      }
    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	$this->db->where($key, $value); 
	endforeach;
      }
    if ($search)
      {   
	$this->db->where("(`id` LIKE '%$search%' OR `name` LIKE '%$search%' OR `parent` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 
  function load_parent_names($term)
  {
    $this->db->where('parent', 'none'); 
    $this->db->select('name'); 
    $this->db->order_by('name', 'asc');
    $this->db->like('name', $term);
    $result = $this->db->get('category')->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->name);
    return $array;
  }
 
  function load_category_names($term)
  {
    $this->db->select('name'); 
    $this->db->order_by('name', 'asc');
    $this->db->like('name', $term);
    $result = $this->db->get('category')->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->name);
    return $array;
  }
  
  function load_category($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('category')->result();
    
    return $result[0];
    
  }

  function category_update($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('category')->result();
    $prev_cate = $result[0]->name;
    
    $this->db->where('id', $id);
    $cols = array('name', 'parent', 'order');
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
    $this->db->update('category', $data);

    
    // update items
    $this->db->where('category', $prev_cate);
    
    $query = $this->db->get('items');
    foreach($query->result() as $item) {      
      $item->category = $data['name'];
      $this->db->where('id', $item->id);
      $this->db->update('items', $item);
    }
    

    $this->db->where('parent', $prev_cate);
    $query = $this->db->get('category');
    foreach($query->result() as $cate) {      
      $cate->parent = $data['name'];
      $this->db->where('id', $cate->id);
      $this->db->update('category', $cate);
    }
  }

  function category_add() {

    $cols = array('name','parent', 'order');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);

    $this->db->insert('category', $data); 
  }



  function category_del($id){

    $this->db->where('id', $id);
    $this->db->delete('category');
  }

  }
