<script>
$(function() {
    $( ".category" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/cate_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  }
	})
      .click(function() {
	  $(this).autocomplete( "search" , '' );
	});

      
  });
</script>
<script type="text/javascript">
$(function() {
    $('.uploadForm').ajaxForm({
        beforeSubmit: function(a,f,o) {
            $('#uploadOutput').html('Submitting...');
        },
        success: function(data) {
	     var cart = $.parseJSON(data);
	     mark_added(cart.contents);	 
	     $('#fnoty #cart-info')
	       .html('Cart updated. '+ cart.item +' items, Total: $' + cart.total) 
	       .fadeIn();	
            var $out = $('#uploadOutput');
            $out.html('Form success handler received. ');
            $out.append('<span> Total items: '+ cart.total.toFixed(2) +'</span>');
	    
        }
    });
    var obj_cart = $.parseJSON('<?php echo $json_cart;?>');
 
	mark_added(obj_cart);
});
function mark_added(obj_cart){
      for (x in obj_cart)
	{
 		var id = obj_cart[x]['id'];  
 		$('#'+id).css("background-color","#7D8893");
	}
}
</script> 

<script>
$(function() {
    $('.item').dblclick(function(){
	$('#uf'+this.id).submit();
      });
  })
</script>
<!-- noty -->
   
   <div id="fnoty">
   <span id="cart-info">
   <?php echo 'Cart updated. '.$cart->total_items().' items, Total: $'.$cart->total();?>
   </span><br /><br />
   <a href="<?php echo site_url('quoting/empty_cart');?>" class="white font9px">Empty Cart</a>
   &nbsp;&nbsp;&nbsp;&nbsp;
   <a href="<?php echo site_url('stat_order');?>" class="white font9px">Order Report</a>
      </div>
<!-- end noty -->
<div class="ui_button font10px" id="cates">
 <?php 
$category = isset($category) ? $category : '';

$cates = get_cates();
foreach ($cates as $cate) 
  {
    if (is_array($cate)) 
      {
	$parent = array_shift($cate);
	echo '<span class="link">';
	echo ($parent->name == $category) ? '<button disabled="disabled">'.$parent->name.'</button>' : '<a href="'.site_url('quoting/cate/'.$parent->name).'" >'.$parent->name.'</a>';  
	echo '</span>';
	foreach ($cate as $sub)
	  {	  
	echo '<span class="link">';
	    echo ($sub->name == $category) ? '<button disabled="disabled">'.$sub->name.'</button>' : '<a href="'.site_url('quoting/cate/'.$sub->name).'" >'.$sub->name.'</a>';	  
	echo '</span>';
	  }
      }
    else
      {		echo '<span class="link">';
	echo ($cate->name == $category) ? '<button disabled="disabled">'.$cate->name.'</button>' : '<a href="'.site_url('quoting/cate/'.$cate->name).'" >'.$cate->name.'</a>';
	echo '</span>';
      }
  }
?>
</div>
<div style="float: left; padding:20px 22px;">
<div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

<table border="0" cellpadding="5" cellspacing="0" width="840px" style="margin:30px auto;">

<tr>
<th colspan="8"  align="left"><?php echo $table_title;?></th>
</tr>

<tr class="bgc">
<td colspan="8"  align="left" class="ui_button font9px">

<?php 
echo form_open('quoting/search', array('id' => 'myform', 'style' => 'display:inline;'));
?>
<span class="font11px">
<?php
  echo form_input( 
		  array(
			'name'        => 'search',
			'value'	      => 'Search..',
			'class'       => 'blur  ui-corner-all',
			'style'       => 'width:200px;margin:10px;'
			) 
		   );
?>
</span>
<?php
  echo form_submit(array(
			 'name'        => 'submit',
			 'value'       => 'Go'
			 )
		   );
?>

<?php echo form_close(); ?>
<span class="font12px">
<?php 
if ($search)
{
echo '&nbsp;&nbsp;&nbsp; Listing search results for: '.$search;
}
echo '&nbsp;&nbsp;&nbsp; Total: '.$total;
echo nbs(10);
?>
Ajax Output:
</span>

 <span class="font12px" id="uploadOutput"></span>
</td>
</tr>

<tr class="bgc">
<td colspan="8"  align="right" class="ui_button font9px"><?php echo $this->pagination->create_links(); 
$attr = ($per_page == 50) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("quoting/filter/per_page/$total", 'Display All', $attr);
?>
</td>
</tr>

<tr>
<th align="left" valign="middle" width="40">
<?php echo anchor("quoting/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("quoting/orderby/category", 'Category', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'category'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("quoting/orderby/title", 'Title', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'title'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("quoting/orderby/price", 'Price', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'price'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" ></th>
<th align="center" valign="middle" width="100"> </th>
<th align="center" valign="middle" width="200"> </th>
<th align="center" valign="middle" width="200"> </th>
</tr>


<tr>
<td colspan="8" align="left" valign="middle">
<?php foreach($query->result() as $row):?>
<div id="<?php echo $row->id;?>" style="width:120px; height: 80px;padding:10px;float:left;margin:10px;border:1px solid #909090;" class="bgc item" >
 
  <div style="width:80px; height:80px;float:left;">
<b><?php  echo $row->title;?></b><br />
<?php 
 echo $row->category.br();
echo 'Price: $'.((($row->price_s > 0) && ($row->price > $row->price_s)) ?  $row->price_s : $row->price).br();
// echo  'Price: $'.$row->price.br();
echo  'Stock: '.$row->qty.br();
?>
 </div>
  <div style="width:40px; height:80px;float:right;">
  <?php echo form_open_multipart('quoting/submit', 
  array('class' => 'uploadForm', 'id' => 'uf'.$row->id), 
  array('id' => $row->id, 'name' => $row->title, 'price' => ((($row->price_s > 0) && ($row->price > $row->price_s)) ?  $row->price_s : $row->price) )
  );
?>
  <?php 
  echo form_input(
		  array(
			'name'=>'qty', 
			'id'=>'qty', 
			'value'=> '1',
			'class'       => 'ui-corner-all',
			'style'       => 'width:30px;margin:10px;'
			)
		  );
?>
<span  class="ui_button font9px">
<?php echo form_submit(array('value'=>'Add'));?>
</span>
<?php echo form_close(); ?>
 </div>
<br style="clear:both;"/>
 </div>
<?php endforeach;?>
</td>
</tr>
</table>

</div>
</div>