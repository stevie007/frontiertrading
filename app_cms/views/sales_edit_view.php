<?php echo form_open_multipart('sales/sales_submit', array('id' => 'myform'), array('id' => $sales->id));?>

<div style="float: left; padding:20px 22px;">
<div style="float: left; width:960px;background: #e1e1e1;color:#000000;">


<table border="0" cellpadding="10" cellspacing="0" width="800px" style="margin:30px auto;">

<tr>
<th colspan="2"  align="left"><?php echo $table_title;?></th>
</tr>


<tr>
<th align="left" valign="middle" width="150">&nbsp;</th>
<th align="left" valign="middle">&nbsp;</th>
</tr>
 
<tr>
<th align="left" valign="middle">ID</th>
<td align="left" valign="middle"><?php echo $sales->id;?></td>
</tr>
 
 
<tr class="bgc">
<th align="left" valign="middle">Firstname</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'firstname', 'id' => 'firstname', 'value' => $sales->firstname,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Lastname</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'lastname', 'id' => 'lastname', 'value' => $sales->lastname,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Email</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'email', 'id' => 'email', 'value' => $sales->email,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Phone</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'phone', 'id' => 'phone', 'value' => $sales->phone,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Mobile</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'mobile', 'id' => 'mobile', 'value' => $sales->mobile,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Fax</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'fax', 'id' => 'fax', 'value' => $sales->fax,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Address</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'address', 'id' => 'address', 'value' => $sales->address,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Suburb</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'suburb', 'id' => 'suburb', 'value' => $sales->suburb,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">City</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'city', 'id' => 'city', 'value' => $sales->city,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Postcode</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'postcode', 'id' => 'postcode', 'value' => $sales->postcode,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>


</table>

</div>
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php echo form_submit(array('name'=>'submit', 'value'=>'Save'));?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php echo form_input(array('type' =>'button', 'name' =>'button', 'value'=>'Cancel', 'onclick'=>"window.location.href='".site_url('cms')."'"));?>
</div>

<?php echo form_close(); ?>
