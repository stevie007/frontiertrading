<script>
$(function() {
    $( ".category" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/cate_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  }
	})
      .click(function() {
	  $(this).autocomplete( "search" , '' );
	});

      
  });
</script>
<script type="text/javascript">
$(function() {
    $('.uploadForm').ajaxForm({
        beforeSubmit: function(a,f,o) {
            $('#uploadOutput').html('Submitting...');
        },
        success: function(data) {
            var $out = $('#uploadOutput');
	    var img_path = '<?php echo dirname(base_url()).'/uploads/item_images/';?>';
            $out.html('Form success handler received.');
	    var item = $.parseJSON(data);
     //       $out.append('<span>'+ item.id +'</span>');
	    $('img#'+item.id).attr('src', img_path + item.image1);
	    if (item.special)  $('span#special'+item.id).html('Special');
        }
    });
});

</script> 

<div class="ui_button font10px" id="cates">
 <?php 
$category = isset($category) ? $category : '';

$cates = get_cates();
foreach ($cates as $cate) 
  {
    if (is_array($cate)) 
      {
	$parent = array_shift($cate);
	echo '<span class="link">';
	echo ($parent->name == $category) ? '<button disabled="disabled">'.$parent->name.'</button>' : '<a href="'.site_url('item/cate/'.$parent->name).'" >'.$parent->name.'</a>';  
	echo '</span>';
	foreach ($cate as $sub)
	  {	  
	echo '<span class="link">';
	    echo ($sub->name == $category) ? '<button disabled="disabled">'.$sub->name.'</button>' : '<a href="'.site_url('item/cate/'.$sub->name).'" >'.$sub->name.'</a>';	  
	echo '</span>';
	  }
      }
    else
      {		echo '<span class="link">';
	echo ($cate->name == $category) ? '<button disabled="disabled">'.$cate->name.'</button>' : '<a href="'.site_url('item/cate/'.$cate->name).'" >'.$cate->name.'</a>';
	echo '</span>';
      }
  }
?>
</div>
<div style="float: left; padding:20px 22px;">
<div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

<table border="0" cellpadding="5" cellspacing="0" width="800px" style="margin:30px auto;">

<tr>
<th colspan="7"  align="left"><?php echo $table_title;?></th>
</tr>

<tr class="bgc">
<td colspan="7"  align="left" class="ui_button font9px">
<?php 
$attr = (isset($filter['new'])) ? array('class' => 'white') : array();
echo anchor("item/filter/new/1", 'New Items',$attr);
echo nbs(4);
$attr = (isset($filter['special'])) ? array('class' => 'white') : array();
echo anchor("item/filter/special/1", 'Special Items',$attr);
?>
&nbsp;


<?php 
echo form_open('item/search', array('id' => 'myform', 'style' => 'display:inline;'));
?>
<span class="font11px">
<?php
  echo form_input( 
		  array(
			'name'        => 'search',
			'value'	      => 'Search..',
			'class'       => 'blur  ui-corner-all',
			'style'       => 'width:200px;margin:10px;'
			) 
		   );
?>
</span>
<?php
  echo form_submit(array(
			 'name'        => 'submit',
			 'value'       => 'Go'
			 )
		   );
?>

<?php echo form_close(); ?>
<span class="font12px">
<?php 
if ($search)
{
echo '&nbsp;&nbsp;&nbsp; Listing search results for: '.$search;
}
echo '&nbsp;&nbsp;&nbsp; Total: '.$total;
echo nbs(10);
?>
Ajax Output:
</span>

 <span class="font12px" id="uploadOutput"></span>
</td>
</tr>

<tr class="bgc">
<td colspan="7"  align="right" class="ui_button font9px"><?php echo $this->pagination->create_links(); ?></td>
</tr>

<tr>
<th align="left" valign="middle" width="60" >
Sort By:  
</th>
<th align="left" valign="middle" width="40">
<?php echo anchor("item/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("item/orderby/category", 'Category', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'category'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("item/orderby/title", 'Title', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'title'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php 
echo anchor("item/orderby/price", 'Price', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'price'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="100">
<?php
echo anchor("item/orderby/qty", 'Qty', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'qty'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="200"> </th>
</tr>
</table>
<?php foreach($query->result() as $row):?> 
 
<?php echo form_open_multipart('item/item_submit', array('class' => 'uploadForm'), array('id' => $row->id));?>
<table border="0" cellpadding="5" cellspacing="0" width="800px" style="margin:30px auto;" class="bgc">
<tr >
<th align="left" valign="middle" width="30"><?php echo $row->id;?></th>
<td colspan="4" align="left" valign="middle" >
<?php 
   echo form_input(
		   array(
			 'name'        => 'category',
			 'id'        => 'category',
			 'value'	  => $row->category,
			 'title'=>'Product Category',
			 'class'       => 'category ui-corner-all',
			 'style'       => 'width:100px;margin:10px;'
			 ) 
		   );

?>
 
<?php 
  echo form_input(
		  array(
			'name'=>'title', 
			'id'=>'title', 
			'value'=>$row->title,
			'title'=>'Product Title',
			'class'       => 'ui-corner-all',
			'style'       => 'width:100px;margin:10px;'
			)
		  );
?>
<?php 
  echo form_input(
		  array(
			'name'=>'price', 
			'id'=>'price', 
			'value'=>$row->price,
			'title'=>'Price',
			'class'       => 'ui-corner-all',
			'style'       => 'width:100px;margin:10px;'
			)
		  );
?>

<?php 
  echo form_input(
		  array(
			'name'=>'price_s', 
			'id'=>'price_s', 
			'value'=>$row->price_s,
			'title'=>'Price Special',
			'class'       => 'ui-corner-all',
			'style'       => 'width:100px;margin:10px;'
			)
		  );
?>
<span id="special<?php echo $row->id;?>">
<?php echo $row->special ? 'Special' : '';?>
</span>
</td>
<td align="center" valign="middle" rowspan="2">
<?php
echo img(array(
          'src' =>  dirname(base_url()).'/uploads/item_images/'.$row->image1,
	  'id' => $row->id,
          'alt' => 'Thumb image',
          'height' => '60',
          'title' => 'Thumb image'
));
?>
</td>
<td align="center" valign="middle" class="ui_button font9px"><?php echo anchor("item/del_msg/$row->id/".$offset, 'Del');?>
</td>
</tr>
<tr>
<td>Detail</td>
<td colspan="4"  align="left" class="ui_button font9px" valign="middle">
<?php 

  echo form_input(
		  array(
			'name'=>'detail', 
			'id'=>'detail', 
			'value'=>$row->detail,
			'class'       => 'ui-corner-all',
			'style'       => 'width:340px;margin:10px;'
			)
		  );
?>
Qty
<?php 
  echo form_input(
		  array(
			'name'=>'qty', 
			'id'=>'qty', 
			'value'=>$row->qty,
			'class'       => 'ui-corner-all',
			'style'       => 'width:30px;margin:10px;'
			)
		  );
?>
</td>
<td align="center" valign="middle" ></td>
</tr>

<tr >
<th align="left" valign="middle"> </th>


<td  colspan="5" align="left" valign="middle" class="ui_button font9px" > 
<?php 
echo form_upload('userfile');
echo nbs(3);
?>
 
<?php 
echo form_checkbox($data = array(
				 'name'        => 'new',
				 'id'          => 'new'.$row->id,
				 'value'       => 'accept',
				 'class'       => 'check',
				 'checked'     => $row->new
				 ));
echo form_label('New', 'new'.$row->id);
echo nbs(3);
?>  
<?php /*
echo form_checkbox($data = array(
				 'name'        => 'special',
				 'id'          => 'special'.$row->id,
				 'value'       => 'accept',
				 'class'       => 'check',
				 'checked'     => $row->special
				 ));
echo form_label('Special', 'special'.$row->id);
echo nbs(3);*/
?> 
<?php 
echo form_checkbox($data = array(
				 'name'        => 'online',
				 'id'          => 'online'.$row->id,
				 'value'       => 'accept',
				 'class'       => 'check',
				 'checked'     => $row->online
				 ));
echo form_label('Online', 'online'.$row->id);
echo nbs(3);
?> 
<?php echo form_submit(array('value'=>'Save'));?>
</td> 

<td align="center" valign="middle" class="ui_button font9px"> 
</td>
</tr>
</table>
<?php form_hidden('special', $row->special);
?>
<?php echo form_close(); ?>
<?php endforeach;?>

 

</div>
</div>