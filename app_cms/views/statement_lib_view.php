<script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},



		      });


    });
</script>





<script type="text/javascript">
$(function() {
    $('.uploadForm').ajaxForm({
        beforeSubmit: function(a,f,o) {

        },
        success: function(data) {
	     var statement_print_list = $.parseJSON(data);
	     var htmls = 'Statement Print List, ' + statement_print_list.count + ' items.<br /><br />';

		$.each(statement_print_list.statements, function(key, value) {
			htmls += key + ' - ' + value + '<br />';
		});

	     $('#fnoty #cart-info')
	       .html(htmls)
	       .fadeIn();
        }
    });
});
</script>


<!-- noty -->
   <div id="fnoty">
   <span id="cart-info">
   <?php
   $statement_print_list = $this->session->userdata('statement_print_list');

   echo 'Statement Print List, ' . sizeof($statement_print_list['statements']) . ' items.<br /><br />';

	if (sizeof($statement_print_list['statements']))
	{
		foreach ($statement_print_list['statements'] as $key => $value) {
			echo $key . ' - ' . $value . '<br />';
		}
	}


   ?>
   </span><br /><br />
   <a href="<?php echo site_url('statement/empty_statement_print_queue');?>" class="white font9px">Empty Print List</a>
   <?php echo nbs(30);?>
   <a href="JavaScript:void(0);" onclick="openView('<?php echo site_url('statement/print_selected_statements');?>', 'Print Selected Statements');" class="white font9px">Print</a>
   </div>
<!-- end noty -->







<?php echo form_open('statement/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $customer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      )
		);

?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td align="left" class="font10px">

</td><td align="left" class="font10px">

</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      )
		 );
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>
</table>
<?php echo form_close(); ?>

<div style="float: left; padding:20px 22px;">
<div style="float: left; width:960px;background: #e1e1e1;color:#000000;">


<style type="text/css">

.inactive {
	background-color: LightSteelBlue; color: black;
}

</style>


<table border="0" cellpadding="5" cellspacing="0" width="800px" style="margin:30px auto;">

<tr>
<th colspan="5"  align="left"><?php echo $table_title;?></th>
<th colspan="2">
    <?php
    // Send all emails
    if (pass_auth('admin')) {
        $data = array('class' => 'invo-lib-btn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only','content' => 'Send All', 'onclick'=>"window.location='".site_url("statement/sendBatchMsg")."'");
        echo form_button($data);
    }
    ?>
</th>
</tr>

<tr class="bgc">
<td colspan="7"  align="right" class="ui_button font9px"><?php echo $this->pagination->create_links(); ?></td>
</tr>

<tr>
<th align="left" valign="middle" width="40">
<?php echo anchor("statement/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>

<th align="left" valign="middle"  >
<?php echo anchor("statement/orderby/company", 'Company', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'company'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="100">
<?php echo anchor("statement/orderby/firstname", 'Firstname', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'firstname'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="100" >
<?php echo anchor("statement/orderby/email", 'Email', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'email'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="100" > 
<?php
echo anchor("statement/orderby/balance", 'Balance', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'balance'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;


?>
</th>
<th align="center" valign="middle" width="200">Operations</th>
</tr>

<?php foreach($query->result() as $row):
$class = $row->active ? '' : 'inactive';
?>


<?php echo form_open_multipart('statement/add_statement_to_print',
  array('class' => 'uploadForm', 'id' => 'statement'.$row->id),
  array('statement_id' => $row->id, 'company'=>$row->company)
  );
?>


<tr class="bgc <?php echo $class;?>" id="cust<?php echo $row->id;?>">
<td align="left" valign="middle">
<a target="_blank" href="<?php echo site_url('member/edit/'.$row->id);?>" style="color: #000000;">
<?php echo $row->id;?>
</a>
</td>
<td align="left" valign="middle"><?php echo $row->company;?></td>
<td align="left" valign="middle"><?php echo $row->firstname.' '.$row->lastname;?></td>
<td align="left" valign="middle"><?php echo $row->email?></td>
<td align="right" valign="middle"><?php echo $row->balance;?></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php //echo anchor("member/edit/$row->id", 'Edit').
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("statement/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
?>
<?php
$data = array('class' => 'invo-lib-btn','content' => 'PDF', 'onclick'=>"window.location='".site_url("statement/pdf/$row->id")."'");
echo form_button($data);
?>
<?php
echo form_submit(array('value'=>'Add', 'class'=>'invo-lib-btn'));
?>
&nbsp;&nbsp;&nbsp;&nbsp;
<?php
if (valid_email($row->email))
{
$data = array('class' => 'invo-lib-btn','content' => 'Send', 'onclick'=>"window.location='".site_url("statement/send_msg/$row->id")."'");
}
else
{
$data = array('disabled' => 'disabled','class' => 'invo-lib-btn','content' => 'Send');
}

echo form_button($data);

?>
</td>
</tr>

<?php echo form_close(); ?>

<?php endforeach;?>

</table>

</div>
</div>