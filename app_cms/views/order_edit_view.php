

<script>
$(function() {
    $( "#category" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/cate_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  }
	})
      .click(function() {
	  $(this).autocomplete( "search" , '' );
	});

      
  });
</script>

 <?php echo form_open_multipart('item/item_submit', array('id' => 'myform'), array('id' => $item->id));?>

<div style="float: left; padding:20px 22px;">
   <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">


   <table border="0" cellpadding="10" cellspacing="0" width="800px" style="margin:30px auto;">

   <tr>
   <th colspan="2"  align="left"><?php echo $table_title;?></th>
   </tr>


   <tr>
   <th align="left" valign="middle" width="150">&nbsp;</th>
   <th align="left" valign="middle">&nbsp;</th>
   </tr>
 
   <tr>
   <th align="left" valign="middle">ID</th>
   <td align="left" valign="middle"><?php echo $item->id;?></td>
   </tr>
 
 
   <tr class="bgc">
   <th align="left" valign="middle">Title</th>
   <td align="left" valign="middle">
   <?php 
   echo form_input(
		   array(
			 'name'=>'title', 
			 'id'=>'title', 
			 'value'=>$item->title,
			 'class'       => 'ui-corner-all',
			 'style'       => 'width:200px;margin:10px;'
			 )
		   );
?>
   </td>
   </tr>

   <tr class="bgc">
   <th align="left" valign="middle">Category</th>
   <td align="left" valign="middle">
   <?php 
   echo form_input(
		   array(
			 'name'        => 'category',
			 'id'        => 'category',
			 'value'	  => $item->category,
			 'class'       => 'ui-corner-all',
			 'style'       => 'width:200px;margin:10px;'
			 ) 
		   );

?>
</td>
</tr>

<tr class="bgc">
   <th align="left" valign="middle">Label</th>
   <td align="left" valign="middle" class="ui_button font9px">
   <?php 
   echo form_checkbox($data = array(
				    'name'        => 'boolean1',
				    'id'          => 'boolean1',
				    'value'       => 'accept',
				    'class'       => 'check',
				    'checked'     => $item->boolean1
				    ));
echo form_label('boolean1', 'boolean1');
echo '&nbsp;&nbsp;&nbsp;&nbsp;';
echo form_checkbox($data = array(
				 'name'        => 'boolean2',
				 'id'          => 'boolean2',
				 'value'       => 'accept',
				 'class'       => 'check',
				 'checked'     => $item->boolean2
				 ));
echo form_label('boolean2', 'boolean2');
?>  
	  
	   
</td>
</tr>

<tr class="bgc">
  <th align="left" valign="middle">Image</th>
  <td align="left" valign="middle">
  <?php 
  echo img(array(
		 'src' =>  dirname(base_url()).'/uploads/item_images/'.$item->image_small,
		 'alt' => 'Thumb image',
		 'title' => 'Thumb image'
		 ));
echo '&nbsp;&nbsp;&nbsp;&nbsp;';
echo form_upload('userfile');?>
</td>
</tr>



<tr class="bgc">
  <th align="left" valign="middle">Detail</th>
  <td align="left" valign="middle">
  <?php echo form_textarea(array('name'=>'detail', 'id'=>'detail', 'value'=>$item->detail));?>
  </td>
  </tr>


  <tr class="bgc">
  <th align="left" valign="middle">Detail: <span style="color:red;">member only</span></th>
  <td align="left" valign="middle">
  <?php echo form_textarea(array('name'=>'detail_member', 'id'=>'detail_member', 'value'=>$item->detail));?>
  </td>
  </tr>




  </table>

  </div>
  </div>
  <br style="clear:both">
  <div style="float: left; padding:0 5px;" class="ui_button">
  <?php echo form_submit(array('name'=>'submit', 'value'=>'Save'));?>
  </div>
  <div style="float: right; padding:0 5px;" class="ui_button">
  <?php echo form_input(array('type' =>'button', 'name' =>'button', 'value'=>'Cancel', 'onclick'=>"window.location.href='".site_url('cms')."'"));?>
  </div>

  <?php echo form_close(); ?>

  <script type="text/javascript" src="<?php echo base_url();?>lib/ckeditor/ckeditor.js"></script>
  
  <script type="text/javascript">CKEDITOR.replace( 'detail',{height:"300", width:"600",toolbar : 'Standard600px'});
CKEDITOR.config.contentsCss = '<?php echo base_url();?>css/site.css';
CKEDITOR.config.baseHref = '<?php echo dirname(base_url()).'/sub';?>/';
CKEDITOR.config.filebrowserBrowseUrl = '<?php echo base_url();?>lib/kcfinder/browse.php?type=files';
CKEDITOR.config.filebrowserImageBrowseUrl = '<?php echo base_url();?>lib/kcfinder/browse.php?type=images';
CKEDITOR.config.filebrowserFlashBrowseUrl = '<?php echo base_url();?>lib/kcfinder/browse.php?type=flash';
CKEDITOR.config.filebrowserUploadUrl = '<?php echo base_url();?>lib/kcfinder/upload.php?type=files';
CKEDITOR.config.filebrowserImageUploadUrl = '<?php echo base_url();?>lib/kcfinder/upload.php?type=images';
CKEDITOR.config.filebrowserFlashUploadUrl = '<?php echo base_url();?>lib/kcfinder/upload.php?type=flash';
</script>
<script type="text/javascript">CKEDITOR.replace( 'detail_member',{height:"160", width:"600",toolbar : 'Standard400px'}); </script>

