<script>

var json_url = '<?php echo $json_url;?>';
var post_url = '<?php echo $post_url;?>';
$(function() {
    $( "#customer" )
	.autocomplete({
			minLength: 1,				      
	                select: function() { setTimeout(function() { $('#customer').keyup(); }, 300);},
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_id_names');?>',
		  		   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
			
			
			
		      });
      
 
      
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/payment_edit_view.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>js/phpjs.js"></script>
<div style="padding:10px 0;" id="inner-wrapper">
<div style="width:680px;background: #e1e1e1;color:#000000;" id="inner-text">


<table border="0" cellpadding="5" cellspacing="0" style="width:680px;padding:10px;">
<tr class="bgc">
<td class="ui_button" align="left">
<?php
echo form_label('Select Customer:', 'customer');?>
</td>
<td colspan="5" align="left">
<?php
$attr = array('name' => 'customer', 'id' => 'customer', 'value' => '', 'class' => 'ui-corner-all', 'style' => 'width:300px;') ;
echo form_input($attr);
?>   
</td>
</tr>
<tr class="bgc">
<td class="ui_button" align="left">
<?php
echo form_label('Company:', 'cust_name');?>
</td>
<td colspan="4" align="left">
<?php
$attr = array('name' => 'cust_name', 'id' => 'cust_name', 'value' => '', 'class' => 'ui-corner-all', 'style' => 'width:260px;') ;
echo form_input($attr);
?>   
</td>
<td>
<?php
$attr = array('name' => 'cust_id', 'id' => 'cust_id', 'value' => '', 'class' => 'ui-corner-all', 'readonly' => 'readonly', 'style' => 'width:80px;') ;
echo form_input($attr);
?> 
</td>
</tr>


<tr>
<td align="left">
<?php
echo form_label('Payment ID:', 'id');?>
</td>
<td align="left">
<?php
$attr = array('name' => 'id', 'id' => 'id', 'class' => 'ui-corner-all', 'readonly' => 'readonly', 'style' => 'width:100px;') ;
echo form_input($attr);

?>  
</td>
<td>
<?php
echo form_label('Date:', 'date');?>
</td>
<td>
<?php
$attr = array('name' => 'date', 'id' => 'date', 'class' => 'ui-corner-all', 'readonly' => 'readonly', 'style' => 'width:100px;'); 
echo form_input($attr);

?>
</td>
<td></td><td></td>
</tr>


<tr class="bgc">
<td align="left">
<?php
echo form_label('Subtotal:', 'subtotal');?>
</td>
<td align="left">
<?php
$attr = array('name' => 'subtotal', 'id' => 'subtotal', 'class' => 'ui-corner-all invo', 'readonly' => 'readonly'); 
echo form_input($attr);
?>  
</td>
<td>
<?php
echo form_label('GST:', 'gst');?>
</td>
<td>
<?php
$attr = array('name' => 'gst', 'id' => 'gst', 'class' => 'ui-corner-all invo', 'readonly' => 'readonly'); 
echo form_input($attr);
?>
</td>
<td>
<?php
echo form_label('Total:', 'total');?>
</td>
<td>
<?php
$attr = array('name' => 'total', 'id' => 'total', 'class' => 'ui-corner-all invo'); 
echo form_input($attr);
?>
</td>
</tr>
<tr class="bgc">
<td align="left">
<?php
  echo form_label('Invoice ID:', 'inv_id');?>
</td><td>
<?php
$attr = array('name' => 'inv_id', 'id' => 'inv_id', 'class' => 'hide ui-corner-all invo'); 
echo form_input($attr);
?>  
</td>
<td align="left">
<?php
  //echo form_label('Paid:', 'paid');
?>
</td><td>
<?php
$attr = array('name' => 'paid', 'id' => 'paid', 'class' => 'hide ui-corner-all invo'); 
echo form_input($attr);
?>
</td>
<td>
<?php
  //echo form_label('Debit:', 'debit');
?>
</td><td>
<?php
$attr = array('name' => 'debit', 'id' => 'debit', 'class' => 'hide ui-corner-all invo', 'readonly' => 'readonly'); 
echo form_input($attr);
?>
</td>
</tr>
<tr class="bgc">
<td >
<?php
  //echo form_label('Extra info:', 'info');
?>
</td>
<td colspan="5">
<?php
$attr = array('name' => 'info', 'id' => 'info', 'class' => 'hide ui-corner-all', 'style' => 'width:500px;'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<td>Payment Method: </td>
<td colspan="5">
<div class="ui_buttonset font10px">
<?php
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_cash', 'value'=>'Cash', 'style'=>'margin:10px'));
echo form_label('Cash', 'pmt_cash');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_eftpos', 'value'=>'Eftpos', 'style'=>'margin:10px'));
echo form_label('Eftpos', 'pmt_eftpos');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_cheque', 'value'=>'Cheque', 'style'=>'margin:10px'));
echo form_label('Cheque', 'pmt_cheque');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_banks', 'value'=>'Banks', 'style'=>'margin:10px'));
echo form_label('Banks', 'pmt_banks');
echo form_checkbox(array('name'=>'payment', 'id'=>'pmt_other', 'value'=>'Other', 'style'=>'margin:10px'));
echo form_label('Other', 'pmt_other');
?>
</div>
</td>
</tr>

<tr class="bgc">
<td valign="top">
<?php
echo form_label('Comment:', 'comments');
?>
</td>
<td colspan="5">
<textarea name="comments" id="comments" rows="2" style="width:500px" class="ui-corner-all">

</textarea>
</td>
</tr>
<tr class=" ">
<td valign="top">Operation Logs</td>
<td colspan="5">
<div id="logs" style="width:500px">

</div>

</td>
</tr>
</table>

   
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
echo form_input(array(
			 'type'  => 'button',
			 'name'  => 'save',
			 'id'    => 'save',
			 'value' => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'close',
			 'id'   => 'close',
			 'value' => 'Close'
			 )
		   );
?>
</div>


</div>

