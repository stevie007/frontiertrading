<div style="float: left; padding:20px 22px;">
<div style="float: left; width:960px;background: #e1e1e1;color:#000000;">


<style type="text/css">

.inactive {
	background-color: LightSteelBlue; color: black;
}

</style>
 
		
<table border="0" cellpadding="5" cellspacing="0" width="800px" style="margin:30px auto;">

<tr>
<th colspan="5"  align="left"><?php echo $table_title;?></th>
</tr>

<tr class="bgc">
<td colspan="5"  align="left" class="ui_button font9px">
<?php 
$attr = (isset($filter['active'])) ? array('class' => 'white') : array();
echo anchor("member/filter/active/1", 'Active Member',$attr);
?>


<?php 
echo form_open('member/search', array('id' => 'myform', 'style' => 'display:inline;'));
?>
<span class="font11px">
<?php
  echo form_input( 
		  array(
			'name'        => 'search',
			'value'	      => 'Search..',
			'class'       => 'blur  ui-corner-all',
			'style'       => 'width:200px;margin:10px;'
			) 
		   );
?>
</span>
<?php
  echo form_submit(array(
			 'name'        => 'submit',
			 'value'       => 'Go'
			 )
		   );
?>

<?php echo form_close(); ?>
<span class="font12px">
<?php 
if ($search)
{
echo '&nbsp;&nbsp;&nbsp; Listing search results for: '.$search;
}
echo '&nbsp;&nbsp;&nbsp; Total: '.$total;
?>
</span>
</td>
</tr>

<tr class="bgc">
<td colspan="5"  align="right" class="ui_button font9px"><?php echo $this->pagination->create_links(); ?></td>
</tr>

<tr>
<th align="left" valign="middle" width="40">
<?php echo anchor("member/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="100" >
<?php echo anchor("member/orderby/company", 'Company', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'company'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="100">
<?php echo anchor("member/orderby/firstname", 'Firstname', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'firstname'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>

<th align="left" valign="middle" width="100" >
<?php echo anchor("member/orderby/email", 'Email', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'email'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100" >
<?php echo anchor("member/orderby/telephone", 'Telephone', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'telephone'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100" >
<?php echo anchor("member/orderby/city", 'City', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'city'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="200">Operations</th>
</tr>

<?php foreach($query->result() as $row):
$class = $row->active ? '' : 'inactive';
?>
<tr class="bgc <?php echo $class;?>">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo $row->company;?></td>
<td align="left" valign="middle"><?php echo $row->firstname;?></td>
<td align="left" valign="middle"><?php echo $row->email;?></td>
<td align="left" valign="middle"><?php echo $row->telephone;?></td>
<td align="left" valign="middle"><?php echo $row->city;?></td>
<td align="center" valign="middle" class="ui_button font9px"><?php echo anchor("member/edit/$row->id", 'Edit')
.nbs(2)
.anchor("member/del_msg/$row->id", 'Del');?></td>
</tr>
<?php endforeach;?>

</table>

</div>
</div>