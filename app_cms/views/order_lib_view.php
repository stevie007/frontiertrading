<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>

<div style="float: left; padding:20px 22px;">
<div style="float: left; width:960px;background: #e1e1e1;color:#000000;">


 
		
<table border="0" cellpadding="5" cellspacing="0" width="800px" style="margin:30px auto;">

<tr>
<th colspan="5"  align="left"><?php echo $table_title;?></th>
</tr>

<tr class="bgc">
<td colspan="5"  align="left" class="ui_button font9px">
<!--
<?php 
$attr = (isset($filter['boolean1'])) ? array('class' => 'white') : array();
echo anchor("order/filter/boolean1/1", 'Boolean1',$attr);
?>

<?php 
$attr = (isset($filter['boolean2'])) ? array('class' => 'white') : array();
echo anchor("order/filter/boolean2/1", 'Boolean2',$attr);
?>

<?php 
$attr = (isset($filter['category'])) ? array('class' => 'white') : array();
echo anchor("order/filter/category/category3", 'category3',$attr);
?>
-->
<?php 
echo form_open('order/search', array('id' => 'myform', 'style' => 'display:inline;'));
?>
<span class="font11px">
<?php
  echo form_input( 
		  array(
			'name'        => 'search',
			'value'	      => 'Search..',
			'class'       => 'blur  ui-corner-all',
			'style'       => 'width:200px;margin:10px;'
			) 
		   );
?>
</span>
<?php
  echo form_submit(array(
			 'name'        => 'submit',
			 'value'       => 'Go'
			 )
		   );
?>

<?php echo form_close(); ?>
<span class="font12px">
<?php 
if ($search)
{
echo '&nbsp;&nbsp;&nbsp; Listing search results for: '.$search;
}
echo '&nbsp;&nbsp;&nbsp; Total: '.$total;
?>
</span>
</td>
</tr>

<tr class="bgc">
<td colspan="5"  align="right" class="ui_button font9px"><?php echo $this->pagination->create_links(); ?></td>
</tr>

<tr>
<th align="left" valign="middle" width="40">
<?php echo anchor("order/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="150">
<?php echo anchor("order/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle">
<?php echo anchor("order/orderby/cust_id", 'Cust_Id', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cust_id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="80">Total</th>
<th align="center" valign="middle" width="100">Operations</th>
</tr>

<?php foreach($orders as $row):

$job_status = $row->trans ? '' : 'uf';

?>
<tr class="bgc <?php echo $job_status;?>">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo $row->date;?></td>
<td align="left" valign="middle"><?php echo $row->cust_id.' ('.$row->cust_name.')';?></td>
<td align="right" valign="middle">
<?php echo $row->total;?>
</td>
<td align="center" valign="middle" class="ui_button font9px">
  <?php //echo anchor("order/edit/$row->id", 'Edit').' '.anchor("order/del/$row->id", 'Del');
?>
<?php
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("order/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
?>
</td>
</tr>
<?php endforeach;?>

<tr><td colspan="8" align="left"><br /><br /></td></tr>

<tr><td colspan="8" align="left">

<span class="sample"><span class="uf sam-box">&nbsp;&nbsp;</span> -- Sent but Transferred </span><br />


</td></tr>
</table>

</div>
</div>
