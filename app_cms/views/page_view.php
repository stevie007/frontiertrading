<?php echo form_open('page/page_update', array('id' => 'myform'), array('id' => $page->id));?>

<div style="float: left; padding:20px 22px;">

 
<?php  
    echo form_textarea(array(
			     'name'        => 'text1',
			     'id'          => 'editor1',
			     'value'       => $page->text1,
			     'cols'        => '80',
			     'rows'        => '10'
			     ));
?>

</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php 
  echo form_submit(array(
			 'name'        => 'submit',
			 'value'       => 'Save'
			 )
		   );
?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php 
  echo form_input(array(
			 'type' => 'button',
                         'name' => 'button',
			 'value'       => 'Cancel',
                         'onclick'     => "window.location.href='".site_url('cms')."'"
			 )
		   );
?>
</div>

<?php echo form_close(); ?>

<script type="text/javascript" src="<?php echo base_url();?>lib/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
      CKEDITOR.replace( 'editor1',
			{	height:"600", 
				width:"960",
				extraPlugins : 'uicolor',
				uiColor: '#696969'
			} );
CKEDITOR.config.contentsCss = '<?php echo base_url();?>css/site.css';
CKEDITOR.config.baseHref = '<?php echo dirname(base_url());?>/';
CKEDITOR.config.filebrowserBrowseUrl = '<?php echo base_url();?>lib/kcfinder/browse.php?type=files';
CKEDITOR.config.filebrowserImageBrowseUrl = '<?php echo base_url();?>lib/kcfinder/browse.php?type=images';
CKEDITOR.config.filebrowserFlashBrowseUrl = '<?php echo base_url();?>lib/kcfinder/browse.php?type=flash';
CKEDITOR.config.filebrowserUploadUrl = '<?php echo base_url();?>lib/kcfinder/upload.php?type=files';
CKEDITOR.config.filebrowserImageUploadUrl = '<?php echo base_url();?>lib/kcfinder/upload.php?type=images';
CKEDITOR.config.filebrowserFlashUploadUrl = '<?php echo base_url();?>lib/kcfinder/upload.php?type=flash';
</script>
