<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
<?php $this->load->view('includes/html_head'); ?>

<style>
   body {
 background: #ffffff;
 color:#000000;
 margin:auto;
   text-align: left;
  font-size: 12px;
  font-family: helvetica, arial, sans-serif;
  background-color:#FFF;
width: 700px;
 }
#func-menu{
margin:10px auto 10px auto;
     
padding:5px;
}
button {
margin:0 5px 0 5px;
}
   </style>



   <script type="text/javascript"> 
$(document)
    .ready(function() {
	$('#print').click(function(){
		$('#func-menu').hide();
		window.print();
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});
	$('#close').click(function(){
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});
 
}) 
</script>



   </head>
<body>
<div id="func-menu" class="ui-widget-content ui-corner-all ui_button">
<?php
$data = array('id' => 'print', 'class' => 'font10px flt', 'content' => 'Print');
echo form_button($data);
?>

<?php
$data = array('id' => 'close', 'class' => 'font10px frt', 'content' => 'Close');
echo form_button($data);
?>
<br style="clear:both;"/>
</div>

<h1><?php echo $title;?></h1>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font11px" style="width:200px">

<?php 
echo 'From:'.nbs(4);
echo date('d/m/Y', $date['fr']);
?>
</td><td align="left" class="ui_button font11px" style="width:200px">
<?php
  echo 'To:'.nbs(4);
echo date('d/m/Y', $date['to']);
?>
</td><td align="left" class="ui_button font11px">
<?php 
echo ($customer == 'Customer..') ? '' : 'Customer:'.nbs(4).$customer;

?>
</td><td align="left" class="ui_button font10px"></td></tr>

<tr><td colspan="2" align="left" class="font11px">


</td><td align="left" class="ui_button font11px">
<?php 
  echo ($search == 'Search..') ? '' : 'Search result:'.nbs(4).$search;
?>
</td><td align="left" class="ui_button font10px">
</td></tr>
</table>


		
<table border="0" cellpadding="5" cellspacing="0" width="700px" style="margin:30px auto;">

<tr>
<th colspan="8" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr>
<td colspan="8" align="right" class="ui_button font9px">

</td>
</tr>

<tr>
<th align="left" valign="middle" width="60">
  <?php echo '<span class="flt">ID</span>';
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="120">
<?php  echo '<span class="flt">Date</span>';
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo '<span class="flt">Customer</span>';
if (isset($orderby) && $orderby['order'] == 'cust_name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo '<span class="flt">Purchase Info</span>';
if (isset($orderby) && $orderby['order'] == 'purchase_info'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
<?php echo '<span class="frt">Subtotal</span>';
if (isset($orderby) && $orderby['order'] == 'subtotal'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" width="70">
<?php echo '<span class="frt">Total</span>';
if (isset($orderby) && $orderby['order'] == 'total'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>

</tr>

<?php 
$accu = new stdClass;
$accu->debit = $accu->subtotal = $accu->total = 0;


foreach($query->result() as $row):

$row->debit = $row->total - $row->paid;
$accu->subtotal += $row->subtotal;
$accu->total += $row->total;
$accu->debit += $row->debit;
?> 
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo date('d/m/Y H:i:s', strtotime($row->date));?></td>
<td align="left" valign="middle"><?php echo $row->cust_name;?></td>
<td align="left" valign="middle"><?php echo $row->purchase_info;?></td>
<td align="right" valign="middle"><?php echo number_format($row->subtotal,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo number_format($row->total,2,'.',',');?></td>
</tr>
<?php endforeach;?>

<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="3"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle">Accumulated Total ............</th>
<th align="right" valign="middle"><?php echo number_format($accu->subtotal,2,'.',',');?></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
</tr>
</table>


</body>
</html>
