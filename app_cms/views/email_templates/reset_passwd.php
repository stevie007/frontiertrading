Hi <?php echo $member->firstname;?>,

We have recently started to use our new website http://new.frontiertrading.co.nz/, 

and we've transferred your account to our new system except the password.

Please reset your <?php echo $website;?> password for our new system. 

To complete the process, please follow this link:

<?php echo dirname(base_url());?>/login/recover/<?php echo $member->id;?>/<?php echo $member->resetcode;?>


After reset the password please use your email address <?php echo $member->email;?> and your new password to login.
 

Thanks,
<?php echo $sender;?>