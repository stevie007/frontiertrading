<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">


<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">
 
<script type="text/javascript" src="<?php echo base_url(); ?>/lib/jquery-ui/js/jquery-1.4.2.min.js"></script>

<link href="<?php echo base_url(); ?>lib/jquery-ui/css/trontastic/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>lib/jquery-ui/js/jquery-ui-1.8.6.custom.min.js"></script>
 

<!-- START custom -->
<link href="<?php echo base_url(); ?>css/main.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.functions.js"></script>
<link href="<?php echo base_url(); ?>css/jquery.css" rel="stylesheet" type="text/css" media="screen" />
<!-- END custom -->

   <script type="text/javascript"> 
$(document)
    .ready(function() {
	$('#print').click(function(){
		$('#func-menu').hide();
		window.print();
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});
	$('#close').click(function(){
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});
	$('#transfer').click(function(){
		$('body').fadeOut('fast', function() {
			window.close(); 
			window.opener.location.href = '<?php echo site_url("back_order/trans/$back_order->id");?>';
  		});
	});
 
}) 
</script>


<style>
   body {
 margin:auto;
   text-align: left;
  font-size: 12px;
  font-family: helvetica, arial, sans-serif;
  background-color:#FFF;
width: 700px;

 }
table
{


  font-size: 12px;
  font-family: helvetica, arial, sans-serif;
  background-color:#FFF;
  }

#func-menu{
margin:10px auto 10px auto;
     
padding:5px;
}
button {
margin:0 5px 0 5px;
}
</style> </head>

  <body>
<div id="func-menu" class="ui-widget-content ui-corner-all ui_button">
<?php
$data = array('id' => 'print', 'class' => 'font10px flt', 'content' => 'Print');
echo form_button($data);
?>
<?php
$data = array('id' => 'transfer', 'class' => 'font10px flt', 'content' => 'Transfer');
echo form_button($data);
?>
 
<?php
$data = array('id' => 'close', 'class' => 'font10px frt', 'content' => 'Close');
echo form_button($data);
?>
<br style="clear:both;"/>
</div>

<table width="100%" align="center" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td valign="top">
      <h2>TAX BACK ORDER</h2>
      <p>FRONTIER INTERNATIONAL TRADING LTD<br />
	19B PURIRI ST<br />
	NEW LYNN<br />

	AUCKLAND, 0600</p>
    </td>
    <td valign="top" width="10%" align="right"><img border="0" src="http://www.frontiertrading.co.nz/components/com_virtuemart/shop_image/vendor/Frontier_Int_Tra_4d703bc77f8f8.png" alt="Frontier Int Trading" /></td>
  </tr>
</table>
<table width="100%">
  <tr>
    <td width="100%" align="center">
    </td>

  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="2" width="100%">
  <!-- begin customer information --> 
  <tr class="sectiontableheader"> 
    <th colspan="2" align="left" bgcolor="#CCCCCC">Back Order Information</th>
  </tr>
  <tr> 
    <td>Back Order Number:</td>

    <td><?php echo $back_order->id;?></td>
  </tr>
  
  <tr> 
    <td>Back Order Date:</td>
    <td><?php echo $back_order->date;?></td>
  </tr>
  <tr> 
    <td>Back Order Status:</td>

    <td>Pending</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <!-- End Customer Information --> 
  <!-- Begin 2 column bill-ship to --> 
  <tr class="sectiontableheader"> 
    <th colspan="2" align="left" bgcolor="#CCCCCC">Customer Information</th>

  </tr>
  <tr valign="top"> 
    <td width="50%"> <!-- Begin BillTo -->
      <table width="100%" cellspacing="0" cellpadding="2" border="0">
	<tr> 
	  <td colspan="2"><div align="left"><strong>Back Order To</strong></div></td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Email:</div></td>

	  <td><?php echo !$customer ? '' : $customer->email;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Company Name:</div></td>
	  <td><?php echo !$customer ? '' : $customer->company;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">First Name:</div></td>

	  <td><?php echo !$customer ? '' : $customer->firstname;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Last Name:</div></td>
	  <td><?php echo !$customer ? '' : $customer->lastname;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Address 1:</div></td>

	  <td><?php echo !$customer ? '' : $customer->address;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Address 2:</div></td>
	  <td>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">City:</div></td>

	  <td>	<?php echo !$customer ? '' : $customer->city;?>		</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Postal Code:</div></td>
	  <td><?php echo !$customer ? '' : $customer->postcode;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Country:</div></td>

	  <td><?php echo 'New Zealand';?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Phone:</div></td>
	  <td><?php echo !$customer ? '' : $customer->telephone;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Mobile phone:</div></td>

	  <td>	<?php echo !$customer ? '' : $customer->mobile;?>		</td>
	</tr>
      </table>
      <!-- End BillTo --> </td>
    <td width="50%"> <div align="left">
	<!-- Begin ShipTo --> 
	
      </div>

      <table width="100%" cellspacing="0" cellpadding="2" border="0">

	<tr> 
	  <td colspan="2"><div align="left"><strong>Deliver To</strong></div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Company Name:</td>
	  <td width="65%"><div align="left">
	      <?php echo !$customer ? '' : $customer->company;?>	    </div></td>

	</tr>

	<tr> 
	  <td width="35%" align="left">&nbsp;First Name:</td>
	  <td width="65%"><div align="left">
	      <?php echo !$customer ? '' : $customer->firstname;?>	    </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Last Name:</td>

	  <td width="65%"><div align="left">

	      <?php echo !$customer ? '' : $customer->lastname;?>	    </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Address 1:</td>
	  <td width="65%"><div align="left">
	      <?php echo !$customer ? '' : $customer->address;?>		    </div></td>

	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Address 2:</td>

	  <td width="65%"><div align="left">
	  </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;City:</td>

	  <td width="65%"><div align="left">
	      <?php echo !$customer ? '' : $customer->city;?>		    </div></td>
	</tr>

	<tr> 
	  <td width="35%" align="left">&nbsp;Postal Code:</td>
	  <td width="65%"><div align="left">
              <?php echo !$customer ? '' : $customer->postcode;?>	    </div></td>

	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Country:</td>
	  <td width="65%"><div align="left">

	      New Zealand		    </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Phone:</td>

	  <td width="65%"><div align="left">
	      <?php echo !$customer ? '' : $customer->telephone;?>		    </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Mobile phone:</td>

	  <td width="65%"><?php echo !$customer ? '' : $customer->mobile;?><div align="left">
	  </div></td>
	</tr>

      </table>
      <!-- End ShipTo --> 
      <!-- End Customer Information --> 
    </td>
  </tr>
  <tr> 
    <td colspan="2">&nbsp;</td>

  </tr>
  
  <tr>
    <td colspan="2">&nbsp;</td>

  </tr>
  <!-- Begin Back Order Items Information --> 
  <tr class="sectiontableheader"> 
    <th colspan="2" align="left" bgcolor="#CCCCCC">Back Order Items</th>
  </tr>
  <tr>

    <td colspan="4">
    </td>
  </tr>

  <!-- END HACK EUGENE -->
  <tr> 
    <td colspan="2"> 
      <table width="100%" cellspacing="0" cellpadding="2" border="0">
	<tr align="left"> 
	  <th>ID</th>
	  <th>Name</th>

	  <th>QTY</th>

	  <th>Price</th>
	  <th align="right">Total&nbsp;&nbsp;&nbsp;</th>
	</tr>
	<?php foreach ($back_order->content as $item): ?>
	<tr align="left"> 
	  <td valign="top"><?php echo $item->id; ?></td>
	  <td valign="top"><?php echo $item->name; ?>	          </td>

	  <td valign="top"><?php echo $item->qty; ?></td>
	  <td valign="top">NZD $<?php echo $item->price; ?></td>
	  <td valign="top" align="right">NZD $<?php echo $item->price * $item->qty; ?>&nbsp;&nbsp;&nbsp;</td>
	</tr> 
	<?php endforeach; ?>
	
	
	<tr> 
	  <td colspan="4" align="right">&nbsp;&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>

	<tr> 
	  <td colspan="4" align="right">SubTotal :</td>
	  <td align="right">NZD $<?php echo number_format($back_order->subtotal, 2,'.',','); ?> &nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr> 
	  <td colspan="4" align="right">GST :</td>
	  <td align="right">NZD $<?php echo number_format($back_order->tax, 2,'.',',');?>&nbsp;&nbsp;&nbsp;</td>

	</tr>

	<tr> 
	  <td colspan="3" align="right">&nbsp;</td>
	  <td colspan="2" align="right"><hr/></td>
	</tr>
	<tr> 
	  <td colspan="4" align="right">
	    <strong>Total: </strong>
	  </td>
  
	  <td align="right"><strong>NZD $<?php echo number_format($back_order->total, 2,'.',',');?></strong>&nbsp;&nbsp;&nbsp;</td>

	</tr>
	<tr> 
	  <td colspan="3" align="right">&nbsp;</td>
	  <td colspan="2" align="right"><hr/></td>
	</tr>
	<tr> 
	  <td colspan="3" align="right">&nbsp;</td>
	  <td colspan="2" align="right">&nbsp;&nbsp;&nbsp;</td>

	</tr>
      </table>

    </td>
  </tr>
</table>
<div align="right"><!-- End Back Order Items Information --> 
  
  <strong>PLEASE NOTE:</strong>Delivery costs are not included in your Back Order Total<br />
  
  <!-- Begin Payment Information --> 
  
</div>

<table width="100%">
  <tr class="sectiontableheader"> 
    <th align="left" colspan="2">Payment Information</th>

  </tr>
  <tr> 
    <td width="20%">Payment Method :</td>
    <td>On Invoice </td>
  </tr>

  <!-- end payment information --> 
</table>

<table width="100%">
  <tr class="sectiontableheader"> 
    <th align="left" colspan="2"></th>

  </tr>
  <tr> 
    <td width="20%"></td>
    <td> </td>
  </tr>


</table> 

  </body> 
</html>


