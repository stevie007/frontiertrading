<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
      <style>
	body {
	background-color:#ffffff;
	text-align: center; font-family:arial,sans-serif;
	}
	table {
	border-collapse: collapse; 
	clear:both;
	width: 700px;
	}
	table td,
	table th {
	padding: 0; 
	} 
	.border-hdr th {
	border:1px solid #000000;
	padding: 0 5px;
	}
	.border-cnt td {
	border-left:1px solid #000000;
	border-right:1px solid #000000;
	padding: 0 5px;
	}
	.border-btm td {
	border-bottom:1px solid #000000;
	border-left:1px solid #000000;
	border-right:1px solid #000000;
	padding: 0 5px;
	}
	.title {
	font-size: 11px;
	}
	.title h2 {font-size: 18px;}
	.info {
	font-size:14px;
	font-weight: 900;
	}
        
        .invoice-image {
            display: <?php 
            if(isset($_GET['show']) && $_GET['show']=='yes') {
                echo "block";
            } else {
                echo "none";
            }
            ?>;
        }
      </style>

    </head>
    <body>
      
      <table border="0" align="center"> 
	<tbody>
	  <tr>
	    <td colspan="4"  align="center" class="title"><h2>FRONTIER INTERNATIONAL TRADING LTD.</h2>
	      PO Box 48191 Blockhouse Bay, Auckland, NZ<br />
	      Ph: 09-9293668/021-918965 Fax: 09-8276468<br />
	      Website: www.frontiertrading.co.nz<br />
	      Email: sales@frontiertrading.co.nz<br /> 
	      GST Reg. Number: 91-582-252<br />
	    </td>
	  </tr>
	  <tr>
	    <td colspan="4"> <br />
	    </td>
	  </tr>
	  <tr class="info">
	    <td width="10%">&nbsp;
	    </td>
	    <td align="left" width="50%">
	       
	      <?php echo !$customer ? '' : ucwords($customer->company).br();?> 
	      <?php echo !$customer ? '' : ucwords($customer->address.br());?> 
	      <?php echo !$customer ? '' : ($customer->suburb) ? ucwords($customer->suburb.br()) : '';?>
	      <?php echo !$customer ? '' : ucwords($customer->city).br();?> 
	      <?php echo !$customer ? '' : $customer->telephone;?> 
	    </td>
	    <td align="left" width="25%">
	      <?php echo ($invoice->total >= 0) ? 'Tax' : 'Credit';?> Invoice No:<br />
	      Invoice Date:<br />
	      P/O No:<br />
	      <br />
	      <br />
	      <br />
	    </td>
	    <td align="left" width="15%">
	      <?php echo $invoice->id;?><br />
	      <?php echo $invoice->date;?><br />
	      <?php echo $invoice->purchase_info;?><br />
	      <br />
	      <br />
	      <br />
	    </td>
	  </tr>
	</tbody>
      </table>

      <table border="0" align="center"> 
	<tbody>
	  <tr>
	    <td align="left"><br /> 
	    </td>
	  </tr>
	</tbody>
      </table>

 
      <table align="center" id="invoices-table" <?php 
            if(isset($_GET['show']) && $_GET['show']=='yes') {
                echo "class='show-img'";
            }
            ?>> 
	<tbody>
	  <tr class="border-hdr">
	    <th width="10%" align="right">QTY.</th>
	    <th align="left">DESCRIPTION</th>
	    <th class="invoice-image">IMAGE</th>
	    <th width="15%">PRICE</th>
	    <th width="10%">DISC</th>
	    <th width="15%">AMOUNT</th>
	  </tr>
	  
<?php 
$count = 0;

foreach ($invoice->content as $item): ?>
	  <tr class="border-cnt"  style="border-bottom: 1px solid #000;">
	    <td align="right"><?php echo $item->qty; ?></td>
            <td align="left"><?php echo $item->name; ?></td>
            <td align="center" class="invoice-image"><img class="invoice_thumbnail" alt="N/A" style="width: 40px; margin: 10px 0px; display: block" src="<?php 
                if (property_exists($item, 'img')){
                    echo '/uploads/item_images/' . $item->img;
                }
                
            ?>" ></td>
	    <td align="right">$<?php echo number_format($item->price, 2,'.',','); ?></td>
	    <td align="right">&nbsp;</td>
            <td align="right">$<?php echo number_format($item->price * $item->qty, 2,'.',',');?></td>
	  </tr> 	
<?php 

$count++;
endforeach; ?>


<?php if ($invoice->bk_order_id) {?>
	  <tr class="border-cnt">
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td><br />BACKORDERED #<?php echo $bk_order->id;?><br /><br /></td>
	    <td></td>
	    <td class="invoice-image">&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>  

<?php foreach ($bk_order->content as $item): ?>
	  <tr class="border-cnt">
	    <td align="right"><?php echo $item->qty; ?></td>
	    <td align="left"><?php echo $item->name; ?></td>
	    <td align="left"><?php echo $item->name; ?></td>
	    <td align="right"> </td>
	    <td align="right" class="invoice-image">&nbsp;</td>
	    <td align="right"> </td>
	  </tr> 	
<?php 
$count++;
endforeach; ?>
<?php } ?>


<?php if ($invoice->discount != 0) { ?>
	<tr class="border-cnt">
	  
	    <td>&nbsp;</td>
	  <td >&nbsp;&nbsp;&nbsp;&nbsp;PROMOTION: ( Discount: <?php echo $invoice->discount;?>% off )</td>
	 
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td class="invoice-image">&nbsp;</td>
	  <td  align="right"> -$<?php echo number_format($invoice->subtotal / (1 - $invoice->discount / 100) - $invoice->subtotal, 2, '.', ',') ?></td>
	</tr>
<?php } ?>

<?php 
for ( $i = 0; $i <= 24 - $count; $i++) { ?>
	  <tr class="border-cnt">
	    <td style="color:white;">.</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td class="invoice-image">&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>     
<?php } ?>
 
	  <tr class="border-btm">
	    <td style="color:white;">.</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td class="invoice-image">&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>
	</tbody>
      </table>
 
      <table border="0" align="center"> 
	<tbody>
	  <tr class="title">
	    <td align="left">
	      The above stocks are the properties of Frontier International Trading Ltd before the full payment is made.<br /><br />
	    </td>
	  </tr>
	</tbody>
      </table>
      
      <table border="0" align="center"> 
	<tbody>
	  <tr>
	    <td align="left" width="70%">
	      Payable to: Frontier International Trading Ltd.<br />
	      PO Box 48191 Blockhouse Bay, Auckland<br />
	      New Zealand 0644<br />
	      Account No. 12-3233-0212083-00<br /><br />
	      <br />
	    </td>
	    <td align="right" width="15%">
	      SALE:<br />
	      FREIGHT:<br />
	      GST:<br />
	      TOTAL:<br /><br />

	      <strong>DUE DATE</strong>:<br />
	    </td>
	    <td align="right" width="15%">
	      $<?php echo number_format($invoice->subtotal, 2,'.',','); ?><br />
	      $<?php echo number_format($invoice->freight, 2,'.',','); ?><br />
	      $<?php echo number_format($invoice->tax, 2,'.',',');?><br />
	      <strong>$<?php echo number_format($invoice->total, 2,'.',',');?></strong><br /><br />
	      <?php echo $invoice->duedate;?><br />
	    </td>
	  </tr>
	</tbody>
      </table>
      

      
        <script type="text/javascript">
            (function($){
                var _$images = $(".invoice-image"),
                    _$showButton = $("#show-invoice-img"),
                    _$table = $("#invoices-table");

                _$showButton.click(function(){
                    if (_$table.hasClass('show-img')) {
                        _$images.hide();
                        _$table.removeClass('show-img');
                        _$showButton.find("span").html('Show Image');
                    } else {
                        _$images.show();
                        _$table.addClass('show-img');
                        _$showButton.find("span").html('Hide Image');
                    }
                    
                });
                
            })(jQuery);
        </script>
    </body> 
  </html>