<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">

      <style>
	body {
	background-color:#ffffff;
	text-align: center; font-family:arial,sans-serif;
	}
	table {
	border-collapse: collapse; 
	clear:both;
	width: 700px;
	}
	table td,
	table th {
	padding: 0; 
	} 
	.border-hdr th {
	border:1px solid #000000;
	padding: 0 5px;
	}
	.border-cnt td {
	border-left:1px solid #000000;
	border-right:1px solid #000000;
	padding: 0 5px;
	}
	.border-btm td {
	border-bottom:1px solid #000000;
	border-left:1px solid #000000;
	border-right:1px solid #000000;
	padding: 0 5px;
	}	
	.title {
	font-size: 11px;
	}
	.title h2 {font-size: 18px;}
	.info {
	font-size:14px;
	font-weight: 900;
	
	}
      </style>

      

    </head>
    <body>
      
      <table border="0"> 
	<tbody>
	  <tr>
	    <td colspan="4" class="title" align="center"><h2>FRONTIER INTERNATIONAL TRADING LTD.</h2>
	      PO Box 48191 Blockhouse Bay, Auckland, NZ<br />
	      Ph: 09-9293668/021-918965 Fax: 09-8276468<br />
	      Website: www.frontiertrading.co.nz<br />
	      Email: sales@frontiertrading.co.nz<br />
	      GST Reg. Number: 91-582-252<br />
	    </td>
	  </tr>

	  <tr>
	    <td colspan="4"> <br />
	    </td>
	  </tr>
	  <tr class="info">
	    <td width="10%">&nbsp;
	    </td>
	    <td align="left" width="50%">
 
	      <?php echo ucwords($customer->company);?><br />
	      <?php echo ucwords($customer->address);?>	<br />
	      <?php echo $customer->suburb ? ucwords($customer->suburb.br()) : '';?>	 
	      <?php echo ucwords($customer->city);?><br />
	      <?php echo $customer->telephone;?><br />
	       <br />

	    </td>
	    <td align="left" width="15%">
	      <br />
	       <br />
	       <br />
	      <br />
	      <br />

	      <br />
	    </td>
	    <td align="center" width="25%">
	      STATEMENT<br />
	      DATE<br />
	      <?php echo date("d/m/Y", $date['to']);?><br />
	      <br />
	      <br />

	      <br />
	    </td>
	  </tr>
	</tbody>
      </table>

      <table border="0" align="center"> 
	<tbody>
	  <tr>
	    <td align="left"><br /> 
	    </td>

	  </tr>
	</tbody>
      </table>


<?php 
$accu = $accu60plus = $accu30_60 = $accu30 = $accu0 = $accu_all = $deduction;
?>
       
<br style="clear:both;" />
<br style="clear:both;" />
   <table align="center"> 
	<tbody>
	  <tr class="border-hdr">
	    <th width="10%" align="left">DATE</th>
	    <th width="15%">TAX INVOICE</th>
	    <th width="30%">DESCRIPTION</th>
	    <th width="15%">CHARGES</th>
	    <th width="15%">PAYMENTS</th>
	    <th width="15%">BALANCE</th>
	  </tr>
      <tr class="border-hdr">
          <th width="85%" colspan="5" align="left">PREVIOUS BALANCE</th>
          <th width="15%"><?php echo '$'.$deduction; ?></th>
      </tr>
<?php 

$count = 0;
foreach ($transactions as $data): 

if ($data->type == 'invoice')
  {
    $accu = $data->total;
  } 
if ($data->type == 'payment')
  {
    $accu = -$data->total;
  }
$accu_all += $accu;

if (strtotime($data->date) < strtotime("-90 days", $date['to']))
  $accu60plus += $accu;
else if (strtotime($data->date) < strtotime("-60 days", $date['to']))
  $accu30_60 += $accu;
else if (strtotime($data->date) < strtotime("-30 days", $date['to']))
  $accu30 += $accu;
else
  $accu0 += $accu;

?>
	  <tr class="border-cnt">
	    <td><?php echo date("d/m/Y", strtotime($data->date)); ?></td>
	    <td><?php echo $data->type == 'invoice' ? $data->id : '';?></td>
	    <td style="color: <?php echo ($data->total >= 0) ? '#000000' : 'red';?>">
	    <?php echo ($data->type == 'invoice')&&($data->total < 0)  ? 'Credit ' : '';?>
<?php echo ucfirst($data->type);?>
	    </td>
	    <td style="color: <?php echo ($data->total >= 0) ? '#000000' : 'red';?>">
	    <?php 
					       if ($data->type == 'invoice')
						 {						   
						   echo '$'.$data->total; 
						 }
 ?></td>
	    <td>
	    <?php 
					       if ($data->type == 'payment')
						 {
						   echo '$'.$data->total;
						 }
 ?>
 </td>

	    <td>$<?php echo number_format($accu_all, 2,'.',',');?></td>
	  </tr> 
<?php 
$count++;
endforeach; 
	$acc_subtotal = $accu0 / 1.15;
	$acc_gst = $acc_subtotal * 0.15;
?>


<?php 
for ( $i = 0; $i <= 18 - $count; $i++) { ?>
	  <tr class="border-cnt">
	    <td style="color:white;">.</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>     
<?php } ?>

   
	  <tr class="border-btm">
	    <td style="color:white;">.</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr> 
	 
	  <tr class="border-hdr">
 
	    <th width="10%"  style="border:none;"> </th>
	    <th width="15%" style="border:none;">   </th>
	    <th width="30%" style="border:none;"> </th>
	    <th width="15%" style="border:none;"> </th>
 
	    <th width="15%" style="border-top:none;">AMOUNT DUE</th>
	    <th  style="border-top:none;" width="15%">$<?php echo number_format($accu_all, 2, '.', ',');?></th>
	  </tr>
	 
	</tbody>
      </table>	
      <table border="0" align="center"> 
	<tbody>
<tr><td colspan="2">&nbsp;</td></tr>
	  <tr>
	    <td align="left" width="70%">
	      Payable to: Frontier International Trading Ltd.<br />
	      PO Box 48191 Blockhouse Bay, Auckland<br />
	      New Zealand 0644<br />
	      Account No. 12-3233-0212083-00<br />
              Please quote your Invoice No. as reference when you pay<br />
	      <br />

	    </td>
	    <td align="right" width="30%">

	      <?php echo ucwords($customer->company);?><br />
	      <?php echo ucwords($customer->address);?>	<br />
	      <?php echo $customer->suburb ?  ucwords($customer->suburb.br()) : '' ;?> 
	      <?php echo ucwords($customer->city);?><br />
	      <?php echo $customer->telephone;?><br />
	    </td> 
	  </tr>
	</tbody>
      </table>
      
    <table border="0" align="center"> 
	<tbody>

	  <tr>
	    <td align="left">
	      If you have paid after the statement date please ignore this statement.<br /><br />
	    </td>
	  </tr>
	</tbody>
      </table>
      

    </body> 
  </html> 