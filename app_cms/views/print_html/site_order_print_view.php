
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td valign="top">
      <h2>Purchase Order</h2>
      <p>Company LTD<br />Address<br />Address<br />AUCKLAND</p>
    </td>
    <td valign="top" width="10%" align="right"></td>
  </tr>
</table>

<!-- begin order information --> 
<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr> 
    <th colspan="2" align="left" bgcolor="#CCCCCC">Order Information</th>
  </tr>
  <tr> 
    <td>Order Number:</td>
    <td><?php echo $site_order->id;?></td>
  </tr>
  
  <tr> 
    <td>Order Date:</td>
    <td><?php echo $site_order->date;?></td>
  </tr>
  <!-- End order Information --> 

  <!-- Begin Customer Information --> 
  <tr> 
    <th colspan="2" align="left" bgcolor="#CCCCCC">Customer Information</th>
  </tr>
  <tr valign="top"> 
    <td width="50%"> <!-- Begin BillTo -->
      <table width="100%" cellspacing="0" cellpadding="2" border="0">
	<tr> 
	  <td colspan="2"><strong>Invoice To</strong></td>
	</tr>
	<tr> 
	  <td align="left">Company Name:</td>
	  <td><?php echo '';?></td>
	</tr>
	<tr> 
	  <td align="left">First Name:</td>

	  <td><?php echo !$customer ? '' : $customer->firstname;?></td>
	</tr>
	<tr> 
	  <td align="left">Last Name:</td>
	  <td><?php echo !$customer ? '' : $customer->lastname;?></td>
	</tr>
	<tr> 
	  <td align="left">Address:</td>
	  <td><?php echo !$customer ? '' : $customer->address;?></td>
	</tr>
	<tr> 
	  <td align="left">Suburb:</td>
	  <td>	<?php echo !$customer ? '' : $customer->suburb;?></td>
	</tr>
	<tr> 
	  <td align="left">City:</td>
	  <td><?php echo !$customer ? '' : $customer->city;?></td>
	</tr>
	<tr> 
	  <td align="left">Postal Code:</td>
	  <td><?php echo !$customer ? '' : $customer->postcode;?></td>
	</tr>
	<tr> 
	  <td align="left">Country:</td>
	  <td><?php echo 'New Zealand';?></td>
	</tr>
	<tr> 
	  <td align="left">Phone:</td>
	  <td><?php echo !$customer ? '' : $customer->phone;?></td>
	</tr>
	<tr> 
	  <td align="left">Mobile phone:</td>
	  <td>	<?php echo !$customer ? '' : $customer->mobile;?></td>
	</tr>
	<tr> 
	  <td align="left">Email:</td>
	  <td><?php echo !$customer ? '' : $customer->email;?></td>
	</tr>
      </table>
      <!-- End BillTo --> </td>

    <td width="50%"> 
	<!-- Begin ShipTo --> 
      <table width="100%" cellspacing="0" cellpadding="2" border="0">

	<tr> 
	  <td colspan="2"><strong>Deliver To</strong></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Company Name:</td>
	  <td width="65%"></td>

	</tr>

	<tr> 
	  <td width="35%" align="left">&nbsp;First Name:</td>
	  <td width="65%"><?php echo !$customer ? '' : $customer->firstname;?></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Last Name:</td>
	  <td width="65%"><?php echo !$customer ? '' : $customer->lastname;?></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Address:</td>
	  <td width="65%"><?php echo !$customer ? '' : $customer->address;?></td>

	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Suburb:</td>
	  <td width="65%"><?php echo !$customer ? '' : $customer->suburb;?></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;City:</td>
	  <td width="65%"><?php echo !$customer ? '' : $customer->city;?></td>
	</tr>

	<tr> 
	  <td width="35%" align="left">&nbsp;Postal Code:</td>
	  <td width="65%"><?php echo !$customer ? '' : $customer->postcode;?></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Country:</td>
	  <td width="65%">New Zealand</td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Phone:</td>
	  <td width="65%"><?php echo !$customer ? '' : $customer->phone;?></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Mobile phone:</td>
	  <td width="65%"><?php echo !$customer ? '' : $customer->mobile;?>
	  </td>
	</tr>
      </table>
      <!-- End ShipTo --> 

    </td>
  </tr>
      <!-- End Customer Information --> 
  <tr><td colspan="2">&nbsp;</td></tr>
  
  <!-- Begin Order Items Information --> 
  <tr><th colspan="2" align="left" bgcolor="#CCCCCC">Order Items</th></tr>
  <tr><td colspan="2"></td></tr>

  <!-- END HACK EUGENE -->
  <tr> 
    <td colspan="2"> 
      <table width="100%" cellspacing="0" cellpadding="2" border="0">
	<tr> 
	  <th align="left">ID</th>
	  <th align="left">Name</th>
	  <th align="right">QTY</th>
	  <th align="right">Price</th>
	  <th align="right">Total&nbsp;&nbsp;&nbsp;</th>
	</tr>

	<?php foreach ($site_order->content as $item): ?>
	<tr> 
	  <td align="left"><?php echo $item->id;?></td>
	  <td align="left"><?php echo $item->name;?></td>
	  <td align="right"><?php echo $item->qty;?></td>
	  <td align="right">NZD $<?php echo $item->price;?></td>
	  <td align="right">NZD $<?php echo $item->subtotal;?>&nbsp;&nbsp;&nbsp;</td>
	</tr> 
	<?php endforeach;?>
		
	<tr> 
	  <td colspan="4" align="right">&nbsp;&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>

	<tr> 
	  <td colspan="4" align="right">Subtotal :</td>
	  <td align="right">NZD $<?php echo number_format($site_order->subtotal, 2,'.',','); ?>&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr> 
	  <td colspan="4" align="right">GST :</td>
	  <td align="right">NZD $<?php echo number_format($site_order->gst, 2,'.',',');?>&nbsp;&nbsp;&nbsp;</td>

	</tr>

	<tr> 
	  <td colspan="3" align="right">&nbsp;</td>
	  <td colspan="2" align="right"><hr/></td>
	</tr>
	<tr> 
	  <td colspan="4" align="right"><strong>Total: </strong></td>
	  <td align="right"><strong>NZD $<?php echo number_format($site_order->total, 2,'.',',');?></strong>&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr> 
	  <td colspan="3" align="right">&nbsp;</td>
	  <td colspan="2" align="right"><hr/></td>
	</tr>
	<tr> 
	  <td colspan="5" align="right">&nbsp;</td>
	</tr>
      </table>

    </td>
  </tr>
</table>

  
  
  
  <!-- Begin Payment Information --> 

<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr> 
    <td align="left" colspan="2"></td>
  </tr>
  <tr> 
    <td align="left" colspan="2">Payment Information</td>
  </tr>
  <tr> 
    <td width="20%">Payment Method :</td>
    <td></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr> 
    <th align="left" colspan="2">Customer's Note</th>
  </tr>
  <tr> 
    <td width="20%"><?php echo $site_order->note;?></td>
    <td> </td>
  </tr>
</table>
