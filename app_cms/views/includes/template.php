<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<?php $this->load->view('includes/html_head'); ?>

<body>
<div id="wrapper" class="ui-widget">

<!-- START HEADER -->
<?php $this->load->view('includes/header'); ?>
<!-- END HEADER -->


<!-- START CONTENT -->
<div id="content-wrapper" class="ui-widget-content"> 

<div id="navigation">
   <?php if (isset($menu1)) $this->load->view('includes/menu_1'); ?>
   <?php if (isset($menu2)) $this->load->view('includes/menu_2'); ?>
   <?php if (isset($menu3)) $this->load->view('includes/menu_3'); ?>
   <?php if (isset($menu4)) $this->load->view('includes/menu_4'); ?>
   <?php if (isset($menu5)) $this->load->view('includes/menu_5'); ?>
<?php $this->load->view('includes/nav_menu'); ?>
</div>


<!-- START CONTENT INNER -->
<div id="content-inner">
<div style="float:left; padding:0 5px; clear:both;">
<h2 ><?php echo $title;?></h2>
</div>

<?php $this->load->view('includes/session_msg'); ?>

<br style="clear:both;">

<?php $this->load->view($main_content); ?>


<br style="clear:both;"><br />
</div>
<!-- END CONTENT INNER -->

<br style="clear:both;"/><br />
<br /><br /><br /><br />
</div>
<!-- END CONTENT -->

<?php $this->load->view('includes/footer'); ?>

</div>
</body>
</html>
