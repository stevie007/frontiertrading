<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Feslen CMS 2011 - 3A WEB SOLUTION LTD</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

   <link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico" />
 
   <script type="text/javascript" src="<?php echo base_url(); ?>/lib/jquery-ui/js/jquery-1.4.2.min.js"></script>
   <link href="<?php echo base_url(); ?>lib/jquery-ui/css/trontastic/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" media="screen" />
   <script type="text/javascript" src="<?php echo base_url(); ?>lib/jquery-ui/js/jquery-ui-1.8.6.custom.min.js"></script>

   <!-- START custom -->

   <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.functions.js"></script>
   <link href="<?php echo base_url(); ?>css/jquery.css" rel="stylesheet" type="text/css" media="screen" />
   <!-- END custom -->
<style>
      body {
      background-color:#ffffff;      
      font-family: Arial, Helvetica, sans-serif;
      font-size: 13px;
      }
      table {
      margin-top: 10px;
      border-collapse: collapse; clear:both;
      }
      table td,
      table th {
      padding: 0; 
      } 
#func-menu{
margin:10px 0;
padding:5px;
}
button {
margin:0 5px 0 5px;
}
</style>
<script type="text/javascript">
  $(document).ready(function(){
      /*
      $('table').dblclick(function(){
	  $('#func-menu').hide();
	  window.print();
	  window.close();
	});
      */
      $('#print').click(function(){
	  $('#func-menu').hide();
	  window.print();
	  $('body').fadeOut('fast', function() {
	      window.close();
	    });
	});
      $('#close').click(function(){
	  $('body').fadeOut('fast', function() {
	      window.close();
	    });
	});
	$('#pay').click(function(){
		$('body').fadeOut('fast');
		window.location.href = '<?php echo site_url("payment/trans/$invoice->id");?>';		
	});
      
    });
</script>
</head>
<body>
<div id="wrapper">
<div id="func-menu" class="ui-widget-content ui-corner-all ui_button">
<button name="" type="button" id="print" class="font10px flt" >Print</button>
  <span class="flt">&nbsp;&nbsp;</span>
<?php
$data = array('id' => 'pay', 'class' => 'font10px flt', 'content' => 'Payment');
echo form_button($data);
?>
<button id="show-invoice-img">
    <?php 
            if(isset($_GET['show']) && $_GET['show']=='yes') {
                echo "Hide Image";
            } else {
                echo "Show Image";
            }
            ?></button>
<button name="" type="button" id="close" class="font10px frt" >Close</button><br style="clear:both;"/>
</div>

<?php $this->load->view($main_content); ?>

</div>
</body>
</html>
