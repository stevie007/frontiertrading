<h3>Order, Quoting, Invoice &amp; Payment Management</h3>


<div id="radio_menu3a" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu3a', 'id'=>'radio_menu3a_1', 'onclick'=>"window.location.href='".site_url('order')."'"));
echo form_label('Orders', 'radio_menu3a_1');

echo form_radio(array('name'=>'radio_menu3a', 'id'=>'radio_menu3a_3', 'onclick'=>"window.location.href='".site_url('back_order')."'"));
echo form_label('Back Orders', 'radio_menu3a_3');
 
?>
</div>


<div id="radio_menu7" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu7', 'id'=>'radio_menu7_1', 'onclick'=>"window.location.href='".site_url('quoting')."'"));
echo form_label('Quoting', 'radio_menu7_1');
?>
</div>


<div id="radio_menu4" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu4', 'id'=>'radio_menu4_1', 'onclick'=>"window.location.href='".site_url('invoice')."'"));
echo form_label('Invoices', 'radio_menu4_1');
echo form_radio(array('name'=>'radio_menu4', 'id'=>'radio_menu4_2', 'onclick'=>"openView('".site_url('invoice/add')."','add');"));
echo form_label('&nbsp;<span class="ui-icon ui-icon-plus"></span>', 'radio_menu4_2');
echo form_radio(array('name'=>'radio_menu4', 'id'=>'radio_menu4_3', 'onclick'=>"window.location.href='".site_url('invo_trash')."'"));
echo form_label('Trash', 'radio_menu4_3');


?>
    
    
</div>

<div id="radio_menu5" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu5', 'id'=>'radio_menu5_1', 'onclick'=>"window.location.href='".site_url('payment')."'"));
echo form_label('Payments', 'radio_menu5_1');
echo form_radio(array('name'=>'radio_menu5', 'id'=>'radio_menu5_2', 'onclick'=>"openView('".site_url('payment/add')."','add');"));
echo form_label('&nbsp;<span class="ui-icon ui-icon-plus"></span>', 'radio_menu5_2');
echo form_radio(array('name'=>'radio_menu5', 'id'=>'radio_menu5_3', 'onclick'=>"window.location.href='".site_url('pmt_trash')."'"));
echo form_label('Trash', 'radio_menu5_3');
?>
</div>

<div id="radio_menu6" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu6', 'id'=>'radio_menu6_1', 'onclick'=>"window.location.href='".site_url('statement')."'"));
echo form_label('Customer Statments', 'radio_menu6_1');
?>
</div>
<?php if (pass_auth('administrator')): ?>

<?php endif; ?>