  <head>
    <title>Feslen CMS 2011 - 3A WEB SOLUTION LTD</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">
 
<script type="text/javascript" src="<?php echo base_url(); ?>/lib/jquery-ui/js/jquery-1.4.2.min.js"></script>

<link href="<?php echo base_url(); ?>lib/jquery-ui/css/trontastic/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>lib/jquery-ui/js/jquery-ui-1.8.6.custom.min.js"></script>


<!-- START custom -->
<link href="<?php echo base_url(); ?>css/main.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.functions.js"></script>
<link href="<?php echo base_url(); ?>css/jquery.css" rel="stylesheet" type="text/css" media="screen" />
<!-- END custom -->



<script type="text/javascript">

$(document).ready(function(){
/** start doc ready **/
<?php 
$logged_in = $this->session->userdata('is_logged_in');
?>


<?PHP if(!$logged_in) { ?>
  $(function () {
    	$('.buttonset').delay(600).animate({"opacity": 0.6});
  });
<?php } ?>


/** end doc ready **/
});

function openView(url, name) {
    window.open(url, name, "height=780,width=730,location=no, status=no, resizable=no, scrollbars=yes, toolbar=no");
}
</script>



<script type="text/javascript" src="<?php echo base_url();?>lib/jquery.form.js"></script> 


  </head>