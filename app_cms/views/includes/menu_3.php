<h3>Customer, Salesperson</h3>

<div id="radio_menu3" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_1', 'onclick'=>"window.location.href='".site_url('member')."'"));
echo form_label('Customers', 'radio_menu3_1');
echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_2', 'onclick'=>"window.location.href='".site_url('member/add')."'"));
echo form_label('&nbsp;<span class="ui-icon ui-icon-plus"></span>', 'radio_menu3_2');
echo form_radio(array('name'=>'radio_menu3', 'id'=>'radio_menu3_3', 'onclick'=>"window.location.href='".site_url('member/edit')."'"));
echo form_label('&nbsp;<span class="ui-icon ui-icon-pencil"></span>', 'radio_menu3_3');
?>
</div>

<?php if (pass_auth('administrator')): ?>
<div id="radio_menu3b" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu3b', 'id'=>'radio_menu3b_1', 'onclick'=>"window.location.href='".site_url('sales')."'"));
echo form_label('Salespersons', 'radio_menu3b_1');
echo form_radio(array('name'=>'radio_menu3b', 'id'=>'radio_menu3b_2', 'onclick'=>"window.location.href='".site_url('sales/add')."'"));
echo form_label('&nbsp;<span class="ui-icon ui-icon-plus"></span>', 'radio_menu3b_2');
echo form_radio(array('name'=>'radio_menu3b', 'id'=>'radio_menu3b_3', 'onclick'=>"window.location.href='".site_url('sales/edit')."'"));
echo form_label('&nbsp;<span class="ui-icon ui-icon-pencil"></span>', 'radio_menu3b_3');
?>
</div>
<?php endif; ?>