<h3>Inventory Management</h3>
<div id="radio_menu2" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu2', 'id'=>'radio_menu2_1', 'onclick'=>"window.location.href='".site_url('item')."'"));
echo form_label('Inventory', 'radio_menu2_1');
echo form_radio(array('name'=>'radio_menu2', 'id'=>'radio_menu2_2', 'onclick'=>"window.location.href='".site_url('item/add')."'"));
echo form_label('&nbsp;<span class="ui-icon ui-icon-plus"></span>', 'radio_menu2_2');
echo form_radio(array('name'=>'radio_menu2', 'id'=>'radio_menu2_3', 'onclick'=>"window.location.href='".site_url('item_trash')."'"));
echo form_label('Trash', 'radio_menu2_3');
?>
</div>
<?php if (pass_auth('administrator')): ?>
<div id="radio_menu2a" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu2a', 'id'=>'radio_menu2a_1', 'onclick'=>"window.location.href='".site_url('category')."'"));
echo form_label('Categories', 'radio_menu2a_1');
echo form_radio(array('name'=>'radio_menu2a', 'id'=>'radio_menu2a_2', 'onclick'=>"window.location.href='".site_url('category/add')."'"));
echo form_label('&nbsp;<span class="ui-icon ui-icon-plus"></span>', 'radio_menu2a_2');
echo form_radio(array('name'=>'radio_menu2a', 'id'=>'radio_menu2a_3', 'onclick'=>"window.location.href='".site_url('category/edit')."'"));
echo form_label('&nbsp;<span class="ui-icon ui-icon-pencil"></span>', 'radio_menu2a_3');
?>		 
</div>
<?php endif; ?>