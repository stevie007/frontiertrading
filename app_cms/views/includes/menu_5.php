<?php if (pass_auth('administrator')): ?>

<h3>Statistics &amp; Others</h3>

<div id="radio_menu8" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu8', 'id'=>'radio_menu8_1', 'onclick'=>"window.location.href='".site_url('stat_inven')."'"));
echo form_label('Inventory Adjustment Log', 'radio_menu8_1');
?>

</div>

<div id="radio_menu9" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu9', 'id'=>'radio_menu9_1', 'onclick'=>"window.location.href='".site_url('stat_order')."'"));
echo form_label('Inventory Order Report', 'radio_menu9_1');
echo form_radio(array('name'=>'radio_menu9', 'id'=>'radio_menu9_2', 'onclick'=>"window.location.href='".site_url('stat_invoice')."'"));
echo form_label('Inventory Invoice Report', 'radio_menu9_2');
?>
</div>



<div id="radio_menu10" style="display:inline;" class="buttonset">
<?php
echo form_radio(array('name'=>'radio_menu10', 'id'=>'radio_menu10_1', 'onclick'=>"window.location.href='".site_url('backup')."'"));
echo form_label('Backup Database', 'radio_menu10_1');
echo form_radio(array('name'=>'radio_menu10', 'id'=>'radio_menu10_2', 'onclick'=>"window.location.href='".site_url('backup/email_backup')."'"));
echo form_label('Backup By Email', 'radio_menu10_2');
?>
</div>

<?php endif; ?>