<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
<?php $this->load->view('includes/html_head'); ?>

<style>
   body {
 background: #ffffff;
 color:#000000;
 margin:auto;
   text-align: left;
  font-size: 12px;
  font-family: helvetica, arial, sans-serif;
  background-color:#FFF;
width: 700px;
 }
#func-menu{
margin:10px auto 10px auto;
     
padding:5px;
}
button {
margin:0 5px 0 5px;
}
   </style>



   <script type="text/javascript"> 
$(document)
    .ready(function() {
	$('#print').click(function(){
		$('#func-menu').hide();
		window.print();
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});
	$('#close').click(function(){
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});
 
}) 
</script>



   </head>
<body>
<div id="func-menu" class="ui-widget-content ui-corner-all ui_button">
<?php
$data = array('id' => 'print', 'class' => 'font10px flt', 'content' => 'Print');
echo form_button($data);
?>

<?php
$data = array('id' => 'close', 'class' => 'font10px frt', 'content' => 'Close');
echo form_button($data);
?>
<br style="clear:both;"/>
</div>

<table>
<tr>
<th>Frontier International Trading Ltd<br /><br /></th>
</tr>

<tr><td>
Analyse Sales<br /><br />
</td></tr>

<tr><td>
<?php 
echo date('d/m/Y', $date['fr']);
echo ' through ';
echo date('d/m/Y', $date['to']);
?>
<br /><br />
</td></tr>

		    
		      
</table>
<?php 
    $tmpl = array (
		   'table_open'          => '<table border="1" cellpadding="2" cellspacing="1">',		   
                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th>',
                    'heading_cell_end'    => '</th>',
                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td align="right">',
                    'cell_end'            => '</td>',
                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td align="right">',
                    'cell_alt_end'        => '</td>',
                    'table_close'         => '</table>'
              );

    $this->table->set_template($tmpl); 
    $table_heading = array_keys($table1[0]);
       
        
        $this->table->set_heading($table_heading);
        echo $this->table->generate($table1);  
?>


</body>
</html>
