  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#title" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/item_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
		      });

      
    });
</script>

      <!-- START  -->

<script type="text/javascript" src="<?php echo base_url(); ?>lib/highcharts/js/highcharts.js"></script>
 		
<!-- 1b) Optional: the exporting module -->
<script type="text/javascript" src="<?php echo base_url(); ?>lib/highcharts/js/modules/exporting.js"></script>	
<!-- END  -->


<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>

<div class="ui_button font10px" id="cates">
 <?php 
$category = isset($category) ? $category : '';

$cates = get_cates();
foreach ($cates as $cate) 
  {
    if (is_array($cate)) 
      {
	$parent = array_shift($cate);
	echo '<span class="link">';
	echo ($parent->name == $category) ? '<button disabled="disabled">'.$parent->name.'</button>' : '<a href="'.site_url('stat_invoice/cate/'.$parent->name).'" >'.$parent->name.'</a>';  
	echo '</span>';
	foreach ($cate as $sub)
	  {	  
	echo '<span class="link">';
	    echo ($sub->name == $category) ? '<button disabled="disabled">'.$sub->name.'</button>' : '<a href="'.site_url('stat_invoice/cate/'.$sub->name).'" >'.$sub->name.'</a>';	  
	echo '</span>';
	  }
      }
    else
      {		echo '<span class="link">';
	echo ($cate->name == $category) ? '<button disabled="disabled">'.$cate->name.'</button>' : '<a href="'.site_url('stat_invoice/cate/'.$cate->name).'" >'.$cate->name.'</a>';
	echo '</span>';
      }
  }
?>
</div>
<br style="clear:both;"/>
<?php echo form_open('stat_invoice/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
/*
echo form_input(
		array(
		      'name'        => 'title',
		      'id'        => 'title',
		      'value'	  => $this->session->userdata('title'),
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);
		*/

?>
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td>
<td align="left" class="ui_button font10px">

</td>
</tr>

<tr><td align="left" class="font10px">
<span class="ui_buttonset"> 
</span>
<span class="ui_buttonset">
</span>
</td><td align="left" class="font10px">
</td><td align="left" class="ui_button font10px">
<?php 
/*
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );*/
?>
</td><td align="left" class="ui_button font10px">

</td></tr>
</table>
<?php echo form_close(); ?>



<div style="float: left; padding:20px 22px;">
  <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

		
<table border="0" cellpadding="5" cellspacing="0" width="900px" style="margin:30px auto;">

<tr>
<th colspan="8" align="left"><?php echo $table_title;?></th>
</tr>



<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">

		<div id="container" style="width: 900px; height: 450px; margin: 0 auto"></div>
		<script type="text/javascript">
		
			var chart;
 
				chart = new Highcharts.Chart({
				        colors: ['#000000'],
					chart: {
						renderTo: 'container',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},    
					credits: {
        					enabled: false	
    					},
					title: {
						text: 'Turnover by Division '
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true, 
							cursor: 'pointer', 
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
								}
							}
						}				
					},
				    series: [{
						type: 'pie',
						name: 'Turnover division',       
						data: [
<?php
foreach ($ordered as $item) { echo "['".$item['title']." qty:".$item['ordered']."', ".$item['ordered']."],"; }
?>
						]
					}]
				});
	 
				
		</script>
<?php 
//print_r($ordered);

?>
</td>
</tr>
 
<th align="left" valign="middle" width="70">

</th>

<th align="right" valign="middle" width="70">

</th>

<th align="right" valign="middle" width="20"></th>

<th align="center" valign="middle" width="190"></th>
</tr>


<tr><td colspan="8" align="left">
<br />
</td></tr>

<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 

$data = array('content' => 'Print', 'onclick'=>"openView('".site_url("stat_invoice/print_view")."','print');");
$date['style'] = 'margin:0 10px;';
echo form_button($data);
 

?>


</td>
</tr>

<tr><td colspan="8" align="left">

<?php 
    $tmpl = array (
		   'table_open'          => '<table border="1" cellpadding="2" cellspacing="1">',		   
                    'heading_row_start'   => '<tr>',
                    'heading_row_end'     => '</tr>',
                    'heading_cell_start'  => '<th>',
                    'heading_cell_end'    => '</th>',
                    'row_start'           => '<tr>',
                    'row_end'             => '</tr>',
                    'cell_start'          => '<td align="right">',
                    'cell_end'            => '</td>',
                    'row_alt_start'       => '<tr>',
                    'row_alt_end'         => '</tr>',
                    'cell_alt_start'      => '<td align="right">',
                    'cell_alt_end'        => '</td>',
                    'table_close'         => '</table>'
              );

    $this->table->set_template($tmpl); 
    $table_heading = array_keys($table1[0]);
       
        
        $this->table->set_heading($table_heading);
        echo $this->table->generate($table1);  
?>


</td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo '';?></th>
<th align="left" valign="middle" colspan="2"></th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"></th>
</tr>

<tr><td colspan="8" align="left"><br /><br /></td></tr>


</table>

 
 

  </div>
  </div>