<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">


<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">
 
<script type="text/javascript" src="<?php echo base_url(); ?>/lib/jquery-ui/js/jquery-1.4.2.min.js"></script>

<link href="<?php echo base_url(); ?>lib/jquery-ui/css/trontastic/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>lib/jquery-ui/js/jquery-ui-1.8.6.custom.min.js"></script>
 

<!-- START custom -->
<link href="<?php echo base_url(); ?>css/main.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.functions.js"></script>
<link href="<?php echo base_url(); ?>css/jquery.css" rel="stylesheet" type="text/css" media="screen" />
<!-- END custom -->

   <script type="text/javascript"> 
$(document)
    .ready(function() {
	$('#print').click(function(){
		$('#func-menu').hide();
		window.print();
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});
	$('#close').click(function(){
		$('body').fadeOut('fast', function() {
			window.close();
  		});
	});
 
}) 
</script>


<style>
   body {
 margin:auto;
   text-align: left;
  font-size: 12px;
  font-family: helvetica, arial, sans-serif;
  background-color:#FFF;
width: 700px;

 }
table
{


  font-size: 12px;
  font-family: helvetica, arial, sans-serif;
  background-color:#FFF;
  }

#func-menu{
margin:10px auto 10px auto;
     
padding:5px;
}
button {
margin:0 5px 0 5px;
}
</style> </head>

  <body>
<div id="func-menu" class="ui-widget-content ui-corner-all ui_button">
<?php
$data = array('id' => 'print', 'class' => 'font10px flt', 'content' => 'Print');
echo form_button($data);
?>

 
<?php
$data = array('id' => 'close', 'class' => 'font10px frt', 'content' => 'Close');
echo form_button($data);
?>
<br style="clear:both;"/>
</div>
<?php $this->load->view('print_html/statement_print_view'); ?>
  </body> 
</html>


