<?php echo form_open_multipart('member/member_submit', array('id' => 'myform'), array('id' => $member->id));?>

<div style="float: left; padding:20px 22px;">
<div style="float: left; width:960px;background: #e1e1e1;color:#000000;">


<table border="0" cellpadding="10" cellspacing="0" width="800px" style="margin:30px auto;">

<tr>
<th colspan="2"  align="left"><?php echo $table_title;?></th>
</tr>


<tr>
<th align="left" valign="middle" width="180">&nbsp;</th>
<th align="left" valign="middle">&nbsp;</th>
</tr>
 
<tr>
<th align="left" valign="middle">ID</th>
<td align="left" valign="middle"><?php echo $member->id;?></td>
</tr>
 
 
<tr class="bgc">
<th align="left" valign="middle">Company</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'company', 'id' => 'company', 'value' => $member->company,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Firstname</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'firstname', 'id' => 'firstname', 'value' => $member->firstname,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Lastname</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'lastname', 'id' => 'lastname', 'value' => $member->lastname,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>


<tr class="bgc">
   <th align="left" valign="middle">Label</th>
   <td align="left" valign="middle" class="ui_button font9px">
   <?php 
   echo form_checkbox($data = array(
				    'name'        => 'active',
				    'id'          => 'active',
				    'value'       => 'accept',
				    'class'       => 'check',
				    'checked'     => $member->active
				    ));
echo form_label('Active', 'active');
?>  
	  
	   
</td>
</tr>


<tr class="bgc">
<th align="left" valign="middle">Email</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'email', 'id' => 'email', 'value' => $member->email,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Telephone</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'telephone', 'id' => 'telephone', 'value' => $member->telephone,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Mobile</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'mobile', 'id' => 'mobile', 'value' => $member->mobile,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Fax</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'fax', 'id' => 'fax', 'value' => $member->fax,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Address (Delivery)</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'address', 'id' => 'address', 'value' => $member->address,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Suburb (Delivery)</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'suburb', 'id' => 'suburb', 'value' => $member->suburb,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">City (Delivery)</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'city', 'id' => 'city', 'value' => $member->city,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Postcode (Delivery)</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'postcode', 'id' => 'postcode', 'value' => $member->postcode,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Address (Billing)</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'address2', 'id' => 'address2', 'value' => $member->address2,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Suburb (Billing)</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'suburb2', 'id' => 'suburb2', 'value' => $member->suburb2,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">City (Billing)</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'city2', 'id' => 'city2', 'value' => $member->city2,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
  <th align="left" valign="middle">Postcode (Billing)</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'postcode2', 'id' => 'postcode2', 'value' => $member->postcode2,  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
  <th align="left" valign="middle">Management Notes</th>
<td align="left" valign="middle">
<?php
    $attr = array('name' => 'notes', 'id' => 'notes', 'value' => $member->notes,  'class' => 'ui-corner-all', 'style'=>'width:480px; height:260px;'); 
echo form_textarea($attr);
?>
</td>
</tr>
<!--
<tr class="bgc">
<th align="left" valign="middle">Authority</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'authority', 'id' => 'authority', 'value' => '',  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>
-->

<tr class="bgc">
<th align="left" valign="middle">Resetcode</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'resetcode', 'id' => 'resetcode', 'value' => '',  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>


<tr class="bgc">
<th align="left" valign="middle" style="color:red;">Reset Password</th>
<td align="left" valign="middle">
<?php
$attr = array('name' => 'password', 'id' => 'password', 'value' => '',  'class' => 'ui-corner-all'); 
echo form_input($attr);
?>
</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle">Last Statement Sent</th>
<td align="left" valign="middle">
<?php
echo $member->date_sent;
?>
</td>
</tr>
<tr class="bgc">
   <th align="left" valign="middle">Auto Send Statement</th>
   <td align="left" valign="middle" class="ui_button font9px">
   <?php 
   echo form_checkbox($data = array(
				    'name'        => 'auto_send',
				    'id'          => 'auto_send',
				    'value'       => 'accept',
				    'class'       => 'check',
				    'checked'     => $member->auto_send
				    ));
echo form_label('Auto Send', 'auto_send');
?>  
	  
	   
</td>
</tr>
</table>

</div>
</div>
<br style="clear:both">
<div style="float: left; padding:0 5px;" class="ui_button">
<?php echo form_submit(array('name'=>'submit', 'value'=>'Save'));?>
</div>
<div style="float: right; padding:0 5px;" class="ui_button">
<?php echo form_input(array('type' =>'button', 'name' =>'button', 'value'=>'Cancel', 'onclick'=>"window.location.href='".site_url('cms')."'"));?>
</div>

<?php echo form_close(); ?>