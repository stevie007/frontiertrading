  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#customer" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/cust_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
		      });

      $( "#sales" )
	.autocomplete({
			minLength: 1,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/sales_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
		      });


    });
</script>




<script type="text/javascript">
$(function() {
    $('.uploadForm').ajaxForm({
        beforeSubmit: function(a,f,o) {

        },
        success: function(data) {
	     var invoice_print_list = $.parseJSON(data);
	     var htmls = 'Invoice Print List, ' + invoice_print_list.count + ' items.<br /><br />';

		$.each(invoice_print_list.invoices, function(key, value) {
			htmls += key + ' - ' + value + '<br />';
		});

	     $('#fnoty #cart-info')
	       .html(htmls)
	       .fadeIn();
        }
    });
});
</script>


<!-- noty -->
   <div id="fnoty">
   <span id="cart-info">
   <?php
   $invoice_print_list = $this->session->userdata('invoice_print_list');

   echo 'Invoice Print List, ' . sizeof($invoice_print_list['invoices']) . ' items.<br /><br />';

	if (sizeof($invoice_print_list['invoices']))
	{
		foreach ($invoice_print_list['invoices'] as $key => $value) {
			echo $key . ' - ' . $value . '<br />';
		}
	}


   ?>
   </span><br /><br />
   <a href="<?php echo site_url('invoice/empty_invoice_print_queue');?>" class="white font9px">Empty Print List</a>
   <?php echo nbs(30);?>
   <a href="JavaScript:void(0);" onclick="openView('<?php echo site_url('invoice/print_selected_invoices');?>', 'Print Selected Invoices');" class="white font9px">Print</a>
   </div>
<!-- end noty -->






<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('invoice/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'customer',
		      'id'        => 'customer',
		      'value'	  => $customer,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      )
		);

?>
</td>
<td align="left" class="ui_button font10px">

<?php
echo form_input(
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      )
		 );
?>
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td></tr>

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php
$attr = (isset($filter['2']['delivered'])
	 && !$filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/2/delivered/0", 'X',$attr);
$attr = (isset($filter['2']['delivered'])
	 && $filter['2']['delivered']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/2/delivered/1", 'Delivered',$attr);
?>
</span>
<span class="ui_buttonset">
<?php
$attr = (isset($filter['3']['paid'])
	 && !$filter['3']['paid']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/3/paid/0", 'X',$attr);
$attr = (isset($filter['3']['paid'])
	 && $filter['3']['paid']) ? array('class' => 'white') : array();
echo anchor("invoice/filter/3/paid/1", 'Paid',$attr);
?>
</span>
</td><td align="left" class="font10px">
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'sales',
		      'id'        => 'sales',
		      'value'	  => $sales,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      )
		);

?>
</td><td align="left" class="ui_button font10px">

</td></tr>
</table>
<?php echo form_close(); ?>



<div style="float: left; padding:20px 22px;">
  <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">


<table border="0" cellpadding="5" cellspacing="0" width="900px" style="margin:30px auto;">

<tr>
<th colspan="8" align="left"><?php echo $table_title;?></th>
</tr>



<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php
echo $this->pagination->create_links();
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("invoice/filter/per_page/$total", 'Display All', $attr);

$data = array('content' => 'View Report', 'onclick'=>"openView('".site_url("invoice/report")."','report');");
$date['style'] = 'margin:0 10px;';
echo form_button($data);

$data = array('content' => 'Export', 'onclick'=>"window.location='".site_url("invoice/export")."'");
$data['style'] = 'margin:0 10px;';
echo form_button($data);

?>


</td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="60">
<?php echo anchor("invoice/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="80">
<?php echo anchor("invoice/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("invoice/orderby/cust_name", 'Customer', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'cust_name'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("invoice/orderby/purchase_info", 'Purchase Info', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'purchase_info'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="70">
<?php echo anchor("invoice/orderby/sales", 'Sales', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'sales'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>

<th align="right" valign="middle" width="70">
<?php echo anchor("invoice/orderby/total", 'Total', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'total'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>

<th align="right" valign="middle" width="10"></th>

<th align="center" valign="middle" width="270">Operations</th>
</tr>

<?php
$accu = new stdClass;
$accu->subtotal = $accu->total = 0;


foreach($query->result() as $row):

$row->debit = $row->total - $row->paid;
$accu->subtotal += $row->subtotal;
$accu->total += $row->total;

$delivered = $row->delivered ? '' : '&bull;';
$job_status = $row->paid ? '' : 'uf';
?>

<?php echo form_open_multipart('invoice/add_invoice_to_print',
  array('class' => 'uploadForm', 'id' => 'invoice'.$row->id),
  array('invoice_id' => $row->id, 'cust_name'=>$row->cust_name)
  );
?>



<tr class="bgc <?php echo $job_status;?>" id="cust<?php echo $row->id;?>">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo date('d/m/Y', strtotime($row->date));?></td>
<td align="left" valign="middle">

<a target="_blank" href="<?php echo site_url('member/edit/'.$row->cust_id);?>" style="color: #000000;">
<?php echo $row->cust_name;?>
</a>

</td>
<td align="left" valign="middle"><?php echo $row->purchase_info;?></td>
<td align="left" valign="middle"><?php echo $row->sales;?></td>
<td align="right" valign="middle"><?php echo number_format($row->total,2,'.',',');?></td>
<td align="right" valign="middle"><?php echo $delivered;?></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php
$data = array('class' => 'invo-lib-btn','content' => 'Edit', 'onclick'=>"openView('".site_url("invoice/edit/$row->id")."','edit-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'View', 'onclick'=>"openView('".site_url("invoice/view/$row->id")."','view-".$row->id."');");
echo form_button($data);
$data = array('class' => 'invo-lib-btn','content' => 'PDF', 'onclick'=>"window.location='".site_url("invoice/pdf/$row->id")."'");
echo form_button($data);

echo form_submit(array('value'=>'Add', 'class'=>'invo-lib-btn'));

$data = array('class' => 'invo-lib-btn','content' => 'Send', 'onclick'=>"window.location='".site_url("invoice/send_msg/$row->id")."'");
echo form_button($data);

$data = array('class' => 'invo-lib-btn','content' => 'Del', 'onclick'=>"window.location='".site_url("invoice/del_msg/$row->id")."'");
echo form_button($data);


?>
</td></tr>

<?php echo form_close(); ?>

<?php endforeach;?>

<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle" colspan="2">Accumulated Total of the Page............</th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"><?php echo number_format($accu->total,2,'.',',');?></th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"><?php echo '';?></th>
</tr>

<tr><td colspan="8" align="left"><br /><br /></td></tr>

<tr><td colspan="8" align="left">

<span class="sample"><span class="uf sam-box">&nbsp;&nbsp;</span> -- Unpaid </span><br />
<span class="sample"><span class="sam-box">&nbsp;&bull;</span> -- Undelivered </span>


</td></tr>
</table>




  </div>
  </div>