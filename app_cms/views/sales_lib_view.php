<div style="float: left; padding:20px 22px;">
<div style="float: left; width:960px;background: #e1e1e1;color:#000000;">


 
		
<table border="0" cellpadding="5" cellspacing="0" width="800px" style="margin:30px auto;">

<tr>
<th colspan="7"  align="left"><?php echo $table_title;?></th>
</tr>

<tr class="bgc">
<td colspan="7"  align="left" class="ui_button font9px">
<!--
<?php 
$attr = (isset($filter['boolean1'])) ? array('class' => 'white') : array();
echo anchor("#sales/filter/boolean1/1", 'Boolean1',$attr);
?>

<?php 
$attr = (isset($filter['boolean2'])) ? array('class' => 'white') : array();
echo anchor("#sales/filter/boolean2/1", 'Boolean2',$attr);
?>
-->
<?php 
echo form_open('sales/search', array('id' => 'myform', 'style' => 'display:inline;'));
?>
<span class="font11px">
<?php
  echo form_input( 
		  array(
			'name'        => 'search',
			'value'	      => 'Search..',
			'class'       => 'blur  ui-corner-all',
			'style'       => 'width:200px;margin:10px;'
			) 
		   );
?>
</span>
<?php
  echo form_submit(array(
			 'name'        => 'submit',
			 'value'       => 'Go'
			 )
		   );
?>

<?php echo form_close(); ?>
<span class="font12px">
<?php 
if ($search)
{
echo '&nbsp;&nbsp;&nbsp; Listing search results for: '.$search;
}
echo '&nbsp;&nbsp;&nbsp; Total: '.$total;
?>
</span>
</td>
</tr>

<tr class="bgc">
<td colspan="7"  align="right" class="ui_button font9px"><?php echo $this->pagination->create_links(); ?></td>
</tr>

<tr>
<th align="left" valign="middle" width="40">
<?php echo anchor("sales/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100">
<?php echo anchor("sales/orderby/firstname", 'Firstname', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'firstname'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100" >
<?php echo anchor("sales/orderby/lastname", 'Lastname', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'lastname'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100" >
<?php echo anchor("sales/orderby/email", 'Email', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'email'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100" >
<?php echo anchor("sales/orderby/mobile", 'Mobile', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'mobile'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="100" >
<?php echo anchor("sales/orderby/city", 'City', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'city'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s"></span>' : '<span class="ui-icon ui-icon-triangle-1-n"></span>';
endif;
?>
</th>
<th align="center" valign="middle" width="200">Operations</th>
</tr>

<?php foreach($query->result() as $row):?>
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo $row->firstname;?></td>
<td align="left" valign="middle"><?php echo $row->lastname;?></td>
<td align="left" valign="middle"><?php echo $row->email;?></td>
<td align="left" valign="middle"><?php echo $row->mobile;?></td>
<td align="left" valign="middle"><?php echo $row->city;?></td>
<td align="center" valign="middle" class="ui_button font9px"><?php echo anchor("sales/edit/$row->id", 'Edit').' '.anchor("sales/del_msg/$row->id", 'Del');?></td>
</tr>
<?php endforeach;?>

</table>

</div>
</div>
