
<script>
$(function() {
    $( "#parent" )
      .autocomplete({
	minLength: 0,
	    source: function(request, response){
	    $.ajax({
	      url: '<?php echo site_url('ajax/parent_cate_names');?>',
		  dataType: 'json',
		  type: 'POST',
		  data: request,
		  success: function(data){
		  response(data);
		}
	      });
	  }
	})
      .click(function() {
	  $(this).autocomplete( "search" , '' );
	});

      
  });
</script>

 <?php echo form_open_multipart('category/category_submit', array('id' => 'myform'), array('id' => $category->id));?>

<div style="float: left; padding:20px 22px;">
   <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">


   <table border="0" cellpadding="10" cellspacing="0" width="800px" style="margin:30px auto;">

   <tr>
   <th colspan="2"  align="left"><?php echo $table_title;?></th>
   </tr>


   <tr>
   <th align="left" valign="middle" width="150">&nbsp;</th>
   <th align="left" valign="middle">&nbsp;</th>
   </tr>
 
   <tr>
   <th align="left" valign="middle">ID</th>
   <td align="left" valign="middle"><?php echo $category->id;?></td>
   </tr>
 
 
   <tr class="bgc">
   <th align="left" valign="middle">Parent</th>
   <td align="left" valign="middle">
   <?php 
   echo form_input(
		   array(
			 'name'=>'parent', 
			 'id'=>'parent', 
			 'value'=>$category->parent,
			 'class'       => 'ui-corner-all',
			 'style'       => 'width:200px;margin:10px;'
			 )
		   );
?>
   </td>
   </tr>

   <tr class="bgc">
   <th align="left" valign="middle">Name</th>
   <td align="left" valign="middle">
   <?php 
   echo form_input(
		   array(
			 'name'        => 'name',
			 'id'        => 'name',
			 'value'	  => $category->name,
			 'class'       => 'ui-corner-all',
			 'style'       => 'width:200px;margin:10px;'
			 ) 
		   );

?>
</td>
</tr>

   <tr class="bgc">
   <th align="left" valign="middle">Order</th>
   <td align="left" valign="middle">
   <?php 
   echo form_input(
		   array(
			 'name'        => 'order',
			 'id'        => 'order',
			 'value'	  => $category->order,
			 'class'       => 'ui-corner-all',
			 'style'       => 'width:200px;margin:10px;'
			 ) 
		   );

?>
</td>
</tr>
  </table>

  </div>
  </div>
  <br style="clear:both">
  <div style="float: left; padding:0 5px;" class="ui_button">
  <?php echo form_submit(array('name'=>'submit', 'value'=>'Save'));?>
  </div>
  <div style="float: right; padding:0 5px;" class="ui_button">
  <?php echo form_input(array('type' =>'button', 'name' =>'button', 'value'=>'Cancel', 'onclick'=>"window.location.href='".site_url('cms')."'"));?>
  </div>

  <?php echo form_close(); ?>

  