  <script>
  $(function() {
      var dates = $( "#from, #to" )
	.datepicker({
		      dateFormat: 'dd/mm/yy',
		      defaultDate: "+1w",  
		      onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
			instance = $( this ).data( "datepicker" );
			date = $.datepicker.parseDate(
			  instance.settings.dateFormat ||
			    $.datepicker._defaults.dateFormat,
			  selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		      }
		    });


      $( "#title" )
	.autocomplete({
			minLength: 2,
			source: function(request, response){
			  $.ajax({
				   url: '<?php echo site_url('ajax/item_names');?>',
				   dataType: 'json',
				   type: 'POST',
				   data: request,
				   success: function(data){
				     response(data);
				   }
				 });
			},
		      });

      
    });
</script>

<style type="text/css">
.fp {
	color: black;
}
.fup {
	background-color: LightSteelBlue; color: black;
}
.uf {
	background-color: #C68E17; color: black;
}
.sample {
display: inline-block;
width: 160px;
height:20px;
}
.sam-box {
display: inline-block;
width: 10px;
padding: 0 4px;
}
</style>
<?php echo form_open('stat_inven/search', array('id' => 'myform', 'style' => 'display:inline;'));?>
<table border="0" cellpadding="0" cellspacing="0" >
<tr><td align="left" class="ui_button font10px" style="width:200px">
<?php 
echo form_label('<button type="button">From</button>', 'from');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'from',
		      'name'        => 'from',
		      'value'       => date('d/m/Y', $date['fr']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px" style="width:200px">
<?php
echo form_label('<button type="button">To</button>', 'to');
echo form_input(
		array(
		      'type'	      => 'text',
		      'id'          => 'to',
		      'name'        => 'to',
		      'value'       => date('d/m/Y', $date['to']),
		      'class'       => 'ui-corner-all',
		      'style'       =>'width:100px;margin:10px;')
		);
?>
</td><td align="left" class="ui_button font10px">
<?php
echo form_input(
		array(
		      'name'        => 'title',
		      'id'        => 'title',
		      'value'	  => $this->session->userdata('title'),
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		);

?>
</td>
<td align="left" class="ui_button font10px">

</td></tr>

<tr><td align="left" class="font10px">
<span class="ui_buttonset">
<?php 
$attr = (isset($filter['2']['add']) 
	 && !$filter['2']['add']) ? array('class' => 'white') : array();
echo anchor("stat_inven/filter/2/add/0", nbs(3).'-'.nbs(3) ,$attr);
$attr = (isset($filter['2']['add']) 
	 && $filter['2']['add']) ? array('class' => 'white') : array();
echo anchor("stat_inven/filter/2/add/1", nbs(3).'+'.nbs(3) ,$attr);
?>
</span>
<span class="ui_buttonset">
</span>
</td><td align="left" class="font10px">
</td><td align="left" class="ui_button font10px">
<?php 
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => $search,
		      'class'       => 'blur ui-corner-all',
		      'style'       => 'width:200px;margin:10px;'
		      ) 
		 );
?>
<?php
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go'
		       )
		 );
?>
</td><td align="left" class="ui_button font10px">

</td></tr>
</table>
<?php echo form_close(); ?>



<div style="float: left; padding:20px 22px;">
  <div style="float: left; width:960px;background: #e1e1e1;color:#000000;">

		
<table border="0" cellpadding="5" cellspacing="0" width="900px" style="margin:30px auto;">

<tr>
<th colspan="8" align="left"><?php echo $table_title;?></th>
</tr>

 

<tr class="bgc">
<td colspan="8" align="right" class="ui_button font9px">
<?php 
echo $this->pagination->create_links(); 
$attr = ($per_page == 25) ? array() : array('class' => 'white');
$attr['style'] = 'margin:0 10px;';
echo anchor("stat_inven/filter/per_page/$total", 'Display All', $attr);

?></td>
</tr>

<tr class="bgc">
<th align="left" valign="middle" width="80">
<?php echo anchor("stat_inven/orderby/id", 'ID', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'id'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="180">
<?php echo anchor("stat_inven/orderby/date", 'Date', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'date'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" >
<?php echo anchor("stat_inven/orderby/title", 'Title', array('class' => 'black flt'));
if (isset($orderby) && $orderby['order'] == 'title'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s flt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n flt"></span>';
endif;
?>
</th>
<th align="right" valign="middle" >
<?php echo anchor("stat_inven/orderby/adjust", 'Adjust', array('class' => 'black frt'));
if (isset($orderby) && $orderby['order'] == 'adjust'):
echo  ($orderby['sort'] == 'desc') ?'<span class="ui-icon ui-icon-triangle-1-s frt"></span>' : '<span class="ui-icon ui-icon-triangle-1-n frt"></span>';
endif;
?>
</th>
<th align="left" valign="middle" width="70">

</th>

<th align="right" valign="middle" width="70">

</th>

<th align="right" valign="middle" width="20"></th>

<th align="center" valign="middle" width="190"></th>
</tr>

<?php 

foreach($query->result() as $row):

?> 
<tr class="bgc">
<td align="left" valign="middle"><?php echo $row->id;?></td>
<td align="left" valign="middle"><?php echo date('d/m/Y H:i:s', strtotime($row->date));?></td>
<td align="left" valign="middle"><?php echo $row->title;?></td>
<td align="right" valign="middle"><?php echo $row->adjust;?></td>
<td align="right" valign="middle"><?php ?></td>
<td align="right" valign="middle"><?php ?></td>
<td align="right" valign="middle"><?php ?></td>
<td align="center" valign="middle" class="ui_button font9px">
<?php
  //$data = array('class' => 'invo-lib-btn','content' => 'Del', 'onclick'=>"window.location='".site_url("stat_inven/del_msg/$row->id")."'");
//echo form_button($data);

?>
</td></tr>
<?php endforeach;?>

<tr><td colspan="8" align="left"></td></tr>

<tr>
<th align="left" valign="middle" colspan="2"><?php echo 'Total Data Returned: ';echo $total;?></th>
<th align="left" valign="middle" colspan="2"></th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="right" valign="middle"></th>
<th align="center" valign="middle" class="ui_button font9px"></th>
</tr>

<tr><td colspan="8" align="left"><br /><br /></td></tr>


</table>

 
 

  </div>
  </div>
