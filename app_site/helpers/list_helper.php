<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Form Declaration
 *
 * Creates the opening portion of the form.
 *
 * @access	public
 * @param	string	the URI segments of the form destination
 * @param	array	a key/value pair of attributes
 * @param	array	a key/value pair hidden data
 * @return	string
 */	
if ( ! function_exists('get_cates'))
{
	function get_cates()
	{
		$CI =& get_instance();

		$rta = array();
		$CI->db->where('parent', 'none'); 
		$CI->db->order_by("order", "asc");
		$query =  $CI->db->get('category');
		
		foreach ($query->result() as $row)
		  { 
		    $CI->db->where('parent', $row->name); 
		    $CI->db->order_by("order", "asc");
		    $query =  $CI->db->get('category');
		    if ($query->num_rows) {
		      $tmp = array(); 
		      array_push($tmp, $row);
		      foreach ($query->result() as $row)
			{
			  array_push($tmp, $row);
			}
		      array_push($rta, $tmp);
		    } else {
		      array_push($rta, $row);
		    }
		  }
		
		return $rta;
	}
}