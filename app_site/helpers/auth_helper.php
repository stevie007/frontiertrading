<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Form Declaration
 *
 * Creates the opening portion of the form.
 *
 * @access	public
 * @param	string	the URI segments of the form destination
 * @param	array	a key/value pair of attributes
 * @param	array	a key/value pair hidden data
 * @return	string
 */	
if ( ! function_exists('is_logged_in'))
{
	function is_logged_in()
	{
		$CI =& get_instance();

		$is_logged_in = $CI->session->userdata('is_site_logged_in');

		if(!isset($is_logged_in) || !$is_logged_in )
		{
			redirect();	
		}		
	}
}

if ( ! function_exists('logged_in'))
{
	function logged_in()
	{
		$CI =& get_instance();

		$is_logged_in = $CI->session->userdata('is_site_logged_in');

		if(!isset($is_logged_in) || !$is_logged_in )
		  {
		    return FALSE;
		  }	
		else 
		  {
		    return TRUE;
		  }	
	}
}
