<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Member extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('list');
    is_logged_in();
  }
 
  function index()
  {
 
  }
  	
	
  
  
  function change_password()
  { 
    $this->load->library('form_validation');		
    $this->form_validation->set_rules('password_old', 'Current password', 'trim|required|min_length[4]|max_length[32]');
    $this->form_validation->set_rules('password_new', 'New password', 'trim|required|min_length[4]|max_length[32]');
    $this->form_validation->set_rules('password_rep', 'Password Confirmation', 'trim|required|matches[password_new]');
        
    if(!$this->form_validation->run())
      {
	$data['session_msg'] = validation_errors('<span class="error">', '</span>'.nbs(2)).$this->session->flashdata('msg');
	$data['main_content'] = 'forms/change_password_form';  
	$data['page_title'] = 'Change Password';
	$this->load->view('includes/template', $data);
      }    
    else
      {	
	$this->load->model('member_model');		

	if($this->member_model->change_password())
	  {
	    $msg = 'Password changed successfully.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }
	else
	  {
	    $msg = 'Password could not be changed.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }

      }
  } // end change password

 

 
  function profile()
  {
    $this->load->library('form_validation');		
    $this->form_validation->set_rules('company', 'Company', 'trim|required');
    $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
    $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
    $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
    $this->form_validation->set_rules('address', 'Address', 'trim|required');
    $this->form_validation->set_rules('city', 'City', 'trim|required');
    $this->form_validation->set_rules('suburb', 'Suburb', 'trim|required');
    $this->form_validation->set_rules('telephone', 'Phone', 'trim|required');
    $this->form_validation->set_rules('postcode', 'Postcode', 'trim');
   
    $this->form_validation->set_rules('address2', 'Address', 'trim');
    $this->form_validation->set_rules('city2', 'City', 'trim');
    $this->form_validation->set_rules('suburb2', 'Suburb', 'trim');
    $this->form_validation->set_rules('postcode2', 'Postcode', 'trim');
    // not pass input validation
    if(!$this->form_validation->run())
      {	  

	$set_value['company'] = set_value('company');
	$set_value['firstname'] = set_value('firstname');
	$set_value['lastname'] = set_value('lastname');
	$set_value['email'] = set_value('email');
	$set_value['telephone'] = set_value('telephone'); 
	$set_value['mobile'] = set_value('mobile');
	$set_value['address'] = set_value('address');
	$set_value['city'] = set_value('city');
	$set_value['suburb'] = set_value('suburb');
	$set_value['postcode'] = set_value('postcode');
	$set_value['address2'] = set_value('address2');
	$set_value['city2'] = set_value('city2');
	$set_value['suburb2'] = set_value('suburb2');
	$set_value['postcode2'] = set_value('postcode2');

	$this->session->set_flashdata('set_value', $set_value);
	$this->session->set_flashdata('msg', validation_errors('<span class="error">', '</span>'.nbs(2)));
	
	redirect('my-account');
	/*
	$email = $this->session->userdata('email');
	
	$this->load->model('member_model');
	
	if ($member = $this->member_model->load_member_by_email($email)) {
	  $data['member'] = $member;
	  $data['session_msg'] = $this->session->flashdata('msg');

	  $data['main_content'] = 'forms/edit_profile_form';  
	  $data['page_title'] = 'Change Password';
	  $this->load->view('includes/template', $data);
	}
	*/
	
      }		
    else
      {			
	$this->load->model('member_model');

	// check for update
	if(!$this->member_model->check_update_member())
	  {
	    $msg = 'Your have already registed with this email address, please try another one.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }
	else 
	  {		
	    // check if created sucessfully	
	    if($this->member_model->update_member())
	      {
		$this->session->set_userdata('email', $this->input->post('email'));
		$msg = 'Profile has been updated. Your login ID is: '.$this->input->post('email');
		$this->session->set_flashdata('msg', $msg);
		redirect();
	      }
	    else
	      {	
		$msg = 'Some problems occurred, please try again later.';
		$this->session->set_flashdata('msg', $msg);
		redirect();
	      }
	  }
      }
    
  } // end update member
 


}

/*

	 
 $json_obj->staff->date_of_cmnc 
 = date("Y-m-d", strtotime(str_replace('/', '-', $json_obj->staff->date_of_cmnc)));

*/
