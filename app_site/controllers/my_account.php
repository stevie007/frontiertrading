<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_account extends CI_Controller {
  
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('list');
    is_logged_in();	
  }
  
  function index()
  {    
    
    $this->db->where('id', 3);
    
    $result = $this->db->get('pages')->result();
    
    $data['main_data'] = $result[0]->text1;  
    
    
    $data['set_value'] = $this->session->flashdata('set_value');

    $email = $this->session->userdata('email');
	
    $this->load->model('member_model');
    
    if ($member = $this->member_model->load_member_by_email($email)) {
      $data['member'] = $member;
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['main_content'] = 'my_account_view';  
      $data['page_title'] = 'About Us';
      $this->load->view('includes/template', $data);
    }
  }
}

/* End of file my_account.php */
/* Location: ./system/application/controllers/my_account.php */
