<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends CI_Controller {
  
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('list');	
  }
  
  function index()
  {    
    
    $this->db->where('id', 4);
    
    $result = $this->db->get('pages')->result();
    
    $data['main_data'] = $result[0]->text1;  
    
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'contact_us_view';  
    $data['page_title'] = 'Contact Us';
    $this->load->view('includes/template', $data);
  }



  function sendmail ()
  {
    $this->load->library('form_validation');
    
    $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
    $this->form_validation->set_rules('phone', 'Phone ', 'trim');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
    $this->form_validation->set_rules('message', 'Message', 'trim|required');
    
    $this->form_validation->set_message('required', '%s is required.');

    if($this->form_validation->run())
      {	

	$this->load->library('email');
	
	$this->email->from($this->input->post('email'), $this->input->post('name'));
	$this->email->to('sales@frontiertrading.co.nz');
	//$this->email->to('ke@3a.co.nz');
	$this->email->subject('Email Form | '.$this->input->post('subject'));
	
	$msg = 'Name: '.$this->input->post('name')."\n"
	  .'Email: '.$this->input->post('email')."\n"
	  .'Comment: '.$this->input->post('message');
	
	$this->email->message($msg);
	
	$sent = 'Message Sent Successfully. Thank you for contacting Frontier Trading.';
	$error = 'Error: The message could not be sent, please email us at sales@frontiertrading.co.nz';
	
	$msg = ($this->email->send()) ? $sent : $error;
	$this->session->set_flashdata('msg', $msg);
	redirect('contact-us');
	
      }    
    else 
      {
	$msg = validation_errors('<span class="error">', '</span>'.nbs(2));
	
	$this->session->set_flashdata('msg', $msg);
	redirect('contact-us');
	
      }
    
  }
    
}

/* End of file contact_us.php */
/* Location: ./system/application/controllers/contact_us.php */
