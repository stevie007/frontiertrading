<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller {
  
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('list');	
  }
  
  function index()
  {    
    
    $this->db->where('id', 2);
    
    $result = $this->db->get('pages')->result();
    
    $data['main_data'] = $result[0]->text1;  
    
    
    $data['set_value'] = $this->session->flashdata('set_value');

    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'registration_view';  
    $data['page_title'] = 'Customer Registration';
    $this->load->view('includes/template', $data);
  }
  
  function view(){      
    $this->load->view('terms_view');
  }
  
}

/* End of file registration.php */
/* Location: ./system/application/controllers/registration.php */
