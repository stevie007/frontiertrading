<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terms extends CI_Controller {
  
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('list');	
  }
  
  function index()
  {    
    
    $this->db->where('id', 5);
    
    $result = $this->db->get('pages')->result();
    
    $data['main_data'] = $result[0]->text1;  
 
 
    $this->load->view('terms_view', $data);
 
  }

    
}

/* End of file contact_us.php */
/* Location: ./system/application/controllers/contact_us.php */
