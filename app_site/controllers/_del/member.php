<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Member extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');	
    $this->load->model('member_model');
    is_logged_in();
  }
 
  function index()
  {
 
  }
  	
  function change_password()
  {
    // $data['set_value'] = $this->session->flashdata('set_value');
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'forms/change_password_form';  
    $data['page_title'] = 'Change Password';
    $this->load->view('includes/template', $data);
  }

  function my_profile()
  {      
    $email = $this->session->userdata('email');
    if ($member = $this->member_model->load_by_email($email)) {
      $data['member'] = $member;
      $data['set_value'] = $this->session->flashdata('set_value');
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['main_content'] = 'forms/my_profile_form';  
      $data['page_title'] = 'My Account';
      $this->load->view('includes/template', $data);
    }
  }
    
  function validate_change_password()
  { 
    $this->load->library('form_validation');		
    $this->form_validation->set_rules('password_old', 'Current password', 'trim|required|min_length[4]|max_length[32]');
    $this->form_validation->set_rules('password_new', 'New password', 'trim|required|min_length[4]|max_length[32]');
    $this->form_validation->set_rules('password_rep', 'Password Confirmation', 'trim|required|matches[password_new]');
        
    if(!$this->form_validation->run())
      {
       	// $this->session->set_flashdata('set_value', $set_value);
	$this->session->set_flashdata('msg', validation_errors('<span class="error">', '</span><br />'));	
	redirect('member/change-password');
      }    
    else
      {	
	$this->load->model('member_model');		

	if($this->member_model->change_password())
	  {
	    $msg = 'Password changed successfully.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }
	else
	  {
	    $msg = 'Password could not be changed.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }

      }
  } // end change password

 

 
  function validate_my_profile()
  {
    $this->load->library('form_validation');		
    $this->form_validation->set_rules('firstname', 'Name', 'trim|required');
    $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
    $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
    $this->form_validation->set_rules('phone', 'Phone', 'trim');
    $this->form_validation->set_rules('mobile', 'Mobile', 'trim');
    $this->form_validation->set_rules('fax', 'Fax', 'trim');
    $this->form_validation->set_rules('address', 'Address Line 1', 'trim');
    $this->form_validation->set_rules('suburb', 'Address Line 2', 'trim');
    $this->form_validation->set_rules('city', 'City', 'trim');
    $this->form_validation->set_rules('postcode', 'postcode', 'trim');
   
    // not pass input validation
    if(!$this->form_validation->run())
      {	
	$set_value['firstname'] = set_value('firstname');
	$set_value['lastname'] = set_value('lastname');
	$set_value['email'] = set_value('email');
	$set_value['phone'] = set_value('phone');
	$set_value['mobile'] = set_value('mobile');
	$set_value['fax'] = set_value('fax');
	$set_value['address'] = set_value('address');
	$set_value['suburb'] = set_value('suburb');
	$set_value['city'] = set_value('city');	
	$set_value['postcode'] = set_value('postcode');

	$this->session->set_flashdata('set_value', $set_value);
	$this->session->set_flashdata('msg', validation_errors('<span class="error">', '</span><br />'));
	
	redirect('member/my-profile');
      }		
    else
      {		
	// check for update
	if(!$this->member_model->check_update())
	  {
	    $msg = 'Your have already registed with this email address, please try another one.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('member/my-profile');
	  }
	else 
	  {		
	    // check if created sucessfully	
	    if($this->member_model->update())
	      {
		$this->session->set_userdata('email', $this->input->post('email'));
		$msg = 'Profile has been updated. <br />Your login ID is: '.$this->input->post('email');
		$this->session->set_flashdata('msg', $msg);
		redirect('member/my-profile');
	      }
	    else
	      {	
		$msg = 'Nothing has been changed.';
		$this->session->set_flashdata('msg', $msg);
		redirect('member/my-profile');
	      }
	  }
      }
    
  } // end update member
 


}

/*

	 
 $json_obj->staff->date_of_cmnc 
 = date("Y-m-d", strtotime(str_replace('/', '-', $json_obj->staff->date_of_cmnc)));

*/