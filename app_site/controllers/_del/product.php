<?<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {
  
  function __construct()
  {
    parent::__construct();
    $this->load->model('product_model');	
    $this->load->model('category_model');
  }


  function index()
  {     
   
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category');
	
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    redirect('product/lib');
   
  }
  
  function detail(){

    if($url_title = $this->uri->segment(3))
      {  
    	if ($data['product'] = $this->product_model->load_by_url($url_title)) 
	  {      
	    $data['main_content'] = 'product_detail_view';  
	    $data['page_title'] = $data['product']->title;
	    $data['session_msg'] = $this->session->flashdata('msg');
	    $this->load->view('includes/template', $data);	
	  }
	else
	  { // product not found
	    show_404();
	  }
      }
  }

  function lib()
  { 
       
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');
    $data['category'] = $this->session->userdata('category');
    
    // start-cate: current main category and sub category 
    $data['cate_parent'] = $data['cate_sub'] = FALSE;
    if ($cate_obj = $this->category_model->load_by_url($data['category']) ) 
      { 
	$data['cate_parent'] = $cate_obj->parent == '(none)' ? $cate_obj->name : $cate_obj->parent;
	$data['cate_sub'] = $cate_obj->parent == '(none)' ? FALSE : $cate_obj->name;
      }
    else 
      {
	$data['cate_parent'] = $data['cate_sub'] = FALSE;
      }

    $data['cate_tree'] = $this->category_model->load_tree();
    // end-cate

    $this->load->library('pagination');
    $config['next_link'] = '&gt;';
    $config['prev_link'] = '&lt;';
    $config['first_link'] = $config['last_link'] = FALSE;

    $config['base_url'] = site_url('product/lib');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 6;
    $config['num_links'] = 5;
    $data['offset'] = $this->uri->segment(3);

    
    $product_list = $this->product_model->product_list(
						       $data['category'],
						       $data['search'], 
						       $data['filter'], 
						       $data['orderby'], 
						       $config['per_page'], 
						       $data['offset']						       
						       );
    
    $config['total_rows'] = $data['total'] = $product_list->total;
    $this->pagination->initialize($config);
    
    $data['query'] = $product_list->query;
   
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'product_view'; 
    $data['page_title'] = 'Product';     
    $this->load->view('includes/template', $data);
  }

  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('item/lib');
  }
  
  function cate()
  {
  
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category');    
    
    if ($url_title = $this->uri->segment(3))
      {
	$this->session->set_userdata('category', $url_title);
      }    
    redirect('product/lib');
  }
  

 
  function search() 
  {    
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category');
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('product/lib');
  }


  /*
// example - using of filter session variable
  function special()
  {
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category');
    
    $filter['special'] = 1;
    $this->session->set_userdata('filter', $filter);   
    redirect('product/lib');
  }
  
  function new_arrivals()
  {
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category');
    
    $filter['new'] = 1;
    $this->session->set_userdata('filter', $filter);   
    redirect('product/lib');
  }
  */

}

/* End of file product.php */
/* Location: ./system/application/controllers/product.php */