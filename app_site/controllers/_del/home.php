<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('cookie');	
  }
  
  function index()
  {
    /* keep sign in */
    if(!logged_in() && get_cookie('passwd') && get_cookie('email'))
      {
	$this->_keepsignin();
      }      
    /* end of keep sign in */
    
    $this->load->model('product_model');
   
    
    $data['session_msg'] = $this->session->flashdata('msg');
    
    $data['main_content'] = 'home_view';  
    $data['page_title'] = 'Home';
    $this->load->view('includes/template', $data);	
    
  }

  function _keepsignin()
  {
   
    if(get_cookie('email') && get_cookie('passwd'))
      {
	$email = get_cookie('email');
	$passwd = get_cookie('passwd');

	$this->load->model('member_model');
	$query = $this->member_model->login_cookie($email, $passwd);

	if($query) // if validated...
	  { 
	    $member = $this->member_model->load_member_by_email($email);
	    $data = array(
			  'email' => $email,
			  'name' => $member->firstname,
			  'avator' => $member->avator_s,
			  'is_site_logged_in' => true
			  );
	    
	    $this->session->set_userdata($data);
	    //  $msg = 'Welcome back!';
	    //  $this->session->set_flashdata('msg', $msg);	    
	  }
      }
  }



}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
