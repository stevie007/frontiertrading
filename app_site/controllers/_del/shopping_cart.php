<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shopping_cart extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('product_model');
    $this->load->helper('auth');
    //   $this->load->helper('list');	
  }
	
  function index()
  {   
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'shopping_cart_view';  
    $data['page_title'] = 'shopping cart';     
    $this->load->view('includes/template', $data);
  }


  function add_ajax() {
		
    $results = $this->db->get_where('product', array('id' => $this->input->post('id')))->result();

    $product =  $results[0];
    
    $insert = array(
		    'id' => $this->input->post('id'),
		    'qty' => $this->input->post('qty'),
		    'price' => $product->price,
		    'name' => $product->title
		    );
	 
		
    $this->cart->insert($insert);

    $data['item'] = $this->cart->total_items();
    $data['total'] = $this->cart->total();
    echo json_encode($data);
   // redirect($this->input->post('uri').'#cart');
		
  }

  function add() {
    
    $results = $this->db->get_where('product', array('id' => $this->input->post('id')))->result();
    
    $product =  $results[0];
    
    $insert = array(
		    'id' => $this->input->post('id'),
		    'qty' => $this->input->post('qty'),
		    'price' => $product->price,
		    'name' => $product->title
		    );
	 
		
    $this->cart->insert($insert);
    
    redirect('shopping-cart');
  }
  
	
  function remove($rowid) {
		
    $this->cart->update(array(
			      'rowid' => $rowid,
			      'qty' => 0
			      ));
		
    redirect('shopping-cart#cart');
		
  }

  function update() {
   
    $this->cart->update(array(
			      'rowid' => $this->input->post('rowid'),
			      'qty' => $this->input->post('qty')
			      ));
		
    redirect('shopping-cart#cart');
		
  }

}

/* End of file shopping_cart.php */
/* Location: ./system/application/controllers/shopping_cart.php */