<?<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
  var $cf_site;

  function __construct()
  {    
    parent::__construct();
    $this->load->helper('auth');	
    $this->load->helper('cookie');    
    $this->cf_site = $this->config->item('site');
    $this->load->model('member_model');
    session_start();
  }

  function index()
  {  
    if ($this->session->userdata('is_site_logged_in'))
      {
	$msg = 'Your are already logged in.';
	$this->session->set_flashdata('msg', $msg);
	redirect();
      }
    else
      {
	$data['session_msg'] = $this->session->flashdata('msg');
	$data['main_content'] = 'forms/login_form';  
	$data['page_title'] = 'Login';
	$this->load->view('includes/template', $data);
      } 
  }
	
	
  function log_out()
  {
    session_destroy();
    $data = array(
		  'email' => true, 
		  'user_name' => true, 
		  'user_id' => true, 
		  'is_site_logged_in' => true
		  );
    $this->session->unset_userdata($data);    
    $msg = 'Logged out successfully. Bye!';
    $this->session->set_flashdata('msg', $msg);
    redirect();
  }

  function sign_up()
  {
    $data['set_value'] = $this->session->flashdata('set_value');
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'forms/sign_up_form';  
    $data['page_title'] = 'Sign Up';
    $this->load->view('includes/template', $data);
  }

  function forget_password()
  {
    $data['set_value'] = $this->session->flashdata('set_value');
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'forms/forget_password_form';  
    $data['page_title'] = 'Forget Password';
    $this->load->view('includes/template', $data);
  }

  function recover_password()
  {
    $data['set_value'] = $this->session->flashdata('set_value');
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'forms/recover_password_form';  
    $data['page_title'] = 'Recover Password';
    $this->load->view('includes/template', $data);
  }

  function cancel($id = '', $code = ''){
    $msg = 
      ($id && $code && $this->member_model->cancel_resetcode($id, $code)) 
      ? 'Your reset code have been cancelled.' 
      : 'Invalid Link.';	
    $this->session->set_flashdata('msg', $msg);
    redirect();
  } // cancel

 
  function validate_login()
  {		
    $user = $this->member_model->login();
		
    if($user) // if validated...
      {
	if ($this->input->post('keepsignin')) 
	  {	    
	    $cookie_passwd = array(
				   'name'   => 'passwd',
				   'value'  => sha1($this->input->post('password')),
				   'expire' => 3600*24*14,
				   'secure' => TRUE
				   );
	    $cookie_email = array(
				  'name'   => 'email',
				  'value'  => $this->input->post('email'),
				  'expire' => 3600*24*14,
				  'secure' => TRUE
				  );
	    set_cookie($cookie_passwd); 
	    set_cookie($cookie_email); 
	  }
	
	$data = array(
		      'email' => $this->input->post('email'),
		      'user_name' => $user->firstname,
		      'user_id' => $user->id,
		      'is_site_logged_in' => true
		      );
	
	$this->session->set_userdata($data);
	$msg = 'Logged in successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect();
      }
    else // incorrect email or password
      {	
	$msg = 'Sorry, we could not log you in with that login ID and password.';
	$this->session->set_flashdata('msg', $msg);
	redirect('login');
      }
  }	

  function validate_sign_up()
  {
    $this->load->library('form_validation');
		
    $this->form_validation->set_rules('firstname', 'Name', 'trim|required');
    $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
    $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
    $this->form_validation->set_rules('email2', 'Email Confirmation', 'trim|required|matches[email]');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
    
    // pass input validation
    if($this->form_validation->run() == FALSE)
      { 
	$set_value['firstname'] = set_value('firstname');
	$set_value['lastname'] = set_value('lastname');
	$set_value['email'] = set_value('email');
	$set_value['email2'] = set_value('email2');

	$this->session->set_flashdata('set_value', $set_value);
	$this->session->set_flashdata('msg', validation_errors('<span class="error">', '</span><br />'));
	
	redirect('login/sign-up');

      }		
    else
      {			

	// check if registed
	if($this->member_model->has_email($this->input->post('email')))
	  {
	    $msg = 'Your have already registed with this email address.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('login/sign-up');
	  }
	else 
	  {		
	    // check if created sucessfully
	    if($user = $this->member_model->add())
	      {
		$msg = 'Congrats! Your account has been created. Please log in.';
		$this->session->set_flashdata('msg', $msg);
		redirect('login');
	      }
	    else
	      {
		$msg = 'Some problems occurred, please try again later.';
		$this->session->set_flashdata('msg', $msg);
		redirect('login');
	      }
	  }
      }
    
  } // end create member
  
  function validate_recover_password() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
    $this->form_validation->set_rules('resetcode', 'Reset Code', 'trim|required'); 
    
    if(!$this->form_validation->run())
      {	
	$set_value['email'] = set_value('email');
	$set_value['resetcode'] = set_value('resetcode');	
	$this->session->set_flashdata('set_value', $set_value);
	$this->session->set_flashdata('msg', validation_errors('<span class="error">', '</span><br />'));	
	redirect('login/recover-password');
      }
    else
      {	
	$member = $this->member_model->load_by_email($this->input->post('email'));
	$this->validate_reset_password($member->id, $this->input->post('resetcode'));
      }
  }
  
  function validate_reset_password($id = '', $code = ''){
    
    // if from post otherwise its from link
    // reset id and code can come from either link or post, if has post overwrite the link value
    if ($this->input->post('id') && $this->input->post('resetcode'))
      { 
	$id = $this->input->post('id');	    
	$code = $this->input->post('resetcode');
      }
    
    if ($this->member_model->check_reset($id, $code)) 
      {	    
	$this->load->library('form_validation');
	$this->form_validation->set_rules('password_new', 'New password', 'trim|required|min_length[4]|max_length[32]');
	$this->form_validation->set_rules('password_rep', 'Password Confirmation', 'trim|required|matches[password_new]');    
	
	if(!$this->form_validation->run())
	  {	
	    $member = $this->member_model->load_by_id($id);
	    $data['member'] = $member;
	    $data['resetcode'] = $code;
	    $data['id'] = $id;
	    $data['session_msg'] = validation_errors('<span class="error">', '</span><br />'); 
	    $data['main_content'] = 'forms/reset_password_form';  
	    $data['page_title'] = 'Reset Password';
	    $this->load->view('includes/template', $data);
	  }    
	else
	  {			
	    $msg = ($this->member_model->reset_password()) 
	      ? 'Password changed successfully.' 
	      : 'Password could not be changed.';	
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }
      }
    else 
      {
	$msg = 'Reset code or email address invalid.';
	$this->session->set_flashdata('msg', $msg);
	redirect();
      }
       
    
  } // end reset


  function validate_forget_password(){
    
    $this->load->library('form_validation');    
    $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
    
    if(!$this->form_validation->run())
      {	 
	$set_value['email'] = set_value('email');
	$this->session->set_flashdata('set_value', $set_value);
	$this->session->set_flashdata('msg', validation_errors('<span class="error">', '</span><br />'));	
	redirect('login/forget-password');	
      }
    else
      {		
	// check if not registed
	if(!$this->member_model->has_email($this->input->post('email')))
	  {
	    $msg = 'Your are not registed with this email address.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect('login/forget-password');
	  }
	else  // email resetcode to user  
	  {
	    $code = $this->member_model->set_resetcode($this->input->post('email'));
	    
	    $data['member'] = $this->member_model->load_by_email($this->input->post('email')); 
	    $data['sender'] = $this->cf_site['sender'];
	    $data['website'] = $this->cf_site['name'];	    
	    $letter = $this->load->view('email_templates/reset_passwd', $data, true);	    
	    $this->load->library('email');
	    $this->email->from($this->cf_site['email'], $this->cf_site['sender']);
	    $this->email->to($this->input->post('email'));
	    $this->email->subject('You requested to reset your password');
   	    $this->email->message($letter);
	    $msg = $this->email->send() ? 'Password reset code has been sent to '.$this->input->post('email').'<br />Please check your email.' : 'Email could not be sent, please contact us.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }
      }
  } // end forget password


} // end class