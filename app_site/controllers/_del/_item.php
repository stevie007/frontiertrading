<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends CI_Controller {
  
  function __construct()
  {
    parent::__construct();	
    $this->load->model('item_model');
  }


  function index()
  {     
   
    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
	
    $orderby['order'] = 'id';
    $orderby['sort'] = 'desc'; 
    $this->session->set_userdata('orderby', $orderby);
    redirect('item/lib');
   
  }
  
  function detail(){

    if($url_title = $this->uri->segment(3))
      {  
    	if ($data['item'] = $this->item_model->load_item_by_url($url_title)) 
	  {      
	    $data['main_content'] = 'item_detail_view';  
	    $data['page_title'] = $data['item']->title;
	    $this->load->view('includes/template', $data);	
	  }
	else
	  { // item not found
	    show_404();
	  }
      }
  }

  function lib()
  {  
    
    $data['cates'] = $this->item_model->load_cate_names();    
    
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');

    $this->load->library('pagination');
    $config['next_link'] = '&gt;';
    $config['prev_link'] = '&lt;';
    $config['first_link'] = $config['last_link'] = FALSE;

    $config['base_url'] = site_url('item/lib');    
    $config['uri_segment'] = 3;
    $config['per_page'] = 6;
    $config['num_links'] = 5;

    
    $item_list = $this->item_model->item_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $this->uri->segment(3)
					      );
    
    $config['total_rows'] = $data['total'] = $item_list->total;
    $this->pagination->initialize($config);
    
    $data['query'] = $item_list->query;
   
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'item_view'; 
    $data['page_title'] = 'Item';     
    $this->load->view('includes/template', $data);
  }

  function cate()
  {
    if ($category = $this->uri->segment(3))
      {
	$filter['category'] = $category;
	$this->session->set_userdata('filter', $filter);
      }    
    redirect('item/lib');
  }
 
  function search() 
  {
    if ($search = $this->input->post('search')) {      
      $this->session->set_userdata('search', $search);
    }
    redirect('item/lib');
  }


}

/* End of file item.php */
/* Location: ./system/application/controllers/item.php */
