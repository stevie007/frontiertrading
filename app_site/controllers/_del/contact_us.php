<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends CI_Controller {
  
  var $cf_site;

  function __construct()
  {
    parent::__construct();
    $this->cf_site = $this->config->item('site');	
  }
  
  function index()
  {
    
    
    $this->db->where('id', 7);
      
    $result = $this->db->get('page')->result();
      
    $data['main_data'] = $result[0]->text1;  
    
    $data['set_value'] = $this->session->flashdata('set_value');
    $data['main_content'] = 'contact_us_view';  
    $data['session_msg'] = $this->session->flashdata('msg');

    $this->load->view('includes/template', $data);
  }



  function sendmail ()
  {
    $this->load->library('form_validation');
		
    $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
    $this->form_validation->set_rules('phone', 'Phone ', 'trim');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
    $this->form_validation->set_rules('message', 'Comment', 'trim|required');
    
    if($this->form_validation->run())
      {	

	$this->load->library('email');
	
	$this->email->from($this->input->post('email'), $this->input->post('name'));
	$this->email->to($this->cf_site['email']);
	
	$this->email->subject('Online Email Form');
	
	$msg = 'Name: '.$this->input->post('name')."\n"
	  .'Email: '.$this->input->post('email')."\n"
	  .'Phone: '.$this->input->post('phone')."\n"
	  .'Comment: '.$this->input->post('message');
	
	$this->email->message($msg);
	
	$sent = 'Message Sent Successfully. Thank you.';
	$error = 'Error: The message could not be sent, please email us at '.$this->cf_site['email'];
	
	$msg = ($this->email->send()) ? $sent : $error;
	$this->session->set_flashdata('msg', $msg);

	redirect('contact-us');
	
      }    
    else 
      {
	$set_value['name'] = set_value('name');
	$set_value['phone'] = set_value('phone');
	$set_value['email'] = set_value('email');
	$set_value['message'] = set_value('message');
	
	$this->session->set_flashdata('set_value', $set_value);
	$this->session->set_flashdata('msg', validation_errors('<span class="error">', '</span><br />'));
	
	redirect('contact-us');
	
      }
    
   
  }



}

/* End of file contact_us.php */
/* Location: ./system/application/controllers/contact_us.php */
