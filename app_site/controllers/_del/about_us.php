<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About_us extends CI_Controller {
  
  function __construct()
  {
    parent::__construct();
  }
  
  function index()
  {    
    
    $this->db->where('id', 2);
    
    $result = $this->db->get('page')->result();
    
    $data['main_data'] = $result[0]->text1;  
    
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'about_us_view';  
    $data['page_title'] = 'About Us';
    $this->load->view('includes/template', $data);
  }
}

/* End of file about_us.php */
/* Location: ./system/application/controllers/about_us.php */