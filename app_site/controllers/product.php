<?php

class Product extends CI_Controller {
  
  function __construct()
  {
    parent::__construct();	
    $this->load->model('item_model');
    $this->load->helper('auth');
    $this->load->helper('list');
  }


  function index()
  {    
    if (!logged_in()) {
      $msg = 'Please login to view product information.';
      $this->session->set_flashdata('msg', $msg);
      redirect();
    }

    $this->session->unset_userdata('orderby');
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category');

    $orderby['order'] = 'title';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);
    
    $this->session->set_userdata('perpage', 10);
    redirect('product/lib');
   
  }
  
  function detail(){

    if($url_title = $this->uri->segment(3))
      {  
    	if ($data['item'] = $this->item_model->load_item_by_url($url_title)) 
	  {      
	    $data['main_content'] = 'item_detail_view';  
	    $data['page_title'] = $data['item']->title;
	    $this->load->view('includes/template', $data);	
	  }
	else
	  { // item not found
	    show_404();
	  }
      }
  }

  function lib()
  {  
    
    $data['cates'] = $this->item_model->load_cate_names();    
    
    $data['orderby'] = $this->session->userdata('orderby');
    $data['filter'] = $this->session->userdata('filter');
    $data['search'] = $this->session->userdata('search');
    $data['perpage'] = $this->session->userdata('perpage') ? $this->session->userdata('perpage') : 10;
    $data['category'] = $this->session->userdata('category');

    $this->load->library('pagination');
    $config['full_tag_open'] = '<span class="page-tag">';
    $config['full_tag_close'] = '</span>'.nbs(3);
    $config['num_tag_close'] = $config['cur_tag_close'] = nbs(3);

    $config['next_link'] = 'Next »'.nbs(3);
    $config['prev_link'] = '« Prev'.nbs(3);
    $config['first_link'] = '«« Start'.nbs(3);
    $config['last_link'] = 'End »»'.nbs(3);

    $config['base_url'] = site_url('product/lib');  
    $config['uri_segment'] = 3;
    $config['per_page'] = $data['perpage'];
    $config['num_links'] = 3;
    $data['offset'] = $this->uri->segment(3, 0);
 
    $item_list = $this->item_model->item_list(
					      $data['search'], 
					      $data['filter'], 
					      $data['orderby'], 
					      $config['per_page'], 
					      $data['offset'],
					      $data['category']
					      );
    
    $config['total_rows'] = $data['total'] = $item_list->total;
    $this->pagination->initialize($config);
    
    $data['query'] = $item_list->query;
   
    $data['json_cart'] = json_encode($this->cart->contents());
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'product_view'; 
    $data['page_title'] = 'Item';     
    $this->load->view('includes/template', $data);
  }

  function cate()
  {
    if ($this->uri->segment(4)) 
      {
	$category = urldecode($this->uri->segment(3).'/'.$this->uri->segment(4));
      }
    else 
      {
	$category = urldecode($this->uri->segment(3));
      }

    $this->session->set_userdata('category', $category);
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('search');

    $orderby['order'] = 'title';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);
    
    $this->session->set_userdata('perpage', 10);

    redirect('product/lib');
  }
 
  function search() 
  {

    $this->session->set_userdata('search', $this->input->post('search'));
    $this->session->unset_userdata('filter');
    $this->session->unset_userdata('category');
    
    $orderby['order'] = 'title';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);

    redirect('product/lib');
  }

  function new_arrivals()
  {    
    $filter['new'] = 1;
    $this->session->set_userdata('filter', $filter);  
 
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category');

    $orderby['order'] = 'title';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);
    
    $this->session->set_userdata('perpage', 10);
    redirect('product/lib');
  }
 
 
   function specials()
  {    
    $filter['special'] = 1;
    $this->session->set_userdata('filter', $filter);  
 
    $this->session->unset_userdata('search');
    $this->session->unset_userdata('category');

    $orderby['order'] = 'title';
    $orderby['sort'] = 'asc'; 
    $this->session->set_userdata('orderby', $orderby);
    
    $this->session->set_userdata('perpage', 10);
    redirect('product/lib');
  }
 
  function filter() 
  {
    if (($key = $this->uri->segment(3))
	&& 
	($value = $this->uri->segment(4))) 
      {	
	$value = $this->uri->segment(5) ? $value.'/'.$this->uri->segment(5) : $value;
	
	$filter = $this->session->userdata('filter');
	if (isset($filter[$key])):
	  unset($filter[$key]);
	else:
	  $filter[$key] = $value;
	endif;
	$this->session->set_userdata('filter', $filter);	
      }
    redirect('product/lib#cart');
  }
  

  function perpage($num) 
  {
    if ($num) {      
      $this->session->set_userdata('perpage', $num);
    }
    redirect('product/lib#cart');
  }

}

/* End of file product.php */
/* Location: ./system/application/controllers/product.php */