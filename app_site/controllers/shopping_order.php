<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shopping_order extends CI_Controller {
  var $cf_site;
  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('list');	
    $this->cf_site = $this->config->item('site');
  }
	
  function index()
  {
 
    if ($this->cart->total_items()) {

      $this->_save();

      $id = $this->session->userdata('order_id');
      $this->db->where('id', $id);
      $result = $this->db->get('site_order')->result();
		
      $data['order'] = $result[0];

      $data['order']->content = json_decode($data['order']->content); 
      
      $data['session_msg'] = $this->session->flashdata('msg');
      $data['main_content'] = 'shopping_order_view';  
      $data['page_title'] = 'Item';     
      $this->load->view('includes/template', $data);

    } else {
      $msg = 'Your shopping cart is empty!';
      $this->session->set_flashdata('msg', $msg);
      redirect('product/lib');
    }
  }

  function _save() {
	
    $order = new stdClass;

    $order->id = $this->session->userdata('order_id') ? $this->session->userdata('order_id') : 'new';

    $order->date = date("Y-m-d H:i:s"); 
    $order->subtotal = $this->cart->total(); 
    $order->gst = $this->cart->total() * 0.15;
    //    $order->delivery = 9;
    $order->total = $this->cart->total() * 1.15;

    $order->content = json_encode($this->cart->contents()); 
    $order->cust_id = $this->session->userdata('user_id'); 

    if ($order->id == 'new') {
      $this->db->insert('site_order', $order); 
      $this->session->set_userdata('order_id', $this->db->insert_id());
    } else {
      $this->db->where('id', $order->id);
      $this->db->update('site_order', $order); 
    }

  }
	
  function cancel() {
		
    $id = $this->session->userdata('order_id');   
    $this->db->where('id', $id);
    $this->db->delete('site_order');
    $this->session->unset_userdata('order_id');
    redirect('products');
  }
  function prt(){       
    $id =  $this->session->userdata('sent_order_id'); 
    $this->db->where('id', $id);
    $result = $this->db->get('site_order')->result();
    
    $data['order'] = $result[0];
    
    $data['order']->content = json_decode($data['order']->content); 
    
    $data['order']->cust_id;
    // client
    $this->db->where('id', $data['order']->cust_id);
    
    $result = $this->db->get('member')->result();

    $data['customer'] =  $result[0];      
    $this->load->view('shopping_order_print_view',$data);
  }

  function view(){
  
    
    $id =  $this->session->userdata('sent_order_id'); 
    $this->db->where('id', $id);
    $result = $this->db->get('site_order')->result();
    
    $data['order'] = $result[0];
    
    $data['order']->content = json_decode($data['order']->content); 
    
    $data['order']->cust_id;
    // client
    $this->db->where('id', $data['order']->cust_id);
    
    $result = $this->db->get('member')->result();

    $data['customer'] =  $result[0];
    
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'shopping_order_sent_view';  
    $data['page_title'] = 'Item';     
    $this->load->view('includes/template', $data);
  }



  function send()
  {


    if($id = $this->input->post('order_id'))
      {	
	// finishise order and empty cart
	$this->session->unset_userdata('order_id');
	$this->cart->destroy();

	$this->db->where('id', $id);
	$result = $this->db->get('site_order')->result();	
	$order = $result[0];	

	// add client note
	$order->trans = 0; // mark as not transferred
	$order->note = $this->input->post('note');
	$this->db->where('id', $order->id);
	$this->db->update('site_order', $order);





  $From     = $this->session->userdata('email');
  //$to       = $this->cf_site['email'];
  $to       = "sales@frontiertrading.co.nz";
  $subject  = 'Online Order #'.$order->id;


  $headers= 'From: '.$From. "\r\n" .
          'Reply-To: '.$to. "\r\n";


  // $strFilesName = 'INVOICE_'.$data['invoice']->id.'.pdf';
  // //$headers.= "--".$strSid."\n";  
  // $headers.= "Content-Type: application/octet-stream; name=\"".$strFilesName."\"\n";  
  // $headers.= "Content-Transfer-Encoding: base64\n";  
  // $headers.= "Content-Disposition: attachment; filename=\"".$strFilesName."\"\n\n";  
  // $headers.= $attach."\n\n"; 


  $msg = 'Name: '.$this->session->userdata('user_name')."\n"
    .'Email: '.$this->session->userdata('email')."\n"
    .'Note: '.$this->input->post('note');



  mail($to, $subject, $msg, $headers);





	// $this->load->library('email');
	
	// $this->email->from($this->session->userdata('email'), $this->session->userdata('user_name'));
	// $this->email->to($this->cf_site['email']);

	
	// $this->email->subject('Online Order #'.$order->id);
	
	// $msg = 'Name: '.$this->session->userdata('user_name')."\n"
	//   .'Email: '.$this->session->userdata('email')."\n"
	//   .'Note: '.$this->input->post('note');
	
	// $this->email->message($msg);
	
	$sent = 'Order Sent Successfully. Thank you.<script language="javascript">alert("Order Sent Successfully. Thank you.")</script>';
	$error = 'Error: The message could not be sent, please email us at '.$this->cf_site['email'].'<script language="javascript">alert("The message could not be sent, please email us at '.$this->cf_site['email'].'")</script>';
	
	$this->session->set_userdata('sent_order_id', $id);
	
	// $msg = ($this->email->send()) ? $sent : $error;
	// $this->session->set_flashdata('msg', $msg);
	redirect('shopping-order/view');
	
      }    
  
  }// end send

}

/* End of file shopping_order.php */
/* Location: ./system/application/controllers/shopping_order.php */
