<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
	
  var $cf_site;

  function __construct()
  {    
    parent::__construct();
    $this->load->helper('auth');	
    $this->load->helper('cookie');
    $this->load->helper('list');    
    $this->cf_site = $this->config->item('site');
    session_start();
  }

  function index()
  {
    if ($this->session->userdata('is_site_logged_in'))
      {
	$msg = 'Your are already logged in.';
	$this->session->set_flashdata('msg', $msg);
	redirect();
      }
    else
      {
	redirect('registration'); 
	/*
	$data['session_msg'] = $this->session->flashdata('msg');
	$data['main_content'] = 'forms/login_form';  
	$data['page_title'] = 'Login';
	$this->load->view('includes/template', $data);
	*/
      } 
  }
	
  function validate_login()
  {
    $this->load->model('member_model');
    $user = $this->member_model->login();
    
    if($user) // if validated...
      {

	if ($this->input->post('keepsignin')) 
	  {	    
	    $cookie_passwd = array(
				   'name'   => 'passwd',
				   'value'  => sha1($this->input->post('password')),
				   'expire' => 3600*24*14,
				   'secure' => TRUE
				   );
	    $cookie_email = array(
				  'name'   => 'email',
				  'value'  => $this->input->post('email'),
				  'expire' => 3600*24*14,
				  'secure' => TRUE
				  );
	    set_cookie($cookie_passwd); 
	    set_cookie($cookie_email); 
	  }
	
	$data = array(
		      'email' => $this->input->post('email'),
		      'user_name' => $user->firstname,
		      'user_id' => $user->id,
		      'is_site_logged_in' => true
		      );
	
	$this->session->set_userdata($data);
	$msg = 'Logged in successfully.';
	$this->session->set_flashdata('msg', $msg);
	redirect();
      }
    else // incorrect email or password
      {	
	$msg = 'Sorry, we could not log you in with that login ID and password. If your account is inactive please contact us.';
	$this->session->set_flashdata('msg', $msg);
	redirect();
      }
  }	
	
	
  function logout()
  {
    $this->session->sess_destroy();
    session_destroy();
    redirect();
  }



  function checkbox_check($str)
  {
    if ($str != 'on')
      {
	$this->form_validation->set_message('checkbox_check', 'Please agree to the Terms & Conditions.');
	return FALSE;
      }
    else
      {
	return TRUE;
      }
  }
	
  function signup()
  {
    $this->load->library('form_validation');

    $this->form_validation->set_rules('checkbox', 'checkbox', 'callback_checkbox_check');

    $this->form_validation->set_rules('company', 'Company', 'trim|required');
    $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
    $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
    $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
    $this->form_validation->set_rules('email2', 'Email Confirm', 'trim|required|matches[email]');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');    
		
    $this->form_validation->set_rules('address', 'Address', 'trim|required');
    $this->form_validation->set_rules('city', 'City', 'trim|required');
    $this->form_validation->set_rules('suburb', 'Suburb', 'trim|required');
    $this->form_validation->set_rules('telephone', 'Phone', 'trim|required');
    $this->form_validation->set_rules('postcode', 'Suburb', 'trim');

    $this->form_validation->set_rules('address2', 'Address', 'trim');
    $this->form_validation->set_rules('city2', 'City', 'trim');
    $this->form_validation->set_rules('suburb2', 'Suburb', 'trim');
    $this->form_validation->set_rules('postcode2', 'Suburb', 'trim');

    $this->form_validation->set_message('required', '%s is required.');

    // pass input validation
    if($this->form_validation->run() == FALSE)
      { 
	$set_value['company'] = set_value('company');
	$set_value['firstname'] = set_value('firstname');
	$set_value['lastname'] = set_value('lastname');
	$set_value['email'] = set_value('email');
	$set_value['email2'] = set_value('email2');
	$set_value['telephone'] = set_value('telephone'); 
	$set_value['mobile'] = set_value('mobile');
	$set_value['address'] = set_value('address');
	$set_value['city'] = set_value('city');
	$set_value['suburb'] = set_value('suburb');
	$set_value['postcode'] = set_value('postcode');
	$set_value['address2'] = set_value('address2');
	$set_value['city2'] = set_value('city2');
	$set_value['suburb2'] = set_value('suburb2');
	$set_value['postcode2'] = set_value('postcode2');
	

	$this->session->set_flashdata('set_value', $set_value);
	$this->session->set_flashdata('msg', validation_errors('<span class="error">', '</span>'.nbs(2)));
	
	redirect('registration');
      }		
    else
      {			
	$this->load->model('member_model');

	// check if registed
	if($this->member_model->has_email($this->input->post('email')))
	  {
	    $msg = 'Your have already registed with this email address.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }
	else 
	  {		
	    // check if created sucessfully
	    if($query = $this->member_model->create_member())
	      {
	      
	      	$this->load->library('email');
		$this->email->from($this->input->post('email'), $this->input->post('firstname'));

		$this->email->to($this->cf_site['email']);
		$this->email->subject('Online Registration');
		$msg = 'Please check member database.';
		$this->email->message($msg);
		$this->email->send();
		
		$msg = 'Congrats! Your account has been created. We will contact you and activate your account.';
		$this->session->set_flashdata('msg', $msg);
		redirect();
	      }
	    else
	      {
		$msg = 'Some problems occurred, please try again later.';
		$this->session->set_flashdata('msg', $msg);
		redirect();
	      }
	  }
      }
    
  } // end create member
  

  function recover($id = '', $code = ''){
  
    if (!$id && !$code && !$this->input->post('id')) 
      {
	$this->load->library('form_validation');
	$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
	$this->form_validation->set_rules('resetcode', 'Reset Code', 'trim|required');
 
 
	if(!$this->form_validation->run())
	  {	
	    $data['session_msg'] = validation_errors('<span class="error">', '</span>'.nbs(2)).$this->session->flashdata('msg');
	    $data['main_content'] = 'forms/recover_form';  
	    $data['page_title'] = 'Recover Password';
	    $this->load->view('includes/template', $data);
	  }
	else
	  {	
	    $this->load->model('member_model'); 
	    $member = $this->member_model->load_member_by_email($this->input->post('email'));
	    $this->recover($member->id, $this->input->post('resetcode'));
	  }
      
      } 
    else 
      { 
	$this->load->model('member_model');
	
	// if from post otherwise its from link
	// reset id and code can come from either link or post, if has post overwrite the link value
	if ($this->input->post('id') && $this->input->post('resetcode'))
	  {
	    $id = $this->input->post('id');	    
	    $code = $this->input->post('resetcode');
	  }
	
	if ($this->member_model->check_reset($id, $code)) 
	  {	    
	    $this->load->library('form_validation');
	    $this->form_validation->set_rules('password_new', 'New password', 'trim|required|min_length[4]|max_length[32]');
	    $this->form_validation->set_rules('password_rep', 'Password Confirmation', 'trim|required|matches[password_new]');
    
    
	    if(!$this->form_validation->run())
	      {	
		$member = $this->member_model->load_member_by_id($id);
		$data['member'] = $member;
		$data['resetcode'] = $code;
		$data['id'] = $id;

 
		$data['session_msg'] = validation_errors('<span class="error">', '</span>'.nbs(2)).$this->session->flashdata('msg');
		$data['main_content'] = 'forms/reset_password_form';  
		$data['page_title'] = 'Reset Password';
		$this->load->view('includes/template', $data);
	      }    
	    else
	      {			
		$msg = ($this->member_model->reset_password()) 
		  ? 
		  'You have set your password successfully. Now you can use it to login.<script language="javascript">alert("You have set your password successfully. Now you can use it to login.")</script>' 
		  : 
		  'Password could not be changed.';	
		$this->session->set_flashdata('msg', $msg);
		redirect();
	      }
	  }
	else 
	  {
	    $msg = 'Reset code expired or email address invalid.<script language="javascript">alert("Reset code expired or email address invalid.")</script>';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }
      }     
    
  } // end recover



  function forget_password(){
    
    $this->load->library('form_validation');
    
    $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
    
    
    if(!$this->form_validation->run())
      {	 
	$data['session_msg'] = validation_errors('<span class="error">', '</span>'.nbs(2)).$this->session->flashdata('msg');
	$data['main_content'] = 'forms/forget_password_form';  
	$data['page_title'] = 'Forget Password';
	$this->load->view('includes/template', $data);
      }
    else
      {		
	$this->load->model('member_model');

	// check if not registed
	if(!$this->member_model->has_email($this->input->post('email')))
	  {
	    $msg = 'Your are not registed with this email address.';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }
	else  // email resetcode to user  
	  {
	    $code = $this->member_model->set_resetcode($this->input->post('email'));
	    
	    $data['member'] = $this->member_model->load_member_by_email($this->input->post('email')); 
	    $data['sender'] = $this->cf_site['sender'];
	    $data['website'] = $this->cf_site['name'];
	    $letter = $this->load->view('email_templates/reset_passwd', $data, true);
	    
	    
	    $this->load->library('email');
	    $this->email->from($this->cf_site['email'], $this->cf_site['sender']);
	    $this->email->to($this->input->post('email'));

	    
	    $this->email->subject('You requested to reset your password');
   
	    $this->email->message($letter);
	    
	    $this->email->send();
	    
	    $msg = 'Please check your email.<script language="javascript">alert("Please check your email.")</script>';
	    $this->session->set_flashdata('msg', $msg);
	    redirect();
	  }
      }
  } // end forget password

  function cancel($id = '', $code = ''){
    $this->load->model('member_model');
    $msg = ($id && $code && $this->member_model->cancel_resetcode($id, $code)) 
      ? 
      'Your reset code have been cancelled.' :'Invalid Link.';	
    $this->session->set_flashdata('msg', $msg);
    redirect();
  } // cancel
}
