<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->helper('auth');
    $this->load->helper('cookie');
    $this->load->helper('list');	
  }
  

  function index()
  {    
    /* keep sign in */
    if(!logged_in() && get_cookie('passwd') && get_cookie('email'))
      {
	$this->_keepsignin();
      }      
    /* end of keep sign in */
    
    $this->load->model('item_model');
    $data['cates'] = $this->item_model->load_cate_names();    
    
    $this->db->where('id', 1);
    $result = $this->db->get('pages')->result();
    $data['main_data'] = $result[0]->text1;  
    $data['session_msg'] = $this->session->flashdata('msg');
    $data['main_content'] = 'home_view';  
    $data['page_title'] = 'Home';
    $this->load->view('includes/template', $data);	
    
  }

  function _keepsignin()
  {
   
    if(get_cookie('email') && get_cookie('passwd'))
      {
	$email = get_cookie('email');
	$passwd = get_cookie('passwd');

	$this->load->model('member_model');
	$query = $this->member_model->login_cookie($email, $passwd);

	if($query) // if validated...
	  { 
	    $member = $this->member_model->load_member_by_email($email);
	    $data = array(
			  'email' => $email,
			  'name' => $member->firstname,
			  'is_site_logged_in' => true
			  );
	    
	    $this->session->set_userdata($data);
	    //  $msg = 'Welcome back!';
	    //  $this->session->set_flashdata('msg', $msg);	    
	  }
      }
  }



}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
