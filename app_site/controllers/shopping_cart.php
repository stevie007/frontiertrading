<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shopping_cart extends CI_Controller {

  function __construct()
  {
    parent::__construct();	
    $this->load->model('item_model');
    $this->load->helper('auth');
    $this->load->helper('list');	
  }
	
  function index()
  {   
    $msg = ''; 
    if ($cart = $this->cart->contents()) 
	 foreach ($cart as $item) {
	  if (!$this->item_model->check_by_id($item['id'])) {
	  	$this->cart->update(array('rowid' => $item['rowid'], 'qty' => 0));
	  	$msg .= 'Remove Item: '.$item['name'].'(not available for order).';
	  }
	}
    $msg .= $msg ? '<script language="javascript">alert("'.$msg.'")</script>' : '';
    
    $data['session_msg'] = $this->session->flashdata('msg').$msg;
    $data['main_content'] = 'shopping_cart_view';  
    $data['page_title'] = 'shopping cart';     
    $this->load->view('includes/template', $data);
     
  }


  function add() {
		
    $results = $this->db->get_where('items', array('id' => $this->input->post('id')))->result();

    $product =  $results[0];
    
    $insert = array(
		    'id' => $this->input->post('id'),
		    'qty' => $this->input->post('qty'),
		    'price' => $this->input->post('price'),
		    'name' => $product->title
		    );
	 
		
    $this->cart->insert($insert);

    $data['item'] = $this->cart->total_items();
    $data['total'] = $this->cart->total();
    $data['contents'] = $this->cart->contents();
    echo json_encode($data);
   // redirect($this->input->post('uri').'#cart');
		
  }
  
  function cart() {
	
	  
    echo json_encode($this->cart->contents());
 
		
  }
	
  function remove($rowid) {
		
    $this->cart->update(array(
			      'rowid' => $rowid,
			      'qty' => 0
			      ));
		
    redirect('shopping-cart#cart');
		
  }

  function update() {
   
    $this->cart->update(array(
			      'rowid' => $this->input->post('rowid'),
			      'qty' => $this->input->post('qty')
			      ));
		
    redirect('shopping-cart#cart');
		
  }

}

/* End of file shopping_cart.php */
/* Location: ./system/application/controllers/shopping_cart.php */