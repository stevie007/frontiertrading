<?php echo $main_data;?>



<form id="form2" name="form2" method="post" action="<?php echo site_url('member/profile');?>">
<?php echo form_hidden(array('id' => $member->id));?>
  <table width="590" border="0">
    <tr>
      <td width="445"><span class="black14bold">My Account Information:</span><br /></td>
      <td width="409">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Company Name (*)</td>
      <td></td>
    </tr>
    <tr>
      <td><input type="text" name="company" id="company" class="textbox" value="<?php echo $set_value ? $set_value['company'] : $member->company;?>"/></td>
      <td></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>First Name (*)</td>
      <td>Last Name (*)</td>
    </tr>
    <tr>
      <td><input type="text" name="firstname" id="firstname" class="textbox" value="<?php echo $set_value ? $set_value['firstname'] : $member->firstname;?>"/></td>
      <td><input type="text" name="lastname" id="lastname" class="textbox" value="<?php echo $set_value ? $set_value['lastname'] : $member->lastname;?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Phone (*)</td>
      <td>Mobile</td>
    </tr>
    <tr>
      <td><input type="text" name="telephone" id="telephone" class="textbox" value="<?php echo $set_value ? $set_value['telephone'] : $member->telephone;?>"/></td>
      <td><input type="text" name="mobile" id="mobile" class="textbox" value="<?php echo $set_value ? $set_value['mobile'] : $member->mobile;?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
   <tr>
      <td colspan="2"><span class="black14bold">Delivery Address</span><br /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Address (*)</td>
      <td>Suburb (*)</td>

    </tr>
    <tr>
      <td><input type="text" name="address" id="address" class="textbox" value="<?php echo $set_value ? $set_value['address'] : $member->address;?>"/></td>
      <td><input type="text" name="suburb" id="suburb" class="textbox" value="<?php echo $set_value ? $set_value['suburb'] : $member->suburb;?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>City (*)</td>
      <td>Post Code </td>

    </tr>
    <tr>
      <td><input type="text" name="city" id="city" class="textbox" value="<?php echo $set_value ? $set_value['city'] : $member->city;?>"/></td>
      <td><input type="text" name="postcode" id="postcode" class="textbox" value="<?php echo $set_value ? $set_value['postcode'] : $member->postcode;?>"/></td>
    </tr>

    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
   <tr>
      <td colspan="2"><span class="black14bold">Billing Address (If different from Delivery Address)</span><br /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Address</td>
      <td>Suburb</td>

    </tr>
    <tr>
      <td><input type="text" name="address2" id="address2" class="textbox" value="<?php echo $set_value ? $set_value['address2'] : $member->address2;?>"/></td>
      <td><input type="text" name="suburb2" id="suburb2" class="textbox" value="<?php echo $set_value ? $set_value['suburb2'] : $member->suburb2;?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>City </td>
      <td>Post Code</td>

    </tr>
    <tr>
      <td><input type="text" name="city2" id="city2" class="textbox" value="<?php echo $set_value ? $set_value['city2'] : $member->city2;?>"/></td>
      <td><input type="text" name="postcode2" id="postcode2" class="textbox" value="<?php echo $set_value ? $set_value['postcode2'] : $member->postcode2;?>"/></td>
    </tr>
    <tr>
      <td colspan="2"><hr class="line03"/>
        <br />
        <br />
        <br /></td>
    </tr>
   <tr>
      <td colspan="2"><span class="black14bold">Login User Detail</span><br /></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td>Email Address (your login ID)</td>
      <td></td>
    </tr>
    <tr>
      <td><input type="text" name="email" id="email" class="textbox" value="<?php echo $set_value ? $set_value['email'] : $member->email;?>"/></td>
      <td></td>
    </tr>
   
    <tr>
      <td colspan="2"><hr class="line03"/>
        <br />
        <br />
        <br /></td>
    </tr>
    <tr>
      <td colspan="2" align="left"><input type="submit" name="send" id="send" value="Submit" class="button03"  />
   </td>
    </tr>
  </table>
</form>
