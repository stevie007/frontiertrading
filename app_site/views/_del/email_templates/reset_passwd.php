Hi <?php echo $member->firstname;?>,

You recently asked to reset your <?php echo $website;?> password. To complete your request, please follow this link:

<?php echo base_url();?>/login/recover-password/<?php echo $member->id;?>/<?php echo $member->resetcode;?>

Alternately, you may go to <?php echo base_url();?>/login/recover-password and enter the following password reset code:

<?php echo $member->resetcode;?>


*Didn't Request This Change?*
If you did not request a new password, please follow this link to cancel:

<?php echo base_url();?>/login/cancel/<?php echo $member->id;?>/<?php echo $member->resetcode;?>

Thanks,
<?php echo $sender;?>
