

<h1 id="main-title">Product Page</h1> 
<h2>Product Catalogue Mechanisms</h2>



   <ul id="cates-menu-main">
   <?php
   
foreach ($cate_tree as $cate) 
  {
    if (is_array($cate)) 
      {
	$cate = array_shift($cate);
	$class = strcasecmp($cate->name, $cate_parent) ? 'cate-menu' : 'black';
	echo '<li><a href="'.site_url('product/cate/'.$cate->url_title).'" class="'.$class.'">'.$cate->name.'</a></li>'; 
	
      }
    else
      {	
	$class = strcasecmp($cate->name, $cate_parent) ? 'cate-menu' : 'black';
	echo '<li><a href="'.site_url('product/cate/'.$cate->url_title).'" class="'.$class.'">'.$cate->name.'</a></li>';
      }
  }
?>     
</ul>

  <ul id="cates-menu-sub">
  <?php
  foreach ($cate_tree as $cate) 
{
  if (is_array($cate)) 
    {
      $tmp = array_shift($cate);
        if (strcasecmp($cate_parent, $tmp->name) == 0) {
	foreach ($cate as $subcate){
	  $class = strcasecmp($subcate->name, $cate_sub) ? 'cate-menu' : 'black';
	  echo '<li><a href="'.site_url('product/cate/'.$subcate->url_title).'" class="'.$class.'">'.$subcate->name.'</a></li>';
      	}
      }
    }
}
?>
</ul>


<br />
<h2>Search</h2>
<br class="clr" />
<?php 
 
echo form_open('product/search', array('id' => 'standard'));
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => 'Search..',
		      'class'       => 'blur mid'
		      ) 
		 );
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go',
		       'class'       => 'btn-mid'
		       )
		 );
echo form_close(); 
 

if ($search)
{
echo '&nbsp;&nbsp;&nbsp; Listing search results for: '.$search;
}
echo '&nbsp;&nbsp;&nbsp; Total: '.$total;

?>

   <h2>
   <?php echo $this->pagination->create_links(); ?>
   </h2>


   <br class="clr" />
   <?php 
   foreach($query->result() as $row):
   ?>
   <p>
   <img src="<?php echo base_url().'uploads/product_images/'.$row->image_small;?>" alt="" />
   <br />
   Title:
   <?php echo $row->title;?>
   <br />
   Category:	   
   <?php echo $row->category;?>

   <a href="<?php echo base_url().'product/detail/'.$row->url_title;?>">&lt;--Detail--&gt;</a>	  
   </p>

 <?php endforeach;?>

	





