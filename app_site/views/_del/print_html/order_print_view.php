
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td valign="top">
      <h2>Purchase Order</h2>
      <p>Company LTD<br />
	Address<br />
	Address<br />

	AUCKLAND</p>
    </td>
    <td valign="top" width="10%" align="right"></td>
  </tr>
</table>
<table width="100%">
  <tr>
    <td width="100%" align="center">
    </td>

  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="2" width="100%">
  <!-- begin customer information --> 
  <tr class="sectiontableheader"> 
    <th colspan="2" align="left" bgcolor="#CCCCCC">Order Information</th>
  </tr>
  <tr> 
    <td>Order Number:</td>

    <td><?php echo $order->id;?></td>
  </tr>
  
  <tr> 
    <td>Order Date:</td>
    <td><?php echo $order->date;?></td>
  </tr>
  <tr> 
    <td>Order Status:</td>

    <td>Pending</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <!-- End Customer Information --> 
  <!-- Begin 2 column bill-ship to --> 
  <tr class="sectiontableheader"> 
    <th colspan="2" align="left" bgcolor="#CCCCCC">Customer Information</th>

  </tr>
  <tr valign="top"> 
    <td width="50%"> <!-- Begin BillTo -->
      <table width="100%" cellspacing="0" cellpadding="2" border="0">
	<tr> 
	  <td colspan="2"><div align="left"><strong>Invoice To</strong></div></td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Email:</div></td>

	  <td><?php echo $customer->email;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Company Name:</div></td>
	  <td><?php echo '';?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">First Name:</div></td>

	  <td><?php echo $customer->firstname;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Last Name:</div></td>
	  <td><?php echo $customer->lastname;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Address 1:</div></td>

	  <td><?php echo $customer->address1;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Address 2:</div></td>
	  <td>	<?php echo $customer->address2;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">City:</div></td>

	  <td>	<?php echo $customer->city;?>		</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Postal Code:</div></td>
	  <td><?php echo $customer->postcode;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Country:</div></td>

	  <td><?php echo 'New Zealand';?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Phone:</div></td>
	  <td><?php echo $customer->telephone;?>			</td>
	</tr>
	<tr> 
	  <td align="right"><div align="left">Mobile phone:</div></td>

	  <td>	<?php echo $customer->mobile;?>		</td>
	</tr>
      </table>
      <!-- End BillTo --> </td>
    <td width="50%"> <div align="left">
	<!-- Begin ShipTo --> 
	
      </div>

      <table width="100%" cellspacing="0" cellpadding="2" border="0">

	<tr> 
	  <td colspan="2"><div align="left"><strong>Deliver To</strong></div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Company Name:</td>
	  <td width="65%"><div align="left">
	      <?php echo '';?>	    </div></td>

	</tr>

	<tr> 
	  <td width="35%" align="left">&nbsp;First Name:</td>
	  <td width="65%"><div align="left">
	      <?php echo $customer->firstname;?>	    </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Last Name:</td>

	  <td width="65%"><div align="left">

	      <?php echo $customer->lastname;?>	    </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Address 1:</td>
	  <td width="65%"><div align="left">
	      <?php echo $customer->address1;?>		    </div></td>

	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Address 2:</td>

	  <td width="65%"><div align="left"><?php echo $customer->address2;?>	
	  </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;City:</td>

	  <td width="65%"><div align="left">
	      <?php echo $customer->city;?>		    </div></td>
	</tr>

	<tr> 
	  <td width="35%" align="left">&nbsp;Postal Code:</td>
	  <td width="65%"><div align="left">
	     	<?php echo $customer->postcode;?>	    </div></td>

	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Country:</td>
	  <td width="65%"><div align="left">

	      New Zealand		    </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Phone:</td>

	  <td width="65%"><div align="left">
	      <?php echo $customer->telephone;?>		    </div></td>
	</tr>
	<tr> 
	  <td width="35%" align="left">&nbsp;Mobile phone:</td>

	  <td width="65%"><?php echo $customer->mobile;?><div align="left">
	  </div></td>
	</tr>

      </table>
      <!-- End ShipTo --> 
      <!-- End Customer Information --> 
    </td>
  </tr>
  <tr> 
    <td colspan="2">&nbsp;</td>

  </tr>
  
  <tr>
    <td colspan="2">&nbsp;</td>

  </tr>
  <!-- Begin Order Items Information --> 
  <tr class="sectiontableheader"> 
    <th colspan="2" align="left" bgcolor="#CCCCCC">Order Items</th>
  </tr>
  <tr>

    <td colspan="4">
    </td>
  </tr>

  <!-- END HACK EUGENE -->
  <tr> 
    <td colspan="2"> 
      <table width="100%" cellspacing="0" cellpadding="2" border="0">
	<tr align="left"> 
	  <th>ID</th>
	  <th>Name</th>

	  <th>QTY</th>

	  <th>Price</th>
	  <th align="right">Total&nbsp;&nbsp;&nbsp;</th>
	</tr>
	<?php foreach ($order->content as $item): ?>
	<tr align="left"> 
	  <td valign="top"><?php echo $item->id; ?></td>
	  <td valign="top"><?php echo $item->name; ?>	          </td>

	  <td valign="top"><?php echo $item->qty; ?></td>
	  <td valign="top">NZD $<?php echo $item->price; ?></td>
	  <td valign="top" align="right">NZD $<?php echo $item->subtotal; ?>&nbsp;&nbsp;&nbsp;</td>
	</tr> 
	<?php endforeach; ?>
	
	
	<tr> 
	  <td colspan="4" align="right">&nbsp;&nbsp;</td>
	  <td>&nbsp;</td>
	</tr>

	<tr> 
	  <td colspan="4" align="right">SubTotal :</td>
	  <td align="right">NZD $<?php echo number_format($order->subtotal, 2,'.',','); ?> &nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr> 
	  <td colspan="4" align="right">GST :</td>
	  <td align="right">NZD $<?php echo number_format($order->gst, 2,'.',',');?>&nbsp;&nbsp;&nbsp;</td>

	</tr>

	<tr> 
	  <td colspan="3" align="right">&nbsp;</td>
	  <td colspan="2" align="right"><hr/></td>
	</tr>
	<tr> 
	  <td colspan="4" align="right">
	    <strong>Total: </strong>
	  </td>
  
	  <td align="right"><strong>NZD $<?php echo number_format($order->total, 2,'.',',');?></strong>&nbsp;&nbsp;&nbsp;</td>

	</tr>
	<tr> 
	  <td colspan="3" align="right">&nbsp;</td>
	  <td colspan="2" align="right"><hr/></td>
	</tr>
	<tr> 
	  <td colspan="3" align="right">&nbsp;</td>
	  <td colspan="2" align="right">&nbsp;&nbsp;&nbsp;</td>

	</tr>
      </table>

    </td>
  </tr>
</table>
<div align="right"><!-- End Order Items Information --> 
  
  <strong>PLEASE NOTE:</strong>Delivery costs are not included in your Order Total<br />
  
  <!-- Begin Payment Information --> 
  
</div>

<table width="100%">
  <tr class="sectiontableheader"> 
    <th align="left" colspan="2">Payment Information</th>

  </tr>
  <tr> 
    <td width="20%">Payment Method :</td>
    <td>On Invoice </td>
  </tr>

  <!-- end payment information --> 
</table>

<table width="100%">
  <tr class="sectiontableheader"> 
    <th align="left" colspan="2">Customer's Note</th>

  </tr>
  <tr> 
    <td width="20%"><?php echo $order->note;?></td>
    <td> </td>
  </tr>


</table>
