


<h2>Website User Login Mechanisms</h2>


<ul>
<li>
<?php echo anchor('login', 'Log In');?>
</li>
<li>
<br />
</li>
<li>
<?php echo anchor('login/sign-up', 'Create Account');?>
</li>
<li>
<?php echo anchor('login/forget-password', 'Forgot your password?'); ?>
</li>
<li>
<?php echo anchor('login/recover-password', 'Recover Password'); ?>
</li>
<li>
<br />
</li>
<li>
<?php echo anchor('member/my-profile', 'Edit Profile'); ?>
</li>
<li>
<?php echo anchor('member/change-password', 'Change Password'); ?>
</li>
<li>
<?php echo anchor('login/log-out', 'Log Out'); ?>
</li>
</ul>

<h2>Product Catalogue Mechanisms</h2>
<br />
<?php echo anchor('product', 'Product Library'); ?>
<br />
<br />
<?php 
echo form_open('product/search', array('id' => 'standard'));
echo form_input( 
		array(
		      'name'        => 'search',
		      'value'	      => 'Search..',
		      'class'       => 'blur mid'
		      ) 
		 );
echo form_submit(
		 array(
		       'name'        => 'submit',
		       'value'       => 'Go',
		       'class'       => 'btn-mid'
		       )
		 );
echo form_close(); 
?>
