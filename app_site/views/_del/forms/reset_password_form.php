
<div id="main">
<h1 id="title">Reset Password</h1>	
<br />
<?php
echo form_open('login/validate_reset_password', array('id' => 'standard'));
echo form_hidden(array('resetcode' => $resetcode));
echo form_hidden(array('id' => $id));

echo form_label('Your login ID (Email):', 'email', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'email',
		      'id'          => 'email',
		      'value'       => $member->email,
		      'readonly'    => 'readonly',
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);
echo form_label('Choose a new password:', 'passwd_new', array('class' => 'mid'));
echo form_password(
		   array(
			 'name' => 'password_new', 
			 'id' => 'passwd_new',
			 'class'       => 'mid'
			 )
		   ); 
echo br(2);
echo form_label('Re-enter new password:', 'passwd_rep', array('class' => 'mid'));
echo form_password(array('name' => 'password_rep', 
			 'id' => 'passwd_rep',
			 'class'       => 'mid'
			 )); 
echo br(3);
echo form_submit(array('name'=>'submit', 'class'=>'btn-mid'), 'Submit');
echo br(4);
echo form_close();
?>
<?php 
//echo validation_errors(); 
?>
</div>



