
<div id="main">
<h1 id="title">My Profile</h1>	
<br />
<div id="btn-menu">
<?php
echo anchor('member/change-password', 'Change Password', array('class'=>'btn-mid'));
echo nbs(4);
echo anchor('login/log-out', 'Log Out', array('class'=>'btn-mid'));
?>
</div>
<br />
<?php
echo form_open('member/validate_my_profile', array('id' => 'standard'));
echo form_hidden(array('id' => $member->id));

echo form_label('First Name:', 'firstname', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'firstname',
		      'id'          => 'firstname',
		      'value'       => $set_value ? $set_value['firstname'] : $member->firstname,
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Last Name:', 'lastname', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'lastname',
		      'id'          => 'lastnem',
		      'value'       => $set_value ? $set_value['lastname'] : $member->lastname,
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Your Email:', 'email', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'email',
		      'id'          => 'email',
		      'value'       => $set_value ? $set_value['email'] : $member->email,
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Your Phone:', 'phone', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'phone',
		      'id'          => 'phone',
		      'value'       => $set_value ? $set_value['phone'] : $member->phone,
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Your Mobile:', 'mobile', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'mobile',
		      'id'          => 'mobile',
		      'value'       => $set_value ? $set_value['mobile'] : $member->mobile,
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Your Fax:', 'fax', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'fax',
		      'id'          => 'fax',
		      'value'       => $set_value ? $set_value['fax'] : $member->fax,
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Your Address:', 'address', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'address',
		      'id'          => 'address',
		      'value'       => $set_value ? $set_value['address'] : $member->address,
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Your Suburb:', 'suburb', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'suburb',
		      'id'          => 'suburb',
		      'value'       => $set_value ? $set_value['suburb'] : $member->suburb,
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Your City:', 'city', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'city',
		      'id'          => 'city',
		      'value'       => $set_value ? $set_value['city'] : $member->city,
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Your Postcode:', 'postcode', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'postcode',
		      'id'          => 'postcode',
		      'value'       => $set_value ? $set_value['postcode'] : $member->postcode,
		      'class'       => 'mid'
		      ) 
		 );
echo br(3);
echo form_submit(array('name'=>'submit', 'class'=>'btn-mid'), 'Save');
echo br(4);

?>

<?php
// echo validation_errors('<p class="error">');
 ?>


</div>

