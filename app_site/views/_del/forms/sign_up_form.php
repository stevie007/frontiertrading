

<div id="main">
<h1 id="title">Create an Account!</h1>	
<br />
<div id="btn-menu">
<?php
echo anchor('login/forget-password', 'Forgot your password?', array('class'=>'btn-mid'));
echo nbs(4);
echo anchor('login/recover-password', 'Recover Password', array('class'=>'btn-mid'));
?>
</div>
<br />
<?php

echo form_open('login/validate_sign_up', array('id' => 'standard'));

echo form_label('First Name:', 'firstname', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'firstname',
		      'id'          => 'firstname',
		      'value'       => $set_value['firstname'],
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Last Name:', 'lastname', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'lastname',
		      'id'          => 'lastname',
		      'value'       => $set_value['lastname'],
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Your Email:', 'email', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'email',
		      'id'          => 'email',
		      'value'       => $set_value['email'],
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Email Confirm:', 'email2', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'email2',
		      'id'          => 'email2',
		      'value'       => $set_value['email2'],
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);

echo form_label('Password:', 'password', array('class' => 'mid'));
echo form_password(array(
			 'name' => 'password', 
			 'id' => 'password',
			 'class'       => 'mid'
			 )); 
echo br(3);
echo form_submit(array('name'=>'submit', 'class'=>'btn-mid'), 'Submit');
echo br(4);
?>
</div>
