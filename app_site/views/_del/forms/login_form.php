
<div id="main">
<h1 id="title">Login</h1>	
<br />
    <?php 
	echo form_open('login/validate_login', array('id' => 'standard'));

echo form_label('Your login ID (Email):', 'email-mid', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'email',
		      'id'          => 'email-mid',
		      'value'       => '',
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);
echo form_label('Your password:', 'passwd-password', array('class' => 'mid'));
echo form_password(array('name' => 'password', 
			 'id' => 'passwd-mid',
			 'class'       => 'mid'
			 )); 
echo br(2);


$data = array(
              'name'        => 'keepsignin',
	      'id'          => 'keepsignin',
              'value'       => 'keepsignin',
	      'checked'     => FALSE
	      );
echo form_checkbox($data);
echo form_label('&nbsp;&nbsp;Keep me logged in', 'keepsignin', array('style' => 'width:160px;'));

echo form_submit(array('name'=>'submit', 'class'=>'btn-mid frt', 'style'=>'margin-right:60px'), 'Login');

echo br(2);
echo anchor('login/sign-up', 'Sign Up', array('class'=>'btn-mid'));

echo form_close();
?>

</div><!-- end login_form-->
