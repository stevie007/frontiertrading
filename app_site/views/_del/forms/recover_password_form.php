
<div id="main">
<h1 id="title">Recover Password</h1>	
<br />

<?php

echo form_open('login/validate_recover_password', array('id' => 'standard'));
echo form_label('Your login ID (Email):', 'email', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'email',
		      'id'          => 'email',
		      'value'       => $set_value['email'],
		      'class'       => 'mid'
		      ) 
		 );
echo br(2);
echo form_label('Your Reset Code:', 'resetcode', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'resetcode',
		      'id'          => 'resetcode',
		      'value'       => $set_value['resetcode'],
		      'class'       => 'mid'
		      ) 
		 );
echo br(3);
echo form_submit(array('name'=>'submit', 'class'=>'btn-mid'), 'Submit');
echo br(4);
echo form_close(); 
?>

<?php 
//echo validation_errors(); 
?>



</div>



