
<div id="main">
<h1 id="title">Forget Password</h1>	
<br />
 
<p class="content"> 
Before we can reset your password, you need to enter the your Login ID (Email Address) below to help identify your account.
</p>
<?php
echo form_open('login/validate_forget_password', array('id' => 'standard'));
echo form_label('Your login ID (Email):', 'email', array('class' => 'mid'));
echo form_input( 
		array(
		      'name'        => 'email',
		      'id'          => 'email',
		      'value'       => '',
		      'class'       => 'mid'
		      ) 
		 );
echo br(3);
echo form_submit(array('name'=>'submit', 'class'=>'btn-mid'), 'Submit');
echo br(4);
echo form_close(); 
?>

<?php
// echo validation_errors(); 
?>



</div>


