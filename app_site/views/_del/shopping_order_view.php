       

             <table width="590" border="0" cellpadding="5" cellspacing="0" style="padding-top:3px;">
               <tr>
                 
                 <td width="60" height="25" class="black12bold" align="left">ID</td>
                 <td width="80" height="25" class="black12bold">Name</td>
                 <td width="60" height="25" class="black12bold" align="right">Price</td>
                 <td width="100" height="25" class="black12bold" align="right">Quantity</td>
                 <td width="100" height="25" class="black12bold" align="right">Subtotal</td>
                 
               </tr>
           
    <?php foreach ($order->content as $item): ?>

               <tr>
                 
                 <td width="60" height="25" class="orange12"><?php echo $item->id; ?></td>
                 <td width="80" height="25" class="black12"><?php echo $item->name; ?></td>
                 <td width="60" height="25" class="black12" align="right">$<?php echo number_format($item->price,2,'.',','); ?></td>
                 <td width="100" height="25" class="black12" align="right" style="text-valign:middle;height:25px;">
   <?php echo $item->qty;?>

</td>
                 <td width="100" height="25" class="black12" align="right" style="text-valign:middle">$<?php echo number_format($item->subtotal,2,'.',','); ?>
</td>
		 
               </tr>
        	<?php endforeach; ?>
     </table>


	
		 
	
             <table width="590" border="0" cellpadding="0" cellspacing="0">
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">Subtotal</td>
                 <td width="100" height="25" class="black12">$<?php echo number_format($order->subtotal, 2,'.',','); ?> </td>
               </tr>
            
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">GST</td>
  <td width="100" height="25" class="black12">$<?php echo number_format($order->gst, 2,'.',',');?></td>
               </tr>
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">TOTAL</td>
                 <td width="100" height="25" class="black12bold">$<?php echo number_format($order->total, 2,'.',',');?></td>
               </tr>
             </table>
             
<p>Please Note: Delivery costs not included in the above total and will be added in your invoice!</p>
	     <p>Payment Method: On Invoice</p>
<?php
$hidden = array('order_id' => $order->id);
echo form_open('shopping-order/send', '', $hidden);
?>
             
  <p>Please leave a note with your order for additional information:</p>
<?php 

$data = array(
              'name'        => 'note',
              'id'          => 'note',
              'value'       => '',
              'style'       => 'width:400px;height:100px;',
            );

echo form_textarea($data);
echo br(3);
?>
  <a href="<?php echo site_url('product/lib#cart');?>" class="btn-mid">Continue Shopping</a>
 <?php echo nbs(4);?>
<input type="submit" name="send" id="send" value="Send Order" class="btn-mid"  >

  <?php echo form_close();?>
