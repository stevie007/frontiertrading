       



             <table border="0" cellpadding="5" cellspacing="0" style="padding-top:3px;">
               <tr>
                 <td width="60" height="25" class="black12bold" align="left">ID</td>
                 <td width="100" height="25" class="black12bold">Name</td>
                 <td width="60" height="25" class="black12bold" align="right">Price</td>
                 <td width="140" height="25" class="black12bold" align="right">Quantity / Update</td>
                 <td width="140" height="25" class="black12bold" align="right">Subtotal / Delete</td>
               </tr>
            <?php if ($cart = $this->cart->contents()): ?>	 
		<?php foreach ($cart as $item): ?>
        <tr>


            <?php
echo form_open('shopping-cart/update', '', array('rowid' => $item['rowid']));
?>                 

                 <td ><?php echo $item['id']; ?></td>
                 <td ><?php echo $item['name']; ?></td>
                 <td align="right">$<?php echo  number_format($item['price'],2,'.',','); ?></td>
                 <td align="right" style="text-valign:middle;height:25px;width:200px;">
                 <input name="qty" type="text" value="<?php echo $item['qty'];?>" class="small" style="width:15px;" />
<?php 
    echo nbs(4);
echo form_input(array('type' => 'submit', 'value' => 'update', 'class' => 'btn-small' ));
?>
</td>
                 <td align="right" style="text-valign:middle">$<?php echo number_format($item['subtotal'],2,'.',','); ?>
<?php 
echo nbs(4);
echo anchor('shopping-cart/remove/'.$item['rowid'],'del', array('class'=>'btn-small')); ?>

<?php echo form_close();?>
               </tr>
        	<?php endforeach; ?>
     </table>
<?php endif; ?>
<table border="0" cellpadding="5" cellspacing="0">
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">Subtotal</td>
                 <td width="100" height="25" class="black12">$<?php echo number_format($this->cart->total(),2,'.',','); ?> </td>
               </tr>
          
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">GST</td>
  <td width="100" height="25" class="black12">$<?php echo number_format(($this->cart->total() * 0.15),2,'.',',');?></td>
               </tr>
          
          
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">TOTAL</td>
                 <td width="100" height="25" class="black12bold">$<?php echo number_format(($this->cart->total() * 1.15),2,'.',',');?></td>
               </tr>
           </table>
<a href="<?php echo site_url('product/lib#cart');?>" class="btn-mid">Continue Shopping</a>
  <?php echo nbs(4);?>
  <a href="<?php echo site_url('shopping-order');?>" class="btn-mid">Check Out</a>

           
            