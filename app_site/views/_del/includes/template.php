<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>

<?php $this->load->view('includes/html_head'); ?>  
<?php $this->load->view('includes/html_fancybox'); ?>  
  </head>
  <body>

    <div id="wrap">


<?php $this->load->view('includes/header'); ?>     
<!-- END header -->

     

<?php $this->load->view('includes/menu'); ?>   
<!-- END menu -->

<!-- session -->
<?php if($session_msg) { ?>
   <div id="sess-noty">
      <b>Session Message:</b> <br />
      <?php echo $session_msg;?> <br />
      <sub>[click to close]</sub>
   </div>
<script>$('#sess-noty').fadeIn().click(function() {$(this).fadeOut();});</script>
<?php } ?> 

<!-- END session -->

<?php $this->load->view($main_content); ?>
<!-- END main -->
    

<?php $this->load->view('includes/footer'); ?>  
<!-- END footer -->
      
     
    </div> <!-- END wrap -->
    

  </body>
</html>
