<script type="text/javascript" src="<?php echo base_url();?>lib/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>lib/fancybox/jquery.fancybox-1.3.1.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>lib/fancybox/jquery.fancybox-1.3.1.css" media="screen" />

<script type="text/javascript">
   $(document).ready(function() {
       $("a.fancybox").fancybox({
	     'transitionIn'	:	'elastic',
	     'transitionOut'    :	'elastic',
	     'titlePosition'		:   'inside',
	     'padding'   : 5
	     });
     });
</script>  