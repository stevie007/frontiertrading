     <div id="cart">
        <div class="cart-block"><img src="<?php echo base_url();?>images/cart-yourcart.jpg" alt="" width="155" height="30" /></div>     
        <div class="cart-block" class="black16bold" >Shopping cart summary: </div>

        <div class="cart-block" >
        	<span id="cart-items"><?php echo $this->cart->total_items(); ?></span> 
        	Items &nbsp;|&nbsp; Total: $<span id="cart-total">
        	<?php echo number_format($this->cart->total(),2,'.',','); ?></span>
        </div>        
        <div class="cart-block">
         
<a class="show-cart" href="<?php echo site_url('shopping-cart#cart');?>" class="black16bold"></a>

        </div>
	    <hr class="line02"/>                    
     </div>
    <!-- End of Cart -->