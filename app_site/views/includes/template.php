<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
<?php $this->load->view('includes/html_head'); ?>  
<?php $this->load->view('includes/html_fancybox'); ?>  
    </head>
    <body>
      
      <div id="wrap">
	<div id="header">
	  <?php $this->load->view('includes/header'); ?>     
	</div>
	<!-- End of Header -->
	
        <ul id="menu">
	  <?php $this->load->view('includes/menu'); ?>   
	</ul>
	<!-- End of menu -->

	<div id="banner">  

	</div>
	<!-- End of banner -->
        <div id="session">  
   <?php 
   echo $session_msg ? 'Notification Message:'.nbs(2).$session_msg : '';
?>
	</div> 
	<!-- end session -->
<!-- noty -->
   
   <div id="fnoty">
      </div>
<!-- end noty -->
   
<?php if (logged_in()) $this->load->view('includes/cart_view'); ?>  

	<!-- End of session -->
	
	<div id="leftpanel">
	  <?php $this->load->view('includes/leftpanel_view'); ?>  
	</div>  
	<!-- End of Left Panel -->     
	
	
	<div id="rightpanel">  
	  <div id="rightpanel-top"></div>     
          <div id="rightpanel-mid">

	    <?php $this->load->view($main_content); ?>

	  </div>     
          <div id="rightpanel-bottom"></div>        
	</div>        
	<!-- End of Right Panel --> 
	
	<div id="footer">
	  <?php $this->load->view('includes/footer'); ?>  
	</div>      
	<!-- End of Footer -->      
	
	<br class="clr"/>
      </div>
      <!-- End of Wrap --> 
      
    </body>
  </html>