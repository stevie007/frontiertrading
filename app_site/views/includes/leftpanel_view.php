<?php if (logged_in()) {
$category = isset($category) ? $category : '';
$search = isset($search) ? $search : '';
?>

<div class="leftpanel-header">
  <img src="<?php echo base_url();?>images/productcategories.jpg" alt="" width="150" height="30" border="0" />
</div>  
<div class="leftpanel-table"> 
<a href="<?php echo site_url('product');?>" class="<?php echo ($category || isset($filter['new']) || isset($filter['special']) || $search) ? 'black16bold' :  'blk16b-act';?>">List All Products</a>
<a href="<?php echo site_url('product/new-arrivals');?>" class="<?php echo isset($filter['new']) ? 'blk16b-act' : 'black16bold';?>">New Arrivals</a>
<a href="<?php echo site_url('product/specials');?>" class="<?php echo isset($filter['special']) ? 'blk16b-act' : 'black16bold';?>">Specials</a>
  <br />
  <hr class="line"/> 
   <ul id="cate-menu">
<?php 
$cates = get_cates();
foreach ($cates as $cate) 
  {
    if (is_array($cate)) 
      {
	$parent = array_shift($cate);		
	$class = ($parent->name == $category) ? 'blk14b-act' : 'black14bold';
	echo '<li><a href="'.site_url('product/cate/'.$parent->name).'#cart" id="list1" class="'.$class.'">'.$parent->name.'</a><ul>';      
	foreach ($cate as $sub)
	  {	  		
	    $class = ($sub->name == $category) ? 'blk14b-act' : 'black14bold';
	    echo '<li>'.nbs(4).'<a href="'.site_url('product/cate/'.$sub->name).'#cart" class="'.$class.'">'.$sub->name.'</a></li>';	  
	  }
	echo '</ul></li>';
      }
    else
      {			
	$class = ($cate->name == $category) ? 'blk14b-act' : 'black14bold';
	echo '<li><a href="'.site_url('product/cate/'.$cate->name).'#cart" id="list1" class="'.$class.'">'.$cate->name.'</a></li>';
      }
  }
?>
</ul>

</div>  
<div class="leftpanel-list"> 
  <hr class="line"/> 
  <div class="leftpanel-textline">
    <span class="black16bold">Product Search</span>
  </div>
<?php 
echo form_open('product/search', array('id' => 'myform', 'style' => 'display:inline;'));
?>
  <input type="text" name="search" id="search" class="leftpanel-textbox" value="<?php echo isset($search) ? $search : '';?>"/>
  <div class="leftpanel-search">
    <input type="submit" name="send" id="send" value="Search" class="button03" border="0"/>
  </div>
<?php echo form_close(); ?>
</div>
<br class="clr"/>
Hi, <?php echo $this->session->userdata('user_name');?>
  <br />
  <br />

<div class="leftpanel-logout">
  <div id="button" class="button03"><a href="<?php echo base_url();?>login/logout" class="white16bold">Logout</a></div>
</div>  

<br class="clr"/><br />
<div class="leftpanel-loginhelp">
» <a href="<?php echo base_url();?>member/change_password" class="black12bold">Change Password</a>
</div> 

<?php } else { ?>

<div class="leftpanel-header">
  <img src="<?php echo base_url();?>images/cart-custlogin.jpg" alt="" width="150" height="30" border="0" />
</div>
 <?php 	echo form_open('login/validate_login'); ?>
  <div class="leftpanel-login">
  <span class="black16bold">Your Email</span>
  <br class="clr"/>
  <input type="text" name="email" id="email" class="leftpanel-textbox"/>
  <div id="leftpanel" class="leftpanel-textline">
  <span class="black16bold">Password</span>
  <br class="clr"/>
  </div> 
  <input type="password" name="password" id="password" class="leftpanel-textbox"/>
  <br class="clr"/><br />
  
   <?php echo form_submit(array('class' => 'button03', 'name'=>'submit'), 'Login');?>

  <br class="clr"/><br />
  <?php echo form_close();?>
  <div class="leftpanel-loginhelp">
» <a href="<?php echo base_url();?>login/forget_password" class="black12bold">Forgot Your Password</a>
    <br class="clr"/>
» <a href="<?php echo base_url();?>registration" class="black12bold">Register Now</a>
    <br class="clr"/>
» <a href="<?php echo base_url();?>login/recover" class="black12bold">Recover Password</a>
  </div> 
</div>   
<?php }  ?>
<p style="margin-top: 40px;border-top: 2px dotted #238CC6;"><span style="font-size:12px; ">For <span style="font-weight:bold;font-size:15px">Leggings/tights</span> please click</span><br /><a href="http://www.ardencollections.co.nz/" class="footer" style="font-size:13px;" target="_blank" >www.ardencollections.co.nz</a></p>