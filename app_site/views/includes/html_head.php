<title>Frontier Trading</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta http-equiv="Content-Language" content="en-us" />
 
    
 

    <meta name="description" content="" />
    <meta name="keywords" content="" />

<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/main.css" />

<script type="text/javascript" src="<?php echo base_url();?>lib/jquery-1.4.4.min.js"></script> 


<script type="text/javascript" src="<?php echo base_url();?>lib/jquery.form.js"></script> 


<script>
$(document).ready(function() {

    /** START jquery input blur **/
    
    $('.blur').focus(function() {
	if (this.value == this.defaultValue){ 
	    this.value = '';
	}
	if(this.value != this.defaultValue){
	    this.select();
	}
    });
    
    $('.blur').blur(function() {
	if ($.trim(this.value) == ''){
	    this.value = (this.defaultValue ? this.defaultValue : '');
	}
    });
    
    /** END jquery input blur **/
  

    /* end doc ready */
});
function openView(url, name) {
    window.open(url, name, "height=780,width=730,location=no, status=no, resizable=no, scrollbars=yes, toolbar=no");
}
</script>