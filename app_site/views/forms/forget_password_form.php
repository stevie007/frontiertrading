
<p class="black24bold">Forget Password</p>
<br />
Before we can reset your password, you need to enter the your Login ID (Email Address) below to help identify your account:

<?php
$attributes = array('id' => 'myform');
echo form_open('login/forget_password',$attributes);
?>
<?php echo br(2);?>

Your login ID (Email Address):
<?php echo br(2);?>
<?php 
echo form_input( 
		array(
		      'name'        => 'email',
		      'value'       => '',
		      'class' => "textbox"
		      ) 
		 );
?>
<?php echo br(2);?>

<br class="clr"/>
<?php 
echo form_submit(array('class' => 'button03'), 'Submit');
?>



<?php echo form_close(); ?>

<?php 
//echo validation_errors();
 ?>



