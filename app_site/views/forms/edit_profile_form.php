

<h1>Edit Profile</h1>

<?php
   
echo form_open('member/profile');
echo form_hidden(array('id' => $member->id));

echo form_input('firstname', set_value('firstname', $member->firstname));
echo br(2);

echo form_input('lastname', set_value('lastname', $member->lastname));
echo br(2);

echo form_input('email', set_value('email', $member->email));
echo br(2);

echo form_input('telephone', set_value('telephone', $member->telephone));
echo br(2);

echo form_input('mobile', set_value('mobile', $member->mobile));
echo br(2);

echo form_input('fax', set_value('fax', $member->fax));
echo br(2);

echo form_input('address1', set_value('address1', $member->address1));
echo br(2);

echo form_input('address2', set_value('address2', $member->address2));
echo br(2);

echo form_input('city', set_value('city', $member->city));
echo br(2);

echo form_input('postcode', set_value('postcode', $member->postcode));
echo br(2);

echo form_submit('submit', 'Save Profile');
?>

<?php echo validation_errors('<p class="error">'); ?>

