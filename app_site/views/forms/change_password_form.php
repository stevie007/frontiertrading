<p class="black24bold">Change Password</p>
<br />
<?php
$attributes = array('id' => 'myform');
echo form_open('member/change_password',$attributes);
?>


Your login ID (Email):
<?php echo br(2);?>
<?php 
echo form_input( 
		array(
		      'name'        => 'email',
		      'value'       => $this->session->userdata('email'),
		      'readonly'    => 'readonly',
		      'class' => "textbox"
		      ) 
		 );
?>
<br class="clr"/><br />

Your current password: 
<?php echo br(2);?>

<?php 
echo form_password(array('name' => 'password_old',
		      'class' => "textbox")); 
?>

<br class="clr"/><br />
Choose a new password:
<?php echo br(2);?>
<?php 
echo form_password(array('name' => 'password_new',
		      'class' => "textbox")); 
?>

<br class="clr"/><br />
Re-enter new password:
<?php echo br(2);?>
<?php 
echo form_password(array('name' => 'password_rep',
		      'class' => "textbox")); 
?>
<br class="clr"/><br />
<?php 
echo form_submit(array('class' => 'button03'), 'Submit');
?>



<?php echo form_close(); ?>

<?php 
		     //echo validation_errors(); 
?>




