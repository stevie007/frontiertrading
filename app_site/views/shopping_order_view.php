       

 <table width="590" border="0" cellpadding="0" cellspacing="0">
           <tr style="background-image:url(images/cart-toppanel.jpg); background-repeat:repeat-x;">
             <td>
             <table width="590" border="0" cellpadding="0" cellspacing="0" style="padding-top:3px;">
               <tr>
                 <td width="25" height="25" class="black12bold">&nbsp;</td>
                 <td width="60" height="25" class="black12bold" align="left">ID</td>
                 <td width="80" height="25" class="black12bold">Name</td>
                 <td width="60" height="25" class="black12bold" align="right">Price</td>
                 <td width="100" height="25" class="black12bold" align="right">Quantity</td>
                 <td width="100" height="25" class="black12bold" align="right">Subtotal</td>
                  <td width="25" height="25" class="black12bold">&nbsp;</td>
               </tr>
             </table>
             </td>
           </tr>
           <tr style="background-color:#fcfcfc;">
             <td>
    <?php ?>
    <?php foreach ($order->content as $item): ?>


             <table width="590" border="0" cellpadding="5" cellspacing="0">
               <tr>
                 <td width="25" height="25" class="orange12">&nbsp;</td>
                 <td width="60" height="25" class="orange12"><?php echo $item->id; ?></td>
                 <td width="80" height="25" class="black12"><?php echo $item->name; ?></td>
                 <td width="60" height="25" class="black12" align="right">$<?php echo number_format($item->price,2,'.',','); ?></td>
                 <td width="100" height="25" class="black12" align="right" style="text-valign:middle;height:25px;">
   <?php echo $item->qty;?>

</td>
                 <td width="100" height="25" class="black12" align="right" style="text-valign:middle">$<?php echo number_format($item->subtotal,2,'.',','); ?>

		 <td width="25" height="25" class="black12bold">&nbsp;</td>
               </tr>
             </table>


		<?php endforeach; ?>
		 
	
             </td>
           </tr>
           <tr style="background-color:#fcfcfc;">
             <td height="25">&nbsp;</td>
           </tr>
           <tr style="background-color:#fcfcfc;">
             <td><table width="590" border="0" cellpadding="0" cellspacing="0">
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">Subtotal</td>
                 <td width="100" height="25" class="black12">$<?php echo number_format($order->subtotal, 2,'.',','); ?> </td>
               </tr>
             </table>
             <table width="590" border="0" cellpadding="0" cellspacing="0">
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">GST</td>
  <td width="100" height="25" class="black12">$<?php echo number_format($order->gst, 2,'.',',');?></td>
               </tr>
             </table>
             <table width="590" border="0" cellpadding="0" cellspacing="0" style="border-bottom:solid; border-bottom-color:#e0e0e0;">
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">TOTAL</td>
                 <td width="100" height="25" class="black12bold">$<?php echo number_format($order->total, 2,'.',',');?></td>
               </tr>
             </table>
             </td>
           </tr>
           <tr>
             <td></td>
           </tr>
           <tr>
             <td></td>
           </tr>
           <tr>
             <td height="15">&nbsp;</td>
           </tr>
           <tr>
             <td>
<p>Please Note: Delivery costs not included in the above total and will be added in your invoice!</p>
	     <p>Payment Method: On Invoice</p>
<?php
$hidden = array('order_id' => $order->id);
echo form_open('shopping-order/send', '', $hidden);
?>
             <table width="590" border="0" cellpadding="0" cellspacing="0">
	       <tr>
                 <td colspan="3" align="middle">
  <p>Please leave a note with your order for additional information:</p>
<?php 

$data = array(
              'name'        => 'note',
              'id'          => 'note',
              'value'       => '',
              'style'       => 'width:400px;height:100px;',
            );

echo form_textarea($data);
echo br(3);
?>
</td>
               
               </tr>
               <tr>
                 <td width="325" height="25">&nbsp;</td>
                 <td width="165" height="25">
                 <div id="button" class="button05"><a href="<?php echo site_url('product/lib#cart');?>" class="white12bold">Continue Shopping</a></div>
                 </td>
                 <td width="100" height="25">
<input type="submit" name="send" id="send" value="Send Order" class="button06" border="0" style="float:left;"/>

                 </td>
               </tr>
             </table>
  <?php echo form_close();?>
             </td>
           </tr>
         </table>