       


 <table width="590" border="0" cellpadding="0" cellspacing="0">
           <tr style="background-image:url(images/cart-toppanel.jpg); background-repeat:repeat-x;">
             <td>
             <table width="590" border="0" cellpadding="0" cellspacing="0" style="padding-top:3px;">
               <tr>
                 <td width="25" height="25" class="black12bold">&nbsp;</td>
                 <td width="60" height="25" class="black12bold" align="left">ID</td>
                 <td width="80" height="25" class="black12bold">Name</td>
                 <td width="60" height="25" class="black12bold" align="right">Price</td>
                 <td width="100" height="25" class="black12bold" align="right">Quantity / Update</td>
                 <td width="100" height="25" class="black12bold" align="right">Subtotal / Delete</td>
                  <td width="25" height="25" class="black12bold">&nbsp;</td>
               </tr>
             </table>
             </td>
           </tr>
           <tr style="background-color:#fcfcfc;">
             <td>
<?php if ($cart = $this->cart->contents()): ?>	 
		<?php foreach ($cart as $item): ?>

            <?php
echo form_open('shopping-cart/update', '', array('rowid' => $item['rowid']));
?>
             <table width="590" border="0" cellpadding="5" cellspacing="0">
               <tr>
                 <td width="25" height="25" class="orange12">&nbsp;</td>
                 <td width="60" height="25" class="orange12"><?php echo $item['id']; ?></td>
                 <td width="80" height="25" class="black12"><?php echo $item['name']; ?></td>
                 <td width="60" height="25" class="black12" align="right">$<?php echo  number_format($item['price'],2,'.',','); ?></td>
                 <td width="100" height="25" class="black12" align="right" style="text-valign:middle;height:25px;">
    <input name="qty" type="text" class="product-quantity" value="<?php echo $item['qty'];?>" style=""/>
<?php 
echo nbs(4);
echo form_input(array('type' => 'image', 'src' =>  'images/cart-update.jpg', 'value' => '', 'style' =>"height:15px;" ));
?>
</td>
                 <td width="100" height="25" class="black12" align="right" style="text-valign:middle">$<?php echo number_format($item['subtotal'],2,'.',','); ?>
<?php 
echo nbs(4);
echo anchor('shopping-cart/remove/'.$item['rowid'],'<img src="images/cart-cancel.jpg" width="15" height="15" border="0" style="display:inline;"/>'); ?>
		 <td width="25" height="25" class="black12bold">&nbsp;</td>
               </tr>
             </table>

<?php echo form_close();?>
		<?php endforeach; ?>
		 
			
 
<?php endif; ?>

             </td>
           </tr>
           <tr style="background-color:#fcfcfc;">
             <td height="25">&nbsp;</td>
           </tr>
           <tr style="background-color:#fcfcfc;">
             <td><table width="590" border="0" cellpadding="0" cellspacing="0">
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">Subtotal</td>
                 <td width="100" height="25" class="black12">$<?php echo number_format($this->cart->total(),2,'.',','); ?> </td>
               </tr>
             </table>
             <table width="590" border="0" cellpadding="0" cellspacing="0">
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">GST</td>
  <td width="100" height="25" class="black12">$<?php echo number_format(($this->cart->total() * 0.15),2,'.',',');?></td>
               </tr>
             </table>
             <table width="590" border="0" cellpadding="0" cellspacing="0" style="border-bottom:solid; border-bottom-color:#e0e0e0;">
               <tr>
                 <td width="365" height="25">&nbsp;</td>
                 <td width="125" height="25" class="black12">TOTAL</td>
                 <td width="100" height="25" class="black12bold">$<?php echo number_format(($this->cart->total() * 1.15),2,'.',',');?></td>
               </tr>
             </table>
             </td>
           </tr>
           <tr>
             <td></td>
           </tr>
           <tr>
             <td></td>
           </tr>
           <tr>
             <td height="15">&nbsp;</td>
           </tr>
           <tr>
             <td>
             <table width="590" border="0" cellpadding="0" cellspacing="0">
               <tr>
                 <td width="325" height="25">&nbsp;</td>
                 <td width="165" height="25">
                 <div id="button" class="button05"><a href="<?php echo site_url('product/lib#cart');?>" class="white12bold">Continue Shopping</a></div>
                 </td>
                 <td width="100" height="25">
                 <div id="button" class="button06"><a href="<?php echo site_url('shopping-order');?>" class="white12bold">Check Out</a></div>
                 </td>
               </tr>
             </table>
             </td>
           </tr>
         </table>