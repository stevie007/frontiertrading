<script type="text/javascript">
    $(function() {
	$('.plus').click(function(){
	    var curr_val = $(this).prevAll('input').val();
	    $(this).prevAll('input').val(++curr_val);
	  });
	$('.minus').click(function(){
	    var curr_val = $(this).prevAll('input').val();
	    if (curr_val > 1) $(this).prevAll('input').val(--curr_val);
	  });	
      });
</script>

<script type="text/javascript">
   $(function() {
       $('.itemForm').ajaxForm({
	 beforeSubmit: function(a,f,o) {
	     $('#session').html('Submitting...');
	   },
	     success: function(data) {
	     var cart = $.parseJSON(data);
	     mark_added(cart.contents);
	     $('#cart-items').html(cart.item);
	     $('#cart-total').html(cart.total.toFixed(2));
	     $('#session').html('Shopping cart updated.');
	     $('#fnoty')
	       .html('Shopping cart updated. '+ cart.item+' items, Total: $' + cart.total) 
	       .fadeIn().delay(3000).fadeOut();		       
	       
	   }
	 });
      
      var obj_cart = $.parseJSON('<?php echo $json_cart;?>');
 
	mark_added(obj_cart);
	 
     });
function mark_added(obj_cart){
      for (x in obj_cart)
	{
 		var title = obj_cart[x]['id']; 
 		$('#'+title).attr('class', 'product-bg2');
	}
 
}
</script> 

<?php foreach($query->result() as $row): ?>
<?php echo form_open('shopping-cart/add', array('class' => 'itemForm')); 
echo form_hidden('uri', $this->uri->uri_string());
?>

     <div class="product-bg" id="<?php echo $row->id;?>">
     <div class="product-pic">
     	<a href="<?php echo base_url().'uploads/item_images/'.$row->image1;?>" class="fancybox" title="<?php echo $row->detail;?>"><img src="<?php echo base_url().'uploads/item_images/'.$row->image1;?>" alt="" height="90"  border="0" /></a>
     </div>    
         
     <div class="product-text"><a href="<?php echo base_url().'uploads/item_images/'.$row->image1;?>" class="orange18 fancybox" title="<?php echo $row->detail;?>"><?php echo $row->title;?></a><br />
     <?php if (($row->price_s > 0) && ($row->price > $row->price_s)):?>
     <?php echo form_hidden('price', $row->price_s);?>
          <span class="black14bold" style="text-decoration: line-through;">Was $<?php echo $row->price;?></span><br />
    	  <span class="black14bold" style="color: red;">Now $<?php echo $row->price_s;?></span><br />
     <?php else: ?>
     <?php echo form_hidden('price', $row->price);?>
          <span class="black14bold">$<?php echo $row->price;?></span><br />
     <?php endif; ?>
     </div>
     
     <div class="product-buttons">            
     <span class="black11bold">Quantity:</span>
     <input type="text" name="qty"  class="product-quantity" value="1"/><br />
   	<img class="plus" src="<?php echo base_url();?>images/cart-plus.jpg" alt="" width="15" height="15" border="0" /> 
   	<img class="minus" src="<?php echo base_url();?>images/cart-minus.jpg" alt="" width="15" height="15" border="0" /><br />
	<?php echo form_input(array('type' => 'image', 'src' =>  base_url().'images/cart-addcart.jpg', 'name' => 'image', 'width' => '75', 'height' => '20', 'value' => ''));?>
     </div>
            
     </div>  
<?php echo form_hidden('id', $row->id); ?>
<?php echo form_close(); ?>
<?php endforeach;?>

<div class="black11bold" id="product-bottompanel">Display # 
             <select name="display" class="black11bold" id="display" onchange="location.href=this.value">
     <option value="<?php echo site_url('product/perpage/10');?>" <?php echo $perpage == 10 ? 'selected="selected"' : '';?> >10</option>
     <option value="<?php echo site_url('product/perpage/20');?>" <?php echo $perpage == 20 ? 'selected="selected"' : '';?> >20</option>
     <option value="<?php echo site_url('product/perpage/30');?>" <?php echo $perpage == 30 ? 'selected="selected"' : '';?> >30</option>
     <option value="<?php echo site_url('product/perpage/50');?>" <?php echo $perpage == 50 ? 'selected="selected"' : '';?> >50</option>
     <option value="<?php echo site_url('product/perpage/100');?>" <?php echo $perpage == 100 ? 'selected="selected"' : '';?> >100</option>
             </select>

<?php 
echo nbs(4);
if ($search) {
  echo 'Search: '.$search;
  echo nbs(4);
} 
echo 'Results: '.$offset.' - '.($offset + $perpage).' of '.$total.nbs(4);
echo $this->pagination->create_links(); 
?>
           </div>       
<div id="test"></div>

  
	