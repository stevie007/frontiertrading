<?php echo $main_data;?>


<form id="form2" name="form2" method="post" action="<?php echo site_url('login/signup');?>">
  <table width="590" border="0">
    <tr>
      <td width="445"><span class="black14bold">Customer Information:</span><br /></td>
      <td width="409">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Company Name (*)</td>
      <td></td>
    </tr>
    <tr>
      <td><input type="text" name="company" id="company" class="textbox" value="<?php echo $set_value['company'];?>"/></td>
      <td></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>First Name (*)</td>
      <td>Last Name (*)</td>
    </tr>
    <tr>
      <td><input type="text" name="firstname" id="firstname" class="textbox" value="<?php echo $set_value['firstname'];?>"/></td>
      <td><input type="text" name="lastname" id="lastname" class="textbox" value="<?php echo $set_value['lastname'];?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Phone (*)</td>
      <td>Mobile</td>
    </tr>
    <tr>
          <td><input type="text" name="telephone" id="telephone" class="textbox" value="<?php echo $set_value['telephone'];?>"/></td>
      <td><input type="text" name="mobile" id="mobile" class="textbox" value="<?php echo $set_value['mobile'];?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><span class="black14bold">Delivery Address</span><br /></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td>Address (*)</td>
      <td>Suburb (*)</td>

    </tr>
    <tr>
      <td><input type="text" name="address" id="address" class="textbox" value="<?php echo $set_value['address'];?>"/></td>
      <td><input type="text" name="suburb" id="suburb" class="textbox" value="<?php echo $set_value['suburb'];?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>City (*)</td>
      <td>Post Code</td>

    </tr>
    <tr>
      <td><input type="text" name="city" id="city" class="textbox" value="<?php echo $set_value['city'];?>"/></td>
      <td><input type="text" name="postcode" id="postcode" class="textbox" value="<?php echo $set_value['postcode'];?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><span class="black14bold">Billing Address (If different from Delivery Address)</span><br /></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td>Address</td>
      <td>Suburb</td>
    </tr>
    <tr>
      <td><input type="text" name="address2" id="address2" class="textbox" value="<?php echo $set_value['address2'];?>"/></td>
      <td><input type="text" name="suburb2" id="suburb2" class="textbox" value="<?php echo $set_value['suburb2'];?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>City</td>
      <td>Post Code</td>

    </tr>
    <tr>
      <td><input type="text" name="city2" id="city2" class="textbox" value="<?php echo $set_value['city2'];?>"/></td>
      <td><input type="text" name="postcode2" id="postcode2" class="textbox" value="<?php echo $set_value['postcode2'];?>"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><hr class="line03"/>
        <br />
        <br />
        <br /></td>
    </tr>
    <tr>
      <td colspan="2"><span class="black14bold">Login User Details</span><br /></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td>Email Address (*)</td>
      <td>Email Confirm (*)</td>
    </tr>
    <tr>
      <td><input type="text" name="email" id="email" class="textbox" value="<?php echo $set_value['email'];?>"/></td>
      <td><input type="text" name="email2" id="email2" class="textbox" value="<?php echo $set_value['email2'];?>"/></td>
    </tr>
    <tr>
      <td>Password (*)</td>
      <td></td>
    </tr>
    <tr>
      <td><input type="text" name="password" id="password" class="textbox"/></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2"><hr class="line03"/>
        <br />
        <br />
        <br /></td>
    </tr>
   
    
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2"><span class="black14bold">Acceptance Of Terms</span></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">By checking the I Agree check box you agree to the terms and condition of Frontier International Trading.<br /><br /></td>
    </tr>
    <tr>
      <td colspan="2"><input type="checkbox" name="checkbox" id="checkbox" />
        <label for="checkbox">I agree to the Terms &amp; Conditions (*) </label>
<a href="javascript: void(0)" class="orange14"  onclick="openView('<?php echo site_url("terms");?>','terms');">(view terms click here)</a></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td align="left"><input type="submit" name="send" id="send" value="Submit" class="button03a"  /></td>
      <td align="right"><input type="submit" name="send2" id="send2" value="Reset" class="button03a" /></td>
    </tr>
  </table>
</form>