<?php

class Order_model extends CI_Model {



  function __construct() {
    parent::__construct();
    
  }


  function order_list($search = "", $filter = "", $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->__order_list_sql($search, $filter, $orderby);
    
    $list->total = $this->db->get('site_order')->num_rows();

    $this->__order_list_sql($search, $filter, $orderby);
   
    $list->query = $this->db->get('site_order', $per_page, $offset);
 
    return $list;


  }
	

  function __order_list_sql($search = "", $filter = "", $orderby = ""){
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']); 
      }
    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	$this->db->where($key, $value); 
	endforeach;
      }
    if ($search)
      {   
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `cost` LIKE '%$search%' OR `gst` LIKE '%$search%' OR `contents` LIKE '%$search%' OR `total` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_order($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('site_order')->result();
		
    return $result[0];
		
  }

  function order_update($id)
  {
    $this->db->where('id', $id);

    if($images = $this->pic_upload())
      {
	$data['image_small'] = $images->small;
        $data['image_big'] = $images->big;
      }

    $data['boolean1'] = ($this->input->post('boolean1') == 'accept') ? 1 : 0;
    $data['boolean2'] = ($this->input->post('boolean2') == 'accept') ? 1 : 0;

    $cols = array('title','category','detail','detail_member');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
 
    $this->db->update('site_order', $data);

  }

  function order_add($data) {

    $data->id = NULL;
    $data->content = json_encode($contents);
 
    $this->db->insert('site_order', $data); 
  }



  function order_del($id){

  
    $this->db->where('id', $id);
    $this->db->delete('site_order');
  }

  function pic_upload() {
		
    $config = array(
		    'allowed_types' => 'jpg|jpeg|gif|png',
		    'upload_path' => $this->image_path,
		    'max_size' => 2000
		    );
		
    $this->load->library('upload', $config);

    if ($this->upload->do_upload())
      {
 
	$image_data = $this->upload->data();

	$this->load->library('image_lib');

	$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => $this->image_path.'/big_'.$image_data['file_name'],
			'maintain_ration' => true,
			'width' => 800,
			'height' => 600
			);
	$this->image_lib->initialize($config); 
	$this->image_lib->resize();
	$this->image_lib->clear();

	$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => $this->image_path.'/small_'.$image_data['file_name'],
			'maintain_ration' => true,
			'width' => 160,
			'height' => 120
			);
	$this->image_lib->initialize($config); 
	$this->image_lib->resize();

	unlink($image_data['full_path']);


	$images = new stdClass;
	$images->big = 'big_'.$image_data['file_name'];
	$images->small = 'small_'.$image_data['file_name'];	
	return $images;
      }
    else
      {
	return false;
      }
 
  }

  }
