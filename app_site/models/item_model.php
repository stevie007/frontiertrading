<?php

class Item_model extends CI_Model {

  var $image_path;
  var $db_name;
  function __construct() {
    parent::__construct();
    $this->image_path = realpath(APPPATH.'../../uploads/item_images/');
    $this->db_name = 'items';
  }


  function item_list($search = "", $filter = "", $orderby = "", $per_page, $offset, $cate = "")
  {   
    $cates = array();
    if ($cate)
      {
	$this->db->where('name', $cate);
	$query = $this->db->get('category');
	foreach ($query->result() as $cate) {
	  if ($cate->parent != 'none') 
	    {
	      array_push($cates, $cate->name);
	    }
	  else
	    {	
	      array_push($cates, $cate->name);
	      $this->db->where('parent', $cate->name); 
	      $query =  $this->db->get('category');
	      if ($query->num_rows) {	 
		foreach ($query->result() as $row)
		  {
		    array_push($cates, $row->name);
		  }
	      }
	    }
	}
      }
    //print_r($cates);die;

    $list = new stdClass;
    
    $this->__item_list_sql($search, $filter, $orderby, $cates);
    
    $list->total = $this->db->get('items')->num_rows();

    $this->__item_list_sql($search, $filter, $orderby, $cates);
    
    $list->query = $this->db->get('items', $per_page, $offset);
    // echo $this->db->last_query();die;
    return $list;

  }
	

  function __item_list_sql($search = "", $filter = "", $orderby = "", $cates = ""){
    
    if ($cates)
      {
	foreach ($cates as $cate){
	  $this->db->or_where('category', $cate); 
	}
      }
    if ($orderby)
      {      
	//$this->db->order_by('length('.$orderby['order'].'), '.$orderby['order'], $orderby['sort']); 
		$this->db->order_by($orderby['order'], $orderby['sort']); 
      }    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	  $this->db->where($key, $value); 
	  endforeach;
      }
    if ($search)
      {   
	$this->db->where("(`id` LIKE '%$search%' OR `category` LIKE '%$search%' OR `title` LIKE '%$search%' OR `image1` LIKE '%$search%' OR `price` LIKE '%$search%' OR `detail` LIKE '%$search%')", NULL, FALSE);  
      }
      $this->db->where('qty <>', 0);
      $this->db->where('online', 1);
      $this->db->where('deleted', 0);
  }
 
 
  function load_cate_names($term = '')
  {
    $this->db->distinct();
    $this->db->select('category'); 
    $this->db->order_by('category', 'asc');
    $this->db->like('category', $term);
    $result = $this->db->get('items')->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->category);
    return $array;
  }


  function load_item($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('items')->result();		
    return $result[0];		
  }

  function check_by_id($id)
  {
    $this->db->where('id', $id);    
    $this->db->where('qty <>', 0);
    $this->db->where('online', 1);
    $this->db->where('deleted', 0);
    
    $query = $this->db->get($this->db_name);

    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }


  function load_item_by_url($url)
  {
    $this->db->where('url_title',  strtr($url, '_', '-'));
    $result = $this->db->get('items')->result();

    return $result ? $result[0] : FALSE;
  }
 

}
