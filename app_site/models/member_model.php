<?php

class Member_model extends CI_Model 
{
  

  function __construct() {
    parent::__construct();
    
  }


  function login()
  {
    if (!$this->input->post('email')) return FALSE;
    if (!$this->input->post('email')) return TRUE;
    $this->db->where('email', $this->input->post('email'));
    $this->db->where('password', sha1($this->input->post('password')));
    $this->db->where('active', 1);
    /*
    $query = $this->db->get('member');
    
    if($query->num_rows == 1)
      {
	return true;
      }
    */
    if ($result = $this->db->get('member')->result()) {
      $user = $result[0];
    } else {
      $user = FALSE;
    }	
    return $user;	

  }


  function login_cookie($email, $passwd)
  {
    $this->db->where('email', $email);
    $this->db->where('password', $passwd);
    $query = $this->db->get('member');
    
    if($query->num_rows == 1)
      {
	return true;
      }		
  }


  function change_password()
  {
    $this->db->where('email', $this->input->post('email'));
    $this->db->where('password', sha1($this->input->post('password_old')));
    
    $data = array('password' => sha1($this->input->post('password_new')));
    
    $this->db->update('member', $data);
    
    if($this->db->affected_rows() == 1)
      {
	return true;
      }
    
  }
  


  function has_email($email){
    
    $this->db->where('email', $email);
    $query = $this->db->get('member');
    
    return ($query->num_rows == 1) ? true : false;
 
 
  }
	
  function create_member()
  {

    if (!$this->has_email($this->input->post('email'))) {
       		
      $new_member_insert_data = array(
				      'id' => NULL,
				      'firstname' => $this->input->post('firstname'),
				      'lastname' => $this->input->post('lastname'),
				      'email' => $this->input->post('email'),
				      'password' => sha1($this->input->post('password')),
				      'company' => $this->input->post('company'),
 				      'telephone' => $this->input->post('telephone'),
				      'mobile' => $this->input->post('mobile'),
				      'fax' => $this->input->post('fax'),
				      'address' => $this->input->post('address'),
				      'suburb' => $this->input->post('suburb'),
				      'city' => $this->input->post('city'),
				      'postcode' => $this->input->post('postcode'),
				      'address2' => $this->input->post('address2'),
				      'suburb2' => $this->input->post('suburb2'),
				      'city2' => $this->input->post('city2'),
				      'postcode2' => $this->input->post('postcode2'),
				      'active' => 0						
				      );
      
      $insert = $this->db->insert('member', $new_member_insert_data);
      return $insert;
      
    }

  }

  function check_update_member()
  {
    $pre_data = $this->load_member_by_id($this->input->post('id')); 
    $email = $this->input->post('email');
   
    return (
	    $this->has_email($email) && $email == $pre_data->email
	    ||
	    !$this->has_email($email)
	    ) ? TRUE : FALSE; 

  }


  function update_member()
  {    
    if ($this->check_update_member()) 
      {
	$new_member_update_data = array(
					'company' => $this->input->post('company'),
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'email' => $this->input->post('email'),
					'telephone' => $this->input->post('telephone'),
					'mobile' => $this->input->post('mobile'),
					'fax' => $this->input->post('fax'),
					'address' => $this->input->post('address'),
					'suburb' => $this->input->post('suburb'),
					'city' => $this->input->post('city'),
					'postcode' => $this->input->post('postcode'),
				      'address2' => $this->input->post('address2'),
				      'suburb2' => $this->input->post('suburb2'),
				      'city2' => $this->input->post('city2'),
				      'postcode2' => $this->input->post('postcode2'),
					);
	$this->db->where('id', $this->input->post('id'));
	$update = $this->db->update('member', $new_member_update_data);
	return $update;
      }
  }

  function load_member_by_email($email)
  {
    if ($email) {
      $this->db->where('email', $email);
      $result = $this->db->get('member')->result();
      $member =  $result[0];
      return $member;
    }    
  }


  function load_member_by_id($id)
  {
    $this->db->where('id', $id);
    $result = $this->db->get('member')->result();
    $member =  $result[0];
    return $member;    
  }


  function check_reset($id, $code)
  {

    if ($id && $code) {
      $this->db->where('id', $id);
      $this->db->where('resetcode', $code);
      $query = $this->db->get('member');
    
      if($query->num_rows == 1)
	{
	  return true;
	}    
    }
  }

  function reset_password()
  {

    $this->db->where('email', $this->input->post('email'));
    $this->db->where('resetcode', $this->input->post('resetcode'));
    $this->db->where('id', $this->input->post('id'));
    
    $data = array(
		  'password' => sha1($this->input->post('password_new')),
		  'resetcode' => ''
		  );
    
    $this->db->update('member', $data);
    
    if($this->db->affected_rows() == 1)
      {
	return true;	
      }
    
  }


  function set_resetcode($email)
  {

    $this->load->helper('string');
    $code = strtolower(random_string('alnum', 8));

    $this->db->where('email', $email);

    $data = array('resetcode' => $code);
    
    $this->db->update('member', $data);
    
    if($this->db->affected_rows() == 1)
      {
	return $code;	
      }
    
  }

  function cancel_resetcode($id, $code)
  {
    
    $this->db->where('resetcode', $code);
    $this->db->where('id', $id);
    
    $data = array('resetcode' => '');
    
    $this->db->update('member', $data);
    
    if($this->db->affected_rows() == 1)
      {
	return true;	
      }
    
  }
  
  

}
