<?php
class Category_model extends CI_Model {

  var $db_name;

  function __construct()
  {
    parent::__construct();
    $this->db_name = 'category';
  }


  function load_by_url($url)
  {
    $this->db->where('url_title', $url);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }    
  }

  function load_tree()
  {
    
    $rta = array();
    $this->db->where('parent', '(none)'); 
    $this->db->order_by("order", "asc");
    $query =  $this->db->get($this->db_name);
    
    foreach ($query->result() as $row)
      { 
	$this->db->where('parent', $row->name); 
	$this->db->order_by("name", "asc");
	$query = $this->db->get($this->db_name);
	if ($query->num_rows) {
	  $tmp = array(); 
	  array_push($tmp, $row);
	  foreach ($query->result() as $row)
	    {
	      array_push($tmp, $row);
	    }
	  array_push($rta, $tmp);
	} else {
	  array_push($rta, $row);
	}
      }
    
    return $rta;
  }
  
}