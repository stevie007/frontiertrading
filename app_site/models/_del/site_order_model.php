<?php

class Site_order_model extends CI_Model 
{

  var $db_name;
  
  function __construct()
  {
    parent::__construct();
    $this->db_name = 'site_order';	    
  }


  function site_order_list($search = "", $filter = "", $orderby = "", $per_page, $offset)
  {
    
    $list = new stdClass;

    $this->_site_order_list_sql($search, $filter, $orderby);
    
    $list->total = $this->db->get($this->db_name)->num_rows();

    $this->_site_order_list_sql($search, $filter, $orderby);
   
    $list->query = $this->db->get($this->db_name, $per_page, $offset);
 
    return $list;


  }
	

  function _site_order_list_sql($search = "", $filter = "", $orderby = ""){
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']); 
      }
    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	  $this->db->where($key, $value); 
	  endforeach;
      }
    if ($search)
      {   
	$this->db->where("(`id` LIKE '%$search%' OR `date` LIKE '%$search%' OR `cost` LIKE '%$search%' OR `gst` LIKE '%$search%' OR `contents` LIKE '%$search%' OR `total` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 


  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }		
  }

  function update($id)
  {
    $this->db->where('id', $id);

    $cols = array('title','category','detail','detail_member');
      
    foreach ($cols as $col) $data[$col] = $this->input->post($col);
 
    $this->db->update($this->db_name, $data);

    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;

  }

  function site_order_add($data) {

    $data->id = NULL;
    $data->content = json_encode($contents);
 
    $this->db->insert($this->db_name, $data); 
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;

  }



}
