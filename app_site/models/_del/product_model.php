<?php

class Product_model extends CI_Model {

  var $image_path;
  var $db_name;

  function __construct()
  {
    parent::__construct();
    $this->db_name = 'product';
    $this->image_path = realpath(APPPATH.'../uploads/product_images/');
  }


  function product_list($category = "", $search = "", $filter = "", $orderby = "", $per_page, $offset)
  {
    $cates = $this->_get_cates($category);
  
    $list = new stdClass;

    $this->_product_list_sql($cates, $search, $filter, $orderby);
    
    $list->total = $this->db->get($this->db_name)->num_rows();

    $this->_product_list_sql($cates, $search, $filter, $orderby);
   
    $list->query = $this->db->get($this->db_name, $per_page, $offset);
    // echo    $this->db->last_query();die;
    return $list;

  }
	
  function _get_cates($category){

    $cates = array();    
    $this->db->where('url_title', $category);
    $query = $this->db->get('category');
    foreach ($query->result() as $category) {
      if ($category->parent == '(none)') 
	{
	  array_push($cates, $category->name);
	  $this->db->where('parent', $category->name); 
	  $query = $this->db->get('category');
	  if ($query->num_rows) {	 
	    foreach ($query->result() as $row)
	      {
		array_push($cates, $row->name);
	      }
	  }
	}
      else
	{	
	  array_push($cates, $category->name);
	}
    }
    
    return $cates;
  }

  function _product_list_sql($cates = "", $search = "", $filter = "", $orderby = ""){
    if ($cates) {
      $this->db->where('( 1=', '1', false);    
      $first = TRUE;
      foreach ($cates as $cate){
	if ($first)
	  {
	    $this->db->where('category', $cate); 
	    $first = FALSE;
	  }
	else 
	  {
	    $this->db->or_where('category', $cate); 
	  }	
      }
      $this->db->where('1', '1 )', false);
    } // if cate 
    if ($orderby)
      {      
	$this->db->order_by($orderby['order'], $orderby['sort']); 
      }    
    if ($filter)
      {   
	foreach ($filter as $key => $value):
	  $this->db->where($key, $value); 
	  endforeach;
      }
    if ($search)
      {   
	$this->db->where("(`id` LIKE '%$search%' OR `category` LIKE '%$search%' OR `title` LIKE '%$search%' OR `image_small` LIKE '%$search%' OR `image_big` LIKE '%$search%' OR `detail` LIKE '%$search%')", NULL, FALSE);  
      }
  }
 
 



  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }


  function load_by_url($url)
  {
    $this->db->where('url_title', $url);//strtr($url, '_', '-'));
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }    
  }
 

}
/*
  function load_cate_names($term = '')
  {
    $this->db->distinct();
    $this->db->select('category'); 
    $this->db->order_by('category', 'asc');
    $this->db->like('category', $term);
    $result = $this->db->get($this->db_name)->result();
    $array = array();
    foreach($result as $row) array_push($array, $row->category);
    return $array;
  }
*/