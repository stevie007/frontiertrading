<?php

class Member_model extends CI_Model 
{
  
  var $db_name;
  
  function __construct()
  {
    parent::__construct();
    $this->db_name = 'member';	
  }
  
  function login()
  {
    $this->db->where('email', $this->input->post('email'));
    $this->db->where('password', sha1($this->input->post('password')));
    $query = $this->db->get($this->db_name);

    if ($query->num_rows == 1) {
      $result = $this->db->get($this->db_name)->result();
      $user = $result[0];
    } else {
      $user = FALSE;
    }
    
    return $user;		
  }


  function login_cookie($email, $passwd)
  {
    $this->db->where('email', $email);
    $this->db->where('password', $passwd);
    $query = $this->db->get($this->db_name);
    
    return ($query->num_rows == 1) ? TRUE : FALSE;
  }


  function change_password()
  {
    $this->db->where('email', $this->input->post('email'));
    $this->db->where('password', sha1($this->input->post('password_old')));
    
    $data = array('password' => sha1($this->input->post('password_new')));
    
    $this->db->update($this->db_name, $data);    
   
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE; 
  }
  


  function has_email($email){
    $this->db->where('email', $email);
    $query = $this->db->get($this->db_name);    

    return ($query->num_rows == 1) ? true : false; 
  }
	
  function add()
  {
    
    if (!$this->has_email($this->input->post('email'))) 
      {
	
	$data['id'] = NULL;
	$data['password'] = sha1($this->input->post('password'));
			       
	$cols = array('firstname', 'lastname', 'email');
      
	foreach ($cols as $col) $data[$col] = $this->input->post($col);
      
	$this->db->insert($this->db_name, $data);
      
	return ($this->db->affected_rows() == 1) ? TRUE : FALSE; 
      
      }
    else
      {
	return FALSE;
      }
  }

  function check_update()
  {
    $pre_data = $this->load_by_id($this->input->post('id')); 
    $email = $this->input->post('email');
    
    return (
	    $this->has_email($email) && $email == $pre_data->email
	    ||
	    !$this->has_email($email)
	    ) ? TRUE : FALSE; 

  }


  function update()
  {    
    if ($this->check_update()) 
      {
	$cols = array('firstname', 'lastname', 'email', 'phone', 'mobile', 'fax', 'address', 'suburb', 'city', 'postcode');
	
	foreach ($cols as $col) $data[$col] = $this->input->post($col);
	
	$this->db->where('id', $this->input->post('id'));
	$this->db->update($this->db_name, $data);

	return ($this->db->affected_rows() == 1) ? TRUE : FALSE;

      }
    else
      {
	return false;
      }
    
  }
  
  function load_by_email($email)
  {
    $this->db->where('email', $email);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }


  function load_by_id($id)
  {
    $this->db->where('id', $id);
    $query = $this->db->get($this->db_name);
    
    if ($query->num_rows() > 0)
      {
	$result = $query->result();
	return $result[0];
      }
    else
      {
	return false;
      }
  }


  function check_reset($id, $code)
  {
    $this->db->where('id', $id);
    $this->db->where('resetcode', $code);
    $query = $this->db->get($this->db_name);
      
    if ($query->num_rows == 1)
      {
	return true;
      }    
    else
      {
	return FALSE;
      }
  }

  function reset_password()
  {

    $this->db->where('email', $this->input->post('email'));
    $this->db->where('resetcode', $this->input->post('resetcode'));
    $this->db->where('id', $this->input->post('id'));
    
    $data = array(
		  'password' => sha1($this->input->post('password_new')),
		  'resetcode' => ''
		  );
    
    $this->db->update($this->db_name, $data);
    
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;

  }


  function set_resetcode($email)
  {

    $this->load->helper('string');
    $code = strtolower(random_string('alnum', 8));

    $this->db->where('email', $email);

    $data = array('resetcode' => $code);
    
    $this->db->update($this->db_name, $data);
    
    if($this->db->affected_rows() == 1)
      {
	return $code;	
      }
    else
      {
	return FALSE;
      }
  }

  function cancel_resetcode($id, $code)
  {
    
    $this->db->where('resetcode', $code);
    $this->db->where('id', $id);
    
    $data = array('resetcode' => '');
    
    $this->db->update($this->db_name, $data);
       
    return ($this->db->affected_rows() == 1) ? TRUE : FALSE;
    
  }
  
  

}