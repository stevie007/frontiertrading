$.getJSON(
	  json_url, 
	  function(data) {
	      var json_obj = data;	   
	      var transaction = json_obj.invoice;
	      var contents = $.parseJSON(transaction.contents);


	      var transaction_date = mysqlParseDate(transaction.date);
	      var tax_change_date = new Date("October 1, 2010, 00:00:00");
	
	      if (tax_change_date > transaction_date)
		  {
		      var TAX_RAT = 0.125; //before tax change
		  }
	      else
		  {
		      var TAX_RAT = 0.15; //after tax change
		  }

	      $(function() {
	      
		      var dom_customer = $('#customer');
		      var dom_sales = $('#sales');
		      var dom_cust_name = $('#cust_name');
		      var dom_cust_id = $('#cust_id');
		      var dom_purchase_info = $('#purchase_info');
		      var dom_id = $('#id');
		      var dom_date = $('#date');
		      var dom_subtotal = $('#subtotal'); 
		      var dom_gst = $('#gst'); 
		      var dom_total = $('#total');
		      var dom_freight = $('#freight'); 
		      var dom_discount = $('#discount'); 
		      var dom_paid = $('#paid'); 
		      var dom_debit = $('#debit'); 
		      var dom_finished = $('#finished');
		      var dom_undelivered = $('#undelivered'); 
 
		      var dom_outsourced = $('#outsourced'); 
		      var dom_pmt_cash = $('#pmt_cash'); 
		      var dom_pmt_eftpos = $('#pmt_eftpos'); 
		      var dom_pmt_cheque = $('#pmt_cheque'); 
		      var dom_pmt_banks = $('#pmt_banks'); 
		      var dom_pmt_other = $('#pmt_other'); 
		      var dom_new_cmts = $('#new_cmts');
		      var dom_logs = $('#logs');
		      var dom_info = $('#info');
		      var dom_inv_items = $("#invo-items");

		      dom_cust_name.val(transaction.cust_name);
		      dom_cust_id.val(transaction.cust_id);
		      dom_purchase_info.val(transaction.purchase_info);
		      dom_sales.val(transaction.sales);
		      dom_date.val(date("d/m/Y", strtotime(transaction.date)));	  
		      dom_info.val(transaction.info);	  
		      dom_logs.html(transaction.logs);

		      $('#close')
			  .click(function () { 
				  window.close();	     
			      }); 
	      
		      $('#cart')
			.click(function () { 	 
				   $.getJSON(cart_url, function(json_obj) {

						 var cart = json_obj.cart;
						 var j = contents.length;

						 for (k in cart)
						 {
						     var i = j + k;
						     contents[i] = cart[k];
						     append_item(i);
						     $('#prod'+i).val(contents[i].name);
						     $('#qty'+i).val(contents[i].qty);
						     $('#up'+i).val(contents[i].price);
						     $('#dc'+i).val(contents[i].discount);
						 } 
				  
						 calc();
						 display();				 
				  
					     });
				   
			      }); 
		    
		      $('#save')
			  .click(function () { 
			  // final calc cnt
			  	  get_ctns();
		      		  calc();
		     		  display();
		         // end final calc cnt
				  var serialize  
				      = $("#invo-items").sortable("toArray");
				  var serial_conts = new Array();
				  for (i in serialize){
				      serial_conts[i] = 
					  contents[serialize[i].substr(4)];
				  }
				  contents = serial_conts;
			     
			    			     
				  transaction.cust_name = dom_cust_name.val();
			          transaction.cust_id = dom_cust_id.val();
				  transaction.sales = dom_sales.val();
				  transaction.purchase_info = dom_purchase_info.val();
				  transaction.date = dom_date.val();

				  var post_json = new Object();
				  post_json.transaction = transaction;
				  post_json.contents = contents;
			     
				  $.ajax({
					  type: 'POST',
					      url: post_url,
					      data: {'json' : JSON.stringify(post_json)},
					      success:  function(data) {
					     // alert(data); return;
					      top.window.resizeTo(720,300);
					      $('#save').hide();
					      $('#inner-text')
						  .hide()
						  .html(data)
						  .fadeIn(200, 
							  function () {
							      setTimeout(
									 'window.close()', 500);
							  });
					      window.opener.location.reload(true);
					  }
				      });
			      });
		      calc();
		      display();
		      display_ctns();
	      
		      dom_customer.keyup(function() {
					     var str = dom_customer.val();
					     dom_cust_name.val(str.substring(str.indexOf(' | ')+3));
					     dom_cust_id.val(str.substring(0, str.indexOf(' | ')));
				  });
	              
		    dom_finished
			.click(function(){
				   transaction.finished = $(this).attr('checked') ? 1 : 0;
				   key_logs += $(this).attr('checked') 
				       ? ' &lt;' + this.value + '&gt;  '
				       : ' &gt;' + this.value + '< ';
			       });
		    
		    dom_undelivered
			.click(function(){
				   transaction.delivered = $(this).attr('checked') ? 0 : 1;
			       });
		    
		    dom_paid
			.click(function(){
				   transaction.paid = $(this).attr('checked') ? 1 : 0;	
			       });
		    
		    dom_outsourced
			  .click(function(){
				  transaction.outsourced = $(this).attr('checked') ? 1 : 0;
				  key_logs += $(this).attr('checked') 
				      ? '  &lt;' + this.value + '&gt;  '
				      : '  &gt;' + this.value + '&lt;  ';
			      });
	      
		      $('#pmt_cash, #pmt_eftpos, #pmt_cheque, #pmt_banks, #pmt_other')
			  .click(function(){
				  if ($(this).attr('checked') &&
				      $.inArray(this.value, transaction.payment_meth) == -1) {
				      transaction.payment_meth.push(this.value);
				      key_logs += '  &lt;' + this.value + '&gt;  ';
				  }
				  else{
				      transaction.payment_meth.splice(
								      transaction.payment_meth.indexOf(this.value), 1); 
				      key_logs += '  &gt;' + this.value + '&lt;  ';
				  }
			      });
		      dom_total      
			  .keyup(function() {
				  transaction.total = $(this).val();
				  calc_rvs();
				  display('total');
			      })
			  .bind('paste cut', function() {
				  setTimeout(function() { dom_total.keyup(); }, 300);				      
			      });
		      
		      dom_discount      
			  .keyup(function() {
				  transaction.discount = $(this).val();
				  calc();
				  display('discount');
			      })
			  .bind('paste cut', function() {
				  setTimeout(function() { dom_discount.keyup(); }, 300);				      
			      });
		      
		      dom_freight      
			  .keyup(function() {
				  transaction.freight = $(this).val(); 
				  calc();
				  display('freight');
			      })
			  .bind('paste cut', function() {
				  setTimeout(function() { dom_freight.keyup(); }, 300);				      
			      });
	      
		      
	      	     $('#calc')
			  .click(function () { 
			   
				  get_ctns();
		      		  calc();
		     		  display();
		   
		      	     
			      }); 
			      
	      	      function get_ctns(){
			  for (i in contents)
			      {
				  contents[i].name = $('#prod'+i).val();
				  contents[i].qty = $('#qty'+i).val();
				  contents[i].price = $('#up'+i).val();
				  contents[i].discount = $('#dc'+i).val();
			      }
		      }
		      
		      
	      
		      $('.item')
			  .keyup(function() {
				  alert($(this).val());
			      });
	      
		      $("#item_add")
			  .click(function () { 
				     // deal with array and cart object 
				     var cnt = 0;
				     for( var i in contents ) {cnt++;}  
				     var i = cnt;
				  contents[i] = new Object();
				  contents[i].qty = 1;
				  contents[i].discount = 0;
				  contents[i].price = 0;
				  append_item(i);		     
			      });
	      
		      function display(hide) {
			  
			  dom_id.val(transaction.id);
		  
			  dom_subtotal.val(parseFloat(transaction.subtotal).toFixed(2));
			  dom_gst.val(parseFloat(transaction.tax).toFixed(2)); 
			  if (hide != 'total')
			      dom_total.val(parseFloat(transaction.total).toFixed(2));
			  if (hide != 'discount')
			      dom_discount.val(parseFloat(transaction.discount).toFixed(2));
			  if (hide != 'freight')  
			      dom_freight.val(parseFloat(transaction.freight).toFixed(2));
			 
 

			  if (parseInt(transaction.finished)) 
			      dom_finished.attr('checked', true);	      
			  if (!parseInt(transaction.delivered)) 
			      dom_undelivered.attr('checked', true);	      
			  if (parseInt(transaction.paid)) 
			      dom_paid.attr('checked', true);
 	  
			  $('.ui_buttonset').buttonset( "refresh" );


		      }

	      
		      function display_ctns(){
			  for (i in contents)
			      {
				  append_item(i);
				  $('#prod'+i).val(contents[i].name);
				  $('#qty'+i).val(contents[i].qty);
				  $('#up'+i).val(contents[i].price);
				  $('#dc'+i).val(contents[i].discount);
			      }
		      }
		      
		      
		      function calc(){
			  transaction.discount = parseFloat(transaction.discount);
			  transaction.freight = parseFloat(transaction.freight);		  
			  var item_total = calc_item_total();
			  transaction.subtotal = item_total * (1 - transaction.discount / 100);
			  transaction.tax = (transaction.subtotal + transaction.freight) * TAX_RAT;
			  transaction.total = (transaction.subtotal + transaction.freight) * (1 + TAX_RAT);
		      }
	      
		      function calc_rvs(){
		  
			  transaction.subtotal = transaction.total / (1 + TAX_RAT) - transaction.freight;
			  transaction.tax = transaction.subtotal * TAX_RAT;
			  var item_total = calc_item_total();
			  transaction.discount = (1 - (transaction.subtotal) / item_total) * 100;
		  
		      }

		      function calc_item_total(){
		  
			  var item_total = 0;
			  for (i in contents){
			      if (contents[i] != undefined) {
				  item_total += parseFloat(contents[i].qty)
				      * parseFloat(contents[i].price)
				      * (1 - parseFloat(contents[i].discount) / 100);	  
			      } 
			  }
			  return item_total;

		      }

		      function append_item(i){
			  var html = invo_item_html(i);
			  dom_inv_items.append(html);
			  $('#item' + i).fadeIn("normal"); 
			  $('#prod' + i)
			      .keyup(function() {
				      contents[i].name = $(this).val();
				  })
			      .bind('paste cut', function() {
				      setTimeout(function() { $('#prod' + i).keyup(); }, 300);				      
				  });
			  
			  $('#qty' + i).keyup(function() {
				  contents[i].qty = $(this).val();
				  calc();
				  display();
			      })
			      .bind('paste cut', function() {
				      setTimeout(function() { $('#qty' + i).keyup(); }, 300);				      
				  });

			  $('#up' + i).keyup(function() {
				  contents[i].price = $(this).val();
				  calc();
				  display();
			      })
			      .bind('paste cut', function() {
				      setTimeout(function() { $('#up' + i).keyup(); }, 300);				      
				  });
			  
			  $('#dc' + i).keyup(function() {
				  contents[i].discount = $(this).val();
				  calc();
				  display();
			      })
			      .bind('paste cut', function() {
				      setTimeout(function() { $('#dc' + i).keyup(); }, 300);				      
				  });
			  
			  $('.item_del', '#item' + i)
			      .click(function () {
				      var obj = $(this.parentNode);
				      obj.fadeOut("slow", function () {
					      obj.remove();
					      delete contents[i];  // set i -> undefined
					      calc();
					      display();
					  });			     
				  });    
		      }
	      

	      
	      
	      
	      

		  });//end doc ready


	  });// end getjson

function invo_item_html(id){

    var rt = '<li class="ui-helper-clearfix bgc" id="item' + id + '" style="display:none;">'
	+ '<span class="ui-icon ui-icon-arrowthick-2-n-s pt flt" style="margin:2px 5px 0 0;" title="Drag to sort"></span>'
	+ '<input type="text" name="2" value="" id="prod' + id + '" class="ui-corner-all prod" tabindex="-1" />'
	+ '<input type="text" name="2" value="1" id="qty' + id + '" maxlength="5" class="ui-corner-all qty" />'
	+ '<input type="text" name="2" value="0" id="up' + id + '"  class="ui-corner-all up" tabindex="-1"/>'
	+ '<input type="text" name="2" value="0" id="dc' + id + '" maxlength="5" class="ui-corner-all dc" tabindex="-1"/>'
	+ '<span class="ui-icon ui-icon-closethick pt flt item_del" style="margin:2px 0 0 20px;" title="Delete" id="del' + id + '"></span></li>';

    return rt;
}