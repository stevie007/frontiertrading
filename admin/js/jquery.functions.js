$(document).ready(function() {


    /** jquery button **/

    $("button, input:submit, input:button, a", ".ui_button").button();
    
    
    $('#dialog_link, ul#icons li').hover(
	function() { $(this).addClass('ui-state-hover'); }, 
	function() { $(this).removeClass('ui-state-hover'); }
    );
    
    
    /** jquery button **/
    $(".check").button();
 	
    $(".buttonset").buttonset();

	       $(".ui_radio, .ui_buttonset").buttonset(); 



	       /** jquery icon hover **/
	       $('div.icons, ul.icons li').hover(
		   function() { $(this).addClass('ui-state-hover'); }, 
		   function() { $(this).removeClass('ui-state-hover'); }
	       );
    /** START jquery input blur **/
    
    $('.blur').focus(function() {
	if (this.value == this.defaultValue){ 
	    this.value = '';
	}
	if(this.value != this.defaultValue){
	    this.select();
	}
    });
    
    $('.blur').blur(function() {
	if ($.trim(this.value) == ''){
	    this.value = (this.defaultValue ? this.defaultValue : '');
	}
    });
    
    /** END jquery input blur **/
    
	       /** START jquery input blur **/
	       
	       $('.blur')
		   .focus(function() {
			      if (this.value == 'Customer..' 
				  || this.value == 'Search..'
				  || this.value == 'Vendor..' 
				  || this.value == 'Name..'
				  || this.value == 'Title..'
				 ){ 
				  this.value = '';
			      } else {
				  this.select();
			      }
			  });
	       
	       $('.blur[name="title"]')
		   .blur(function() {
			     if ($.trim(this.value) == ''){
				 this.value = 'Title..';
			     }
			 });
	       $('.blur[name="customer"]')
		   .blur(function() {
			     if ($.trim(this.value) == ''){
				 this.value = 'Customer..';
			     }
			 });

	       $('.blur[name="vendor"]')
		   .blur(function() {
			     if ($.trim(this.value) == ''){
				 this.value = 'Vendor..';
			     }
			 });

	       $('.blur[name="search"]')
		   .blur(function() {
			     if ($.trim(this.value) == ''){
				 this.value = 'Search..';
			     }
			 });
	       $('.blur[name="name"]')
		   .blur(function() {
			     if ($.trim(this.value) == ''){
				 this.value = 'Name..';
			     }
			 });
	       /** END jquery input blur **/


    /* end doc ready */
});
function openView(url, name) {
    window.open(url, name, "height=780,width=730,location=no, status=no, resizable=no, scrollbars=yes, toolbar=no");
}
function mysqlParseDate(string) {
    var date = new Date();
    var parts = String(string).split(/[- :]/);

    date.setFullYear(parts[0]);
    date.setMonth(parts[1] - 1);
    date.setDate(parts[2]);
    date.setHours(parts[3]);
    date.setMinutes(parts[4]);
    date.setSeconds(parts[5]);
    date.setMilliseconds(0);

    return date;
}
var branch_names = ["3A Copy & Design (Epsom)", 
		    "3A Copy & Design (City)",
		    "3A Copy & Design (Newmarket)",
		    "3A Copy & Design (Onehunga)",
		    "3A Copy & Design (Albany)",
		    "3A Copy & Design (Takapuna)",
		    "3A Copy & Design",
		    "3A Web Solution",
		    "3A Energy Signs"];