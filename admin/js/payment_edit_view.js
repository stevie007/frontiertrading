$.getJSON(
    json_url, 
    function(data) {
	var json_obj = data;	   
	var transaction = json_obj.payment;

	var transaction_date = mysqlParseDate(transaction.date);
	transaction.payment_meth = transaction.payment_meth.split(';');
	var tax_change_date = new Date("October 1, 2010, 00:00:00");
	
	

	if (tax_change_date > transaction_date)
	{
	    var TAX_RAT = 0.125; //before tax change
	}
	else
	{
	    var TAX_RAT = 0.15; //after tax change
	}

	$(function() {
	      
	      var dom_customer = $('#customer');
	      var dom_cust_name = $('#cust_name');
	      var dom_cust_id = $('#cust_id');
	      var dom_id = $('#id');
	      var dom_date = $('#date');
	      var dom_subtotal = $('#subtotal'); 
	      var dom_gst = $('#gst'); 
	      var dom_total = $('#total'); 
	      var dom_discount = $('#discount'); 
	      var dom_paid = $('#paid'); 
	      var dom_debit = $('#debit'); 
	      var dom_finished = $('#finished');
	      var dom_delivered = $('#delivered'); 
	      var dom_tot_paid = $('#tot_paid'); 
	      var dom_outsourced = $('#outsourced'); 
	      var dom_pmt_cash = $('#pmt_cash'); 
	      var dom_pmt_eftpos = $('#pmt_eftpos'); 
	      var dom_pmt_cheque = $('#pmt_cheque'); 
	      var dom_pmt_banks = $('#pmt_banks'); 
	      var dom_pmt_other = $('#pmt_other'); 
	      var dom_new_cmts = $('#new_cmts');
	      var dom_logs = $('#logs');
	      var dom_info = $('#info');
	      var dom_inv_items = $("#invo-items");

	      dom_cust_name.val(transaction.cust_name);
	      dom_cust_id.val(transaction.cust_id);
	      dom_date.val(date("d/m/y", strtotime(transaction.date)));	  
	    dom_logs.html(transaction.note); 
	      

	      $('#close')
		  .click(function () { 
			     window.close();	     
			 }); 

	      $('#save')
		  .click(function () { 			    			     
			     transaction.cust_name = dom_cust_name.val();
			     transaction.cust_id = dom_cust_id.val();

			     transaction.payment_meth 
				 = transaction.payment_meth.join(';');
				 
			     transaction.note = "<br />"
			    + " - Comment: " 
			    + $('#comments').val() 			    
			    + transaction.note;
			    
			    
			     var post_json = new Object();
			     post_json.transaction = transaction;
			     
			     $.ajax({
					type: 'POST',
					url: post_url,
					data: {'json' : JSON.stringify(post_json)},
					success:  function(data) {  
					    top.window.resizeTo(720,300);
					    $('#save').hide();
					    $('#inner-text')
						.hide()
						.html(data)
						.fadeIn(200, 
							function () {
							    setTimeout('window.close()', 500);
							});
					    window.opener.location.reload(true);
					}
				    });
			 });
	      calc_rvs();
	      display();
	      
	      dom_customer.keyup(function() {
				     var str = dom_customer.val();
				     dom_cust_name.val(str.substring(str.indexOf(' | ')+3));
				     dom_cust_id.val(str.substring(0, str.indexOf(' | ')));
				 });
	      
	      dom_total      
		  .keyup(function() {
			     transaction.total = $(this).val();
			     calc_rvs();
			     display('total');
			 })
		  .bind('paste cut', function() {
			    setTimeout(function() { dom_total.keyup(); }, 300);				      
			});
	      
	      
	      
	      $('#pmt_cash, #pmt_eftpos, #pmt_cheque, #pmt_banks, #pmt_other')
		  .click(function(){
			     if ($(this).attr('checked') &&
				 $.inArray(this.value, transaction.payment_meth) == -1) {
				 transaction.payment_meth.push(this.value);
			     }
			     else{
				 transaction.payment_meth.splice(
				     transaction.payment_meth.indexOf(this.value), 1); 
			     }
			 });

	      function display(hide) {
		  
		  dom_id.val(transaction.id);
		  
		  dom_subtotal.val(parseFloat(transaction.subtotal).toFixed(2));
		  dom_gst.val(parseFloat(transaction.tax).toFixed(2)); 

		  if (hide != 'total')
		      dom_total.val(parseFloat(transaction.total).toFixed(2));
		  
		  $.each(transaction.payment_meth, function() {
			     if(this.toString()) 
				 $('[value="' + this +'"]').attr('checked', true);
			 });
		  
		  $('.ui_buttonset').buttonset( "refresh" );	 	  

	      }
	      
	      
	      
	      function calc_rvs(){
		  
		  transaction.subtotal = transaction.total / (1 + TAX_RAT);
		  transaction.tax = transaction.subtotal * TAX_RAT;
		  
	      }

	      
	      

	  });//end doc ready


    });// end getjson