﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

///////////////////////////////////////////////
// START interface conf
///////////////////////////////////////////////

   config.toolbar = 'Simple';

   config.toolbar_Simple =
    [
		['Undo','Redo','-','SelectAll','RemoveFormat'],
		['Font','FontSize'],
		['Bold','Italic','Underline','Strike','TextColor','BGColor','-','Subscript','Superscript','SpecialChar']
    ];   

   config.toolbar = 'SimpleBox';

   config.toolbar_SimpleBox =
    [
		['Undo','Redo','-','SelectAll','RemoveFormat'],
		['Subscript','Superscript','SpecialChar'],
		'/',
		['Bold','Italic','Underline','Strike','TextColor','BGColor','-','Font','FontSize']
    ]; 
	
   config.toolbar = 'Standard400px';

   config.toolbar_Standard400px =
	[
		['Source','-','Templates'],
		['Undo','Redo','-','SelectAll','RemoveFormat'],		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link','Unlink','Anchor'],
		'/',
		['Bold','Italic','TextColor','BGColor','Underline','Strike','-','Subscript','Superscript','SpecialChar'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
		
		'/',
		['Styles','Format','Font','FontSize']

	];
	
   config.toolbar = 'Standard600px';

   config.toolbar_Standard600px =
	[
		['Source','-','NewPage','Preview','-','Templates'],
		['Cut','Copy','Paste','PasteText','PasteFromWord','-'],
		['Undo','Redo','-','SelectAll','RemoveFormat'],
		['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],
		'/',
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['BidiLtr', 'BidiRtl'],
		['Link','Unlink','Anchor'],
		
		'/',
		['Styles','Format','Font','FontSize'],
		['TextColor','BGColor'],
		['Maximize', 'ShowBlocks']
	];
	
   config.toolbar = 'Full';

   config.toolbar_Full =
	[
		['Source','-','Save','NewPage','Preview','-','Templates'],
		['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
		'/',
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['BidiLtr', 'BidiRtl'],
		['Link','Unlink','Anchor'],
		['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
		'/',
		['Styles','Format','Font','FontSize'],
		['TextColor','BGColor'],
		['Maximize', 'ShowBlocks']
	];

///////////////////////////////////////////////
// END interface conf
///////////////////////////////////////////////
};
